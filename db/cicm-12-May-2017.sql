-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 12, 2017 at 06:00 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cicm`
--

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` bigint(20) NOT NULL,
  `city_country_id` bigint(20) NOT NULL,
  `city_state_id` bigint(20) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `pin_code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `country_id` bigint(20) NOT NULL,
  `country_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `country_name`) VALUES
(1, 'India');

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks`
--

CREATE TABLE `feedbacks` (
  `id` bigint(20) NOT NULL,
  `init_user_id` bigint(20) NOT NULL,
  `sender_id` bigint(20) NOT NULL,
  `receiver_id` bigint(20) NOT NULL,
  `message` text NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedbacks`
--

INSERT INTO `feedbacks` (`id`, `init_user_id`, `sender_id`, `receiver_id`, `message`, `status`, `created_at`, `updated_at`) VALUES
(1, 21, 21, 1, 'fdsgsdfasd asdfasdf', 1, '2017-05-06 07:58:16', '2017-05-06 07:58:16'),
(2, 21, 21, 1, 'fdsgsdfasd asdfasdf', 1, '2017-05-06 07:59:02', '2017-05-06 07:59:02'),
(3, 21, 21, 1, 'fdsgsdfasd asdfasdf', 1, '2017-05-06 08:03:58', '2017-05-06 08:03:58'),
(4, 21, 21, 1, 'fdsgsdfasd asdfasdf', 1, '2017-05-06 09:20:02', '2017-05-06 09:20:02');

-- --------------------------------------------------------

--
-- Table structure for table `intraday_scanner`
--

CREATE TABLE `intraday_scanner` (
  `id` bigint(20) NOT NULL,
  `intraday_category_id` int(11) NOT NULL,
  `script_name` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `buy_above` int(11) NOT NULL,
  `buy_stoploss` int(11) NOT NULL,
  `buy_target_1` int(11) NOT NULL,
  `buy_target_2` int(11) NOT NULL,
  `buy_target_3` int(11) NOT NULL,
  `sell_below` int(11) NOT NULL,
  `sell_stoploss` int(11) NOT NULL,
  `sell_target_1` int(11) NOT NULL,
  `sell_target_2` int(11) NOT NULL,
  `sell_target_3` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `publish_date` date NOT NULL,
  `created_at` datetime NOT NULL,
  `update_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intraday_scanner`
--

INSERT INTO `intraday_scanner` (`id`, `intraday_category_id`, `script_name`, `company_name`, `buy_above`, `buy_stoploss`, `buy_target_1`, `buy_target_2`, `buy_target_3`, `sell_below`, `sell_stoploss`, `sell_target_1`, `sell_target_2`, `sell_target_3`, `status`, `publish_date`, `created_at`, `update_at`) VALUES
(1, 1, 'ABB', 'ABB INDIA LIMITED', 12, 545, 12, 12, 12, 34, 33, 33, 33, 33, 1, '2017-05-06', '2017-05-06 12:36:39', '2017-05-06 12:36:39'),
(2, 2, 'ABB', 'ABB INDIA LIMITED', 12, 545, 12, 12, 12, 34, 33, 33, 33, 33, 1, '2017-05-06', '2017-05-06 12:36:39', '2017-05-06 12:36:39'),
(3, 3, 'ABB', 'ABB INDIA LIMITED', 12, 545, 12, 12, 12, 34, 33, 33, 33, 33, 1, '2017-05-06', '2017-05-06 12:36:39', '2017-05-06 12:36:39'),
(4, 4, 'ABB', 'ABB INDIA LIMITED', 12, 545, 12, 12, 12, 34, 33, 33, 33, 33, 1, '2017-05-06', '2017-05-06 12:36:39', '2017-05-06 12:36:39');

-- --------------------------------------------------------

--
-- Table structure for table `intraday_scanner_categories`
--

CREATE TABLE `intraday_scanner_categories` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intraday_scanner_categories`
--

INSERT INTO `intraday_scanner_categories` (`id`, `title`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Commodity', 1, '2017-05-06 12:28:51', '2017-05-06 12:28:51'),
(2, 'Stock', 1, '2017-05-06 12:28:51', '2017-05-06 12:28:51'),
(3, 'F&O', 1, '2017-05-06 12:29:26', '2017-05-06 12:29:26'),
(4, 'Currency', 1, '2017-05-06 12:29:26', '2017-05-06 12:29:26');

-- --------------------------------------------------------

--
-- Table structure for table `membership_plans`
--

CREATE TABLE `membership_plans` (
  `plan_id` bigint(20) NOT NULL,
  `plan_created_by` bigint(20) NOT NULL,
  `plan_category` int(11) NOT NULL,
  `plan_title` text NOT NULL,
  `plan_description` text NOT NULL,
  `plan_amount` int(11) NOT NULL,
  `plan_benifits_in_days` int(11) NOT NULL,
  `active_features_in_plan` text,
  `plan_status` int(11) NOT NULL COMMENT '1 = Active, 0 = In Active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `membership_plans`
--

INSERT INTO `membership_plans` (`plan_id`, `plan_created_by`, `plan_category`, `plan_title`, `plan_description`, `plan_amount`, `plan_benifits_in_days`, `active_features_in_plan`, `plan_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Monthly', 'CICM Monthly Plan', 450, 30, 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', 1, '2017-05-01 17:46:38', '2017-05-01 17:46:38'),
(2, 1, 1, 'Quarterly', 'CICM Quarterly Plan', 1500, 90, 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', 1, '2017-05-01 17:47:30', '2017-05-01 17:47:30'),
(3, 1, 1, 'Yearly', 'CICm Yearly Plan', 3000, 365, 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', 1, '2017-05-01 17:48:09', '2017-05-01 17:48:09'),
(4, 1, 2, 'Monthly', 'CICM Monthly Plan', 450, 30, 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', 1, '2017-05-01 17:46:38', '2017-05-01 17:46:38'),
(5, 1, 2, 'Quarterly', 'CICM Quarterly Plan', 1500, 90, 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', 1, '2017-05-01 17:47:30', '2017-05-01 17:47:30'),
(6, 1, 2, 'Yearly', 'CICm Yearly Plan', 3000, 365, 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', 1, '2017-05-01 17:48:09', '2017-05-01 17:48:09'),
(7, 1, 3, 'Monthly', 'CICM Monthly Plan', 450, 30, 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', 1, '2017-05-01 17:46:38', '2017-05-01 17:46:38'),
(8, 1, 3, 'Quarterly', 'CICM Quarterly Plan', 1500, 90, 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', 1, '2017-05-01 17:47:30', '2017-05-01 17:47:30'),
(9, 1, 3, 'Yearly', 'CICm Yearly Plan', 3000, 365, 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', 1, '2017-05-01 17:48:09', '2017-05-01 17:50:43');

-- --------------------------------------------------------

--
-- Table structure for table `membership_plan_categories`
--

CREATE TABLE `membership_plan_categories` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `membership_plan_categories`
--

INSERT INTO `membership_plan_categories` (`id`, `title`, `status`, `created_at`) VALUES
(1, 'Cash', 1, '2017-05-01 22:02:11'),
(2, 'Commodities', 1, '2017-05-01 22:02:11'),
(3, 'Stock Future', 1, '2017-05-01 22:02:27');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` bigint(20) NOT NULL,
  `type` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `extra` text,
  `is_seen` int(11) NOT NULL DEFAULT '0' COMMENT '1 = Yes, 0 = No',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `text`, `extra`, `is_seen`, `created_at`) VALUES
(1, 'device_exists', 'Someone trying to register account by using Androiddevice with device id zsfdasdf1', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf1";s:12:"device_token";s:11:"asdfasdfASD";}', 1, '2017-04-15 14:15:41'),
(2, 'device_exists', 'Someone trying to register account by using Androiddevice with device id: zsfdasdf1', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf1";s:12:"device_token";s:11:"asdfasdfASD";}', 1, '2017-04-15 14:16:15'),
(3, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf1', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf1";s:12:"device_token";s:11:"asdfasdfASD";}', 1, '2017-04-15 15:28:39'),
(4, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf2', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf2";s:12:"device_token";s:11:"asdfasdfASD";}', 1, '2017-04-15 15:33:53'),
(5, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf3', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf3";s:12:"device_token";s:11:"asdfasdfASD";}', 1, '2017-04-15 15:39:22'),
(6, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf6', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf6";s:12:"device_token";s:11:"asdfasdfASD";}', 1, '2017-04-15 13:55:08'),
(7, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf6', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf6";s:12:"device_token";s:11:"asdfasdfASD";}', 1, '2017-04-15 13:56:01'),
(8, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf6', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf6";s:12:"device_token";s:11:"asdfasdfASD";}', 1, '2017-04-15 13:56:46'),
(9, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf6', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf6";s:12:"device_token";s:11:"asdfasdfASD";}', 1, '2017-04-15 13:58:33'),
(10, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf6', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf6";s:12:"device_token";s:11:"asdfasdfASD";}', 1, '2017-04-15 13:59:11'),
(11, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf6', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf6";s:12:"device_token";s:11:"asdfasdfASD";}', 1, '2017-04-15 14:03:27'),
(12, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf6', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf6";s:12:"device_token";s:11:"asdfasdfASD";}', 1, '2017-04-15 14:05:08'),
(13, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf6', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf6";s:12:"device_token";s:11:"asdfasdfASD";}', 1, '2017-04-15 14:10:51'),
(14, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf6', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf6";s:12:"device_token";s:11:"asdfasdfASD";}', 1, '2017-04-15 14:12:50'),
(15, 'membership_expired', 'Freeuser Szfsdfa membership is expired on 19/04/2017', '', 1, '2017-04-19 14:43:17'),
(16, 'membership_expired', 'Freeuser Test membership is expired on 19/04/2017', '', 1, '2017-04-19 14:43:17'),
(17, 'membership_expired', 'Freeuser Siddharth Pandey membership is expired on 19/04/2017', '', 1, '2017-04-19 14:43:18'),
(18, 'membership_expired', 'Freeuser Szfsdfa membership is expired on 19/04/2017', '', 1, '2017-04-19 14:43:44'),
(19, 'membership_expired', 'Freeuser Test membership is expired on 19/04/2017', '', 1, '2017-04-19 14:43:45'),
(20, 'membership_expired', 'Freeuser Siddharth Pandey membership is expired on 19/04/2017', '', 1, '2017-04-19 14:43:45'),
(21, 'membership_expired', 'Free user Siddharth Pandey membership is expired on 19/04/2017.', '', 1, '2017-04-19 17:14:57'),
(22, 'features_changed', 'Application active features is changed for user Siddharth Pandey because of membership expiration', 'a:2:{s:19:"old_active_features";a:12:{i:0;a:1:{s:15:"can_access_tips";s:1:"1";}i:1;a:1:{s:29:"can_access_economic_barometer";s:1:"1";}i:2;a:1:{s:28:"can_access_economic_calendar";s:1:"1";}i:3;a:1:{s:27:"can_access_earning_calendar";s:1:"0";}i:4;a:1:{s:23:"can_access_news_reports";s:1:"0";}i:5;a:1:{s:19:"can_access_hawk_eye";s:1:"0";}i:6;a:1:{s:25:"can_access_traders_clinic";s:1:"0";}i:7;a:1:{s:19:"can_access_feedback";s:1:"0";}i:8;a:1:{s:25:"can_access_invite_friends";s:1:"0";}i:9;a:1:{s:23:"can_access_subscription";s:1:"0";}i:10;a:1:{s:19:"can_access_about_us";s:1:"1";}i:11;a:1:{s:21:"can_access_contact_us";s:1:"0";}}s:19:"new_active_features";a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"0";s:23:"can_access_news_reports";s:1:"0";s:19:"can_access_hawk_eye";s:1:"0";s:25:"can_access_traders_clinic";s:1:"0";s:19:"can_access_feedback";s:1:"0";s:25:"can_access_invite_friends";s:1:"0";s:23:"can_access_subscription";s:1:"0";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"0";}}', 1, '2017-04-19 17:24:28'),
(23, 'membership_expired', 'Free user Siddharth Pandey membership is expired on 19/04/2017.', '', 1, '2017-04-19 17:24:28'),
(24, 'device_exists', 'Someone trying to register account by using Android device with device id: 3456234545234556', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:31:"siddharthpandey21213@gmail.comm";s:12:"phone_number";s:10:"8319290112";s:11:"device_type";s:1:"1";s:9:"device_id";s:16:"3456234545234556";s:12:"device_token";s:11:"asdfasdfASD";}', 1, '2017-04-23 12:51:29'),
(25, 'device_exists', 'Someone trying to register account by using Android device with device id: 34562345452345', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:31:"siddharthpandey21213@gmail.comm";s:12:"phone_number";s:10:"8319290112";s:11:"device_type";s:1:"1";s:9:"device_id";s:14:"34562345452345";s:12:"device_token";s:11:"asdfasdfASD";}', 1, '2017-04-23 12:51:33');

-- --------------------------------------------------------

--
-- Table structure for table `push_notifications`
--

CREATE TABLE `push_notifications` (
  `id` bigint(20) NOT NULL,
  `sent_by` bigint(20) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `push_notifications`
--

INSERT INTO `push_notifications` (`id`, `sent_by`, `subject`, `message`, `created_at`, `updated_at`) VALUES
(1, 1, 'dfsdf', 'sdfsdf', '2017-04-19 18:01:37', '2017-04-19 18:01:37');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `state_id` bigint(20) NOT NULL,
  `state_country_id` bigint(20) NOT NULL,
  `state_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock_market_types`
--

CREATE TABLE `stock_market_types` (
  `id` int(11) NOT NULL,
  `market_type_title` varchar(255) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1 = Active, 0 = In Active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock_market_types`
--

INSERT INTO `stock_market_types` (`id`, `market_type_title`, `status`, `created_at`, `updated_at`) VALUES
(1, 'NCDEX Commodities', 1, '2017-05-03 18:28:04', '2017-05-03 18:28:04'),
(2, 'MCX Commodities', 1, '2017-05-03 18:28:19', '2017-05-03 18:28:19'),
(3, 'NSE Currency', 1, '2017-05-03 18:28:38', '2017-05-03 18:28:38'),
(4, 'NSE Option', 1, '2017-05-03 18:28:49', '2017-05-03 18:28:49'),
(5, 'NSE Future', 1, '2017-05-03 18:28:55', '2017-05-03 18:28:55'),
(6, 'NSE Cash', 1, '2017-05-03 18:29:03', '2017-05-06 13:35:22');

-- --------------------------------------------------------

--
-- Table structure for table `system_preferences`
--

CREATE TABLE `system_preferences` (
  `id` bigint(20) NOT NULL,
  `preference_key` varchar(255) CHARACTER SET utf8 NOT NULL,
  `preference_value` text CHARACTER SET utf8
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_preferences`
--

INSERT INTO `system_preferences` (`id`, `preference_key`, `preference_value`) VALUES
(1, 'app_name', 'Test'),
(2, 'app_description', 'Test'),
(3, 'app_email', 'cicmprakash@gmail.com'),
(4, 'facebook_link', 'https://facebook.com'),
(5, 'twitter_link', ''),
(6, 'google_plus_link', ''),
(7, 'google_map_link', ''),
(8, 'linkedin_link', ''),
(9, 'instagram_link', ''),
(10, 'pinterest_link', ''),
(11, 'youtube_link', ''),
(12, 'copyright_text', ''),
(13, 'footer_name_address', ''),
(14, 'app_logo', NULL),
(15, 'app_favicon', NULL),
(16, 'meta_separator', '|'),
(17, 'meta_description', 'Test'),
(18, 'meta_keywords', 'Test'),
(19, 'free_trial_days_for_new_members', '5'),
(20, 'verification_otp_length', '6'),
(21, 'verification_otp_message', 'Dear %s, Welcome to Test, Please enter verification OTP %s to activate your account. '),
(22, 'device_exists_message', 'Sorry, this device is already registered in our system. Please contact to our support at 9630108245 to activate your account.'),
(23, 'membership_expire_message', 'Dear %s, Your subscription days has been expired, please contact with our support at 9630108245 to extend your services.'),
(24, 'active_features_for_membership_expired_users', 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"0";s:23:"can_access_news_reports";s:1:"0";s:19:"can_access_hawk_eye";s:1:"0";s:25:"can_access_traders_clinic";s:1:"0";s:19:"can_access_feedback";s:1:"0";s:25:"can_access_invite_friends";s:1:"0";s:23:"can_access_subscription";s:1:"0";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"0";}'),
(25, 'customer_care_number', '0731-4083836'),
(26, 'sales_contact_number', '+91-9630108245'),
(27, 'about_us_content', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English.\r\n\r\nMany desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English.\r\n\r\nMany desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).'),
(28, 'free_tc_form_submission_limit', '4'),
(29, 'tc_over_limit_message', 'You can submit only four traders clinic form in a month, for more you have to purchase our services.');

-- --------------------------------------------------------

--
-- Table structure for table `tc_form_messages`
--

CREATE TABLE `tc_form_messages` (
  `id` bigint(20) NOT NULL,
  `tc_id` bigint(20) NOT NULL,
  `from_id` bigint(20) NOT NULL,
  `message` text NOT NULL,
  `status` int(11) NOT NULL COMMENT '1 = Active, 2- In Active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tc_form_messages`
--

INSERT INTO `tc_form_messages` (`id`, `tc_id`, `from_id`, `message`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 26, 'fasdasdf asdfasdfasdf', 1, '2017-05-05 12:11:17', '2017-05-05 12:11:17'),
(2, 2, 26, 'fasdasdf asdfasdfasdf', 1, '2017-05-05 14:13:54', '2017-05-05 14:13:54'),
(3, 2, 26, 'fasdasdf asdfasdfasdf', 1, '2017-05-05 14:14:03', '2017-05-05 14:14:03'),
(4, 2, 26, 'fasdasdf asdfasdfasdf', 1, '2017-05-05 14:14:03', '2017-05-05 14:14:03'),
(5, 2, 26, 'fasdasdf asdfasdfasdf', 1, '2017-05-05 14:14:04', '2017-05-05 14:14:04'),
(6, 2, 26, 'fasdasdf asdfasdfasdf', 1, '2017-05-05 14:14:05', '2017-05-05 14:14:05'),
(7, 1, 26, 'fasdasdf asdfasdfasdf', 1, '2017-05-05 14:14:09', '2017-05-05 14:14:09'),
(8, 2, 26, 'fasdasdf asdfasdfasdf', 1, '2017-05-05 14:14:12', '2017-05-05 14:14:12'),
(9, 2, 26, 'fasdasdf asdfasdfasdf', 1, '2017-05-05 14:14:28', '2017-05-05 14:14:28'),
(10, 1, 1, 'fasdasdf asdfasdfasdf', 1, '2017-05-05 14:14:09', '2017-05-05 14:14:09'),
(11, 2, 1, 'fasdasdf asdfasdfasdf', 1, '2017-05-05 14:14:28', '2017-05-05 14:14:28'),
(12, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:00:31', '2017-05-06 07:00:31'),
(13, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:00:32', '2017-05-06 07:00:32'),
(14, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:00:48', '2017-05-06 07:00:48'),
(15, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:00:48', '2017-05-06 07:00:48'),
(16, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:00:49', '2017-05-06 07:00:49'),
(17, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:00:49', '2017-05-06 07:00:49'),
(18, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:00:49', '2017-05-06 07:00:49'),
(19, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:00:50', '2017-05-06 07:00:50'),
(20, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:00:50', '2017-05-06 07:00:50'),
(21, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:00:50', '2017-05-06 07:00:50'),
(22, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:00:50', '2017-05-06 07:00:50'),
(23, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:00:51', '2017-05-06 07:00:51'),
(24, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:00:51', '2017-05-06 07:00:51'),
(25, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:00:51', '2017-05-06 07:00:51'),
(26, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:00:51', '2017-05-06 07:00:51'),
(27, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:00:52', '2017-05-06 07:00:52'),
(28, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:00:52', '2017-05-06 07:00:52'),
(29, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:00:52', '2017-05-06 07:00:52'),
(30, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:00:52', '2017-05-06 07:00:52'),
(31, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:00:52', '2017-05-06 07:00:52'),
(32, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:00:53', '2017-05-06 07:00:53'),
(33, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:00:53', '2017-05-06 07:00:53'),
(34, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:00:53', '2017-05-06 07:00:53'),
(35, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:41:17', '2017-05-06 07:41:17'),
(36, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 07:41:59', '2017-05-06 07:41:59'),
(37, 8, 21, 'fasdasdf asdfasdfasdf', 1, '2017-05-06 09:20:26', '2017-05-06 09:20:26');

-- --------------------------------------------------------

--
-- Table structure for table `tc_form_submissions`
--

CREATE TABLE `tc_form_submissions` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `script_name` varchar(255) NOT NULL COMMENT '1 = Stock, 2 = F&O, 3 = Commodities, 4 = Currency',
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `position` int(11) NOT NULL COMMENT '1 = Long, 2 = Short',
  `vision` int(11) NOT NULL COMMENT '1 = Buy, 2 = Sell, 3 = Hold',
  `comment` text NOT NULL,
  `status` int(11) NOT NULL COMMENT '1 = Active, 0 = In Active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tc_form_submissions`
--

INSERT INTO `tc_form_submissions` (`id`, `user_id`, `first_name`, `last_name`, `phone_number`, `script_name`, `quantity`, `price`, `position`, `vision`, `comment`, `status`, `created_at`, `updated_at`) VALUES
(1, 26, 'sdfasd', 'asdfasdf', '34563456345', '1,2,3', 1, 10000, 1, 1, 'dsfsdf adfsasdf asdfasd adsfasdf adsfasd asdfasdf', 1, '2017-05-05 10:37:34', '2017-05-05 10:37:34'),
(2, 26, 'sdfasd', 'asdfasdf', '34563456345', '1,2,3', 1, 10000, 1, 1, 'dsfsdf adfsasdf asdfasd adsfasdf adsfasd asdfasdf', 1, '2017-05-05 10:37:39', '2017-05-05 10:37:39'),
(3, 26, 'sdfasd', 'asdfasdf', '34563456345', '1,2,3', 1, 10000, 1, 1, 'dsfsdf adfsasdf asdfasd adsfasdf adsfasd asdfasdf', 1, '2017-05-05 10:37:41', '2017-05-05 10:37:41'),
(4, 26, 'sdfasd', 'asdfasdf', '34563456345', '1,2,3', 1, 10000, 1, 1, 'dsfsdf adfsasdf asdfasd adsfasdf adsfasd asdfasdf', 1, '2017-05-05 10:37:41', '2017-05-05 10:37:41'),
(5, 21, 'sdfasd', 'asdfasdf', '34563456345', '1,2,3', 1, 10000, 1, 1, 'dsfsdf adfsasdf asdfasd adsfasdf adsfasd asdfasdf', 1, '2017-05-06 06:52:01', '2017-05-06 06:52:01'),
(6, 21, 'sdfasd', 'asdfasdf', '34563456345', '1,2,3', 1, 10000, 1, 1, 'dsfsdf adfsasdf asdfasd adsfasdf adsfasd asdfasdf', 1, '2017-05-06 06:52:02', '2017-05-06 06:52:02'),
(7, 21, 'sdfasd', 'asdfasdf', '34563456345', '1,2,3', 1, 10000, 1, 1, 'dsfsdf adfsasdf asdfasd adsfasdf adsfasd asdfasdf', 1, '2017-05-06 06:52:05', '2017-05-06 06:52:05'),
(8, 21, 'sdfasd', 'asdfasdf', '34563456345', '1,2,3', 1, 10000, 1, 1, 'dsfsdf adfsasdf asdfasd adsfasdf adsfasd asdfasdf', 1, '2017-05-06 06:52:06', '2017-05-06 06:52:06');

-- --------------------------------------------------------

--
-- Table structure for table `tips`
--

CREATE TABLE `tips` (
  `tip_id` bigint(20) NOT NULL,
  `tip_created_by` bigint(20) NOT NULL,
  `tip_type` int(11) NOT NULL COMMENT '1 = BUY, 2 =SELL',
  `stock_market_type` int(11) NOT NULL,
  `exchange` varchar(255) NOT NULL,
  `market_type` int(11) NOT NULL COMMENT '1 = Cash, 2 = Future, 3 = Option',
  `exchange_tip_type` int(11) NOT NULL COMMENT '1 = Intraday, 2 = Swing, 3 = Intraday / Swing',
  `price_one` int(11) NOT NULL,
  `price_two` int(11) NOT NULL,
  `stoploss` int(11) NOT NULL,
  `total_added_targets` int(11) NOT NULL,
  `acheived_target` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0 = In Active, 1 = Active , 2 = Pending, 3 = Closed',
  `is_published` int(11) NOT NULL COMMENT '1 = Yes, 0 = No',
  `publish_date` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tips`
--

INSERT INTO `tips` (`tip_id`, `tip_created_by`, `tip_type`, `stock_market_type`, `exchange`, `market_type`, `exchange_tip_type`, `price_one`, `price_two`, `stoploss`, `total_added_targets`, `acheived_target`, `status`, `is_published`, `publish_date`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 6, 'ABB', 1, 2, 111, 112, 123, 5, 0, 1, 1, '2017-05-05', '2017-05-06 14:45:59', '2017-05-12 04:52:25'),
(2, 1, 1, 6, 'ABB', 1, 3, 112, 0, 135, 3, 0, 1, 1, '2017-05-12', '2017-05-10 21:40:02', '2017-05-12 05:19:01'),
(3, 1, 1, 6, 'ABB', 1, 2, 111, 112, 1, 3, 0, 1, 1, '2017-05-12', '2017-05-12 05:14:51', '2017-05-12 05:14:51'),
(4, 1, 2, 6, 'ABB', 1, 1, 111, 112, 122, 3, 0, 1, 1, '2017-05-12', '2017-05-12 05:15:36', '2017-05-12 05:19:41');

-- --------------------------------------------------------

--
-- Table structure for table `tips_follow_ups`
--

CREATE TABLE `tips_follow_ups` (
  `fu_id` bigint(20) NOT NULL,
  `fu_tip_id` bigint(20) NOT NULL,
  `fu_created_by` int(20) NOT NULL,
  `fu_message` text NOT NULL,
  `is_trailed_stoploss` int(11) NOT NULL,
  `is_trailed_targets` int(11) NOT NULL,
  `fu_status` int(11) NOT NULL COMMENT '1 = Active, 0 = in Active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tips_follow_ups`
--

INSERT INTO `tips_follow_ups` (`fu_id`, `fu_tip_id`, `fu_created_by`, `fu_message`, `is_trailed_stoploss`, `is_trailed_targets`, `fu_status`, `created_at`, `updated_at`) VALUES
(4, 1, 1, 'dfgsdf sdfgsdfg', 0, 1, 1, '2017-05-10 17:59:38', '2017-05-10 17:59:38'),
(5, 2, 1, 'Hello', 0, 0, 1, '2017-05-10 21:40:51', '2017-05-10 21:40:51');

-- --------------------------------------------------------

--
-- Table structure for table `tips_targets`
--

CREATE TABLE `tips_targets` (
  `tt_id` bigint(20) NOT NULL,
  `tt_tip_id` bigint(20) NOT NULL,
  `tt_fu_id` bigint(20) NOT NULL DEFAULT '0',
  `tt_created_by` bigint(20) NOT NULL,
  `target_number` int(11) NOT NULL COMMENT 'Tip Target Number',
  `target_amount` int(11) NOT NULL,
  `is_achieved` int(11) NOT NULL COMMENT '1 = Yes, 0 = No',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tips_targets`
--

INSERT INTO `tips_targets` (`tt_id`, `tt_tip_id`, `tt_fu_id`, `tt_created_by`, `target_number`, `target_amount`, `is_achieved`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 1, 1, 291, 0, '2017-05-06 14:45:59', '2017-05-12 04:52:25'),
(2, 1, 0, 1, 2, 292, 0, '2017-05-06 14:45:59', '2017-05-12 04:52:25'),
(3, 1, 0, 1, 3, 293, 0, '2017-05-06 14:45:59', '2017-05-12 04:52:25'),
(4, 1, 0, 1, 4, 294, 0, '2017-05-06 14:46:24', '2017-05-12 04:52:25'),
(10, 1, 4, 1, 5, 12, 0, '2017-05-10 17:59:38', '2017-05-12 04:52:25'),
(11, 2, 0, 1, 1, 122, 0, '2017-05-10 21:40:03', '2017-05-12 05:19:01'),
(12, 2, 0, 1, 2, 125, 0, '2017-05-10 21:40:03', '2017-05-12 05:19:01'),
(13, 2, 0, 1, 3, 102, 0, '2017-05-10 21:40:03', '2017-05-12 05:19:01'),
(14, 3, 0, 1, 1, 2, 0, '2017-05-12 05:14:51', '2017-05-12 05:14:51'),
(15, 3, 0, 1, 2, 3, 0, '2017-05-12 05:14:51', '2017-05-12 05:14:51'),
(16, 3, 0, 1, 3, 4, 0, '2017-05-12 05:14:51', '2017-05-12 05:14:51'),
(17, 4, 0, 1, 1, 1, 0, '2017-05-12 05:15:36', '2017-05-12 05:19:41'),
(18, 4, 0, 1, 2, 2, 0, '2017-05-12 05:15:36', '2017-05-12 05:19:41'),
(19, 4, 0, 1, 3, 3, 0, '2017-05-12 05:15:36', '2017-05-12 05:19:41');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `user_type_id` int(11) NOT NULL COMMENT '1 = Super Admin, 2 = Sub Admin, 3 = Staff  users, 4 = App Users',
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `profile_pic` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `forgot_otp` varchar(255) NOT NULL,
  `verification_otp` varchar(255) NOT NULL,
  `membership_type` int(11) NOT NULL COMMENT '1 = Free, 2 = Paid',
  `membership_expire_on` date NOT NULL COMMENT 'Membership Expiration Date',
  `membership_status` int(11) NOT NULL COMMENT '1 = Active, 0 = In Active',
  `current_active_plan` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `is_user_deleted` int(11) NOT NULL DEFAULT '0' COMMENT '1 = Yes, 0 = No',
  `is_phone_verified` int(11) NOT NULL DEFAULT '0' COMMENT '0 = No, 1 = Yes',
  `created_by` int(11) NOT NULL DEFAULT '0' COMMENT '0 = Self, Other value is created user id.',
  `device_type` int(11) NOT NULL COMMENT '1 = Android, 2 = IOS',
  `device_id` varchar(255) NOT NULL,
  `device_token` varchar(255) NOT NULL,
  `last_login_ip` varchar(255) NOT NULL,
  `last_login_date` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_type_id`, `name`, `email`, `phone_number`, `profile_pic`, `password`, `forgot_otp`, `verification_otp`, `membership_type`, `membership_expire_on`, `membership_status`, `current_active_plan`, `status`, `is_user_deleted`, `is_phone_verified`, `created_by`, `device_type`, `device_id`, `device_token`, `last_login_ip`, `last_login_date`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'test@gmail.com', '9827071943', '', 'e10adc3949ba59abbe56e057f20f883e', '', '', 0, '2017-04-24', 0, 0, 1, 0, 0, 0, 1, 'dfgsdfgsdf', 'sdfgsdfgsdfg', '', '2017-03-28 16:11:53', '2017-03-28 16:11:53', '2017-03-28 16:11:53'),
(2, 4, 'szfsdfa', 'test@test.com', '3456345634563456', '1491819891images.jpg', '', '', '', 1, '2017-04-24', 0, 0, 1, 0, 1, 0, 2, '2', '2', '127.0.0.1', '2017-04-10 15:49:50', '2017-04-10 13:59:01', '2017-04-19 14:43:44'),
(3, 4, 'test', 'test1@test.com', '7894561231', '', '', '', '123456', 1, '2017-04-24', 0, 0, 0, 0, 0, 0, 1, '', 'asdfasdfASD', '', '0000-00-00 00:00:00', '2017-04-10 14:01:18', '2017-04-19 14:43:44'),
(4, 4, 'Siddharth Pandey', 'siddharthpandey21@gmail.com', '9630108245', '1491834689images.jpeg', '', '', '475219', 1, '2017-04-24', 0, 0, 1, 0, 1, 0, 1, '', 'asdfasdfASD', '::1', '2017-04-10 16:31:06', '2017-04-10 16:30:32', '2017-05-07 09:49:23'),
(5, 4, 'Siddharth Pandey', 'siddharthpandey22@gmail.com', '24334234', '', '', '', '314893', 1, '2017-04-20', 1, 0, 0, 0, 0, 0, 1, '123', 'asdfasdfASD', '', '0000-00-00 00:00:00', '2017-04-15 13:07:30', '2017-04-15 13:07:30'),
(6, 4, 'Siddharth Pandey', 'siddharthpandey23@gmail.com', '354335', '', '', '', '651880', 1, '2017-04-20', 1, 0, 0, 0, 0, 0, 1, '', 'asdfasdfASD', '', '0000-00-00 00:00:00', '2017-04-15 13:08:06', '2017-04-15 13:08:06'),
(7, 4, 'Siddharth Pandey', 'siddharthpandey24@gmail.com', '4444', '', '', '', '789834', 1, '2017-04-20', 1, 0, 0, 0, 0, 0, 1, '', 'asdfasdfASD', '', '0000-00-00 00:00:00', '2017-04-15 13:17:41', '2017-04-15 13:17:41'),
(8, 4, 'Siddharth Pandey', 'siddharthpandey25@gmail.com', '345', '', '', '', '', 1, '2017-04-20', 1, 0, 1, 0, 1, 0, 1, '', 'asdfasdfASD', '::1', '2017-04-15 13:20:23', '2017-04-15 13:19:34', '2017-04-15 13:20:23'),
(9, 4, 'Siddharth Pandey', 'siddharthpandey26@gmail.com', '8319290104', '', '', '', '', 1, '2017-04-20', 1, 0, 1, 0, 1, 0, 1, '', 'asdfasdfASD', '::1', '2017-04-15 15:40:49', '2017-04-15 13:35:24', '2017-04-15 15:40:49'),
(10, 4, 'Siddharth Pandey', 'siddharthpandey28@gmail.com', '789456123', '', '', '', '174075', 1, '2017-04-24', 1, 0, 0, 0, 0, 0, 1, '', '456745674567456', '', '0000-00-00 00:00:00', '2017-04-19 13:07:12', '2017-04-19 13:07:12'),
(11, 4, 'Siddharth Pandey', 'siddharthpandey29@gmail.com', '78945612378', '', '', '', '204935', 1, '2017-04-14', 0, 0, 0, 0, 0, 0, 1, '', '45674567dfghdfgh4567456', '', '0000-00-00 00:00:00', '2017-04-19 13:08:24', '2017-04-19 17:24:27'),
(12, 4, 'Siddharth Pandey', '454456745674567@gmail.com', '4567456745674567', '', '', '', '590589', 1, '2017-04-24', 1, 0, 0, 0, 0, 0, 1, '', '45674567dfghdfgh4567456', '', '0000-00-00 00:00:00', '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(13, 4, 'dfgsfgsdfg', 'sdfgsdfg@sdfgsdfg.ty', '34524523452345', '', '', '', '', 1, '2017-05-01', 1, 0, 1, 0, 0, 1, 0, '', '', '', '0000-00-00 00:00:00', '2017-04-22 15:25:48', '2017-04-22 17:14:21'),
(14, 3, 'staff', '', '', '', 'd41d8cd98f00b204e9800998ecf8427e', '', '', 0, '0000-00-00', 0, 0, 0, 1, 0, 1, 0, '', '', '', '0000-00-00 00:00:00', '2017-04-22 15:30:06', '2017-04-22 17:41:35'),
(15, 4, 'sdfgsdafgs', 'dfgsdfg@dfgsdf.ertert', '4252435235235', '', '', '', '', 1, '2017-04-27', 1, 0, 1, 0, 0, 1, 0, '', '', '', '0000-00-00 00:00:00', '2017-04-22 17:21:24', '2017-04-22 17:22:39'),
(16, 4, 'dfgdfg', 'dfgd@fgfdg.ty', '3645123463463456', '', '', '', '', 1, '2017-04-27', 1, 0, 1, 0, 0, 1, 0, '', '', '', '0000-00-00 00:00:00', '2017-04-22 17:36:06', '2017-04-22 17:38:40'),
(17, 4, 'sdfdfs', 'dfsd@sfdasdf.rt', '2334', '', '', '', '', 1, '2017-04-27', 1, 0, 1, 0, 0, 1, 0, '', '', '', '0000-00-00 00:00:00', '2017-04-22 17:46:05', '2017-04-22 17:46:05'),
(18, 4, 'dfgsdfg', 'sdgs@dfgdfg.tyu', '453525', '', '', '', '', 1, '2017-04-27', 1, 0, 1, 0, 0, 1, 0, '', '', '', '0000-00-00 00:00:00', '2017-04-22 17:47:01', '2017-04-22 17:47:01'),
(19, 4, 'Siddharth Pandey', 'siddharthpandey21212@gmail.com', '8319290108', '', '', '', '551159', 1, '2017-04-28', 1, 0, 0, 0, 0, 0, 1, '', 'asdfasdfASD', '', '0000-00-00 00:00:00', '2017-04-23 12:26:59', '2017-04-23 12:26:59'),
(20, 4, 'Siddharth Pandey', 'siddharthpandey21213@gmail.com', '8319290109', '', '', '', '775727', 1, '2017-05-02', 1, 0, 1, 0, 0, 0, 1, '', 'asdfasdfASD', '', '0000-00-00 00:00:00', '2017-04-23 12:32:57', '2017-04-23 13:29:32'),
(21, 4, 'Siddharth Pandey', 'siddharthpandey21213@gmail.comm', '8319290112', '149404910714907580_1777589915839124_6241095592730282220_n.jpg', '', '', '265830', 1, '2017-05-08', 1, 0, 1, 0, 0, 0, 1, '', 'asdfasdfASD', '', '0000-00-00 00:00:00', '2017-04-23 12:51:37', '2017-05-06 07:38:28');

-- --------------------------------------------------------

--
-- Table structure for table `users_active_features`
--

CREATE TABLE `users_active_features` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `feature_title` varchar(255) NOT NULL,
  `feature_prefix` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL COMMENT '1 = Yes, 0 = No',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_active_features`
--

INSERT INTO `users_active_features` (`id`, `user_id`, `feature_title`, `feature_prefix`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 11, 'Tips', 'can_access_tips', 1, '2017-04-19 13:08:25', '2017-04-19 17:24:28'),
(2, 11, 'Economy Barometer', 'can_access_economic_barometer', 1, '2017-04-19 13:08:25', '2017-04-19 17:24:28'),
(3, 11, 'Economic  Calender', 'can_access_economic_calendar', 1, '2017-04-19 13:08:25', '2017-04-19 17:24:28'),
(4, 11, 'Earning Calendar', 'can_access_earning_calendar', 0, '2017-04-19 13:08:25', '2017-04-19 17:24:28'),
(5, 11, 'New & Reports', 'can_access_news_reports', 0, '2017-04-19 13:08:25', '2017-04-19 17:24:28'),
(6, 11, 'Hawk Eye', 'can_access_hawk_eye', 0, '2017-04-19 13:08:25', '2017-04-19 17:24:28'),
(7, 11, 'Traders Clinic', 'can_access_traders_clinic', 0, '2017-04-19 13:08:25', '2017-04-19 17:24:28'),
(8, 11, 'Feedback', 'can_access_feedback', 0, '2017-04-19 13:08:25', '2017-04-19 17:24:28'),
(9, 11, 'Invite Friends', 'can_access_invite_friends', 0, '2017-04-19 13:08:25', '2017-04-19 17:24:28'),
(10, 11, 'Subscription', 'can_access_subscription', 0, '2017-04-19 13:08:25', '2017-04-19 17:24:28'),
(11, 11, 'About Us', 'can_access_about_us', 1, '2017-04-19 13:08:25', '2017-04-19 17:24:28'),
(12, 11, 'Contact Us', 'can_access_contact_us', 0, '2017-04-19 13:08:25', '2017-04-19 17:24:28'),
(13, 12, 'Tips', 'can_access_tips', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(14, 12, 'Economy Barometer', 'can_access_economic_barometer', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(15, 12, 'Economic  Calender', 'can_access_economic_calendar', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(16, 12, 'Earning Calendar', 'can_access_earning_calendar', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(17, 12, 'New & Reports', 'can_access_news_reports', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(18, 12, 'Hawk Eye', 'can_access_hawk_eye', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(19, 12, 'Traders Clinic', 'can_access_traders_clinic', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(20, 12, 'Feedback', 'can_access_feedback', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(21, 12, 'Invite Friends', 'can_access_invite_friends', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(22, 12, 'Subscription', 'can_access_subscription', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(23, 12, 'About Us', 'can_access_about_us', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(24, 12, 'Contact Us', 'can_access_contact_us', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(25, 13, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 15:25:49', '2017-04-22 17:14:21'),
(26, 13, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 15:25:49', '2017-04-22 17:14:21'),
(27, 13, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 15:25:49', '2017-04-22 17:14:21'),
(28, 13, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 15:25:49', '2017-04-22 17:14:21'),
(29, 13, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 15:25:49', '2017-04-22 17:14:21'),
(30, 13, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 15:25:49', '2017-04-22 17:14:21'),
(31, 13, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 15:25:49', '2017-04-22 17:14:21'),
(32, 13, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 15:25:49', '2017-04-22 17:14:21'),
(33, 13, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 15:25:49', '2017-04-22 17:14:21'),
(34, 13, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 15:25:49', '2017-04-22 17:14:21'),
(35, 13, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 15:25:49', '2017-04-22 17:14:21'),
(36, 13, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 15:25:49', '2017-04-22 17:14:21'),
(37, 15, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 17:21:24', '2017-04-22 17:22:39'),
(38, 15, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 17:21:25', '2017-04-22 17:22:39'),
(39, 15, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 17:21:25', '2017-04-22 17:22:39'),
(40, 15, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 17:21:25', '2017-04-22 17:22:39'),
(41, 15, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 17:21:25', '2017-04-22 17:22:39'),
(42, 15, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 17:21:25', '2017-04-22 17:22:39'),
(43, 15, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 17:21:25', '2017-04-22 17:22:39'),
(44, 15, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 17:21:25', '2017-04-22 17:22:39'),
(45, 15, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 17:21:25', '2017-04-22 17:22:39'),
(46, 15, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 17:21:25', '2017-04-22 17:22:39'),
(47, 15, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 17:21:25', '2017-04-22 17:22:39'),
(48, 15, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 17:21:25', '2017-04-22 17:22:39'),
(49, 16, 'Tips', 'can_access_tips', 0, '2017-04-22 17:36:06', '2017-04-22 17:36:06'),
(50, 16, 'Economy Barometer', 'can_access_economic_barometer', 0, '2017-04-22 17:36:06', '2017-04-22 17:36:06'),
(51, 16, 'Economic  Calender', 'can_access_economic_calendar', 1, '2017-04-22 17:36:06', '2017-04-22 17:36:06'),
(52, 16, 'Earning Calendar', 'can_access_earning_calendar', 1, '2017-04-22 17:36:06', '2017-04-22 17:36:06'),
(53, 16, 'New & Reports', 'can_access_news_reports', 1, '2017-04-22 17:36:06', '2017-04-22 17:36:06'),
(54, 16, 'Hawk Eye', 'can_access_hawk_eye', 1, '2017-04-22 17:36:06', '2017-04-22 17:36:06'),
(55, 16, 'Traders Clinic', 'can_access_traders_clinic', 1, '2017-04-22 17:36:06', '2017-04-22 17:36:06'),
(56, 16, 'Feedback', 'can_access_feedback', 1, '2017-04-22 17:36:06', '2017-04-22 17:36:06'),
(57, 16, 'Invite Friends', 'can_access_invite_friends', 1, '2017-04-22 17:36:06', '2017-04-22 17:36:06'),
(58, 16, 'Subscription', 'can_access_subscription', 1, '2017-04-22 17:36:06', '2017-04-22 17:36:06'),
(59, 16, 'About Us', 'can_access_about_us', 1, '2017-04-22 17:36:06', '2017-04-22 17:36:06'),
(60, 16, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 17:36:06', '2017-04-22 17:36:06'),
(61, 17, 'Tips', 'can_access_tips', 1, '2017-04-22 17:46:05', '2017-04-22 17:46:05'),
(62, 17, 'Economy Barometer', 'can_access_economic_barometer', 1, '2017-04-22 17:46:05', '2017-04-22 17:46:05'),
(63, 17, 'Economic  Calender', 'can_access_economic_calendar', 1, '2017-04-22 17:46:05', '2017-04-22 17:46:05'),
(64, 17, 'Earning Calendar', 'can_access_earning_calendar', 1, '2017-04-22 17:46:05', '2017-04-22 17:46:05'),
(65, 17, 'New & Reports', 'can_access_news_reports', 1, '2017-04-22 17:46:05', '2017-04-22 17:46:05'),
(66, 17, 'Hawk Eye', 'can_access_hawk_eye', 1, '2017-04-22 17:46:05', '2017-04-22 17:46:05'),
(67, 17, 'Traders Clinic', 'can_access_traders_clinic', 1, '2017-04-22 17:46:05', '2017-04-22 17:46:05'),
(68, 17, 'Feedback', 'can_access_feedback', 1, '2017-04-22 17:46:05', '2017-04-22 17:46:05'),
(69, 17, 'Invite Friends', 'can_access_invite_friends', 1, '2017-04-22 17:46:05', '2017-04-22 17:46:05'),
(70, 17, 'Subscription', 'can_access_subscription', 1, '2017-04-22 17:46:05', '2017-04-22 17:46:05'),
(71, 17, 'About Us', 'can_access_about_us', 1, '2017-04-22 17:46:05', '2017-04-22 17:46:05'),
(72, 17, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 17:46:05', '2017-04-22 17:46:05'),
(73, 18, 'Tips', 'can_access_tips', 1, '2017-04-22 17:47:01', '2017-04-22 17:47:01'),
(74, 18, 'Economy Barometer', 'can_access_economic_barometer', 1, '2017-04-22 17:47:01', '2017-04-22 17:47:01'),
(75, 18, 'Economic  Calender', 'can_access_economic_calendar', 1, '2017-04-22 17:47:01', '2017-04-22 17:47:01'),
(76, 18, 'Earning Calendar', 'can_access_earning_calendar', 1, '2017-04-22 17:47:01', '2017-04-22 17:47:01'),
(77, 18, 'New & Reports', 'can_access_news_reports', 1, '2017-04-22 17:47:01', '2017-04-22 17:47:01'),
(78, 18, 'Hawk Eye', 'can_access_hawk_eye', 1, '2017-04-22 17:47:01', '2017-04-22 17:47:01'),
(79, 18, 'Traders Clinic', 'can_access_traders_clinic', 1, '2017-04-22 17:47:01', '2017-04-22 17:47:01'),
(80, 18, 'Feedback', 'can_access_feedback', 1, '2017-04-22 17:47:01', '2017-04-22 17:47:01'),
(81, 18, 'Invite Friends', 'can_access_invite_friends', 1, '2017-04-22 17:47:01', '2017-04-22 17:47:01'),
(82, 18, 'Subscription', 'can_access_subscription', 1, '2017-04-22 17:47:01', '2017-04-22 17:47:01'),
(83, 18, 'About Us', 'can_access_about_us', 1, '2017-04-22 17:47:01', '2017-04-22 17:47:01'),
(84, 18, 'Contact Us', 'can_access_contact_us', 1, '2017-04-22 17:47:01', '2017-04-22 17:47:01'),
(85, 19, 'Tips', 'can_access_tips', 1, '2017-04-23 12:26:59', '2017-04-23 12:26:59'),
(86, 19, 'Economy Barometer', 'can_access_economic_barometer', 1, '2017-04-23 12:26:59', '2017-04-23 12:26:59'),
(87, 19, 'Economic  Calender', 'can_access_economic_calendar', 1, '2017-04-23 12:26:59', '2017-04-23 12:26:59'),
(88, 19, 'Earning Calendar', 'can_access_earning_calendar', 1, '2017-04-23 12:26:59', '2017-04-23 12:26:59'),
(89, 19, 'New & Reports', 'can_access_news_reports', 1, '2017-04-23 12:26:59', '2017-04-23 12:26:59'),
(90, 19, 'Hawk Eye', 'can_access_hawk_eye', 1, '2017-04-23 12:26:59', '2017-04-23 12:26:59'),
(91, 19, 'Traders Clinic', 'can_access_traders_clinic', 1, '2017-04-23 12:26:59', '2017-04-23 12:26:59'),
(92, 19, 'Feedback', 'can_access_feedback', 1, '2017-04-23 12:26:59', '2017-04-23 12:26:59'),
(93, 19, 'Invite Friends', 'can_access_invite_friends', 1, '2017-04-23 12:26:59', '2017-04-23 12:26:59'),
(94, 19, 'Subscription', 'can_access_subscription', 1, '2017-04-23 12:26:59', '2017-04-23 12:26:59'),
(95, 19, 'About Us', 'can_access_about_us', 1, '2017-04-23 12:26:59', '2017-04-23 12:26:59'),
(96, 19, 'Contact Us', 'can_access_contact_us', 1, '2017-04-23 12:26:59', '2017-04-23 12:26:59'),
(97, 20, 'Tips', 'can_access_tips', 1, '2017-04-23 12:32:57', '2017-04-23 12:32:57'),
(98, 20, 'Economy Barometer', 'can_access_economic_barometer', 1, '2017-04-23 12:32:57', '2017-04-23 12:32:57'),
(99, 20, 'Economic  Calender', 'can_access_economic_calendar', 1, '2017-04-23 12:32:57', '2017-04-23 12:32:57'),
(100, 20, 'Earning Calendar', 'can_access_earning_calendar', 1, '2017-04-23 12:32:57', '2017-04-23 12:32:57'),
(101, 20, 'New & Reports', 'can_access_news_reports', 1, '2017-04-23 12:32:57', '2017-04-23 12:32:57'),
(102, 20, 'Hawk Eye', 'can_access_hawk_eye', 1, '2017-04-23 12:32:57', '2017-04-23 12:32:57'),
(103, 20, 'Traders Clinic', 'can_access_traders_clinic', 1, '2017-04-23 12:32:57', '2017-04-23 12:32:57'),
(104, 20, 'Feedback', 'can_access_feedback', 1, '2017-04-23 12:32:57', '2017-04-23 12:32:57'),
(105, 20, 'Invite Friends', 'can_access_invite_friends', 1, '2017-04-23 12:32:57', '2017-04-23 12:32:57'),
(106, 20, 'Subscription', 'can_access_subscription', 1, '2017-04-23 12:32:57', '2017-04-23 12:32:57'),
(107, 20, 'About Us', 'can_access_about_us', 1, '2017-04-23 12:32:57', '2017-04-23 12:32:57'),
(108, 20, 'Contact Us', 'can_access_contact_us', 1, '2017-04-23 12:32:57', '2017-04-23 12:32:57'),
(109, 21, 'Tips', 'can_access_tips', 1, '2017-04-23 12:51:37', '2017-04-23 12:51:37'),
(110, 21, 'Economy Barometer', 'can_access_economic_barometer', 1, '2017-04-23 12:51:37', '2017-04-23 12:51:37'),
(111, 21, 'Economic  Calender', 'can_access_economic_calendar', 1, '2017-04-23 12:51:37', '2017-04-23 12:51:37'),
(112, 21, 'Earning Calendar', 'can_access_earning_calendar', 1, '2017-04-23 12:51:37', '2017-04-23 12:51:37'),
(113, 21, 'New & Reports', 'can_access_news_reports', 1, '2017-04-23 12:51:37', '2017-04-23 12:51:37'),
(114, 21, 'Hawk Eye', 'can_access_hawk_eye', 1, '2017-04-23 12:51:37', '2017-04-23 12:51:37'),
(115, 21, 'Traders Clinic', 'can_access_traders_clinic', 1, '2017-04-23 12:51:37', '2017-04-23 12:51:37'),
(116, 21, 'Feedback', 'can_access_feedback', 1, '2017-04-23 12:51:37', '2017-04-23 12:51:37'),
(117, 21, 'Invite Friends', 'can_access_invite_friends', 1, '2017-04-23 12:51:37', '2017-04-23 12:51:37'),
(118, 21, 'Subscription', 'can_access_subscription', 1, '2017-04-23 12:51:37', '2017-04-23 12:51:37'),
(119, 21, 'About Us', 'can_access_about_us', 1, '2017-04-23 12:51:37', '2017-04-23 12:51:37'),
(120, 21, 'Contact Us', 'can_access_contact_us', 1, '2017-04-23 12:51:37', '2017-04-23 12:51:37');

-- --------------------------------------------------------

--
-- Table structure for table `users_active_features_change_history`
--

CREATE TABLE `users_active_features_change_history` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `old_active_features` text,
  `new_active_features` text,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_active_features_change_history`
--

INSERT INTO `users_active_features_change_history` (`id`, `user_id`, `modified_by`, `old_active_features`, `new_active_features`, `created_at`) VALUES
(1, 20, 1, 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', '2017-04-23 13:11:58'),
(2, 20, 1, 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', '2017-04-23 13:16:10'),
(3, 20, 1, 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', '2017-04-23 13:19:19'),
(4, 20, 1, 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', '2017-04-23 13:19:39'),
(5, 20, 1, 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', '2017-04-23 13:20:34'),
(6, 20, 1, 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', '2017-04-23 13:22:26'),
(7, 20, 1, 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', '2017-04-23 13:23:41'),
(8, 20, 1, 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', '2017-04-23 13:24:14'),
(9, 20, 1, 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', '2017-04-23 13:29:32'),
(10, 21, 1, 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', '2017-05-01 21:06:13'),
(11, 21, 1, 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', 'a:12:{s:15:"can_access_tips";s:1:"1";s:29:"can_access_economic_barometer";s:1:"1";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"1";s:21:"can_access_contact_us";s:1:"1";}', '2017-05-01 21:07:33');

-- --------------------------------------------------------

--
-- Table structure for table `users_deleted`
--

CREATE TABLE `users_deleted` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `deleted_by` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_deleted`
--

INSERT INTO `users_deleted` (`id`, `user_id`, `user_type_id`, `deleted_by`, `email`, `phone_number`, `deleted_at`) VALUES
(1, 14, 3, 1, 'staff@staff.com', '1234567899', '2017-04-22 17:41:35');

-- --------------------------------------------------------

--
-- Table structure for table `users_favorite_tips`
--

CREATE TABLE `users_favorite_tips` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `tip_id` bigint(20) NOT NULL,
  `is_favorite` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users_login`
--

CREATE TABLE `users_login` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `device_type` int(11) NOT NULL COMMENT '1 = Android, 2 = IOS',
  `device_id` varchar(255) NOT NULL,
  `device_token` varchar(255) NOT NULL,
  `login_ip` varchar(255) NOT NULL,
  `login_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users_membership_extension`
--

CREATE TABLE `users_membership_extension` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `membership_extended_by` bigint(20) NOT NULL,
  `membership_type` int(11) NOT NULL COMMENT '1 = Free, 2 = Paid',
  `membership_start_date` date NOT NULL,
  `membership_end_date` date NOT NULL,
  `membership_extended_text` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_membership_extension`
--

INSERT INTO `users_membership_extension` (`id`, `user_id`, `membership_extended_by`, `membership_type`, `membership_start_date`, `membership_end_date`, `membership_extended_text`, `created_at`, `updated_at`) VALUES
(1, 6, 1, 0, '2017-04-15', '2017-04-20', 'Membership days entended on first time registration.', '2017-04-15 13:08:06', '2017-04-15 13:08:06'),
(2, 7, 1, 0, '2017-04-15', '2017-04-20', 'Membership days entended on first time registration.', '2017-04-15 13:17:41', '2017-04-15 13:17:41'),
(3, 8, 1, 0, '2017-04-15', '2017-04-20', 'Membership days entended on first time registration.', '2017-04-15 13:19:34', '2017-04-15 13:19:34'),
(4, 9, 1, 0, '2017-04-15', '2017-04-20', 'Membership days entended on first time registration.', '2017-04-15 13:35:24', '2017-04-15 13:35:24'),
(5, 10, 1, 0, '2017-04-19', '2017-04-24', 'Membership days entended on first time registration.', '2017-04-19 13:07:12', '2017-04-19 13:07:12'),
(6, 11, 1, 0, '2017-04-19', '2017-04-24', 'Membership days entended on first time registration.', '2017-04-19 13:08:24', '2017-04-19 13:08:24'),
(7, 12, 1, 0, '2017-04-19', '2017-04-24', 'Membership days entended on first time registration.', '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(8, 17, 1, 0, '2017-04-22', '1970-01-01', 'Membership days entended on first time registration.', '2017-04-22 17:46:05', '2017-04-22 17:46:05'),
(9, 18, 1, 0, '2017-04-22', '2017-04-27', 'Membership days entended on first time registration.', '2017-04-22 17:47:01', '2017-04-22 17:47:01'),
(10, 19, 1, 0, '2017-04-23', '2017-04-28', 'Membership days entended on first time registration.', '2017-04-23 12:26:59', '2017-04-23 12:26:59'),
(11, 20, 1, 0, '2017-04-23', '2017-04-28', 'Membership days entended on first time registration.', '2017-04-23 12:32:57', '2017-04-23 12:32:57'),
(12, 21, 1, 1, '2017-04-23', '2017-04-28', 'Membership days entended on first time registration.', '2017-04-23 12:51:37', '2017-04-23 12:51:37'),
(13, 20, 1, 1, '2017-04-23', '2017-05-02', 'Membership days updated from CICM admin portal', '2017-04-23 13:29:32', '2017-04-23 13:29:32'),
(14, 21, 1, 1, '2017-05-01', '2017-05-03', 'Membership days updated from CICM admin portal', '2017-05-01 21:06:13', '2017-05-01 21:06:13'),
(15, 21, 1, 1, '2017-05-01', '2017-05-08', 'Membership days updated from CICM admin portal', '2017-05-01 21:07:32', '2017-05-01 21:07:32');

-- --------------------------------------------------------

--
-- Table structure for table `users_permission`
--

CREATE TABLE `users_permission` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `permission_label` varchar(255) NOT NULL,
  `permission_value` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_permission`
--

INSERT INTO `users_permission` (`id`, `user_id`, `permission_label`, `permission_value`, `created_at`, `updated_at`) VALUES
(1, 14, 'can_create_users', '0', '2017-04-22 15:30:06', '2017-04-22 17:40:28'),
(2, 14, 'can_edit_user', '1', '2017-04-22 15:30:06', '2017-04-22 17:40:28'),
(3, 14, 'can_reset_user_device', '1', '2017-04-22 15:30:06', '2017-04-22 17:40:28'),
(4, 14, 'can_change_user_active_features', '1', '2017-04-22 15:30:06', '2017-04-22 17:40:28'),
(5, 14, 'can_change_user_membership', '0', '2017-04-22 15:30:06', '2017-04-22 17:40:28'),
(6, 14, 'can_see_user_history', '0', '2017-04-22 15:30:06', '2017-04-22 17:40:28'),
(7, 14, 'can_post_tips', '0', '2017-04-22 15:30:06', '2017-04-22 17:40:28'),
(8, 14, 'can_see_membership_palns', '0', '2017-04-22 15:30:06', '2017-04-22 17:40:28'),
(9, 14, 'can_add_membership_palns', '0', '2017-04-22 15:30:06', '2017-04-22 17:40:28'),
(10, 14, 'can_edit_membership_palns', '0', '2017-04-22 15:30:06', '2017-04-22 17:40:28'),
(11, 14, 'can_access_notifications', '0', '2017-04-22 15:30:06', '2017-04-22 17:40:28'),
(12, 14, 'can_manage_news_and_reports', '0', '2017-04-22 15:30:06', '2017-04-22 17:40:28'),
(13, 14, 'can_send_push_notifications', '0', '2017-04-22 15:30:06', '2017-04-22 17:40:28'),
(14, 14, 'can_change_settings', '1', '2017-04-22 15:30:06', '2017-04-22 17:40:28');

-- --------------------------------------------------------

--
-- Table structure for table `users_subscription_history`
--

CREATE TABLE `users_subscription_history` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `subscribed_plan_id` int(11) NOT NULL,
  `subscription_start_date` date NOT NULL,
  `subscription_end_date` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `prefix` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id`, `type`, `prefix`, `created_at`, `updated_at`) VALUES
(1, 'SUPER_ADMIN', 'SU', '2017-03-28 16:05:11', '2017-03-28 16:05:11'),
(2, 'SUB_ADMIN', 'SA', '2017-03-28 16:05:11', '2017-03-28 16:05:11'),
(3, 'STAFF_USER', 'ST', '2017-03-28 16:05:38', '2017-03-28 16:05:38'),
(4, 'APP_USER', 'AU', '2017-03-28 16:06:39', '2017-03-28 16:06:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `intraday_scanner`
--
ALTER TABLE `intraday_scanner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `intraday_scanner_categories`
--
ALTER TABLE `intraday_scanner_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `membership_plans`
--
ALTER TABLE `membership_plans`
  ADD PRIMARY KEY (`plan_id`);

--
-- Indexes for table `membership_plan_categories`
--
ALTER TABLE `membership_plan_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `push_notifications`
--
ALTER TABLE `push_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `stock_market_types`
--
ALTER TABLE `stock_market_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_preferences`
--
ALTER TABLE `system_preferences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tc_form_messages`
--
ALTER TABLE `tc_form_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tc_form_submissions`
--
ALTER TABLE `tc_form_submissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tips`
--
ALTER TABLE `tips`
  ADD PRIMARY KEY (`tip_id`);

--
-- Indexes for table `tips_follow_ups`
--
ALTER TABLE `tips_follow_ups`
  ADD PRIMARY KEY (`fu_id`);

--
-- Indexes for table `tips_targets`
--
ALTER TABLE `tips_targets`
  ADD PRIMARY KEY (`tt_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_active_features`
--
ALTER TABLE `users_active_features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_active_features_change_history`
--
ALTER TABLE `users_active_features_change_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_deleted`
--
ALTER TABLE `users_deleted`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_favorite_tips`
--
ALTER TABLE `users_favorite_tips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_login`
--
ALTER TABLE `users_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_membership_extension`
--
ALTER TABLE `users_membership_extension`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_permission`
--
ALTER TABLE `users_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_subscription_history`
--
ALTER TABLE `users_subscription_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `type` (`type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `country_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `feedbacks`
--
ALTER TABLE `feedbacks`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `intraday_scanner`
--
ALTER TABLE `intraday_scanner`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `intraday_scanner_categories`
--
ALTER TABLE `intraday_scanner_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `membership_plans`
--
ALTER TABLE `membership_plans`
  MODIFY `plan_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `membership_plan_categories`
--
ALTER TABLE `membership_plan_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `push_notifications`
--
ALTER TABLE `push_notifications`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `state_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stock_market_types`
--
ALTER TABLE `stock_market_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `system_preferences`
--
ALTER TABLE `system_preferences`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `tc_form_messages`
--
ALTER TABLE `tc_form_messages`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `tc_form_submissions`
--
ALTER TABLE `tc_form_submissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tips`
--
ALTER TABLE `tips`
  MODIFY `tip_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tips_follow_ups`
--
ALTER TABLE `tips_follow_ups`
  MODIFY `fu_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tips_targets`
--
ALTER TABLE `tips_targets`
  MODIFY `tt_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `users_active_features`
--
ALTER TABLE `users_active_features`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;
--
-- AUTO_INCREMENT for table `users_active_features_change_history`
--
ALTER TABLE `users_active_features_change_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `users_deleted`
--
ALTER TABLE `users_deleted`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users_favorite_tips`
--
ALTER TABLE `users_favorite_tips`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users_login`
--
ALTER TABLE `users_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users_membership_extension`
--
ALTER TABLE `users_membership_extension`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `users_permission`
--
ALTER TABLE `users_permission`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `users_subscription_history`
--
ALTER TABLE `users_subscription_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
