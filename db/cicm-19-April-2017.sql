-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 19, 2017 at 06:45 PM
-- Server version: 5.7.17-0ubuntu0.16.04.1
-- PHP Version: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cicm`
--

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` bigint(20) NOT NULL,
  `city_country_id` bigint(20) NOT NULL,
  `city_state_id` bigint(20) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `pin_code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `country_id` bigint(20) NOT NULL,
  `country_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `country_name`) VALUES
(1, 'India');

-- --------------------------------------------------------

--
-- Table structure for table `membership_plans`
--

CREATE TABLE `membership_plans` (
  `plan_id` bigint(20) NOT NULL,
  `plan_created_by` bigint(20) NOT NULL,
  `plan_title` text NOT NULL,
  `plan_description` text NOT NULL,
  `plan_amount` int(11) NOT NULL,
  `plan_benifits_in_days` int(11) NOT NULL,
  `active_features_in_plan` text,
  `plan_status` int(11) NOT NULL COMMENT '1 = Active, 0 = In Active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `membership_plans`
--

INSERT INTO `membership_plans` (`plan_id`, `plan_created_by`, `plan_title`, `plan_description`, `plan_amount`, `plan_benifits_in_days`, `active_features_in_plan`, `plan_status`, `created_at`, `updated_at`) VALUES
(2, 1, 'wertwert', 'wertwer', 12, 7, 'a:12:{s:15:"can_access_tips";s:1:"0";s:29:"can_access_economic_barometer";s:1:"0";s:28:"can_access_economic_calendar";s:1:"1";s:27:"can_access_earning_calendar";s:1:"1";s:23:"can_access_news_reports";s:1:"1";s:19:"can_access_hawk_eye";s:1:"1";s:25:"can_access_traders_clinic";s:1:"1";s:19:"can_access_feedback";s:1:"1";s:25:"can_access_invite_friends";s:1:"1";s:23:"can_access_subscription";s:1:"1";s:19:"can_access_about_us";s:1:"0";s:21:"can_access_contact_us";s:1:"0";}', 1, '2017-04-19 18:37:11', '2017-04-19 18:43:39');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` bigint(20) NOT NULL,
  `type` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `extra` text,
  `is_seen` int(11) NOT NULL DEFAULT '0' COMMENT '1 = Yes, 0 = No',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `text`, `extra`, `is_seen`, `created_at`) VALUES
(1, 'device_exists', 'Someone trying to register account by using Androiddevice with device id zsfdasdf1', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf1";s:12:"device_token";s:11:"asdfasdfASD";}', 0, '2017-04-15 14:15:41'),
(2, 'device_exists', 'Someone trying to register account by using Androiddevice with device id: zsfdasdf1', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf1";s:12:"device_token";s:11:"asdfasdfASD";}', 1, '2017-04-15 14:16:15'),
(3, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf1', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf1";s:12:"device_token";s:11:"asdfasdfASD";}', 1, '2017-04-15 15:28:39'),
(4, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf2', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf2";s:12:"device_token";s:11:"asdfasdfASD";}', 1, '2017-04-15 15:33:53'),
(5, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf3', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf3";s:12:"device_token";s:11:"asdfasdfASD";}', 1, '2017-04-15 15:39:22'),
(6, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf6', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf6";s:12:"device_token";s:11:"asdfasdfASD";}', 0, '2017-04-15 13:55:08'),
(7, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf6', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf6";s:12:"device_token";s:11:"asdfasdfASD";}', 0, '2017-04-15 13:56:01'),
(8, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf6', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf6";s:12:"device_token";s:11:"asdfasdfASD";}', 0, '2017-04-15 13:56:46'),
(9, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf6', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf6";s:12:"device_token";s:11:"asdfasdfASD";}', 0, '2017-04-15 13:58:33'),
(10, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf6', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf6";s:12:"device_token";s:11:"asdfasdfASD";}', 0, '2017-04-15 13:59:11'),
(11, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf6', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf6";s:12:"device_token";s:11:"asdfasdfASD";}', 0, '2017-04-15 14:03:27'),
(12, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf6', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf6";s:12:"device_token";s:11:"asdfasdfASD";}', 0, '2017-04-15 14:05:08'),
(13, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf6', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf6";s:12:"device_token";s:11:"asdfasdfASD";}', 0, '2017-04-15 14:10:51'),
(14, 'device_exists', 'Someone trying to register account by using Android device with device id: zsfdasdf6', 'a:6:{s:4:"name";s:16:"Siddharth Pandey";s:5:"email";s:27:"siddharthpandey26@gmail.com";s:12:"phone_number";s:10:"8319290104";s:11:"device_type";s:1:"1";s:9:"device_id";s:9:"zsfdasdf6";s:12:"device_token";s:11:"asdfasdfASD";}', 0, '2017-04-15 14:12:50'),
(15, 'membership_expired', 'Freeuser Szfsdfa membership is expired on 19/04/2017', '', 1, '2017-04-19 14:43:17'),
(16, 'membership_expired', 'Freeuser Test membership is expired on 19/04/2017', '', 1, '2017-04-19 14:43:17'),
(17, 'membership_expired', 'Freeuser Siddharth Pandey membership is expired on 19/04/2017', '', 1, '2017-04-19 14:43:18'),
(18, 'membership_expired', 'Freeuser Szfsdfa membership is expired on 19/04/2017', '', 1, '2017-04-19 14:43:44'),
(19, 'membership_expired', 'Freeuser Test membership is expired on 19/04/2017', '', 1, '2017-04-19 14:43:45'),
(20, 'membership_expired', 'Freeuser Siddharth Pandey membership is expired on 19/04/2017', '', 1, '2017-04-19 14:43:45');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `state_id` bigint(20) NOT NULL,
  `state_country_id` bigint(20) NOT NULL,
  `state_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `system_preferences`
--

CREATE TABLE `system_preferences` (
  `id` bigint(20) NOT NULL,
  `preference_key` varchar(255) CHARACTER SET utf8 NOT NULL,
  `preference_value` text CHARACTER SET utf8
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_preferences`
--

INSERT INTO `system_preferences` (`id`, `preference_key`, `preference_value`) VALUES
(1, 'app_name', 'Test'),
(2, 'app_description', 'Test'),
(3, 'app_email', 'test@gmail.com'),
(4, 'facebook_link', 'https://facebook.com'),
(5, 'twitter_link', ''),
(6, 'google_plus_link', ''),
(7, 'google_map_link', ''),
(8, 'linkedin_link', ''),
(9, 'instagram_link', ''),
(10, 'pinterest_link', ''),
(11, 'youtube_link', ''),
(12, 'copyright_text', ''),
(13, 'footer_name_address', ''),
(14, 'app_logo', ''),
(15, 'app_favicon', NULL),
(16, 'meta_separator', '|'),
(17, 'meta_description', 'Test'),
(18, 'meta_keywords', 'Test'),
(19, 'free_trial_days_for_new_members', '5'),
(20, 'verification_otp_length', '6'),
(21, 'verification_otp_message', 'Dear %s, Welcome to Test, Please enter verification OTP %s to activate your account. '),
(22, 'device_exists_message', 'Sorry, this device is already registered in our system. Please contact to our support at 9630108245 to activate your account.'),
(23, 'membership_expire_message', 'Dear %s, Your subscription days has been expired, please contact with our support at 9630108245 to extend your services.');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `user_type_id` int(11) NOT NULL COMMENT '1 = Super Admin, 2 = Sub Admin, 3 = Staff  users, 4 = App Users',
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `profile_pic` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `forgot_otp` varchar(255) NOT NULL,
  `verification_otp` varchar(255) NOT NULL,
  `membership_type` int(11) NOT NULL COMMENT '1 = Free, 2 = Paid',
  `membership_expire_on` date NOT NULL COMMENT 'Membership Expiration Date',
  `membership_status` int(11) NOT NULL COMMENT '1 = Active, 0 = In Active',
  `status` int(11) NOT NULL DEFAULT '1',
  `is_user_deleted` int(11) NOT NULL DEFAULT '0' COMMENT '1 = Yes, 0 = No',
  `is_phone_verified` int(11) NOT NULL DEFAULT '0' COMMENT '0 = No, 1 = Yes',
  `device_type` int(11) NOT NULL COMMENT '1 = Android, 2 = IOS',
  `device_id` varchar(255) NOT NULL,
  `device_token` varchar(255) NOT NULL,
  `last_login_ip` varchar(255) NOT NULL,
  `last_login_date` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_type_id`, `name`, `email`, `phone_number`, `profile_pic`, `password`, `forgot_otp`, `verification_otp`, `membership_type`, `membership_expire_on`, `membership_status`, `status`, `is_user_deleted`, `is_phone_verified`, `device_type`, `device_id`, `device_token`, `last_login_ip`, `last_login_date`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'test@gmail.com', '9827071943', '', 'e10adc3949ba59abbe56e057f20f883e', '', '', 0, '0000-00-00', 0, 1, 0, 0, 1, 'dfgsdfgsdf', 'sdfgsdfgsdfg', '', '2017-03-28 16:11:53', '2017-03-28 16:11:53', '2017-03-28 16:11:53'),
(2, 4, 'szfsdfa', 'test@test.com', '3456345634563456', '1491819891images.jpg', '', '', '', 1, '0000-00-00', 0, 1, 0, 1, 2, '2', '2', '127.0.0.1', '2017-04-10 15:49:50', '2017-04-10 13:59:01', '2017-04-19 14:43:44'),
(3, 4, 'test', 'test1@test.com', '7894561231', '', '', '', '123456', 1, '0000-00-00', 0, 0, 0, 0, 1, 'zsfdasdf', 'asdfasdfASD', '', '0000-00-00 00:00:00', '2017-04-10 14:01:18', '2017-04-19 14:43:44'),
(4, 4, 'Siddharth Pandey', 'siddharthpandey21@gmail.com', '9630108245', '1491834689images.jpeg', '', '', '', 1, '0000-00-00', 0, 1, 0, 1, 1, 'zsfdasdf', 'asdfasdfASD', '::1', '2017-04-10 16:31:06', '2017-04-10 16:30:32', '2017-04-19 14:43:45'),
(5, 4, 'Siddharth Pandey', 'siddharthpandey22@gmail.com', '24334234', '', '', '', '314893', 1, '2017-04-20', 1, 0, 0, 0, 1, 'zsfdasdf', 'asdfasdfASD', '', '0000-00-00 00:00:00', '2017-04-15 13:07:30', '2017-04-15 13:07:30'),
(6, 4, 'Siddharth Pandey', 'siddharthpandey23@gmail.com', '354335', '', '', '', '651880', 1, '2017-04-20', 1, 0, 0, 0, 1, 'zsfdasdf', 'asdfasdfASD', '', '0000-00-00 00:00:00', '2017-04-15 13:08:06', '2017-04-15 13:08:06'),
(7, 4, 'Siddharth Pandey', 'siddharthpandey24@gmail.com', '4444', '', '', '', '789834', 1, '2017-04-20', 1, 0, 0, 0, 1, 'zsfdasdf', 'asdfasdfASD', '', '0000-00-00 00:00:00', '2017-04-15 13:17:41', '2017-04-15 13:17:41'),
(8, 4, 'Siddharth Pandey', 'siddharthpandey25@gmail.com', '345', '', '', '', '', 1, '2017-04-20', 1, 1, 0, 1, 1, 'zsfdasdf', 'asdfasdfASD', '::1', '2017-04-15 13:20:23', '2017-04-15 13:19:34', '2017-04-15 13:20:23'),
(9, 4, 'Siddharth Pandey', 'siddharthpandey26@gmail.com', '8319290104', '', '', '', '', 1, '2017-04-20', 1, 1, 0, 1, 1, 'zsfdasdf6', 'asdfasdfASD', '::1', '2017-04-15 15:40:49', '2017-04-15 13:35:24', '2017-04-15 15:40:49'),
(10, 4, 'Siddharth Pandey', 'siddharthpandey28@gmail.com', '789456123', '', '', '', '174075', 1, '2017-04-24', 1, 0, 0, 0, 1, '546745674567', '456745674567456', '', '0000-00-00 00:00:00', '2017-04-19 13:07:12', '2017-04-19 13:07:12'),
(11, 4, 'Siddharth Pandey', 'siddharthpandey29@gmail.com', '78945612378', '', '', '', '204935', 1, '2017-04-24', 1, 0, 0, 0, 1, 'fdghdfgh', '45674567dfghdfgh4567456', '', '0000-00-00 00:00:00', '2017-04-19 13:08:24', '2017-04-19 13:08:24'),
(12, 4, 'Siddharth Pandey', '454456745674567@gmail.com', '4567456745674567', '', '', '', '590589', 1, '2017-04-24', 1, 0, 0, 0, 1, '456745674576', '45674567dfghdfgh4567456', '', '0000-00-00 00:00:00', '2017-04-19 13:16:24', '2017-04-19 13:16:24');

-- --------------------------------------------------------

--
-- Table structure for table `users_active_features`
--

CREATE TABLE `users_active_features` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `feature_title` varchar(255) NOT NULL,
  `feature_prefix` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL COMMENT '1 = Yes, 0 = No',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_active_features`
--

INSERT INTO `users_active_features` (`id`, `user_id`, `feature_title`, `feature_prefix`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 11, 'Tips', 'can_access_tips', 1, '2017-04-19 13:08:25', '2017-04-19 13:08:25'),
(2, 11, 'Economy Barometer', 'can_access_economic_barometer', 1, '2017-04-19 13:08:25', '2017-04-19 13:08:25'),
(3, 11, 'Economic  Calender', 'can_access_economic_calendar', 1, '2017-04-19 13:08:25', '2017-04-19 13:08:25'),
(4, 11, 'Earning Calendar', 'can_access_earning_calendar', 1, '2017-04-19 13:08:25', '2017-04-19 13:08:25'),
(5, 11, 'New & Reports', 'can_access_news_reports', 1, '2017-04-19 13:08:25', '2017-04-19 13:08:25'),
(6, 11, 'Hawk Eye', 'can_access_hawk_eye', 1, '2017-04-19 13:08:25', '2017-04-19 13:08:25'),
(7, 11, 'Traders Clinic', 'can_access_traders_clinic', 1, '2017-04-19 13:08:25', '2017-04-19 13:08:25'),
(8, 11, 'Feedback', 'can_access_feedback', 1, '2017-04-19 13:08:25', '2017-04-19 13:08:25'),
(9, 11, 'Invite Friends', 'can_access_invite_friends', 1, '2017-04-19 13:08:25', '2017-04-19 13:08:25'),
(10, 11, 'Subscription', 'can_access_subscription', 1, '2017-04-19 13:08:25', '2017-04-19 13:08:25'),
(11, 11, 'About Us', 'can_access_about_us', 1, '2017-04-19 13:08:25', '2017-04-19 13:08:25'),
(12, 11, 'Contact Us', 'can_access_contact_us', 1, '2017-04-19 13:08:25', '2017-04-19 13:08:25'),
(13, 12, 'Tips', 'can_access_tips', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(14, 12, 'Economy Barometer', 'can_access_economic_barometer', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(15, 12, 'Economic  Calender', 'can_access_economic_calendar', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(16, 12, 'Earning Calendar', 'can_access_earning_calendar', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(17, 12, 'New & Reports', 'can_access_news_reports', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(18, 12, 'Hawk Eye', 'can_access_hawk_eye', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(19, 12, 'Traders Clinic', 'can_access_traders_clinic', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(20, 12, 'Feedback', 'can_access_feedback', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(21, 12, 'Invite Friends', 'can_access_invite_friends', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(22, 12, 'Subscription', 'can_access_subscription', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(23, 12, 'About Us', 'can_access_about_us', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24'),
(24, 12, 'Contact Us', 'can_access_contact_us', 1, '2017-04-19 13:16:24', '2017-04-19 13:16:24');

-- --------------------------------------------------------

--
-- Table structure for table `users_active_features_change_history`
--

CREATE TABLE `users_active_features_change_history` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `old_active_features` text,
  `new_active_features` text,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users_deleted`
--

CREATE TABLE `users_deleted` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `deleted_by` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users_login`
--

CREATE TABLE `users_login` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `device_type` int(11) NOT NULL COMMENT '1 = Android, 2 = IOS',
  `device_id` varchar(255) NOT NULL,
  `device_token` varchar(255) NOT NULL,
  `login_ip` varchar(255) NOT NULL,
  `login_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users_membership_extension`
--

CREATE TABLE `users_membership_extension` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `membership_extended_by` bigint(20) NOT NULL,
  `membership_start_date` date NOT NULL,
  `membership_end_date` date NOT NULL,
  `membership_extended_text` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_membership_extension`
--

INSERT INTO `users_membership_extension` (`id`, `user_id`, `membership_extended_by`, `membership_start_date`, `membership_end_date`, `membership_extended_text`, `created_at`, `updated_at`) VALUES
(1, 6, 1, '2017-04-15', '2017-04-20', 'Membership days entended on first time registration.', '2017-04-15 13:08:06', '2017-04-15 13:08:06'),
(2, 7, 1, '2017-04-15', '2017-04-20', 'Membership days entended on first time registration.', '2017-04-15 13:17:41', '2017-04-15 13:17:41'),
(3, 8, 1, '2017-04-15', '2017-04-20', 'Membership days entended on first time registration.', '2017-04-15 13:19:34', '2017-04-15 13:19:34'),
(4, 9, 1, '2017-04-15', '2017-04-20', 'Membership days entended on first time registration.', '2017-04-15 13:35:24', '2017-04-15 13:35:24'),
(5, 10, 1, '2017-04-19', '2017-04-24', 'Membership days entended on first time registration.', '2017-04-19 13:07:12', '2017-04-19 13:07:12'),
(6, 11, 1, '2017-04-19', '2017-04-24', 'Membership days entended on first time registration.', '2017-04-19 13:08:24', '2017-04-19 13:08:24'),
(7, 12, 1, '2017-04-19', '2017-04-24', 'Membership days entended on first time registration.', '2017-04-19 13:16:24', '2017-04-19 13:16:24');

-- --------------------------------------------------------

--
-- Table structure for table `users_permission`
--

CREATE TABLE `users_permission` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `permission_label` varchar(255) NOT NULL,
  `permission_value` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `prefix` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id`, `type`, `prefix`, `created_at`, `updated_at`) VALUES
(1, 'SUPER_ADMIN', 'SU', '2017-03-28 16:05:11', '2017-03-28 16:05:11'),
(2, 'SUB_ADMIN', 'SA', '2017-03-28 16:05:11', '2017-03-28 16:05:11'),
(3, 'STAFF_USER', 'ST', '2017-03-28 16:05:38', '2017-03-28 16:05:38'),
(4, 'APP_USER', 'AU', '2017-03-28 16:06:39', '2017-03-28 16:06:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `membership_plans`
--
ALTER TABLE `membership_plans`
  ADD PRIMARY KEY (`plan_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `system_preferences`
--
ALTER TABLE `system_preferences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_active_features`
--
ALTER TABLE `users_active_features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_active_features_change_history`
--
ALTER TABLE `users_active_features_change_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_deleted`
--
ALTER TABLE `users_deleted`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_login`
--
ALTER TABLE `users_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_membership_extension`
--
ALTER TABLE `users_membership_extension`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_permission`
--
ALTER TABLE `users_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `type` (`type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `country_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `membership_plans`
--
ALTER TABLE `membership_plans`
  MODIFY `plan_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `state_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_preferences`
--
ALTER TABLE `system_preferences`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users_active_features`
--
ALTER TABLE `users_active_features`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `users_active_features_change_history`
--
ALTER TABLE `users_active_features_change_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users_deleted`
--
ALTER TABLE `users_deleted`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users_login`
--
ALTER TABLE `users_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users_membership_extension`
--
ALTER TABLE `users_membership_extension`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users_permission`
--
ALTER TABLE `users_permission`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
