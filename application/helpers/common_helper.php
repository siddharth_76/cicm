<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Common Helper
 * Author: Siddharth Pandey
 * Author Email: codebuckets@gmail.com
 * Description: This file is auto loaded in this application and we can use all these functions globally in the application.
 * version: 1.0
 */

/**
* Send mail
* @param Message, Subject, Email address
* @return true OR false
*/
if (!function_exists('send_mail')) {
	function send_mail($message, $subject, $email_address) {
	    $ci =&get_instance();
		$ci->load->library('email');
		$config['mailtype'] = 'html';
		$ci->email->initialize($config);	
		$ci->email->from(get_option('app_name').' <'.get_option('app_email').'>');
		$ci->email->to($email_address);
		$ci->email->subject($subject);
        $ci->email->message($message);
        
		if($ci->email->send()) {	
			return true;
		} else {
			return false;
		}
	}
}

/**
* print_r formatting
* @param Array $array
* @return formatted array including <pre> tag.
*/
if(!function_exists('p')) {
	function p($array) {
		echo '<pre>';
		print_r($array);
		echo '</pre>';
	}
}

/**
* check for required value in rest api
* @param Array $chk_params, $converted_array
* @return missed params
*/
if(!function_exists('check_required_value')) {
	function check_required_value($chk_params, $converted_array) {
        foreach ($chk_params as $param) {
            if (array_key_exists($param, $converted_array) && ($converted_array[$param] != '')) {
                $check_error = 0;
            } else {
                $check_error = array('check_error' => 1, 'param' => $param);
                break;
            }
        }
        return $check_error;
	}
}

/**
* Admin assets url for all the resources Eg: css, images, js, font files
* @param NULL
* @return Application url with assets folder
*/
if(!function_exists('admin_assets')) {
	function admin_assets() {
		echo base_url().'assets/admin/';
	}
}

/**
* Admin assets url for all the resources Eg: css, images, js, font files
* @param NULL
* @return Application url with assets folder
*/
if(!function_exists('get_admin_assets')) {
	function get_admin_assets() {
		return base_url().'assets/admin/';
	}
}

/**
* Load admin view dynamically
* @param Content view path = $view_path
* @param Array $data for extra meta info
* @param $header = TRUE
* @param $sidebar = TRUE
* @param $footer = TRUE
*/
if(!function_exists('load_admin_view')) {
	function load_admin_view($view_path, $data = array(), $header = true, $sidebar = true, $footer = true) {
		if(!empty($view_path)) {
			$ci =&get_instance();

			/* Load Header */
			if($header) {
				$ci->load->view(INCLUDES.'header', $data);
			}

			/* Leaft Sidebar */
			if($sidebar) {
				$ci->load->view(INCLUDES.'left-sidebar', $data);
			}

			/* Load content section */
			$ci->load->view($view_path, $data);

			/* Load footer */
			if($footer) {
				$ci->load->view(INCLUDES.'footer', $data);
			}
		} else {
			show_error('Unable to load content view, please check again.');
		}
	}
}

/**
* Add active class on the menu
* @param CI router class by $this->router->fetch_class()
* @return active class based on the router condition
*/
if(!function_exists('add_active_class')) {
	function add_active_class($ccl = '', $class = '') {
		$ci =&get_instance();
		$currentMethod = $ci->router->fetch_method();
		$currentClass = $ci->router->fetch_class();

		if($ccl == $currentClass && $currentMethod == $class) {
			echo 'active';
		}
	}
}

/**
* Add active class on the menu tree
* @param Array of CI router class by $this->router->fetch_class()
* @return active class based on the router condition
*/
if(!function_exists('tree_active_class')) {
	function tree_active_class($ccl = '', $class = array()) {
		$ci =&get_instance();
		$currentMethod = $ci->router->fetch_method();
		$currentClass = $ci->router->fetch_class();
		
		if($ccl == $currentClass && in_array($currentMethod, $class)) {
			echo 'active';
		}
	}
}

/**
* Get pref_value based on pref_key from system_preferences entity
* @param $key = pref_key
* @return pref_value
*/
if(!function_exists('get_option')) {
	function get_option($key = '') {
		$ci =&get_instance();
		$keyArr = array('preference_key' => $key);
	    $settings = $ci->common_model->getSingleRecordById(SYSTEM_PREFERENCE, $keyArr);
	    if(!empty($settings)) {
			return $settings['preference_value'];
		}
	}
}

/**
* Add or update pref_value based on pref_key from system_preferences entity
* @param $key = pref_key
* @return pref_value
*/
if(!function_exists('update_option')) {
	function update_option($key, $val) {
		$ci =&get_instance();
		$keyArr = array('preference_key' => $key);
	    $settings = $ci->common_model->getSingleRecordById(SYSTEM_PREFERENCE, $keyArr);
	    if(!empty($settings)) {
			/* Update key value */
			$data = array('preference_value' => $val);
			$where = array('preference_key' => $key);
			$ci->common_model->updateRecords(SYSTEM_PREFERENCE, $data, $where);
		} else {
			/* Add key value */
			$postData = array('preference_key' => $key, 'preference_value' => $val);
			$ci->common_model->addRecords(SYSTEM_PREFERENCE, $postData);
		}
	}
}

/**
* Admin Meta title
* @param Meta Title dynamically from methods
* @return $title, meta title of the application
*/
if(!function_exists('admin_meta_title')) {
	function admin_meta_title($str = '') {
		$siteName = get_option('app_name');
		
		if(empty($siteName)) {
			$siteName = CMS_NAME;
		}

		if(!empty($str)) {
			$title = $str.' | '.$siteName;
		} else {
			$title = 'Admin Panel | '.$siteName;
		}
		echo $title;
	}
}

/**
* Meta title
* @param Meta Title dynamically from methods
* @return $title, meta title of the application
*/
if(!function_exists('meta_title')) {
	function meta_title($str = '') {
		$churchOption = get_option('app_name');
		if(!empty($str)) {
			$churchName = $str;
		} elseif(empty($churchOption)) {
			$churchName = get_option('app_name');
		} else {
			$churchName = 'Welcome';
		}

		$separator = get_option('meta_separator');
		if(empty($separator)) {
			$separator = '|';
		}

		$curchDescription = get_option('church_description');
		if(empty($curchDescription)) {
			$curchDescription = CMS_NAME;
		}
		$title = $churchName . ' ' . $separator . ' ' . $curchDescription;
		return $title;
	}
}

/**
* Add dymanic class to the body based on the body_class key in any methods.
* @param $class
* @return $class, which has to be added ont the body
*/
if(!function_exists('body_class')) {
	function body_class($class = '') {
		if(is_array($class)) {
			$classRet = implode(' ', $class);
		} else {
			$classRet = $class;
		}
		echo $classRet;
	}
}

/**
* Return cms url
* @param $url = 'controller/method'
* @param if browser have return_url then it's also return return_uri
*/
if(!function_exists('cms_url')) {
	function cms_url($url = '') {
		$ci =&get_instance();
		if(empty($url)) {
			echo base_url();
		} else {
			if($ci->input->get('return_uri')) {
				echo base_url().$url.'?return_uri='.urlencode($ci->input->get('return_uri'));
			} else {
				echo base_url().$url;
			}
		}
	}
}

/**
* Return cms url
* @param $url = 'controller/method'
* @param if browser have return_url then it's also return return_uri
*/
if(!function_exists('get_cms_url')) {
	function get_cms_url($url = '') {
		$ci =&get_instance();
		if(empty($url)) {
			return base_url();
		} else {
			if($ci->input->get('return_uri')) {
				return base_url().$url.'?return_uri='.urlencode($ci->input->get('return_uri'));
			} else {
				return base_url().$url;
			}
		}
	}
}

/**
* File upload
* @param Array $_FILES, $config 
*/
if(!function_exists('upload_file')) {
	function upload_file($file, $config) {
		$ci =&get_instance();
		$ci->load->library('upload', $config);
		$resp = array();
        if (!$ci->upload->do_upload($config['field_name'])) {
            $error = array('error' => $ci->upload->display_errors());
            $resp = array('message' => 'failed', 'error' => $error);
        } else {
        	$uploadPath = explode('/', $config['upload_path']);
        	$uploadPath = $uploadPath[1].'/'.$uploadPath[2].'/';
            $uploadData = array('upload_data' => $ci->upload->data());
            $filename = $uploadData['upload_data']['file_name'];
            $resp = array('message' => 'success', 'upload_data' => $uploadData, 'file_name' => $filename, 'file_path' => get_cms_url('').$uploadPath.$filename);
        }
        return $resp;
	}
}

/**
* Show selected in select tag
* @param $currentValue, $desiredValue
*/
if(!function_exists('show_selected')) {
	function show_selected($currentValue, $desiredValue) {
		if($currentValue == $desiredValue) {
			echo 'selected="selected"';
		}
	}
}

/**
* Error log level
* @return Array of error log level
*/
if(!function_exists('error_log_level')) {
	function error_log_level() {
		return array(
			'1' => 'Critical Errors',
			'2' => 'Warnings and Errors',
			'3' => 'Debug Level'
		);
	}
}

/**
* Admin Favicon
* @return favicon HTML
*/
if(!function_exists('admin_favicon')) {
	function admin_favicon() {
		$favicon = get_option('app_favicon');
		if(empty($favicon)) {
			$favicon = get_admin_assets().'dist/img/favicon.ico';
		}
		echo '<link rel="icon" href="'.$favicon.'"/>';
	}
}

/**
* Custom Pagination
* @param 
*/
if(!function_exists('custom_pagination')) {
	function custom_pagination($url, $total_records, $results, $pull = 'right', $uri_segment = '', $string = '') {
		$ci =&get_instance();
		$config['full_tag_open'] 	= '<ul class="pagination pagination-sm no-margin pull-'.$pull.'">';
		$config['full_tag_close'] 	= '</ul>';				
		$config['cur_tag_open'] 	= '<li class="activePaginationNum paginationNums">';
		$config['cur_tag_close'] 	= '</li>';
		$config['next_link'] 		= '&raquo;';
		$config['next_tag_open'] 	= '<li class="paginationNums">';
		$config['next_tag_close'] 	= '</li>';
		$config['prev_link'] 		= '&laquo;';		
		$config['prev_tag_open'] 	= '<li class="paginationNums">';
		$config['prev_tag_close'] 	= '</li>';	
		$config['first_tag_open'] 	= '<li class="paginationNums">';
		$config['first_tag_close'] 	= '</li>';
		$config['first_link']		= '&laquo; First';	
		$config['last_tag_open'] 	= '<li class="paginationNums">';
		$config['last_tag_close'] 	= '</li>';
		$config['last_link']		= 'Last &raquo;';			
		$config['num_tag_open'] 	= '<li class="paginationNums">';
		$config['num_tag_close'] 	= '</li>';

		if(!empty($uri_segment)) {
			$config['uri_segment'] 	= $uri_segment;
		}

		if(!empty($string)) {
			$config['page_query_string'] = TRUE;
			$config['base_url'] 		 = $url.'?'.$string;
		} else {
			$config['base_url'] 		 = $url;
		}

		$config['total_rows'] 		= $total_records;
		$config['per_page'] 		= $results;

		$ci->pagination->initialize($config);
		return $ci->pagination->create_links();
	}
}

/**
* Decode rest api request
* @param json
* @return array of json
*/
if(!function_exists('decode_request')) {
	function decode_request() {
		$reqArr = json_decode(file_get_contents('php://input'), TRUE);
		if(array_key_exists('reqObj', $reqArr)) {
			return $reqArr['reqObj'];
		} else {
			echo json_encode(array('error' => 'Invalid json request'));
			exit();
		}
	}
}

/**
* Get rest api request
*/
if(!function_exists('get_api_request')) {
	function get_api_request() {
		$request = file_get_contents('php://input');
		return $request;
	}
}

/**
 * Get country name
 * @param $countryId
 * @return $countryName
 */
if(!function_exists('get_country')) {
	function get_country($countryId) {
		$ci =& get_instance();
		$country = $ci->common_model->getSingleRecordById(COUNTRY, array('country_id' => $countryId));
		if(!empty($country)) {
			return $country['country_name'];
		}
	}
}

/**
 * Get state name
 * @param $stateId
 * @return $stateName
 */
if(!function_exists('get_state')) {
	function get_state($stateId) {
		$ci =& get_instance();
		$state = $ci->common_model->getSingleRecordById(STATE, array('state_id' => $stateId));
		if(!empty($state)) {
			return $state['state_name'];
		}
	}
}

/**
 * Get city name
 * @param $cityId
 * @return $cityName
 */
if(!function_exists('get_city')) {
	function get_city($cityId) {
		$ci =& get_instance();
		$city = $ci->common_model->getSingleRecordById(CITY, array('city_id' => $cityId));
		if(!empty($city)) {
			return $city['city_name'];
		}
	}
}

/**
* Convert DB int
* @param $value
* @return $newValue
*/
if(!function_exists('convertDBInt')) {
	function convertDBInt($value) {
	    $newValue = $value*100;
	    return $newValue;
	}
}

/**
* Revert DB int
* @param $value
* @return $newValue
*/
if(!function_exists('revertDBInt')) {
	function revertDBInt($value) {
	    $newValue = $value/100;
	    return $newValue;
	}
}

/**
 * Write Transaction Logs
 */
if(!function_exists('write_transaction_logs')) {
	function write_transaction_logs($data) {
		$filepath = APPPATH . 'transaction_logs/Transaction-log-'.date('Y-m-d').'.txt';
        $handle = fopen($filepath, "a+");
        fwrite($handle, $data . "\n");
        fclose($handle);
	}
}

if(!function_exists('send_apn_notification')) {
	function send_apn_notification($deviceToken, $message) {
		//$deviceToken = '5c96f8747d856c8c938a71a17802aea963a19f0a36b3916f054ec833534b2e50';

		// Put your private key's passphrase here:
		$passphrase = '123456';

		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', 'pushcert.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

		// Open a connection to the APNS server
		$fp = stream_socket_client(APNS_GATEWAY_URL, $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

		if (!$fp) {
			log_message('apn_debug',"APN: Maybe some errors: $err: $errstr");
			//exit("Failed to connect: $err $errstr" . PHP_EOL);
		} else {
			log_message('apn_debug',"Connected to APNS");
			//echo 'Connected to APNS' . PHP_EOL;
		}

		// Create the payload body
		$body['aps'] = array(
			'alert' => $message,
			'sound' => 'default'
			);

		// Encode the payload as JSON
		$payload = json_encode($body);

		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));

		if (!$result) {
			log_message('apn_send_debug',"APN: Message not delivered");
			//echo 'Message not delivered' . PHP_EOL;
		} else {
			log_message('apn_send_debug',"APN: Message successfully delivered");
			//echo 'Message successfully delivered' . PHP_EOL;
		}

		// Close the connection to the server
		fclose($fp);
	}
}

if(!function_exists('apn_feedback')) {
	// designed for retreiving devices, on which app not installed anymore
	function apn_feedback()
	{	
		$ci =&get_instance();
	    $ci->load->library('apn');

	    $unactive = $ci->apn->getFeedbackTokens();

	    if (!count($unactive))
	    {
	        log_message('info','Feedback: No devices found. Stopping.');
	        return false;
	    }

	    foreach($unactive as $u)
	    {
	        $devices_tokens[] = $u['devtoken'];
	    }
	    //p($unactive);
	}
}

if(!function_exists('send_gcm_notification')) {
	function send_gcm_notification($token, $message) {
		$ci =&get_instance();
		// simple loading
    	// note: you have to specify API key in config before
        $ci->load->library('gcm');

	    // simple adding message. You can also add message in the data,
	    // but if you specified it with setMesage() already
	    // then setMessage's messages will have bigger priority
        $ci->gcm->setMessage($message);

    	// add recepient or few
        $ci->gcm->addRecepient($token);
        //$ci->gcm->addRecepient('New reg id');

    	// set additional data
        $ci->gcm->setData(array(
            'some_key' => 'some_val'
        ));

    	// also you can add time to live
        $ci->gcm->setTtl(500);
    	// and unset in further
        $ci->gcm->setTtl(false);

    	// set group for messages if needed
        $ci->gcm->setGroup('Test');
    	// or set to default
        $ci->gcm->setGroup(false);
        $ci->gcm->send();

    	// then send
        if ($ci->gcm->send())
            echo 'Success for all messages';
        else
            echo 'Some messages have errors';

    	// and see responses for more info
        //$ci->gcm->status;
        //$ci->gcm->messagesStatuses;
	}
}

/**
 * Generate random number
 * @param $length
 */
if(!function_exists('random_numbers')) {
	function random_numbers($length) {
		$length = (!empty($length)) ? $length : 6;
	    $min = pow(10, $length - 1);
	    $max = pow(10, $length) - 1;
	    return mt_rand($min, $max);
	}
}

/**
 * Send Text Message
 * @param $number, $message
 */
if(!function_exists('send_sms')) {
	function send_sms($number, $message){
		$url = 'https://control.msg91.com/api/sendhttp.php?authkey='.MSG91_AUTH_KEY.'&mobiles=91'.$number.'&message='.$message.'&sender='.MSG91_SENDER_ID.'&route=4&country=91';
		$curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $result = curl_exec($curl);
        curl_close($curl);
	}
}

/**
 * Parameter Example
 * $data = array('post_id'=>'12345','post_title'=>'A Blog post');
 * $target = 'single tocken id or topic name';
 * or
 * $target = array('token1','token2','...'); // up to 1000 in one request
 */
if(!function_exists('send_fcm_notification')) {
	function send_fcm_notification($data, $target, $isAdmin = 0){
		//FCM api URL
		$url = 'https://fcm.googleapis.com/fcm/send';
		//api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
		if($isAdmin == 1) {
			$server_key = 'AAAA0dHOOMI:APA91bGK8_y7HzI6zPEW8iJKvSsEf0JfcwRMssdB-n37IipYIUfQJ9x2lYtHf3HXqxkzLPs1wi3noPBXfOH56hcvqDC1hQ_U7xQmNbxjTs0N-86SGD-RpJ-r-aAb1tdw19NB89rKbpxe';
		} else {
			$server_key = 'AAAA0dHOOMI:APA91bGK8_y7HzI6zPEW8iJKvSsEf0JfcwRMssdB-n37IipYIUfQJ9x2lYtHf3HXqxkzLPs1wi3noPBXfOH56hcvqDC1hQ_U7xQmNbxjTs0N-86SGD-RpJ-r-aAb1tdw19NB89rKbpxe';
		}
					
		$fields = array();
		$fields['data'] = $data;
		if(is_array($target)){
			$fields['registration_ids'] = $target;
		} else {
			$fields['to'] = $target;
		}
		//header with content_type api key
		$headers = array(
			'Content-Type:application/json',
		  	'Authorization:key='.$server_key
		);
					
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);
		curl_close($ch);
	}
}

/**
 * Send Browser Notifications
 */
if(!function_exists('send_web_notification')) {
	function send_web_notification($message, $heading, $url, $data = array()) {
		$content = array("en" => $message);
		$heading = array("en" => $heading);
		
		$fields = array(
			'app_id' => ONESIGNAL_APP_ID,
			'included_segments' => array('All'),
      		'data' => $data,
			'contents' => $content,
			'headings' => $heading,
			'url' => $url
		);
		
		$fields = json_encode($fields);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8','Authorization: Basic '.ONESIGNAL_API_KEY));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
	}
}

/**
 * Notification icons depends on conditions
 * @param $type
 * @return $class
 */
if(!function_exists('notification_icon')) {
	function notification_icon($type) {
		$class = '';

		if($type == 'device_exists') {
			$class = 'fa fa-mobile';
		}
		if($type == 'membership_expired') {
			$class = 'fa fa-user';
		}
		if($type == 'features_changed') {
			$class = 'fa fa-th';
		}
		if($type == 'tip_published'){
			$class = 'fa fa-external-link';
		}
		return $class;
	}
}

/**
 * Notification type lists
 * @return $types
 */
if(!function_exists('notification_types')) {
	function notification_types() {
		$types = array('device_exists', 'membership_expired', 'features_changed', 'tip_published');
		return $types;
	}
}

/**
 * Get tip types
 */
if(!function_exists('get_tip_type')) {
	function get_tip_type($typeId = '') {
		$types = array(
			BUY_TIP => 'Buy',
			SELL_TIP => 'Sell'
		);

		if(!empty($typeId)) {
			return $types[$typeId];
		} else {
			return $types;
		}
	}
}

/**
 * Get tip exchange type
 */
if(!function_exists('get_tip_exchange_type')) {
	function get_tip_exchange_type($typeId = ''){
		$types = array(
			INTRADAY_TIP => 'Intraday',
			SWING_TIP => 'Swing',
			INTRADAY_SWING_TIP => 'Intraday / Swing',
			BTST_TIP => 'BTST',
			STBT_TIP => 'STBT'
		);

		if(!empty($typeId)) {
			return $types[$typeId];
		} else {
			return $types;
		}
	}
}

/**
 * Get tip exchange market type
 */
if(!function_exists('get_tip_exchange_market_type')) {
	function get_tip_exchange_market_type($typeId = ''){
		$types = array(
			'1' => 'Cash',
			'2' => 'Future',
			'3' => 'Option'
		);

		if(!empty($typeId)) {
			return $types[$typeId];
		} else {
			return $types;
		}
	}
}

/**
 * Get total filled traders clinic form count
 * @param $userId
 * @return $resp
 */
if(!function_exists('get_user_filled_tc_form_count')){
	function get_user_filled_tc_form_count($userId){
		if(isset($userId)) {
			$ci =&get_instance();
			$userData = $ci->common_model->getSingleRecordById(USER, array('id' => $userId));
			if($userData['membership_type'] == FREE_MEMBER) {
				$sql = 'SELECT * FROM `'.TC_FORM_SUBMISSION.'` WHERE user_id = '.$userId.' AND MONTH(created_at) = MONTH(NOW())';
        		$checkLimit = $ci->common_model->getCustomSqlCount($sql);
			} elseif($userData['membership_type'] == PREMIUM_MEMBER) {
				$checkLimit = $userData['filled_tc_form'];
			} else {
				$checkLimit = 0;
			}
        	return $checkLimit;
		}
	}
}

/**
 * Get user traders form history
 * @param $userId
 * @return $resp
 */
if(!function_exists('get_user_tc_form_history')) {
	function get_user_tc_form_history($userId){
		$resp = array();
		$ci =&get_instance();
		$totalFilledForm = get_user_filled_tc_form_count($userId);
		$userData = $ci->common_model->getSingleRecordById(USER, array('id' => $userId));
		if(!empty($userData)) {
			if($userData['membership_type'] == FREE_MEMBER) {
				$tcFormLimit = get_option('free_tc_form_submission_limit');
				$isAbleToFill = ($totalFilledForm >= $tcFormLimit) ? 0 : 1;
			} elseif($userData['membership_type'] == PREMIUM_MEMBER) {
				$tcFormLimit = $userData['tc_form_limit'];
				$isAbleToFill = ($userData['tc_form_limit'] <= 0) ? 0 : 1;
			} else {
				$tcFormLimit = 0;
				$isAbleToFill = 0;
			}

			$resp['total_fill_up_tc_form'] = $totalFilledForm;
	        $resp['tc_form_limit'] = $tcFormLimit;
	        $resp['is_able_to_fill_tc_form'] = $isAbleToFill;
		}
		return $resp;
	}
}

/**
 * Get Device tokens of all users
 * @param $deviceType
 * 1 = Android, 2 = IOS
 */
if(!function_exists('get_all_device_tokens')) {
	function get_all_device_tokens($deviceType) {
		$ci =&get_instance();
		$tokens = array();
		$users = $ci->common_model->getAllRecordsById(USER, array('user_type_id' => APP_USER_TYPE, 'is_user_deleted' => 0, 'status' => 1, 'device_type' => $deviceType));
		if(!empty($users)) {
			foreach($users as $val) {
				if(!empty($val['device_token'])) {
					$tokens[] = $val['device_token'];
				}
			}
		}
		return $tokens;
	}
}

/**
 * Plan Durations
 */
if(!function_exists('plan_durations')) {
	function plan_durations() {
		return array(
			'30' => 'Monthly',
			'90' => 'Quarterly',
			'180' => 'Half Yearly',
			'365' => 'Yearly' 
		);
	}
}

/**
 * Get automizr stoploss
 */
if(!function_exists('get_automization_stoploss')) {
	function get_automization_stoploss($amount, $marketType, $marketCode, $marketExchange){
		$ci =&get_instance();
		if(in_array($marketType, array(4, 5, 6, 8))) {
			$place = 2;
			$round = 5;
			$calAmount = number_format($amount, $place);
			$calAmountArr = explode('.', $calAmount);
			if($calAmountArr[1] > '00') {
				$amount = ceil($calAmountArr[1]/$round) * $round;
				$amount = $calAmountArr[0].'.'.$amount;
			} else {
				$amount = round($calAmountArr[0]);
			}
		} elseif(in_array($marketType, array(3, 7))) {
			$place = 4;
			$round = 25;
			$calAmount = number_format($amount, $place);
			$calAmountArr = explode('.', $calAmount);
			if($calAmountArr[1] > '0000') {
				$amount = ceil($calAmountArr[1]/$round) * $round;
				$amount = $calAmountArr[0].'.'.$amount;
			} else {
				$amount = round($calAmountArr[0]);
			}
		} elseif(in_array($marketType, array(2))) {
			$instrumentData = $ci->common_model->getSingleRecordById(INSTRUMENT, array('exchange' => $marketCode, 'tradingsymbol' => $marketExchange));
			if(!empty($instrumentData)){
				$place = 2;
				$round = $instrumentData['tick_size'];
				if($round == 100) {
					$amount = ceil($amount);
				} else {
					$calAmount = number_format($amount, $place);
					$calAmountArr = explode('.', $calAmount);
					if($calAmountArr[1] > '00') {
						$amount = ceil($calAmountArr[1]/$round) * $round;
						$amount = $calAmountArr[0].'.'.$amount;
					} else {
						$amount = round($calAmountArr[0]);
					}
				}
			} else {
				$amount = ceil($amount);
			}
		}
		return $amount;
	}
}

/**
 * Get tip shared status
 * @param $userId, $tipId
 */
if(!function_exists('get_tip_share_status')) {
	function get_tip_share_status($userId, $tipData){
		$ci =&get_instance();
		$checkShare = $ci->common_model->getRecordCount(USER_SHARE_TIP, array('user_id' => $userId, 'tip_id' => $tipData['tip_id']));
		if($tipData['is_shared'] == 1) {
			if($checkShare > 0) {
				$isShared = 0;
			} else {
				$isShared = 1;
			}
		} else {
			$isShared = 0;
		}
		
		return $isShared;
	}
}

/**
 * Get paid user expiration date
 * @param $userId
 */
if(!function_exists('get_paid_user_membership_expiration_date')) {
	function get_paid_user_membership_expiration_date($userId){
		$ci =&get_instance();
		$data = $ci->common_model->getCustomSqlRow('SELECT MAX(`subscription_end_date`) AS subscription_end_date FROM '.USER_SUBSCRIPTION_HISTORY.' WHERE `user_id` = '.$userId.' AND status = 1');
		if(isset($data['subscription_end_date']) && $data['subscription_end_date'] != null) {
			return $data['subscription_end_date'];
		}
	}
}

/**
 * Get Tip LTP
 * @param $exchangeCode, $instrumentCode
 */
if(!function_exists('get_tip_ltp')) {
	function get_tip_ltp($exchangeCode, $instrumentCode) {
		$ltp = 0;
        $accessToken = get_option('kite_access_token');
        if(!empty($accessToken)) {
            $url = KITE_API_URL."instruments/".$exchangeCode."/".$instrumentCode."?api_key=".ZERODHA_API_KEY."&access_token=".$accessToken;
            $response = file_get_contents($url);
            $response = json_decode($response);
            if($response->status == 'success') {
                $ltp =  $response->data->last_price;
            }
        }
        return $ltp;
    }
}

/**
 * Get tip Targets
 * @param $tipId
 */
if(!function_exists('get_tip_targets')) {
	function get_tip_targets($tipId) {
		$ci =&get_instance();
		$targets = $ci->common_model->getAllRecordsById(TIP_TARGET, array('tt_tip_id' => $tipId));
		return $targets;
	}
}
/* End of file common_helper.php */
/* Location: ./system/application/helpers/common_helper.php */