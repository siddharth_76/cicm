<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* User Helper
* Author: Siddharth Pandey
* Author Email: codebuckets@gmail.com
* Description: This file is auto loaded in this application and we can use all these functions globally in the application.
* version: 1.0
*/

/**
* Admin content Pages Header
* @param Array $data
* @return HTML of content header
*/
if(!function_exists('admin_content_header')) {
	function admin_content_header($title = '', $text = '', $class = '') {
		$html = '<section class="content-header '.$class.'">
	    	<h1>'.ucfirst($title).'
	    		<small>'.ucfirst($text).'</small>
	    	</h1>
	    	'.admin_breadcrumb().'
	  	</section>';
		echo $html;
	}
}

/**
* Admin Breadcrumb
* @param Current Router Class & Router Method
* @return HTML of breadcrumb
*/
if(!function_exists('admin_breadcrumb')) {
	function admin_breadcrumb() {
		/* CI instance */
		$ci =&get_instance();

		/* Router Class and Methods */
		$currentClass = $ci->router->fetch_class();
		$currentMethod = $ci->router->fetch_method();

		$html = '';
		if($currentClass == 'admin') {
			$html .= '<ol class="breadcrumb">
		      <li><a href="'.get_cms_url('admin/dashboard').'" title="Home"><i class="fa fa-dashboard"></i> Home</a></li>
		      <li class="active">'.ucfirst($currentMethod).'</li>
		    </ol>';
		} else {
			$html .= '<ol class="breadcrumb">
		      <li><a href="'.get_cms_url('admin/dashboard').'" title="Home"><i class="fa fa-dashboard"></i> Home</a></li>
		      <li class="active"><a href="'.get_cms_url('admin/'.$currentClass).'" title="'.ucfirst($currentClass).'">'.ucfirst($currentClass).'</a></li>
		      <li class="active">'.ucfirst($currentMethod).'</li>
		    </ol>';
		}
		return $html;
	}
}

/**
* Check login for admin
* @param Optional url $return_uri
*/
if(!function_exists('is_logged_in')) {
	function is_logged_in($return_uri = '', $isExternal = false) {
	    $ci =&get_instance();
		$admin_login = $ci->session->userdata('admin_session_data');
		if(!isset($admin_login['is_logged_in']) || $admin_login['is_logged_in'] != true) {
			if($return_uri) {
				if($isExternal) {
					redirect('admin/login?return_uri='.urlencode($return_uri));
				} else {
					redirect('admin/login?return_uri='.urlencode(base_url().$return_uri));
				}	
			} else {
				redirect("admin/login");	
			}		
		}		
	}
}

/**
* Get admin session information
* @param NULL
* @return Array of admin_session_data from ci_userdata
*/
if(!function_exists('admin_session_data')) {
	function admin_session_data() {
		$ci =&get_instance();
		$session_data = $ci->session->userdata('admin_session_data');
		return $session_data;
	}
}

/**
* Get user info
* @param user_id
* @return Array of user and user_meta information
*/
if(!function_exists('get_user')) {
	function get_user($userId) {
		if($userId) {
			$ci =& get_instance();
			$userData = $ci->common_model->getSingleRecordById(USER, array('id' => $userId));

			if(!empty($userData['country'])) {
				$userData['country_name'] = get_country($userData['country']);
			}

			if(!empty($userData['state'])) {
				$userData['state_name'] = get_state($userData['state']);
			}

			if(!empty($userData['city'])) {
				$userData['city_name'] = get_city($userData['city']);
			}

			if(!empty($userData)) {
				$userData['profile_pic'] = (!empty($userData['profile_pic'])) ? base_url().USER_PROFILE_PIC_PATH.$userData['profile_pic'] : '';
				unset($userData['password'], $userData['is_user_deleted']);
			}

			/* Getting user active features list */
			$userData['active_features'] = get_user_active_features($userId);

			return $userData;
		}
	}
}

/**
* Get meta_value of users_meta based on meta_key
* @param $key = meta_key
* @return meta_value
*/
if(!function_exists('get_user_meta')) {
	function get_user_meta($userId, $key) {
		$ci =&get_instance();
		$keyArr = array('user_id' => $userId, 'meta_key' => $key);
	    $data = $ci->common_model->getSingleRecordById(USER_META, $keyArr);
	    if(!empty($data)) {
			return $data['meta_value'];
		}
	}
}

/**
* Add or update meta_value based on meta_key from users_meta entity
* @param $userId, $key = meta_key
* @return meta_value
*/
if(!function_exists('update_user_meta')) {
	function update_user_meta($userId, $key, $val) {
		$ci =&get_instance();
		$keyArr = array('user_id' => $userId, 'meta_key' => $key);
	    $settings = $ci->common_model->getSingleRecordById(USER_META, $keyArr);
	    if(!empty($settings)) {
			/* Update key value */
			$data = array('meta_value' => $val);
			$where = array('user_id' => $userId, 'meta_key' => $key);
			$ci->common_model->updateRecords(USER_META, $data, $where);
		} else {
			/* Add key value */
			$postData = array('user_id' => $userId, 'meta_key' => $key, 'meta_value' => $val);
			$ci->common_model->addRecords(USER_META, $postData);
		}
	}
}

/**
* Get admin name
* @param $adminId
* @return first name, last name
*/
if(!function_exists('get_admin_name')) {
	function get_admin_name($adminId) {
		$ci =& get_instance();
		$adminData = $ci->common_model->getSinglerecordById(USER, array('id' => $adminId));

      	if(!empty($adminData['name'])) {
        	$name = ucfirst($adminData['name']);
      	} else {
        	$name = ADMIN_FIRST_NAME.' '.ADMIN_LAST_NAME;
      	}
      	$adminName = $name;
      	return $adminName;
	}
}

/**
* Get Admin Avatar
* @param $userId
*/
if(!function_exists('get_admin_avatar')) {
	function get_admin_avatar($userId) {
		$ci =& get_instance();
		$adminData = $ci->common_model->getSinglerecordById(USER, array('id' => $userId));
		if(!empty($adminData['profile_pic'])) {
			$avatar = base_url().USER_PROFILE_PIC_PATH.$adminData['profile_pic'];
		} else {
			$avatar = admin_assets().ADMIN_AVATAR_IMAGE;
		}
		echo $avatar;
	}
}

/**
* Users meta field
* @return $metaArr
*/
if(!function_exists('users_meta_field')) {
	function users_meta_field() {
		$metaArr = array('first_name', 'last_name', 'title', 'display_name', 'website', 'home_phone', 'work_phone', 'address', 'address_line2', 'city', 'state', 'zip', 'cell_phone', 'cell_company', 'bio_text', 'bio_interest', 'core_lead_notes', 'hide_contact_info', 'send_reminder_email', 'send_reminder_text', 'send_remainer_at', 'profile_picture', 'bio_pic1', 'bio_pic2', 'bio_pic3');
		return $metaArr;
	}
}

/**
 * Allowed user types
 * 3 = DISTRIBUTOR
 * 4 = FOS
 * 5 = RETAILER
 * @return $types
 */
if(!function_exists('allowed_types')) {
	function allowed_types(){
		return array(STAFF_USER_TYPE, APP_USER_TYPE);
	}
}


/**
 * User type alias
 * @return $alias
 */
if(!function_exists('user_type_alias')) {
	function user_type_alias() {
		return array(
			SUPER_ADMIN_USER_TYPE => 'SU',
			SUB_ADMIN_USER_TYPE => 'SA',
			STAFF_USER_TYPE => 'ST',
			APP_USER_TYPE => 'AU'
		);
	}
}

/**
 * User login
 * @param $data
 * @return $resp
 */
if(!function_exists('user_login')) {
	function user_login($data){
		$ci =& get_instance();
		$rawSql = 'SELECT * FROM `'.USER.'` WHERE `user_type_id` = '.$data['user_type_id'].' AND (`email` = "'.$data['login'].'" OR `phone_number` = "'.$data['login'].'") AND `password` = "'.md5($data['password']).'" AND is_user_deleted = 0';
		$resp = $ci->common_model->getCustomSqlRow($rawSql);
			
		if(!empty($resp['country'])) {
			$resp['country_name'] = get_country($resp['country']);
		}

		if(!empty($resp['state'])) {
			$resp['state_name'] = get_state($resp['state']);
		}

		if(!empty($resp['city'])) {
			$resp['city_name'] = get_city($resp['city']);
		}

		if(!empty($resp)) {
			$resp['profile_pic'] = (!empty($resp['profile_pic'])) ? base_url().USER_PROFILE_PIC_PATH.$resp['profile_pic'] : '';
		}

		/* Getting user active features list */
		$resp['active_features'] = get_user_active_features($resp['id']);

		return $resp;
	}
}

/**
 * Generate user unique id
 * @param $data
 * @return $uniqueId
 */
if(!function_exists('generate_user_unique_id')) {
	function generate_user_unique_id($typeID, $userId) {
		$alias = user_type_alias();
		$uniqueId = $alias[$typeID].$typeID.$userId;
		return $uniqueId;
	}
}

/**
 * Update user login
 * @param $user
 */
if(!function_exists('update_user_login')) {
	function update_user_login($user) {
		$ci =& get_instance();
		$updateData = array(
			'last_login_ip' => $_SERVER['REMOTE_ADDR'],
			'last_login_date' => date('Y-m-d H:i:s')
		);
		$ci->common_model->updateRecords(USER, $updateData, array('id' => $user['id']));
	}	
}

/**
 * Get user type name
 */
if(!function_exists('get_user_type_name')) {
	function get_user_type_name($typeId) {
		$type = array(
			'1' => 'Super Admin',
			'2' => 'Sub Admin',
			'3' => 'Staff User',
			'4' => 'App User'
		);
		return $type[$typeId];
	}
}

/**
 * Get user type alias
 */
if(!function_exists('get_user_type_alias')) {
	function get_user_type_alias($typeId) {
		$type = array(
			SUPER_ADMIN_USER_TYPE => 'SU',
			SUB_ADMIN_USER_TYPE => 'SA',
			STAFF_USER_TYPE => 'ST',
			APP_USER_TYPE => 'AU'
		);
		return $type[$typeId];
	}
}

/**
 * Get user type name
 */
if(!function_exists('get_admin_user_types_id')) {
	function get_admin_user_types_id() {
		return array(SUPER_ADMIN_USER_TYPE, SUB_ADMIN_USER_TYPE);
	}
}

/**
 * Internal user permissions
 */
if(!function_exists('internal_user_permissions')){
	function internal_user_permissions() {
		return array(
			'can_see_users',
			'can_create_users',
			'can_edit_user',
			'can_extend_user_membership',
			'can_change_user_active_features',
			'can_change_user_membership',
			'can_see_user_history',
			'can_assign_user',
			'can_see_tips',
			'can_post_tips',
			'can_edit_tips',
			'can_see_follow_up',
			'can_edit_follow_up',
			'can_post_follow_up',
			'can_see_news',
			'can_post_news',
			'can_edit_news',
			'can_upload_intraday_scanner_data',
			'can_see_traders_clinic_enquiry',
			'can_see_traders_clinic_conversations',
			'can_reply_on_traders_clinic',
			'can_see_membership_palns',
			'can_add_membership_palns',
			'can_edit_membership_palns',
			'can_see_users_feedback',
			'can_access_feedback_forms',
			'can_access_notifications',
			'can_reset_user_device',
			'can_send_push_notifications',
			'can_change_settings',
			'can_access_payment',
			'can_see_user_subscription_history',
			'can_see_user_membership_extension',
			'can_see_user_shared_tips',
			'can_see_user_payment_history',
			'can_send_sms_to_user',
			'can_see_user_traders_clinic_enquiry',
			'can_reply_on_user_traders_clinic_enquiry',
			'can_see_user_feedbacks',
			'can_log_user_followup_info'
		);
	}
}

/**
 * Check for backend user access
 * @param $feature
 */
if(!function_exists('is_have_access')){
	function is_have_access($feature){
		$ci =& get_instance();
		$permArr = array();
		$session = $ci->session->userdata('admin_session_data');
		if(isset($session['is_logged_in']) && $session['is_logged_in'] == 1) {
			$userId = $session['user_id'];
			if($session['user_type'] != SUPER_ADMIN_USER_TYPE) {
				$permissions = $ci->common_model->getAllRecordsById(USER_PERMISSION, array('user_id' => $userId));
				if(!empty($permissions)) {
					foreach($permissions as $val) {
						$permArr[$val['permission_label']] = $val['permission_value'];
					}

					if($permArr[$feature] != 1) {
						echo "<div class='alert alert-danger' style='background: #dd4b39; color: #fff; padding: 12px; border-radius: 8px; text-align: center; font-family: Arial; width: 80%; margin: 0 auto;'><p>You don't have permission to access this page, for more information please contact with your system administrator.<br/><a href='".get_cms_url('admin/dashboard')."' style='color: #fff;'>Click here</a> to go back.</p></div>";
						exit();
					}
				} else {
					$ci->session->unset_userdata('admin_session_data');
					$ci->session->set_flashdata('logged_out', LOGGED_OUT);
					redirect('admin/login?logged_out=true');
				}
			}
		} else {
			redirect("admin/login");
		}
	}
}

/**
 * Get user permissions
 * @param $userId
 */
if(!function_exists('get_user_permissions')){
	function get_user_permissions($userId){
		$ci =& get_instance();
		$session = $ci->session->userdata('admin_session_data');
		$permArr = array();
		if($session['user_type'] == SUPER_ADMIN_USER_TYPE) {
			foreach(internal_user_permissions() as $val) {
				$permArr[$val] = 1;
			}
		} else {
			$permissions = $ci->common_model->getAllRecordsById(USER_PERMISSION, array('user_id' => $userId));
			if(!empty($permissions)) {
				foreach($permissions as $val) {
					$permArr[$val['permission_label']] = $val['permission_value'];
				}
			}
		}
		return $permArr;
	}
}

/**
 * Application Features
 */
if(!function_exists('application_features')) {
	function application_features() {
		return array(
			array(
				'label' => 'Tips',
				'cond' => 'can_access_tips',
				'value' => 0
			),
			array(
				'label' => 'Economy Barometer',
				'cond' => 'can_access_economic_barometer',
				'value' => 1
			),
			array(
				'label' => 'Economic  Calender',
				'cond' => 'can_access_economic_calendar',
				'value' => 1
			),
			array(
				'label' => 'Earning Calendar',
				'cond' => 'can_access_earning_calendar',
				'value' => 1
			),
			array(
				'label' => 'New & Reports',
				'cond' => 'can_access_news_reports',
				'value' => 1
			),
			array(
				'label' => 'Intraday Scanner',
				'cond' => 'can_access_intraday_scanner',
				'value' => 0
			),
			array(
				'label' => 'Traders Clinic',
				'cond' => 'can_access_traders_clinic',
				'value' => 0
			),
			array(
				'label' => 'Feedback',
				'cond' => 'can_access_feedback',
				'value' => 1
			),
			array(
				'label' => 'Invite Friends',
				'cond' => 'can_access_invite_friends',
				'value' => 1
			),
			array(
				'label' => 'Subscription',
				'cond' => 'can_access_subscription',
				'value' => 1
			),
			array(
				'label' => 'About Us',
				'cond' => 'can_access_about_us',
				'value' => 1
			),
			array(
				'label' => 'Contact Us',
				'cond' => 'can_access_contact_us',
				'value' => 1
			)
		);
	}	
}

/**
* Get user active features
* @param user_id
* @return Array of user active features
*/
if(!function_exists('get_user_active_features')) {
	function get_user_active_features($userId) {
		$features = '';
		if($userId) {
			$ci =& get_instance();
			$activeFeatures = $ci->common_model->getAllRecordsById(USER_ACTIVE_FEATURES, array('user_id' => $userId));

			foreach($activeFeatures as $val) {
				$features[$val['feature_prefix']] = $val['is_active'];
			}
		}
		return $features;
	}
}

/**
 * Filter tips user
 * $stockMarketType
 */
if(!function_exists('filter_tips_user')) {
	function filter_tips_user($stockMarketType){
		$ci =& get_instance();
		/* Get users who have access of tips feature */
		$tipsUser = $ci->common_model->getCustomSqlResult('SELECT user_id, membership_type, membership_status, device_token FROM `'.USER_ACTIVE_FEATURES.'` LEFT JOIN `'.USER.'` ON '.USER_ACTIVE_FEATURES.'.user_id = '.USER.'.id WHERE feature_prefix = "can_access_tips" AND is_active = 1');
		$data = array();
		if(!empty($tipsUser)) {
			foreach($tipsUser as $val) {
				if($val['membership_type'] == FREE_MEMBER && $val['membership_status'] == 1) {
					if(!empty($val['device_token']) && !in_array($val['device_token'], $data)) {
						$data[] = $val['device_token'];
					}
				} elseif($val['membership_type'] == PREMIUM_MEMBER && $val['membership_status'] == 1) {
					$getSubscribedPlans = $ci->common_model->getAllRecordsById(USER_SUBSCRIPTION_HISTORY, array('user_id' => $val['user_id'], 'status' => 1));
					if(!empty($getSubscribedPlans)) {
						foreach($getSubscribedPlans as $sval) {
							$isActive = $ci->common_model->getSinglerecordById(MEMBERSHIP_PLAN_TIP_BENEFIT, array('benefit_plan_id' => $sval['subscribed_plan_id'], 'stock_market_type' => $stockMarketType));
							if(!empty($isActive) && !in_array($val['device_token'], $data)) {
								if(!empty($val['device_token'])) {
									$data[] = $val['device_token'];
								}
							}
						}
					}
				}
			}
		}
		return $data;
	}
}

/**
 * Get paid members phone number by tip segment
 * $stockMarketType
 */
if(!function_exists('get_paid_user_number_by_segment')) {
	function get_paid_user_number_by_segment($stockMarketType){
		$ci =& get_instance();
		/* Get users who have access of tips feature */
		$tipsUser = $ci->common_model->getCustomSqlResult('SELECT user_id, membership_type, membership_status, phone_number FROM `'.USER_ACTIVE_FEATURES.'` LEFT JOIN `'.USER.'` ON '.USER_ACTIVE_FEATURES.'.user_id = '.USER.'.id WHERE feature_prefix = "can_access_tips" AND is_active = 1');
		$data = array();
		if(!empty($tipsUser)) {
			foreach($tipsUser as $val) {
				if($val['membership_type'] == PREMIUM_MEMBER && $val['membership_status'] == 1) {
					$getSubscribedPlans = $ci->common_model->getAllRecordsById(USER_SUBSCRIPTION_HISTORY, array('user_id' => $val['user_id'], 'status' => 1));
					if(!empty($getSubscribedPlans)) {
						foreach($getSubscribedPlans as $sval) {
							$isActive = $ci->common_model->getSinglerecordById(MEMBERSHIP_PLAN_TIP_BENEFIT, array('benefit_plan_id' => $sval['subscribed_plan_id'], 'stock_market_type' => $stockMarketType));
							if(!empty($isActive)) {
								if(!empty($val['phone_number']) && !in_array($val['phone_number'], $data)) {
									$data[] = $val['phone_number'];
								}
							}
						}
					}
				}
			}
		}
		return $data;
	}
}

/**
 * Get user subscribed tip segments
 * $userId
 */
if(!function_exists('get_user_subscribed_tip_segments')) {
	function get_user_subscribed_tip_segments($userId){
		$ci =& get_instance();
		$userData = get_user($userId);
		$segments = '';
		if($userData['membership_type'] == FREE_MEMBER && $userData['membership_status'] == 1) {
			$segments = '-1';
		} elseif($userData['membership_type'] == PREMIUM_MEMBER && $userData['membership_status'] == 1) {
			$getSubscribedPlans = $ci->common_model->getAllRecordsById(USER_SUBSCRIPTION_HISTORY, array('user_id' => $userId, 'status' => 1));
			if(!empty($getSubscribedPlans)) {
				foreach($getSubscribedPlans as $sval) {
					$tipBenefits = $ci->common_model->getAllRecordsById(MEMBERSHIP_PLAN_TIP_BENEFIT, array('benefit_plan_id' => $sval['subscribed_plan_id']));
					if(!empty($tipBenefits)) {
						foreach($tipBenefits as $val) {
							$segments[] = $val['stock_market_type'];
						}
					}
				}
			}
		}
		return (is_array($segments)) ? array_unique($segments) : $segments;
	}
}

/**
 * Get user subscribed intraday scanner segments
 * $userId
 */
if(!function_exists('get_user_subscribed_intraday_scanner_segments')) {
	function get_user_subscribed_intraday_scanner_segments($userId){
		$ci =& get_instance();
		$userData = get_user($userId);
		$segments = '';
		if($userData['membership_type'] == FREE_MEMBER && $userData['membership_status'] == 1) {
			$segments = '-1';
		} elseif($userData['membership_type'] == PREMIUM_MEMBER && $userData['membership_status'] == 1) {
			$getSubscribedPlans = $ci->common_model->getAllRecordsById(USER_SUBSCRIPTION_HISTORY, array('user_id' => $userId, 'status' => 1));
			if(!empty($getSubscribedPlans)) {
				foreach($getSubscribedPlans as $sval) {
					$tipBenefits = $ci->common_model->getAllRecordsById(MEMBERSHIP_PLAN_INTRADAY_BENEFIT, array('benefit_plan_id' => $sval['subscribed_plan_id']));
					if(!empty($tipBenefits)) {
						foreach($tipBenefits as $val) {
							$segments[] = $val['intrday_scanner_cat'];
						}
					}
				}
			}
		}
		return (is_array($segments)) ? array_unique($segments) : $segments;
	}
}
/* End of file user_helper.php */
/* Location: ./system/application/helpers/user_helper.php */