<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Admin Helper
 * Author: Siddharth Pandey
 * Author Email: codebuckets@gmail.com
 * Description: This file is auto loaded in this application and we can use all these functions globally in  the application.
 * version: 1.0
 */


/* End of file admin_helper.php */
/* Location: ./system/application/helpers/admin_helper.php */