<?php
  if(!empty($notifications)) {
    foreach($notifications as $val) {
?>
<li>
    <div class="NotiInner">
      <a href="<?php cms_url('admin/notifications/view/'.$val['id']); ?>" title="View Notification">
        <span class="textMessage">
          <i class="<?php echo notification_icon($val['type']); ?>"></i>
          <?php echo $val['text']; ?>
        </span> 
        <span class="notiDate"><?php echo date('d/m/Y, h:i A', strtotime($val['created_at'])); ?></span>
      </a>  
    </div>
</li>
<?php } } ?>