		<footer class="main-footer">
			<strong>
				Copyright &copy; <?php echo date('Y'); ?> 
				<a href="<?php echo base_url(); ?>" target="_blank">
					<?php echo get_option('app_name'); ?>
				</a>.
			</strong> All rights reserved.
		</footer>
	</div><!-- ./wrapper -->

    <!-- Loader Image -->
    <div class="bodyWrapper">
        <img src="<?php admin_assets(); ?>dist/img/ajax-loader.gif" alt="Loading..." class="ajaxLoaderMain"/>
    </div><!-- .bodyWrapper -->

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <?php if(isset($_GET['run_auto']) && $_GET['run_auto'] == 1) { ?>
        <script type="text/javascript">
            /*$(document).ready(function(){
                setInterval(function () {
                    $.ajax({
                        type: 'GET',
                        url: url+'api/cron/tipautomization',
                        data: '',
                        success:function(response) {
                            console.log(response);
                        }
                    });
                }, 5000);
            });*/
        </script>
    <?php } ?>

    <?php if($permissions['can_access_notifications'] == 1) { ?>
        <script type="text/javascript">
            $(document).ready(function(){
                /* Get unread notifications count */
                setInterval(function () {
                    $.ajax({
                        type: 'POST',
                        url: url+'admin/checkUnSeenNotifications',
                        data: '',
                        success:function(response) {
                            $('.unseenNotifications').text(response);
                        }
                    });
                }, 5000);

                $('a.loadNotifications').click(function(){
                    $(this).toggleClass('loadingNotifications');
                    if($(this).hasClass('loadingNotifications')) {
                        $('.loadedNotifications li.notiloader').fadeIn();
                        $.ajax({
                            type: 'POST',
                            url: url+'admin/loadnotifications',
                            data: '',
                            success:function(response) {
                                setInterval(function () {
                                    $('.loadedNotifications li.notiloader').hide();
                                    $('.loadedNotifications').html(response);
                                }, 2000);
                            }
                        });
                    } 

                    if(!$(this).hasClass('loadingNotifications')) {
                        $('.loadedNotifications li.notiloader').hide();
                        $('.loadedNotifications').html('');
                    }
                });
            });
        </script>
    <?php } ?>

    <script>
        $.widget.bridge('uibutton', $.ui.button);
        $(function () {
            $(".colorpicker").colorpicker();
        });

        $(document).ready(function(){
            $('.bodyWrapper').height($(window).height());
            
            $('select.addNewUserTypeId').on('change', function() {
                var subAdmin = '<?php echo SUB_ADMIN_USER_TYPE; ?>';
                var staffUser = '<?php echo STAFF_USER_TYPE; ?>';
                var appUser = '<?php echo APP_USER_TYPE; ?>';

                if($(this).val() == subAdmin || $(this).val() == staffUser) {
                    $('.validForAppUser').hide();
                    $('.appUserPermissions').hide();
                    $('.subAdminPermissions').fadeIn();
                    $('.usersPassword input[type="password"]').removeAttr('disabled');
                    $('.validForAppUser input[name="free_membership_days"]').attr('disabled', 'disabled');
                    $('.usersPassword').fadeIn();
                } else if($(this).val() == appUser) {
                    $('.subAdminPermissions').hide();
                    $('.usersPassword input[type="password"]').attr('disabled', 'disabled');
                    $('.validForAppUser input[name="free_membership_days"]').removeAttr('disabled');
                    $('.usersPassword').hide();
                    $('.validForAppUser').fadeIn();
                    $('.appUserPermissions').fadeIn();
                } else {
                    $('.subAdminPermissions').hide();
                    $('.usersPassword input[type="password"]').attr('disabled', 'disabled');
                    $('.validForAppUser input[name="free_membership_days"]').attr('disabled', 'disabled');
                    $('.appUserPermissions').hide();
                    $('.validForAppUser').hide();
                    $('.usersPassword').hide();
                }
            });
        }); 
    </script>
    <script src="<?php admin_assets(); ?>js/chosen.jquery.js"></script>
    <script src="<?php admin_assets(); ?>js/dropzone.js"></script>
    <!-- Nestable js -->
    <script src="<?php admin_assets(); ?>js/jquery.nestable.js"></script>
    <!-- Custom js -->
    <script src="<?php admin_assets(); ?>js/custom.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php admin_assets(); ?>bootstrap/js/bootstrap.min.js"></script>
    <!-- Bootstrap Sortable -->
    <script src="<?php admin_assets(); ?>bootstrap/js/bootstrap-sortable.js"></script>
    <!-- Select2 -->
    <script src="<?php admin_assets(); ?>plugins/select2/select2.full.min.js"></script>
    <!-- Morris.js charts -->
    <script src="<?php admin_assets(); ?>js/raphael-min.js"></script>
    <script src="<?php admin_assets(); ?>plugins/morris/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="<?php admin_assets(); ?>plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="<?php admin_assets(); ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?php admin_assets(); ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?php admin_assets(); ?>plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="<?php admin_assets(); ?>js/moment.min.js"></script>
    <script src="<?php admin_assets(); ?>plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="<?php admin_assets(); ?>plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?php admin_assets(); ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php admin_assets(); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php admin_assets(); ?>plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php admin_assets(); ?>dist/js/app.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="<?php admin_assets(); ?>dist/js/pages/dashboard.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="<?php admin_assets(); ?>plugins/chartjs/Chart.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="<?php admin_assets(); ?>dist/js/pages/dashboard2.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php admin_assets(); ?>dist/js/demo.js"></script>
    <!-- bootstrap color picker -->
    <script src="<?php admin_assets(); ?>plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="<?php admin_assets(); ?>plugins/timepicker/bootstrap-timepicker.min.js"></script>
     <!-- InputMask -->
    <script src="<?php admin_assets(); ?>plugins/input-mask/jquery.inputmask.js"></script>
    <script src="<?php admin_assets(); ?>plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="<?php admin_assets(); ?>plugins/input-mask/jquery.inputmask.extensions.js"></script>
  </body>
</html>