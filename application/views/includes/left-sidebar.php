<?php $adminData = admin_session_data(); ?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php get_admin_avatar($adminData['user_id']); ?>" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo get_admin_name($adminData['user_id']); ?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div><!-- .user-panel -->

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
      <!-- Dashboard -->
      <li class="<?php add_active_class('admin', 'dashboard'); ?>">
        <a href="<?php cms_url('admin/dashboard'); ?>" title="Dashboard">
          <i class="fa fa-dashboard"></i> 
          <span>Dashboard</span>
        </a>
      </li>
      <!-- tips -->
      <?php if(!empty($permissions) && $permissions['can_see_tips'] == 1 && ($permissions['can_post_tips'] == 1 || $permissions['can_edit_tips'] == 1 || $permissions['can_see_follow_up'] == 1 || $permissions['can_edit_follow_up'] == 1 || $permissions['can_post_follow_up'] == 1)) { ?>
      <li class="treeview <?php tree_active_class('tips', array('addNew', 'edit','viewAll', 'marketTypes', 'addNewMarketType', 'editMarketType', 'followUps', 'addNewFollowUp', 'editFollowUp')); ?>">
        <a href="javascript:void(0);" title="Manage Tips">
          <i class="fa fa-lightbulb-o"></i> 
          <span>Manage Tips</span> 
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <?php if($permissions['can_post_tips'] == 1) { ?>
            <li class="<?php add_active_class('tips', 'addNew'); ?>">
              <a href="<?php cms_url('admin/tips/add-new'); ?>" title="Add New Tip">
                <i class="fa fa-plus"></i> Add New Tip
              </a>
            </li>
          <?php } ?>
          <?php if($permissions['can_see_tips'] == 1) { ?>
            <li class="<?php add_active_class('tips', 'viewAll'); ?>">
              <a href="<?php cms_url('admin/tips/view-all'); ?>" title="View All Tips">
                <i class="fa fa-eye"></i> View All Tips
              </a>
            </li>
            <li class="<?php tree_active_class('tips', array('marketTypes', 'addNewMarketType', 'editMarketType')); ?>">
              <a href="<?php cms_url('admin/tips/market-types'); ?>" title="View All Market Types">
                <i class="fa fa-exchange"></i> Market Types
              </a>
            </li>
          <?php } ?>
        </ul>
      </li>
      <?php } ?>
      <!-- news -->
      <?php if(!empty($permissions) && $permissions['can_see_news'] == 1 && ($permissions['can_post_news'] == 1 || $permissions['can_edit_news'] == 1)) { ?>
      <li class="treeview <?php tree_active_class('news', array('addNew', 'edit','viewAll')); ?>">
        <a href="javascript:void(0);" title="Manage Tips">
          <i class="fa fa-newspaper-o"></i> 
          <span>Manage News</span> 
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <?php if($permissions['can_post_news'] == 1) { ?>
            <li class="<?php add_active_class('news', 'addNew'); ?>">
              <a href="<?php cms_url('admin/news/add-new'); ?>" title="Add New News">
                <i class="fa fa-plus"></i> Add New News
              </a>
            </li>
          <?php } ?>
          <?php if($permissions['can_see_news'] == 1) { ?>
            <li class="<?php add_active_class('news', 'viewAll'); ?>">
              <a href="<?php cms_url('admin/news/view-all'); ?>" title="View All News">
                <i class="fa fa-eye"></i> View All News
              </a>
            </li>
          <?php } ?>
        </ul>
      </li>
      <?php } ?>
      <!-- users -->
      <?php if(!empty($permissions) && $permissions['can_see_users'] == 1 && ($permissions['can_create_users'] == 1 || $permissions['can_edit_user'] == 1 || $permissions['can_extend_user_membership'] == 1 || $permissions['can_change_user_active_features'] == 1 || $permissions['can_change_user_membership'] == 1 || $permissions['can_see_user_history'] == 1)) { ?>
        <li class="treeview <?php tree_active_class('users', array('addNew', 'edit','viewAll', 'view', 'manageMembership')); ?>">
          <a href="javascript:void(0);" title="Manage Users">
            <i class="fa fa-users"></i> 
            <span>Manage Users</span> 
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <?php if($permissions['can_create_users'] == 1) { ?>
              <li class="<?php add_active_class('users', 'addNew'); ?>">
                <a href="<?php cms_url('admin/users/add-new'); ?>" title="Add New User">
                  <i class="fa fa-plus"></i> Add New User
                </a>
              </li>
            <?php } ?>
            <?php if($permissions['can_see_users'] == 1) { ?>
              <li class="<?php add_active_class('users', 'viewAll'); ?>">
                <a href="<?php cms_url('admin/users/view-all'); ?>" title="View All Users">
                  <i class="fa fa-eye"></i> View All Users
                </a>
              </li>
            <?php } ?>
          </ul>
        </li>
      <?php } ?>
      <!-- Membership Plans -->
      <?php if(!empty($permissions) && $permissions['can_see_membership_palns'] == 1 && ($permissions['can_add_membership_palns'] == 1 || $permissions['can_edit_membership_palns'] == 1)) { ?>
      <li class="treeview <?php tree_active_class('plans', array('addNew', 'edit','viewAll')); ?>">
        <a href="javascript:void(0);" title="Membership Plans">
          <i class="fa fa-bars"></i> 
          <span>Membership Plans</span> 
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <?php if($permissions['can_add_membership_palns'] == 1) { ?>
            <li class="<?php add_active_class('plans', 'addNew'); ?>">
              <a href="<?php cms_url('admin/plans/add-new'); ?>" title="Add New Plan">
                <i class="fa fa-plus"></i> Add New Plan
              </a>
            </li>
          <?php } ?>
          <?php if($permissions['can_see_membership_palns'] == 1) { ?>
            <li class="<?php add_active_class('plans', 'viewAll'); ?>">
              <a href="<?php cms_url('admin/plans/view-all'); ?>" title="View All Plans">
                <i class="fa fa-eye"></i> View All Plans
              </a>
            </li>
          <?php } ?>
        </ul>
      </li>
      <?php } ?>
      <!-- Feedbacks -->
      <?php if(!empty($permissions) && ($permissions['can_see_users_feedback'] == 1 || $permissions['can_access_feedback_forms'] == 1)) { ?>
      <li class="treeview <?php tree_active_class('feedbacks', array('create', 'edit', 'viewAll', 'manage', 'view')); ?>">
        <a href="javascript:void(0);" title="Manage Feedbacks">
          <i class="fa fa-envelope-o"></i> 
          <span>Feedbacks</span> 
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <?php if($permissions['can_access_feedback_forms'] == 1) { ?>
            <li class="<?php add_active_class('feedbacks', 'addNew'); ?>">
              <a href="<?php cms_url('admin/feedbacks/create'); ?>" title="Add New Feedback Form">
                <i class="fa fa-plus"></i> Add New Form
              </a>
            </li>
          <?php } ?>
          <?php if($permissions['can_access_feedback_forms'] == 1) { ?>
            <li class="<?php add_active_class('feedbacks', 'viewAll'); ?>">
              <a href="<?php cms_url('admin/feedbacks/view-all'); ?>" title="View All Feedback Forms">
                <i class="fa fa-eye"></i> View All Forms
              </a>
            </li>
          <?php } ?>
          <?php if($permissions['can_see_users_feedback'] == 1) { ?>
            <li class="<?php add_active_class('feedbacks', 'manage'); ?>">
              <a href="<?php cms_url('admin/feedbacks/manage'); ?>" title="Manage All Feedbacks">
                <i class="fa fa-envelope"></i> Manage Feedbacks
              </a>
            </li>
          <?php } ?>
        </ul>
      </li>
      <?php } ?>
      <!-- Intraday Scanner -->
      <?php if(!empty($permissions) && $permissions['can_upload_intraday_scanner_data'] == 1) { ?>
        <li class="treeview <?php tree_active_class('hawkeye', array('upload', 'viewAll')); ?>">
          <a href="javascript:void(0);" title="Intraday Scanner">
            <i class="fa fa-line-chart"></i> 
            <span>Intraday Scanner</span> 
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li class="<?php add_active_class('hawkeye', 'upload'); ?>">
              <a href="<?php cms_url('admin/hawkeye/upload'); ?>" title="Upload Data">
                <i class="fa fa-plus"></i> Upload Data
              </a>
            </li>
            <li class="<?php add_active_class('hawkeye', 'viewAll'); ?>">
              <a href="<?php cms_url('admin/hawkeye/view-all'); ?>" title="View All Intraday Scanner Data">
                <i class="fa fa-eye"></i> View All Data
              </a>
            </li>
          </ul>
        </li>
      <?php } ?>
      <!-- Traders Clinic -->
      <?php if(!empty($permissions) && $permissions['can_see_traders_clinic_enquiry'] == 1) { ?>
        <li class="<?php tree_active_class('tradersclinic', array('viewAll', 'view')); ?>">
          <a href="<?php cms_url('admin/tradersclinic/view-all'); ?>" title="Traders Clinic">
            <i class="fa fa-ambulance"></i> 
            <span>Traders Clinic</span>
          </a>
        </li>
      <?php } ?>
      <!-- Notification -->
      <?php if(!empty($permissions) && $permissions['can_access_notifications'] == 1) { ?>
        <li class="<?php tree_active_class('notifications', array('manage', 'view')); ?>">
          <a href="<?php cms_url('admin/notifications/manage'); ?>" title="Notifications">
            <i class="fa fa-bell"></i> 
            <span>Notifications</span>
          </a>
        </li>
      <?php } ?>
      <!-- Payments -->
      <?php if(!empty($permissions) && $permissions['can_access_payment'] == 1) { ?>
        <li class="<?php tree_active_class('payments', array('viewAll', 'view')); ?>">
          <a href="<?php cms_url('admin/payments/view-all'); ?>" title="Payments">
            <i class="fa fa-money"></i> 
            <span>Payments</span>
          </a>
        </li>
      <?php } ?>
      <!-- Send Push Notification -->
      <?php if($permissions['can_send_push_notifications'] == 1) { ?>
        <li class="treeview <?php tree_active_class('admin', array('sendPushNotification')); ?>">
          <a href="<?php cms_url('admin/send-push-notification'); ?>" title="Send Push Notification">
            <i class="fa fa-commenting"></i> 
            <span>Send Push Notification</span> 
          </a>
        </li>
      <?php } ?>
      <!-- Setting -->
      <li class="treeview <?php tree_active_class('settings', array('profile', 'general', 'seo', 'app', 'feature')); ?>">
        <a href="javascript:void(0);" title="Settings">
          <i class="fa fa-cog"></i> 
          <span>Settings</span> 
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <?php if(!empty($permissions) && $permissions['can_change_settings'] == 1) { ?>
            <li class="<?php add_active_class('settings', 'general'); ?>">
              <a href="<?php cms_url('admin/settings/general'); ?>" title="General">
                <i class="fa fa-cogs"></i> General
              </a>
            </li>
          <?php } ?>
          <li class="<?php add_active_class('settings', 'profile'); ?>">
            <a href="<?php cms_url('admin/settings/profile'); ?>" title="Profile">
              <i class="fa fa-user"></i> Profile
            </a>
          </li>
          <?php if(!empty($permissions) && $permissions['can_change_settings'] == 1) { ?>
            <li class="<?php add_active_class('settings', 'seo'); ?>">
              <a href="<?php cms_url('admin/settings/seo'); ?>" title="SEO">
                <i class="fa fa-globe"></i> SEO
              </a>
            </li>
            <li class="<?php add_active_class('settings', 'app'); ?>">
              <a href="<?php cms_url('admin/settings/app'); ?>" title="App Setting">
                <i class="fa fa-android"></i> App Setting
              </a>
            </li>
            <li class="<?php add_active_class('settings', 'feature'); ?>">
              <a href="<?php cms_url('admin/settings/feature'); ?>" title="Feature Setting">
                <i class="fa fa-th"></i> Features Setting
              </a>
            </li>
          <?php } ?>
        </ul>
      </li>
      <!-- Sign Out -->
      <li class="<?php add_active_class('logout'); ?>">
        <a href="<?php cms_url('admin/logout'); ?>">
          <i class="fa fa-power-off"></i> 
          <span>Sign Out</span>
        </a>
      </li>
    </ul><!-- .sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>