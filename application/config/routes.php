<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'admin';
$route['404_override'] = 'error/error_404';
$route['translate_uri_dashes'] = FALSE;

/* custom Routung */
$route['admin/forgot-password'] = 'admin/forgotPassword';
$route['admin/change-password'] = 'admin/changePassword';
$route['admin/send-push-notification'] = 'admin/sendPushNotification';
$route['admin/send-push-notification/(:any)'] = 'admin/sendPushNotification/$1';

$route['admin/users/add-new'] = 'admin/users/addNew';

$route['admin/users/view-all'] = 'admin/users/viewAll';
$route['admin/users/view-all/(:any)'] = 'admin/users/viewAll';

$route['admin/users/manage-membership'] = 'admin/users/manageMembership';
$route['admin/users/manage-membership/(:any)'] = 'admin/users/manageMembership';

$route['admin/plans/add-new'] = 'admin/plans/addNew';

$route['admin/plans/view-all'] = 'admin/plans/viewAll';
$route['admin/plans/view-all/(:any)'] = 'admin/plans/viewAll';

$route['admin/tips/add-new'] = 'admin/tips/addNew';

$route['admin/tips/add-new-market-type'] = 'admin/tips/addNewMarketType';

$route['admin/tips/edit-market-type'] = 'admin/tips/editMarketType';
$route['admin/tips/edit-market-type/(:any)'] = 'admin/tips/editMarketType';

$route['admin/tips/add-new-follow-up'] = 'admin/tips/addNewFollowUp';
$route['admin/tips/add-new-follow-up/(:any)'] = 'admin/tips/addNewFollowUp/$1';

$route['admin/tips/edit-follow-up'] = 'admin/tips/editFollowUp';
$route['admin/tips/edit-follow-up/(:any)/(:any)'] = 'admin/tips/editFollowUp/$1/$1';

$route['admin/tips/view-all'] = 'admin/tips/viewAll';
$route['admin/tips/view-all/(:any)'] = 'admin/tips/viewAll';

$route['admin/tips/market-types'] = 'admin/tips/marketTypes';
$route['admin/tips/market-types/(:any)'] = 'admin/tips/marketTypes';

$route['admin/tips/follow-ups'] = 'admin/tips/followUps';
$route['admin/tips/follow-ups/(:any)'] = 'admin/tips/followUps';

$route['admin/news/add-new'] = 'admin/news/addNew';

$route['admin/news/view-all'] = 'admin/news/viewAll';
$route['admin/news/view-all/(:any)'] = 'admin/news/viewAll';

$route['admin/feedbacks/view-all'] = 'admin/feedbacks/viewAll';
$route['admin/feedbacks/view-all/(:any)'] = 'admin/feedbacks/viewAll';

$route['admin/hawkeye/view-all'] = 'admin/hawkeye/viewAll';
$route['admin/hawkeye/view-all/(:any)'] = 'admin/hawkeye/viewAll';

$route['admin/tradersclinic/view-all'] = 'admin/tradersclinic/viewAll';
$route['admin/tradersclinic/view-all/(:any)'] = 'admin/tradersclinic/viewAll';

$route['admin/payments/view-all'] = 'admin/payments/viewAll';
$route['admin/payments/view-all/(:any)'] = 'admin/payments/viewAll';

$route['admin/settings/system-preferences'] = 'admin/settings/systemPreferences';
/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/
//$route['api/example/users/(:num)'] = 'api/example/users/id/$1'; // Example 4
//$route['api/example/users/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/example/users/id/$1/format/$3$4'; // Example 8