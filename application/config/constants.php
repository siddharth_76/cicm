<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/* Error and success messages */
define('ADMIN_LOGIN_HEADING', 'Sign in to access admin panel');
define('ADMIN_CHANGE_PASSWORD_HEADING', 'Enter new password.');
define('ADMIN_FORGOT_PASSWORD_HEADING', 'Enter your email to retrieve new password.');
define('LOGIN_ERROR', 'Invalid user, please check your login details.');
define('LOGGED_OUT', "You've been successfully logged out.");

define('NEW_USER_EMAIL_SUBJECT', 'CICM[Account Created]');
define('NEW_USER_EMAIL_MSG', 'Dear %s,<br/><br/>Welcome to CICM family.<br/><br/>Your account has been created successfully.<br/><br/><b>Account Information:</b><br/><br/>Email: %s<br/>Phone Number: %s<br/><br/><br/>Regards,<br/>CICM');

define('FORGOT_PASSWORD_SUBJECT', 'CICM[Forgot Password]');
define('FORGOT_PASSWORD_MESSAGE', 'Dear %s,<br/><br/>Please enter following otp to change your password, with this password you will be able to login again.<br/><br/><b>OTP: %s</b><br/><br/>If you need any further support, please do not hesitate to contact us at any time.<br/><br/><br/>Regards,<br/>CICM');
define('FORGOT_PASSWORD_SUCCESS', "Please check your mail, we've sent an email to you.");
define('FORGOT_PASSWORD_ERROR', 'This email is not exists in our database.');

define('FORGOT_PASSWORD_ADMIN_MESSAGE', 'Dear %s,<br/><br/>Please click on following link to change your password, with this password you will be able to login again.<br/><br/><b>%s</b><br/><br/>If you need any further support, please do not hesitate to contact us at any time.<br/><br/><br/>Regards,<br/>CICM');

define('INVALID_RESET_KEY', 'Invalid reset key, please check it again.');
define('CHANGE_PASSWORD_SUCCESS', 'Your password has been changed, please login with new password.');
define('NO_MESSAGE_TEXT', "You have no new message.");
define('NO_NOTIFICATION_TEXT', "You have no new notifications.");
define('NO_TASKS_TEXT', "You have no new tasks.");
define('MESSAGE_TEXT', "You have %s message.");
define('NOTIFICATION_TEXT', "You have %s notifications.");
define('TASKS_TEXT', "You have %s tasks.");
define('SEE_ALL_MESSAGES', 'See All Messages');
define('SEE_ALL_NOTIFICATIONS', 'See All Notifications');
define('SEE_ALL_TASKS', 'See All Tasks');
define('EDIT_PROFILE_TEXT', 'Edit Profile');
define('ADMIN_PROFILE_UPDATE_SUCCESS', 'Your profile has been updated successfully.');
define('EMAIL_EXISTS', 'This email is already exists in our system.');
define('PHGONE_NUMBER_EXISTS', 'This phone number is already exists in our system.');
define('GENERAL_SETTING_TEXT', 'General Settings');
define('EMAIL_SETTING_TEXT', 'Email Settings');
define('APP_CONTACT', 'App Contacts');
define('SOCIAL_MEDIA_SETTINGS_TEXT', 'Social Links');
define('LOGO_FAVICON_SETTING_TEXT', 'Logo & Favicon');
define('FOOTER_CONTENT_SETTINGS_TEXT', 'Footer Content');
define('SETTING_SUCCESS', 'Settings has been updated successfully.');
define('LOGO_ERROR', 'Please select valid church logo image.');
define('FAVICON_ERROR', 'Please select valid church favicon.');
define('IMG_ERROR', 'Please select a valid image file.');
define('IMG_SIZE_ERROR_FOR_2', 'Please select image of less than 2MB.');
define('GENERAL_ERROR', 'Some error occured, please try again.');
define('SLIDER_IMAGE_REQ', 'The Slider image field is required.');
define('SLIDER_IMG_ERROR', 'Please select valid slider image.');
define('ALL_DATA', 'All %s');
define('NO_RECORDS_FOUND', 'No %s found.');
define('INVALID_OTP', 'Invalid otp, please try again.');
define('INVALID_ITEM', 'Invalid item, please try again.');
define('ITEM_ADD_SUCCESS', '%s added successfully.');
define('ITEM_ADD_ERROR', '%s not addess successfully, please try again.');
define('ITEM_UPDATE_SUCCESS', '%s updated successfully.');
define('ITEM_DELETE_SUCCESS', '%s deleted successfully.');
define('ITEM_NOT_EXISTS', '%s is not exists in our system.');
define('LOGIN_TO_CONTINUE', 'Password has been changed successfully, please login again with new password to continue.');
define('GROUP_ICON_ERROR', 'Please select valid group icon');
define('GROUP_HEADER_PICTURE_ERROR', 'Please select valid group header picture.');
define('COLLOR_ATTRIBUTES', 'Select Color Attributes');
define('MENU_ORDER_ALREADY_EXISTS', 'This menu order already in use.');
define('LOGIN_AND_SIGNUP_PAGES', 'Login & Signup');
define('ACCOUNT_CREATED_MESSAGE', 'Dear %s, <br/><br/>Your account has been created successfully, please go through the following details.<br/><br/><strong>Login Details:</strong><br/>Email: %s<br/>Password: %s<br/> Url: %s<br/><br/><br/>Regards,<br/>CICM');
define('ACCOUNT_CREATED', 'Account Created');
define('ADMIN_PASSWORD_CHANGE', 'Dear %s,<br/><br/>Your password has been changed by CICM admin, please login through new password.<br/><br/>New Password: %s<br/><br/><br/>Regards,<br/>CICM');

/* Rest API Constant */
define('USER_REGISTERED_SUCCESS', 'User registered successfully.');
define('USER_LOGIN_SUCCESS', 'User loggedin successfully.');
define('EMAIL_VERIFY_TEXT', 'Please verify your email to complete signup process.');
define('PHONE_VERIFY_TEXT', 'Please verify your phone to complete signup process.');
define('INCORRECT_OTP', 'Incorrect otp.');
define('VERIFY_EMAIL_SUCCESS', 'Your email has been successfully verified.');
define('VERIFY_OTP_SUCCESS', 'Your phone number has been successfully verified.');
define('ALREADY_VERIFIED', 'This %s has been already verified.');
define('EMAIL_VERIFICATION_MESSAGE', 'Dear %s %s,<br/><br/>Please click on the following link to verify your email.<br/><br/>%s<br/><br/>If you need any further support, please do not hesitate to contact us at any time.<br/><br/><br/>Regards,<br/>CICM');
define('EMAIL_VERIFICATION_HEADER', 'Verify Email');
define('INVALID_VERIFICATION_CODE', 'Invalid url, please try again.');
define('EMAIL_NOT_VERIFIED', 'Your email is not verified yet, please check your inbox.');
define('PHONE_NOT_VERIFIED', 'Your phone number is not verified yet, please enter otp.');
define('GROUP_CONTACT_US_REQUEST_EMAIL_SUBJECT', 'Group Contact Us Request');
define('GROUP_CONACT_US_REQUEST_EMAIL_MESSAGE', 'Dear %s,<br/><br/>%s has submitted contact us request on %s.<br/><br/>Please go through following link to response this request.<br/><br/><a href="%s">Click Here</a><br/><br/><b>Contact Us Info:</b><br/><br/><b>Question Name:</b> %s<br/><br/><b>Question Text:</b> %s<br/><br/><br/><br/>Regards,<br/>CICM');

/* Basic site settings attributes */
define('CMS_ABR', 'cicm');
define('CMS_NAME', 'CICM');
define('ADMIN_PANEL_HEADING', 'CICM Admin Panel');

define('ADMIN_USER_ID', 1);
define('SUPER_ADMIN_USER_TYPE', 1);
define('SUB_ADMIN_USER_TYPE', 2);
define('STAFF_USER_TYPE', 3);
define('APP_USER_TYPE', 4);

define('FREE_MEMBER', 1);
define('PREMIUM_MEMBER', 2);

define('BUY_TIP', '1');
define('SELL_TIP', '2');
define('NSE_CLOSING_TIME', '15:15');
define('NFO_CLOSING_TIME', '15:15');
define('CDS_CLOSING_TIME', '16:45');
define('MCX_CLOSING_TIME', '23:15');

define('INTRADAY_TIP', '1');
define('SWING_TIP', '2');
define('INTRADAY_SWING_TIP', '3');
define('BTST_TIP', '4');
define('STBT_TIP', '5');

define('ADMIN_FIRST_NAME', 'CICM');
define('ADMIN_LAST_NAME', 'Admin');
define('ADMIN_AVATAR_IMAGE', 'dist/img/malecostume-512.png');
define('RESULT_PER_PAGE', 25);
define('DASHBOARD_LATEST_RECORDS', 25);

/* Admin Views Directory */
define('LOGIN_INCLUDES', 'login_includes/');
define('INCLUDES', 'includes/');

/* Upload paths */
define('USER_PROFILE_PIC_PATH', 'uploads/users/');
define('USER_DEFAULT_PROFILE_PIC', 'assets/admin/dist/img/avatar5.png');
define('NEWS_ATTACHMENT_PATH', 'uploads/news/');
define('INTRADAY_SCANNER_UPLOAD_PATH', 'uploads/intradayScanner/');

/* Database Tables */
define('SYSTEM_PREFERENCE', 'system_preferences');
define('USER', 'users');
define('USER_TYPE', 'user_types');
define('USER_LOGIN', 'users_login');
define('USER_PERMISSION', 'users_permission');
define('USER_DELETED', 'users_deleted');
define('COUNTRY', 'country');
define('CITY', 'city');
define('STATE', 'state');
define('USER_MEMBERSHIP_EXTENSION', 'users_membership_extension');
define('USER_SUBSCRIPTION_HISTORY', 'users_subscription_history');
define('NOTIFICATION', 'notifications');
define('PUSH_NOTIFICATIONS', 'push_notifications');
define('USER_ACTIVE_FEATURES', 'users_active_features');
define('USER_ACTIVE_FEATURES_CHANGE_HISTORY', 'users_active_features_change_history');
define('MEMBERSHIP_PLAN', 'membership_plans');
define('MEMBERSHIP_PLAN_CATEGORY', 'membership_plan_categories');
define('MEMBERSHIP_PLAN_TIP_BENEFIT', 'membership_plan_tips_benefits');
define('MEMBERSHIP_PLAN_INTRADAY_BENEFIT', 'membership_plan_intraday_scanner_benefits');
define('STOCK_MARKET_TYPE', 'stock_market_types');
define('TIP', 'tips');
define('TIP_FOLLOW_UP', 'tips_follow_ups');
define('TIP_TARGET', 'tips_targets');
define('TC_FORM_SUBMISSION', 'tc_form_submissions');
define('TC_FORM_MESSAGE', 'tc_form_messages');
define('FEEDBACK', 'feedbacks');
define('FEEDBACK_FORM_ELEMENT', 'feedback_form_elements');
define('FEEDBACK_FORM_SUBMISSION', 'feedback_form_submissions');
define('USER_FAVORITE_TIP', 'users_favorite_tips');
define('INTRADAY_SCANNER', 'intraday_scanner');
define('INTRADAY_SCANNER_CATEGORY', 'intraday_scanner_categories');
define('PAYMENT', 'payments');
define('NEWS', 'news');
define('SHARE_IMAGE', 'sharing_images');
define('USER_SHARE_TIP', 'user_shared_tips');
define('INSTRUMENT', 'instruments');
define('USER_EARNED_REWARD_POINTS', 'user_earned_reward_points');
define('USER_FOLLOW_UP', 'users_follow_ups');
define('TIP_MESSAGE', 'tips_messages');

/* Rest Api constants */
define('SUCCESS', '100');
define('ERROR', '200');
define('SERVER_ERROR', '300');
define('MISSING_PARAM', '400');
define('SUCCESS_MSG', 'SUCCESS');
define('ERROR_MSG', 'ERROR');
define('SERVER_MSG', 'SERVER_ERROR');

define('MSG91_AUTH_KEY', '73983A3uuIBqY0t58f1f58c');
define('MSG91_SENDER_ID', 'OCICMO');

define('ONESIGNAL_DOMAIN_NAME', 'https://cicm.onesignal.com');
define('ONESIGNAL_APP_ID', 'c1080431-8798-4d72-a6c7-2ef18a839a0f');
define('ONESIGNAL_API_KEY', 'MWJlZDkxMjAtNjU3YS00YWMxLTlmMWItNzI5NGFkMmY5MmM2');
define('ONESIGNAL_SAFARI_KEY', 'web.onesignal.auto.257d0569-0e14-4d06-8d17-3d55d768ff68');

/* Zerodha Details */
define('ZERODHA_CLIENT_ID', 'ZT6818');
define('ZERODHA_API_KEY', 'uzvxlgklzvluse0c');
define('ZERODHA_SECRET_KEY', 'gj0dkylyc7uxvzclv63hoq1x3rh7x0bh');
define('ZERODHA_CONNECT_URL', 'https://kite.zerodha.com/connect/login?api_key='.ZERODHA_API_KEY);
define('KITE_API_URL', 'https://api.kite.trade/');