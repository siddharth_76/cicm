<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->module = $this->router->fetch_module();
        $this->class = $this->router->fetch_class();
        $this->url = $this->module.'/'.$this->class;

        /* Load Excel Library */
        $this->load->library('EXcel');
    }

    /**
	* Index of payments controller
    */
    public function index() {
    	is_logged_in($this->url.'/view-all');
    	redirect($this->url.'/view-all');
    	exit();
    }

    /**
    * View All Payments Data
    */
    public function viewAll() {
        is_logged_in($this->url.'/view-all');
        is_have_access('can_access_payment');
        $data = array();
        $data['meta_title'] = 'View All';
        $data['small_text'] = 'Payments';
        $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'view_all_payments_data');
        $data['session_data'] = admin_session_data();
        $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
        $data['user_info'] = get_user($data['session_data']['user_id']);
        $data['users'] = $this->common_model->getAllRecordsById(USER, array('user_type_id' => APP_USER_TYPE, 'is_user_deleted' => 0));

        /* Fetch Data */
        $offset = 0;
        if(isset($_GET['per_page'])) {
            $offset = $_GET['per_page'];
        } else {
            $offset = $this->uri->segment(4);
        }

        $data['offset'] = $offset;
        $data['pagination'] = '';
        
        $condition = array();
        if(!empty($_GET['user_id'])) {
            $condition['user_id'] = $_GET['user_id'];
        }
        if(isset($_GET['is_reward_points_used'])) {
            $condition['is_reward_points_used'] = $_GET['is_reward_points_used'];
        }
        if(!empty($_GET['custom_text'])) {
            $condition['txn_id'] = $_GET['custom_text'];
        }

        $data['payments'] = $this->common_model->getPaginateRecordsByOrderByLikeCondition(PAYMENT, (isset($_GET['s'])) ? array('txn_id', 'payment_status') : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', 'id', 'DESC', RESULT_PER_PAGE, $offset, $condition);
        if(count($data['payments']) > 0) {        
            /* Pagination records */
            $url = get_cms_url().$this->url.'/view-all';
            $total_records = $this->common_model->getTotalPaginateRecordsByOrderByLikeCondition(PAYMENT, (isset($_GET['s'])) ? array('txn_id', 'payment_status') : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', $condition);
            $data['pagination'] = custom_pagination($url, $total_records, RESULT_PER_PAGE, 'right', '', http_build_query($_GET, '', '&'));
        }

        /* Load admin view */
        load_admin_view('view-all-payments-data', $data);
    }

    /**
    * View
    */
    public function view() {
        is_logged_in($this->url.'/view');
        is_have_access('can_access_payment');
        $paymentId = $this->uri->segment(4);
        $checkPayment = $this->common_model->getSingleRecordById(PAYMENT, array('id' => $paymentId));

        if(!empty($paymentId) && !empty($checkPayment)) {
            $data = array();
            $data['meta_title'] = 'View';
            $data['small_text'] = 'Payment Detail';
            $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'view_payment_detail');
            $data['session_data'] = admin_session_data();
            $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
            $data['user_info'] = get_user($data['session_data']['user_id']);
            $data['payment_id'] = $paymentId;
            $data['payment'] = $checkPayment;
            $data['userData'] = get_user($checkPayment['user_id']);
            $data['subscription'] = $this->common_model->getAllRecordsById(USER_SUBSCRIPTION_HISTORY, array('payment_id' => $paymentId));

            /* Load admin view */
            load_admin_view('view-payment-detail', $data);
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/view-all');
        }
    }
}