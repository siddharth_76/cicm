<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('cookie');
        $this->module = $this->router->fetch_module();
        $this->class = $this->router->fetch_class();
        $this->url = $this->module.'/'.$this->class;
    }

	/**
	* Admin index method
	*/
	public function index() {
		/* Check for already logged in */
		is_logged_in($this->router->fetch_class().'/dashboard');

		/* Redirect to the dashboard if user is already logged in */
		redirect($this->router->fetch_class().'/dashboard');
		exit();
	}

	/**
	* Admin login
	* @param Obj $_POST
	*/
	public function login() {
		$adminData = admin_session_data();
		if(isset($adminData['is_logged_in'])) {
			redirect($this->router->fetch_class().'/dashboard');
		} else {
			$data = array();
			$data['meta_title'] = 'Login';
			$data['body_class'] = 'admin_login';

			/* Process admin login */
			if($this->input->post('submit')) 
			{	
				/* From Validations */
				$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');    
                $this->form_validation->set_rules('password', 'Password', 'trim|required');

                if($this->form_validation->run() == true){
                	/* Check for valid admin user */
                	$sql = 'SELECT * FROM '.USER.' WHERE `email` = "'.$_POST['email'].'" AND `password` = "'.md5($_POST['password']).'" AND `status` = 1 AND (`user_type_id` = '.SUPER_ADMIN_USER_TYPE.' OR `user_type_id` = '.SUB_ADMIN_USER_TYPE.' OR `user_type_id` = '.STAFF_USER_TYPE.')';
					$checkUser = $this->common_model->getCustomSqlRow($sql);
                	if(isset($checkUser)) {
                		/* Saving session data for loggedin user */
                		$sessionArr = array(
                			'is_logged_in' => true,
                			'user_id' => $checkUser['id'],
                			'email' => $checkUser['email'],
                			'user_type' => $checkUser['user_type_id']
            			);
            			$this->session->set_userdata(array('admin_session_data' => $sessionArr));
                		
                		/* Add remember me cookie */
                		if($this->input->post('remember_me')) {
	            			$cookie = array(
						        'name'   => 'login_remember',
						        'value'  => $_POST['email'].'|'.$_POST['password'],
						        'expire' => time()+86500,
						        'domain' => $this->input->ip_address(),
						        'path'   => '/',
						        'prefix' => CMS_ABR.'_',
					        );
							set_cookie($cookie);
                		}

                		/* Redirect to loggedin user */
                		if($this->input->get('return_uri')) {
                            redirect($this->input->get('return_uri'));
                        } else {
                            redirect($this->router->fetch_class().'/dashboard');
                        }
                	} else {
                		$this->session->set_flashdata('login_error', LOGIN_ERROR);
                		if($this->input->get('return_uri')) {
                            redirect($this->router->fetch_class().'/login?return_uri='.urlencode($this->input->get('return_uri')));
                        } else {
                            redirect($this->router->fetch_class().'/login');
                        }
                	}
                }
			}
			/* Load admin view */
			$this->load->view(LOGIN_INCLUDES.'login-header', $data);
			load_admin_view('login', $data, false, false, false);
			$this->load->view(LOGIN_INCLUDES.'login-footer', $data);
		}
	}

	/**
	* Forgot Password
	* @param Obj $_POST
	*/
	public function forgotPassword() {
		$adminData = admin_session_data();
		if(isset($adminData['is_logged_in'])) {
			redirect($this->router->fetch_class().'/dashboard');
		} else {
			$data = array();
			$data['meta_title'] = 'Forgot Password';
			$data['body_class'] = 'admin_forgot_password';

			if($this->input->post('submit')) {
				/* From Validations */
				$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');

                if($this->form_validation->run() == true){
                	/* Check for valid admin user */
                	$sql = 'SELECT * FROM '.USER.' WHERE `email` = "'.$_POST['email'].'" AND (`user_type_id` = '.SUPER_ADMIN_USER_TYPE.' OR `user_type_id` = '.SUB_ADMIN_USER_TYPE.' OR `user_type_id` = '.STAFF_USER_TYPE.')';
					$checkUser = $this->common_model->getCustomSqlRow($sql);
					
					if(isset($checkUser)) {
						/* Generating and Insering random passord reset sring for reference */
						$password_reset_key = substr(md5(time()),rand(7,9),rand(15,25));

						/* Updating user record */
						$post_data = array('forgot_otp' => $password_reset_key);
						$where_condition = array('id' => $checkUser['id'], 'email' => $_POST['email']);
						$this->common_model->updateRecords(USER, $post_data, $where_condition);

						/* Send Email to user */
						$link = get_cms_url('admin/change-password?reset_key='.$password_reset_key);
						$subject = FORGOT_PASSWORD_SUBJECT;
					    $message = sprintf(FORGOT_PASSWORD_ADMIN_MESSAGE, ucfirst($checkUser['name']), $link);
						send_mail($message, $subject, $checkUser['email']);

						/* Set flashdata and redirect */
						$this->session->set_flashdata('forgot_password_success', FORGOT_PASSWORD_SUCCESS);
						redirect($this->router->fetch_class().'/forgot-password');
					} else {
						$this->session->set_flashdata('forgot_password_error', FORGOT_PASSWORD_ERROR);
						redirect($this->router->fetch_class().'/forgot-password');
					}
				}
			}
			/* Load admin view */
			$this->load->view(LOGIN_INCLUDES.'login-header', $data);
			load_admin_view('forgot-password', $data, false, false, false);
			$this->load->view(LOGIN_INCLUDES.'login-footer', $data);
		}
	}

	/**
	* change Password
	* @param $_POST
	*/
	public function changePassword() {
		$adminData = admin_session_data();
		if(isset($adminData['is_logged_in'])) {
			redirect($this->router->fetch_class().'/dashboard');
		} else {
			$resetkey = $this->input->get('reset_key');
			if(isset($resetkey)) {
				$data = array();
				$data['meta_title'] = 'Change Password';
				$data['body_class'] = 'change_password';

				if($this->input->post('submit')) {
					$this->form_validation->set_rules('password', 'Password', 'trim|required');
					$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|min_length[6]|matches[password]');
					
					if($this->form_validation->run() == true) {
						/* Check for reset key */
						$checkKey = $this->common_model->getSingleRecordById(USER, array('forgot_otp' => $resetkey));
						if(isset($checkKey)) {
							/* Updating user password */
							$post_data = array('password' => md5($_POST['password']));
							$where_condition = array('id' => $checkKey['id'], 'forgot_otp' => $resetkey);
							$this->common_model->updateRecords(USER, $post_data, $where_condition);

							/* Set forgot_otp = '' */
							$this->common_model->updateRecords(USER, array('forgot_otp' => ''), array('id' => $checkKey['id']));
							
							/* redirect to the login page */
							$this->session->set_flashdata('change_password_success', CHANGE_PASSWORD_SUCCESS);
							redirect($this->router->fetch_class().'/login');
						} else {
							$this->session->set_flashdata('invalid_reset_key', INVALID_RESET_KEY);
							redirect($this->router->fetch_class().'/change-password?reset_key='.$resetkey);
						}
					}
				}

				/* Load admin view */
				$this->load->view(LOGIN_INCLUDES.'login-header', $data);
				load_admin_view('change-password', $data, false, false, false);
				$this->load->view(LOGIN_INCLUDES.'login-footer', $data);
			} else {
				redirect($this->router->fetch_class().'/forgot-password');
			}
		}
	}

	/**
	* Admin sign out
	*/
	public function logout() {
		$this->session->unset_userdata('admin_session_data');
		$this->session->set_flashdata('logged_out', LOGGED_OUT);
		redirect($this->router->fetch_class().'/login?logged_out=true');
		exit();
	}

	/**
	* Admin Dashboard
	*/
	public function dashboard() {
		is_logged_in($this->router->fetch_class().'/dashboard');
		$data = array();
		$data['meta_title'] = 'Dashboard';
		$data['small_text'] = 'Control panel';
		$data['body_class'] = array('admin_dashboard', 'is_logged_in');
		$data['session_data'] = admin_session_data();
		$data['permissions'] = get_user_permissions($data['session_data']['user_id']);

		/* Load admin view */
		load_admin_view('dashboard', $data);
	}

	/**
	 * Get unhandled notification count
	 */
	public function checkUnSeenNotifications() {
		$newNotification = $this->common_model->getRecordCount(NOTIFICATION, array('is_seen' => 0));
		echo $newNotification;
	}

	/**
	 * Load Notifications
	 */
	public function loadnotifications() {
		$data = array();
		$notifications = $this->common_model->getAllRecordsOrderById(NOTIFICATION, 'created_at', 'DESC', array('is_seen' => 0));

		$data['notifications'] = $notifications;

		/* Load admin view */
		load_admin_view('includes/view-notifications', $data, false, false, false);
	}

	/**
	 * Send Push Notifications
	 */
	public function sendPushNotification() {
		is_logged_in($this->router->fetch_class().'/send-push-notification');
		is_have_access('can_send_push_notifications');
        $data = array();
        $data['meta_title'] = 'Send Push Notification';
        $data['small_text'] = 'To all CICM users';
        $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'send_push_notification');
        $data['session_data'] = admin_session_data();
        $data['permissions'] = get_user_permissions($data['session_data']['user_id']);

        /* Fetching users */
        if(isset($_POST['submit'])) {
        	$this->form_validation->set_rules('subject', 'Notification Subject', 'trim|required');
        	$this->form_validation->set_rules('message', 'Notification Message', 'trim|required');

            if($this->form_validation->run() == true){
				$users = $this->common_model->getAllRecordsById(USER, 'user_type_id IN('.APP_USER_TYPE.')');
				if(!empty($users)) {
					$tokens = array();
					foreach($users as $val) {
						if(!empty($val['device_token'])) {
							$tokens[] = $val['device_token'];
						}
					}

					/* Send Notificxation */
					if(!empty($tokens)) {
						$fcmData = array('title' => $_POST['subject'], 'body' => $_POST['message'], 'type' => 'notification_message');
						send_fcm_notification($fcmData, $tokens);
					}

					/* Adding Record */
					$addData = array(
						'sent_by' => $data['session_data']['user_id'],
						'subject' => $_POST['subject'],
						'message' => $_POST['message'],
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s')
					);
					$notificationId = $this->common_model->addRecords(PUSH_NOTIFICATIONS, $addData);

					if(!empty($notificationId)){
	                    $this->session->set_flashdata('item_success', 'Notification Sent Successfully.');
	                    redirect($this->router->fetch_class().'/send-push-notification');
	                } else {
	                    $this->session->set_flashdata('general_error', GENERAL_ERROR);
	                    redirect($this->router->fetch_class().'/send-push-notification');
	                }
				}
			}
		}

		/* Fetching Data */
		$offset = 0;
        if(isset($_GET['per_page'])) {
            $offset = $_GET['per_page'];
        } else {
            $offset = $this->uri->segment(3);
        }

        $data['offset'] = $offset;
        $data['pagination'] = '';
        
        $condition = '';
        $data['notifications'] = $this->common_model->getPaginateRecordsByOrderByLikeCondition(PUSH_NOTIFICATIONS, (isset($_GET['s'])) ? array('subject', 'message') : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', 'id', 'DESC', RESULT_PER_PAGE, $offset, $condition);
        if(count($data['notifications']) > 0) {
            /* Pagination records */
            $url = get_cms_url('admin/send-push-notification');
            $total_records = $this->common_model->getTotalPaginateRecordsByOrderByLikeCondition(PUSH_NOTIFICATIONS, (isset($_GET['s'])) ? array('subject', 'message') : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', $condition);
            $data['pagination'] = custom_pagination($url, $total_records, RESULT_PER_PAGE, 'right', '', http_build_query($_GET, '', '&'));
        }

		/* Load admin view */
		load_admin_view('send-push-notification', $data);
	}

	/**
	 * Kite Connect
	 */
	public function kiteconnect(){
		$kiteStatus = (!empty($_GET['status'])) ? $_GET['status'] : '';
		$kiteReqToken = (!empty($_GET['request_token'])) ? $_GET['request_token'] : ''; 
		$adminData = admin_session_data();
		if(isset($adminData['is_logged_in'])) {
			if(empty($kiteStatus) && empty($kiteReqToken)) {
				redirect(ZERODHA_CONNECT_URL);
			} else {
				if($kiteStatus == 'success' && !empty($kiteReqToken)){
				    $kite = new $this->kiteconnect(ZERODHA_API_KEY);
				    try {
				        $user = $kite->requestAccessToken($kiteReqToken, ZERODHA_SECRET_KEY);
				        $kite->setAccessToken($user->access_token);
				        $sessionArr = array(
				        	'product' => $user->product,
				        	'user_id' => $user->user_id,
				        	'order_type' => $user->order_type,
				        	'exchange' => $user->exchange,
				        	'access_token' => $user->access_token,
						    'user_type' => $user->user_type,
						    'broker' => $user->broker,
						    'public_token' => $user->public_token,
						    'member_id' => $user->member_id,
						    'user_name' => $user->user_name,
						    'email' => $user->email,
						    'login_time' => $user->login_time
			        	);
				        $this->session->set_userdata(array('kite_connect_session_data' => $sessionArr));
				        update_option('kite_access_token', $user->access_token);
				        $isConnected = 1;
				    } catch(Exception $e) {
				    	$isConnected = 0;
				        //echo "Authentication failed: ".$e->getMessage();
				        //throw $e;
				    }
				    redirect($this->router->fetch_class().'/dashboard?is_kite_connected='.$isConnected);
				}
			}
		} else {
			is_logged_in(ZERODHA_CONNECT_URL, true);
		}
	}
}