<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tips extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->module = $this->router->fetch_module();
        $this->class = $this->router->fetch_class();
        $this->url = $this->module.'/'.$this->class;
    }

    /**
	* Index of tips controller
    */
    public function index() {
    	is_logged_in($this->url.'/view-all');
    	redirect($this->url.'/view-all');
    	exit();
    }

    /**
    * View All Tips
    */
    public function viewAll() {
        is_logged_in($this->url.'/view-all');
        is_have_access('can_see_tips');
        $data = array();
        $data['meta_title'] = 'View Tips';
        $data['small_text'] = 'All Tips';
        $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'view_all_tips');
        $data['session_data'] = admin_session_data();
        $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
        $data['user_info'] = get_user($data['session_data']['user_id']);

        /* Getting all stock markets type */
        $stocks = $this->common_model->getAllRecords(STOCK_MARKET_TYPE);
        if(!empty($stocks)) {
            foreach($stocks as $val) {
                $stockArr[$val['id']] = $val['market_type_title'];
            }
        }
        $data['stock_markets'] = $stockArr;

        /* Fetch Data */
        $offset = 0;
        if(isset($_GET['per_page'])) {
            $offset = $_GET['per_page'];
        } else {
            $offset = $this->uri->segment(4);
        }

        $data['offset'] = $offset;
        $data['users'] = '';
        $data['pagination'] = '';
        
        $condition = '';
        if(isset($_GET['is_search']) && $_GET['is_search'] == 1) {
            if(isset($_GET['status']) && $_GET['status'] >= 0) {
                $condition .= ' status = '.$_GET['status'];
            } else {
                $condition .= 'status IN (0, 1, 2, 3)';
            }
            if(!empty($_GET['stock_market_type'])) {
                $condition .= ' AND stock_market_type = '.$_GET['stock_market_type'];
            }
            if(!empty($_GET['market_type'])) {
                $condition .= ' AND market_type = '.$_GET['market_type'];
            }
            if(!empty($_GET['exchange_tip_type'])) {
                $condition .= ' AND exchange_tip_type = '.$_GET['exchange_tip_type'];   
            }   
            if(!empty($_GET['tip_type'])) {
                $condition .= ' AND tip_type = '.$_GET['tip_type'];
            }
        }

        $data['tips'] = $this->common_model->getPaginateRecordsByOrderByLikeCondition(TIP, (isset($_GET['s'])) ? array('tip_id', 'exchange') : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', 'tip_id', 'DESC', RESULT_PER_PAGE, $offset, $condition);
        if(count($data['tips']) > 0) {
            /* Pagination records */
            $url = get_cms_url().$this->url.'/view-all';
            $total_records = $this->common_model->getTotalPaginateRecordsByOrderByLikeCondition(TIP, (isset($_GET['s'])) ? array('tip_id', 'exchange') : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', $condition);
            $data['pagination'] = custom_pagination($url, $total_records, RESULT_PER_PAGE, 'right', '', http_build_query($_GET, '', '&'));
        }

        /* Load admin view */
        load_admin_view('view-all-tips', $data);
    }

    /**
    * Add new tip
    * @param $_POST
    */
    public function addNew() {
        is_logged_in($this->url.'/add-new');
        is_have_access('can_post_tips');
        $data = array();
        $data['meta_title'] = 'Add New';
        $data['small_text'] = 'Tip';
        $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'add_new_tip');
        $data['session_data'] = admin_session_data();
        $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
        $data['stock_market_types'] = $this->common_model->getAllRecordsOrderById(STOCK_MARKET_TYPE, 'display_order', 'ASC', array('status' => 1));

        if($this->input->post('submit')) {
            $this->form_validation->set_rules('stock_market_type', 'Stock Market Type', 'trim|required');
            $this->form_validation->set_rules('tip_type', 'Tip Type', 'trim|required');
            if(empty($_POST['exchange'])) {
                $this->form_validation->set_rules('exchange_manually', 'Exchange', 'trim|required');
            } else {
                $this->form_validation->set_rules('exchange', 'Exchange', 'trim|required');
            }
            $this->form_validation->set_rules('market_type', 'Market Type', 'trim|required');
            $this->form_validation->set_rules('exchange_tip_type', 'Exchange Tip Type', 'trim|required');
            $this->form_validation->set_rules('tip_publish_status', 'Publish Status', 'trim|required');
            $this->form_validation->set_rules('tip_price', 'Tip Price', 'trim|required');
            $this->form_validation->set_rules('stoploss', 'Stoploss', 'trim|required');

            if($this->form_validation->run() == true){
                $stockMarketCode = $this->common_model->getSingleRecordById(STOCK_MARKET_TYPE, array('id' => $_POST['stock_market_type']));
                $price = explode('-', $_POST['tip_price']);
                if(empty($_POST['exchange'])) {
                    $exchange = $_POST['exchange_manually'];
                } else {
                    $exchange = $_POST['exchange'];
                }
                $addData = array(
                    'tip_created_by' => $data['session_data']['user_id'],
                    'tip_type' => $_POST['tip_type'],
                    'stock_market_type' => $_POST['stock_market_type'],
                    'stock_market_code' => (!empty($stockMarketCode['exchange_code'])) ? $stockMarketCode['exchange_code'] : '',
                    'exchange' => strtoupper($exchange),
                    'market_type' => $_POST['market_type'],
                    'exchange_tip_type' => $_POST['exchange_tip_type'],
                    'price_one' => convertDBInt($price[0]),
                    'price_two' => (!empty($price[1])) ? convertDBInt($price[1]) : '0',
                    'stoploss' => convertDBInt($_POST['stoploss']),
                    'trailed_stoploss' => convertDBInt($_POST['stoploss']),
                    'total_added_targets' => count($_POST['tip_targets']),
                    'acheived_target' => 0,
                    'status' => ($_POST['tip_publish_status'] == 1) ? '1' : '2',
                    'is_published' => $_POST['tip_publish_status'],
                    'publish_date' => ($_POST['tip_publish_status'] == 1) ? date('Y-m-d') : date('Y-m-d', strtotime($_POST['tip_publish_date'])),
                    'is_shared' => 0,
                    'is_automization_on' => $_POST['is_automization_on'],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );

                /* Add record */
                $tipId = $this->common_model->addRecords(TIP, $addData);
                if($tipId) {
                    /* Adding Tip Messages Info For Tip Automiztion */
                    $msgData = array(
                        'tip_id' => $tipId,
                        'is_exec' => 0,
                        'is_not_exec' => 0,
                        'stoploss_triggered' => 0,
                        'trailing_stoploss_triggered' => 0,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    );
                    $this->common_model->addRecords(TIP_MESSAGE, $msgData);
                    
                    /* Adding tip targets */
                    foreach($_POST['tip_targets'] as $key => $val) {
                        $ttData = array(
                            'tt_tip_id' => $tipId,
                            'tt_created_by' => $data['session_data']['user_id'],
                            'target_number' => $key,
                            'target_amount' => convertDBInt($val),
                            'is_achieved' => 0,
                            'stoploss_traling' => convertDBInt($_POST['stoploss_traling'][$key]),
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        );
                        $this->common_model->addRecords(TIP_TARGET, $ttData);
                        unset($ttData);
                    }

                    /* Send Push Notifications */
                    if($_POST['tip_publish_status'] == 1) {
                        $tipMsg = strtoupper($exchange).' '.get_tip_exchange_type($_POST['exchange_tip_type']).' @'.$_POST['tip_price'].' @stoploss '.$_POST['stoploss'];
                        $fcmData = array(
                            'title' => 'New Tip Posted',
                            'body' => $tipMsg,
                            'type' => 'new_tip_posted',
                            'extra' => array('tip_id' => $tipId, 'total_added_targets' => count($_POST['tip_targets']), 'acheived_target' => 0)
                        );
                        send_fcm_notification($fcmData, filter_tips_user($_POST['stock_market_type']));

                        /* Send SMS */
                        send_sms(implode(',', get_paid_user_number_by_segment($_POST['stock_market_type'])), $tipMsg);
                    }

                    $this->session->set_flashdata('item_success', sprintf(ITEM_ADD_SUCCESS, 'Tip'));
                    redirect($this->url.'/view-all');
                } else {
                    $this->session->set_flashdata('general_error', GENERAL_ERROR);
                    redirect($this->url.'/add-new');
                }
            }
        }
        /* Load admin view */
        load_admin_view('add-new-tip', $data);
    }

    /**
    * Edit Tip
    * @param $tipId, $_POST
    */
    public function edit() {
        is_logged_in($this->url.'/view-all');
        is_have_access('can_edit_tips');
        $tipId = $this->uri->segment(4);
        $checkTip = $this->common_model->getSingleRecordById(TIP, array('tip_id' => $tipId));
        if(!empty($checkTip)) {
            if($tipId) {
                $data = array();
                $data['meta_title'] = 'Edit';
                $data['small_text'] = 'Tip';
                $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'edit_tip');
                $data['session_data'] = admin_session_data();
                $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
                $data['stock_market_types'] = $this->common_model->getAllRecordsOrderById(STOCK_MARKET_TYPE, 'display_order', 'ASC', array('status' => 1));
                $data['tip'] = $checkTip;

                $targets = $this->common_model->getAllRecordsById(TIP_TARGET, array('tt_tip_id' => $tipId));
                if(!empty($targets)) {
                    foreach($targets as $val) {
                        $tipTargets[$val['target_number']] = $val['target_amount'];
                    }
                    foreach($targets as $val) {
                        $tipAcheived[$val['target_number']] = $val['is_achieved'];
                    }
                    foreach($targets as $val) {
                        $stoplossTrail[$val['target_number']] = $val['stoploss_traling'];
                    }
                }

                $data['targets'] = $tipTargets;
                $data['tipAcheived'] = $tipAcheived;
                $data['stoplossTrail'] = $stoplossTrail;

                if($this->input->post('submit')) {
                    $this->form_validation->set_rules('stock_market_type', 'Stock Market Type', 'trim|required');
                    $this->form_validation->set_rules('tip_type', 'Tip Type', 'trim|required');
                    $this->form_validation->set_rules('exchange', 'Exchange', 'trim|required');
                    $this->form_validation->set_rules('market_type', 'Market Type', 'trim|required');
                    $this->form_validation->set_rules('exchange_tip_type', 'Exchange Tip Type', 'trim|required');
                    $this->form_validation->set_rules('tip_publish_status', 'Publish Status', 'trim|required');
                    $this->form_validation->set_rules('tip_price', 'Tip Price', 'trim|required');
                    $this->form_validation->set_rules('stoploss', 'Stoploss', 'trim|required');

                    if($this->form_validation->run() == true){
                        $totalAcheived = 0;
                        foreach($_POST['is_acheived'] as $aval) {
                            if($aval == 1) {
                                $totalAcheived++;
                            }
                        }

                        $price = explode('-', $_POST['tip_price']);
                        $stockMarketCode = $this->common_model->getSingleRecordById(STOCK_MARKET_TYPE, array('id' => $_POST['stock_market_type']));
                        $updateData = array(
                            'tip_type' => $_POST['tip_type'],
                            'stock_market_type' => $_POST['stock_market_type'],
                            'stock_market_code' => (!empty($stockMarketCode['exchange_code'])) ? $stockMarketCode['exchange_code'] : '',
                            'exchange' => strtoupper($_POST['exchange']),
                            'market_type' => $_POST['market_type'],
                            'exchange_tip_type' => $_POST['exchange_tip_type'],
                            'price_one' => convertDBInt($price[0]),
                            'price_two' => (!empty($price[1])) ? convertDBInt($price[1]) : '0',
                            'stoploss' => convertDBInt($_POST['stoploss']),
                            'total_added_targets' => count($_POST['tip_targets']),
                            'acheived_target' => $totalAcheived,
                            'status' => $_POST['tip_publish_status'],
                            'is_published' => ($_POST['tip_publish_status'] == 1 || $_POST['tip_publish_status'] == 3) ? 1 : 0,
                            'is_shared' => $_POST['is_shared'],
                            'tip_share_earning' => ($_POST['is_shared'] == 1) ? convertDBInt($_POST['tip_share_earning']) : 0,
                            'tip_share_earning_days' => ($_POST['is_shared'] == 1) ? convertDBInt($_POST['tip_share_earning_days']) : 0,
                            'is_automization_on' => $_POST['is_automization_on'],
                            'updated_at' => date('Y-m-d H:i:s')
                        );

                        if($_POST['tip_publish_status'] == 2) {
                            $updateData['publish_date'] = date('Y-m-d', strtotime($_POST['tip_publish_date']));
                        } elseif($_POST['tip_publish_status'] == 1) {
                            $updateData['publish_date'] = date('Y-m-d');
                        }

                        /* Updating tip data */
                        $this->common_model->updateRecords(TIP, $updateData, array('tip_id' => $tipId));

                        /* Updating tip targets data */
                        foreach($_POST['tip_targets'] as $key => $val){
                            $checkTt = $this->common_model->getRecordCount(TIP_TARGET, array('tt_tip_id' => $tipId, 'target_number' => $key));
                            if(!empty($checkTt)) {
                                $ttUp = array(
                                    'target_amount' => convertDBInt($val),
                                    'is_achieved' => $_POST['is_acheived'][$key],
                                    'stoploss_traling' => convertDBInt($_POST['stoploss_traling'][$key]),
                                    'updated_at' => date('Y-m-d H:i:s')
                                );
                                $this->common_model->updateRecords(TIP_TARGET, $ttUp, array('tt_tip_id' => $tipId, 'target_number' => $key));
                                unset($ttUp);
                            } else {
                                $ttData = array(
                                    'tt_tip_id' => $tipId,
                                    'tt_created_by' => $data['session_data']['user_id'],
                                    'target_number' => $key,
                                    'target_amount' => convertDBInt($val),
                                    'is_achieved' => $_POST['is_acheived'][$key],
                                    'stoploss_traling' => convertDBInt($_POST['stoploss_traling'][$key]),
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'updated_at' => date('Y-m-d H:i:s')
                                );
                                $this->common_model->addRecords(TIP_TARGET, $ttData);
                                unset($ttData);
                            }
                        }

                        /* Send Push Notifications */
                        if($totalAcheived > 1 && $_POST['tip_publish_status'] == 1) {
                            $tipMsg = 'Target updated for tip '.strtoupper($checkTip['exchange']);
                            $fcmData = array(
                                'title' => 'Tip Targets Updated',
                                'body' => $tipMsg,
                                'type' => 'tip_target_updated',
                                'extra' => array('tip_id' => $tipId, 'total_added_targets' => count($_POST['tip_targets']), 'acheived_target' => $totalAcheived)
                            );
                            send_fcm_notification($fcmData, filter_tips_user($_POST['stock_market_type']));

                            /* Send SMS */
                            send_sms(implode(',', get_paid_user_number_by_segment($_POST['stock_market_type'])), $tipMsg);
                        }

                        $this->session->set_flashdata('item_success', sprintf(ITEM_UPDATE_SUCCESS, 'Tip'));
                        redirect($this->url.'/view-all');
                    } 
                }

                /* Load admin view */
                load_admin_view('edit-tip', $data);
            } else {
                $this->session->set_flashdata('invalid_item', INVALID_ITEM);
                redirect($this->url.'/view-all');
            }
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/view-all');
        }
    }

    /**
    * Delete Tip
    * @param $tipId
    */
    public function delete() {
        is_logged_in($this->url.'/view-all');
        is_have_access('can_edit_tips');
        $tipId = $this->uri->segment(4);
        if($tipId) {
            /* Deleting Tip */
            $this->common_model->deleteRecord(TIP, array('tip_id' => $tipId));
            $this->common_model->deleteRecord(TIP_TARGET, array('tt_tip_id' => $tipId));
            $this->common_model->deleteRecord(TIP_FOLLOW_UP, array('fu_tip_id' => $tipId));

            $this->session->set_flashdata('item_success', sprintf(ITEM_DELETE_SUCCESS, 'Tip'));
            redirect($this->url.'/view-all');
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/view-all');
        }
    }

    /**
    * View All Market Types
    */
    public function marketTypes() {
        is_logged_in($this->url.'/market-types');
        is_have_access('can_see_tips');
        $data = array();
        $data['meta_title'] = 'View Market';
        $data['small_text'] = 'All Market Types';
        $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'view_all_market_types');
        $data['session_data'] = admin_session_data();
        $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
        $data['user_info'] = get_user($data['session_data']['user_id']);

        /* Fetch Data */
        $offset = 0;
        if(isset($_GET['per_page'])) {
            $offset = $_GET['per_page'];
        } else {
            $offset = $this->uri->segment(4);
        }

        $data['offset'] = $offset;
        $data['users'] = '';
        $data['pagination'] = '';
        
        $condition = '';

        $data['market_types'] = $this->common_model->getPaginateRecordsByOrderByLikeCondition(STOCK_MARKET_TYPE, (isset($_GET['s'])) ? array('market_type_title') : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', 'id', 'DESC', RESULT_PER_PAGE, $offset, $condition);
        if(count($data['market_types']) > 0) {
            /* Pagination records */
            $url = get_cms_url().$this->url.'/market-types';
            $total_records = $this->common_model->getTotalPaginateRecordsByOrderByLikeCondition(STOCK_MARKET_TYPE, (isset($_GET['s'])) ? array('market_type_title') : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', $condition);
            $data['pagination'] = custom_pagination($url, $total_records, RESULT_PER_PAGE, 'right', '', http_build_query($_GET, '', '&'));
        }

        /* Load admin view */
        load_admin_view('view-all-market-types', $data);
    }

    /**
    * Add new market type
    * @param $_POST
    */
    public function addNewMarketType() {
        is_logged_in($this->url.'/market-types');
        is_have_access('can_see_tips');
        $data = array();
        $data['meta_title'] = 'Add New';
        $data['small_text'] = 'Market Type';
        $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'add_new_market_type');
        $data['session_data'] = admin_session_data();
        $data['permissions'] = get_user_permissions($data['session_data']['user_id']);

        if($this->input->post('submit')) {
            $this->form_validation->set_rules('market_type_title', 'Market Type Title', 'trim|required');
            $this->form_validation->set_rules('exchange_code', 'Market Type Exchange Code', 'trim|required');
            $this->form_validation->set_rules('status', 'Market Type Status', 'trim|required');

            if($this->form_validation->run() == true){
                unset($_POST['submit']);
                $_POST['exchange_code'] = strtoupper($_POST['exchange_code']);
                $_POST['created_at'] = $_POST['updated_at'] = date('Y-m-d H:i:s');

                /* Add record */
                $marketTypeId = $this->common_model->addRecords(STOCK_MARKET_TYPE, $_POST);
                if($marketTypeId) {
                    /* Updating Display Order */
                    $this->common_model->updateRecords(STOCK_MARKET_TYPE, array('display_order' => $marketTypeId), array('id' => $marketTypeId));
                    $this->session->set_flashdata('item_success', sprintf(ITEM_ADD_SUCCESS, 'Market Type'));
                    redirect($this->url.'/market-types');
                } else {
                    $this->session->set_flashdata('general_error', GENERAL_ERROR);
                    redirect($this->url.'/add-new-market-type');
                }
            }
        }
        /* Load admin view */
        load_admin_view('add-new-market-type', $data);
    }

    /**
    * Edit market type
    * @param $tipId, $_POST
    */
    public function editMarketType() {
        is_logged_in($this->url.'/market-types');
        is_have_access('can_see_tips');
        $typeId = $this->uri->segment(4);
        $checkType = $this->common_model->getSingleRecordById(STOCK_MARKET_TYPE, array('id' => $typeId));
        if(!empty($checkType)) {
            if($typeId) {
                $data = array();
                $data['meta_title'] = 'Edit';
                $data['small_text'] = 'market Type';
                $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'edit_market_type');
                $data['session_data'] = admin_session_data();
                $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
                $data['mtype'] = $checkType;

                if($this->input->post('submit')) {
                    $this->form_validation->set_rules('market_type_title', 'Market Type Title', 'trim|required');
                    $this->form_validation->set_rules('exchange_code', 'Market Type Exchange Code', 'trim|required');
                    $this->form_validation->set_rules('status', 'Market Type Status', 'trim|required');

                    if($this->form_validation->run() == true){
                        $_POST['exchange_code'] = strtoupper($_POST['exchange_code']);
                        $_POST['updated_at'] = date('Y-m-d H:i:s');

                        unset($_POST['submit']);

                        /* Updating plan data */
                        $this->common_model->updateRecords(STOCK_MARKET_TYPE, $_POST, array('id' => $typeId));

                        $this->session->set_flashdata('item_success', sprintf(ITEM_UPDATE_SUCCESS, 'Market Type'));
                        redirect($this->url.'/market-types');
                    } 
                }

                /* Load admin view */
                load_admin_view('edit-market-type', $data);
            } else {
                $this->session->set_flashdata('invalid_item', INVALID_ITEM);
                redirect($this->url.'/market-types');
            }
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/market-types');
        }
    }

    /**
     * Tips Follow Ups
     */
    public function followUps(){
        $tipId = $this->uri->segment(4);
        $checkTip = $this->common_model->getSingleRecordById(TIP, array('tip_id' => $tipId));
        if(!empty($checkTip)) {
            is_logged_in($this->url.'/follow-ups');
            is_have_access('can_see_follow_up');
            $data = array();
            $data['meta_title'] = "View Follow Up's";
            $data['small_text'] = "All Tips Follow Up's";
            $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'view_all_follow_ups');
            $data['session_data'] = admin_session_data();
            $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
            $data['user_info'] = get_user($data['session_data']['user_id']);
            $data['tip_id'] = $tipId;
            $data['tip'] = $checkTip;

            /* Fetch Data */
            $offset = 0;
            if(isset($_GET['per_page'])) {
                $offset = $_GET['per_page'];
            } else {
                $offset = $this->uri->segment(5);
            }

            $data['offset'] = $offset;
            $data['users'] = '';
            $data['pagination'] = '';
            
            $condition = 'fu_tip_id = '.$tipId;

            $data['follow_ups'] = $this->common_model->getPaginateRecordsByOrderByLikeCondition(TIP_FOLLOW_UP, (isset($_GET['s'])) ? array('fu_message') : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', 'fu_id', 'DESC', RESULT_PER_PAGE, $offset, $condition);
            if(count($data['follow_ups']) > 0) {
                /* Pagination records */
                $url = get_cms_url().$this->url.'/market-types';
                $total_records = $this->common_model->getTotalPaginateRecordsByOrderByLikeCondition(TIP_FOLLOW_UP, (isset($_GET['s'])) ? array('fu_message') : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', $condition);
                $data['pagination'] = custom_pagination($url, $total_records, RESULT_PER_PAGE, 'right', '', http_build_query($_GET, '', '&'));
            }

            /* Load admin view */
            load_admin_view('view-all-follow-ups', $data);
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/view-all');
        }
    }

    /**
     * Add New Follow Up
     */
    public function addNewFollowUp() {
        $tipId = $this->uri->segment(4);
        $checkTip = $this->common_model->getSingleRecordById(TIP, array('tip_id' => $tipId));
        if(!empty($checkTip)) {
            is_logged_in($this->url.'/follow-ups');
            is_have_access('can_post_follow_up');
            $data = array();
            $data['meta_title'] = 'Add New';
            $data['small_text'] = 'Tip Follow Up';
            $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'add_new_follow_up');
            $data['session_data'] = admin_session_data();
            $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
            $data['tip_id'] = $tipId;
            $data['tip'] = $checkTip;

            if($this->input->post('submit')) {
                $this->form_validation->set_rules('fu_message', 'Tip Follow Up Message', 'trim|required');
                $this->form_validation->set_rules('tip_status', 'Tip Status', 'trim|required');

                if($this->form_validation->run() == true){
                    $addData = array(
                        'fu_tip_id' => $tipId,
                        'fu_created_by' => $data['session_data']['user_id'],
                        'fu_message' => $_POST['fu_message'],
                        'is_trailed_stoploss' => $_POST['trail_stoploss'],
                        'is_trailed_targets' => $_POST['trail_targets'],
                        'fu_status' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    );

                    /* Add record */
                    $followUpId = $this->common_model->addRecords(TIP_FOLLOW_UP, $addData);
                    if($followUpId) {
                        /* Update tip stoploss */
                        if($_POST['trail_stoploss'] == 1) {
                            $this->common_model->updateRecords(TIP, array('trailed_stoploss' => convertDBInt($_POST['trailed_stoploss']), 'updated_at' => date('Y-m-d H:i:s')), array('tip_id' => $tipId));
                        }

                        /* Adding new targets */
                        if($_POST['trail_targets'] == 1) {
                            foreach($_POST['tip_targets'] as $key => $val) {
                                $ttData = array(
                                    'tt_tip_id' => $tipId,
                                    'tt_fu_id' => $followUpId,
                                    'tt_created_by' => $data['session_data']['user_id'],
                                    'target_number' => $key,
                                    'target_amount' => convertDBInt($val),
                                    'is_achieved' => 0,
                                    'stoploss_traling' => convertDBInt($_POST['stoploss_traling'][$key]),
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'updated_at' => date('Y-m-d H:i:s')
                                );
                                $this->common_model->addRecords(TIP_TARGET, $ttData);
                                unset($ttData);
                            }

                            $prvTargets = $checkTip['total_added_targets'];
                            $newTargets = $prvTargets + count($_POST['tip_targets']); 
                            $this->common_model->updateRecords(TIP, array('total_added_targets' => $newTargets, 'updated_at' => date('Y-m-d H:i:s')), array('tip_id' => $tipId));  
                        }

                        if(isset($_POST['tip_status'])) {
                            $this->common_model->updateRecords(TIP, array('status' => $_POST['tip_status'], 'updated_at' => date('Y-m-d H:i:s')), array('tip_id' => $tipId));  
                        }

                        /* Send Push Notifications */
                        $tipData = $this->common_model->getSingleRecordById(TIP, array('tip_id' => $tipId));
                        $tipMsg = $checkTip['exchange'].' '.get_tip_exchange_type($checkTip['exchange_tip_type']);
                        $fcmData = array(
                            'title' => 'New Follow Up',
                            'body' => $tipMsg,
                            'type' => 'new_followup',
                            'extra' => array('tip_id' => $tipId, 'follow_up_id' => $followUpId, 'total_added_targets' => $checkTip['total_added_targets'], 'acheived_target' => $checkTip['acheived_target'])
                        );
                        send_fcm_notification($fcmData, filter_tips_user($checkTip['stock_market_type']));

                        /* Send SMS */
                        send_sms(implode(',', get_paid_user_number_by_segment($checkTip['stock_market_type'])), $tipMsg);

                        $this->session->set_flashdata('item_success', sprintf(ITEM_ADD_SUCCESS, 'Follow Up'));
                        redirect($this->url.'/follow-ups/'.$tipId);
                    } else {
                        $this->session->set_flashdata('general_error', GENERAL_ERROR);
                        redirect($this->url.'/follow-ups/'.$tipId);
                    }
                }
            }
            /* Load admin view */
            load_admin_view('add-new-follow-up', $data);
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/view-all');
        }
    }

    /**
     * Edit Follow Up
     */
    public function editFollowUp() {
        $tipId = $this->uri->segment(4);
        $followUpId = $this->uri->segment(5);

        /* Check for valid items */
        $checkTip = $this->common_model->getSingleRecordById(TIP, array('tip_id' => $tipId));
        $checkFollowUp = $this->common_model->getSingleRecordById(TIP_FOLLOW_UP, array('fu_id' => $followUpId));
        if(!empty($checkTip) && !empty($checkFollowUp)) {
            is_logged_in($this->url.'/follow-ups');
            is_have_access('can_edit_follow_up');
            $data = array();
            $data['meta_title'] = 'Edit Follow Up';
            $data['small_text'] = 'Tip Follow Up';
            $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'edit_follow_up');
            $data['session_data'] = admin_session_data();
            $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
            $data['tip_id'] = $tipId;
            $data['tip'] = $checkTip;
            $data['follow_up_id'] = $followUpId;
            $data['follow_up'] = $checkFollowUp;
            $data['follow_up_targets'] = $this->common_model->getAllRecordsById(TIP_TARGET, array('tt_tip_id' => $tipId, 'tt_fu_id' => $followUpId));

            if($this->input->post('submit')) {
                $this->form_validation->set_rules('fu_message', 'Tip Follow Up Message', 'trim|required');
                $this->form_validation->set_rules('tip_status', 'Tip Status', 'trim|required');

                if($this->form_validation->run() == true){
                    /* Updating follow up data */
                    $update = array(
                        'fu_message' => $_POST['fu_message'],
                        'is_trailed_stoploss' => $_POST['trail_stoploss'],
                        'is_trailed_targets' => $_POST['trail_targets'],
                        'updated_at' => date('Y-m-d H:i:s')
                    );
                    $this->common_model->updateRecords(TIP_FOLLOW_UP, $update, array('fu_tip_id' => $tipId, 'fu_id' => $followUpId));

                    /* Update tip stoploss */
                    if($_POST['trail_stoploss'] == 1) {
                        $this->common_model->updateRecords(TIP, array('trailed_stoploss' => convertDBInt($_POST['trailed_stoploss']), 'updated_at' => date('Y-m-d H:i:s')), array('tip_id' => $tipId));
                    }

                    /* Adding new targets */
                    if($_POST['trail_targets'] == 1) {
                        $newTargets = 0;
                        foreach($_POST['tip_targets'] as $key => $val) {
                            $check = $this->common_model->getRecordCount(TIP_TARGET, array('tt_tip_id' => $tipId, 'tt_fu_id' => $followUpId, 'target_number' => $key));
                            if(!empty($check)) {
                                $this->common_model->updateRecords(TIP_TARGET, array('target_amount' => convertDBInt($val), 'is_achieved' => $_POST['is_acheived'][$key], 'updated_at' => date('Y-m-d H:i:s')), array('tt_tip_id' => $tipId, 'tt_fu_id' => $followUpId, 'target_number' => $key));
                            } else {
                                $ttData = array(
                                    'tt_tip_id' => $tipId,
                                    'tt_fu_id' => $followUpId,
                                    'tt_created_by' => $data['session_data']['user_id'],
                                    'target_number' => $key,
                                    'target_amount' => convertDBInt($val),
                                    'is_achieved' => $_POST['is_acheived'][$key],
                                    'stoploss_traling' => $_POST['stoploss_traling'][$key],
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'updated_at' => date('Y-m-d H:i:s')
                                );
                                $this->common_model->addRecords(TIP_TARGET, $ttData);
                                $newTargets++;
                                unset($ttData);
                            }
                        }

                        $totalAcheived = $checkTip['acheived_target'];
                        foreach($_POST['is_acheived'] as $aval) {
                            if($aval == 1) {
                                $totalAcheived++;
                            } else {
                                $totalAcheived--;
                            }
                        }

                        $prvTargets = $checkTip['total_added_targets'];
                        $newTargets = $prvTargets + $newTargets; 
                        $this->common_model->updateRecords(TIP, array('total_added_targets' => $newTargets, 'acheived_target' => $totalAcheived, 'updated_at' => date('Y-m-d H:i:s')), array('tip_id' => $tipId));  
                        
                        /* Send Push Notifications */
                        if($totalAcheived > 1) {
                            $tipMsg = $checkTip['exchange'].' '.get_tip_exchange_type($checkTip['exchange_tip_type']);
                            $fcmData = array(
                                'title' => 'Tip Targets Updated',
                                'body' => $tipMsg,
                                'type' => 'tip_target_updated',
                                'extra' => array('tip_id' => $tipId, 'total_added_targets' => $newTargets, 'acheived_target' => $totalAcheived)
                            );
                            send_fcm_notification($fcmData, filter_tips_user($checkTip['stock_market_type']));

                            /* Send SMS */
                            send_sms(implode(',', get_paid_user_number_by_segment($checkTip['stock_market_type'])), $tipMsg);
                        }
                    }

                    if(isset($_POST['tip_status'])) {
                        $this->common_model->updateRecords(TIP, array('status' => $_POST['tip_status'], 'updated_at' => date('Y-m-d H:i:s')), array('tip_id' => $tipId));  
                    }

                    $this->session->set_flashdata('item_success', sprintf(ITEM_ADD_SUCCESS, 'Follow Up'));
                    redirect($this->url.'/follow-ups/'.$tipId);
                }
            }
            /* Load admin view */
            load_admin_view('edit-follow-up', $data);
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/view-all');
        }
    }

    /**
     * Delete Follow Up
     */
    public function deleteFollowUp(){
        is_logged_in($this->url.'/view-all');
        is_have_access('can_edit_follow_up');
        $tipId = $this->uri->segment(4);
        $followUpId = $this->uri->segment(5);
        $addedNewTargets = 0;

        /* Check for valid items */
        $checkTip = $this->common_model->getSingleRecordById(TIP, array('tip_id' => $tipId));
        $checkFollowUp = $this->common_model->getSingleRecordById(TIP_FOLLOW_UP, array('fu_id' => $followUpId));
        if(!empty($checkTip) && !empty($checkFollowUp)) {
            if($checkFollowUp['is_trailed_targets'] == 1) {
                $addedNewTargets = $this->common_model->getRecordCount(TIP_TARGET, array('tt_tip_id' => $tipId, 'tt_fu_id' => $followUpId));
            }

            $this->common_model->deleteRecord(TIP_FOLLOW_UP, array('fu_id' => $followUpId, 'fu_tip_id' => $tipId));
            if($addedNewTargets > 0) {
                /* Deleting Tip Targets */
                $this->common_model->deleteRecord(TIP_TARGET, array('tt_fu_id' => $followUpId, 'tt_tip_id' => $tipId));
                
                /* Updating tip total remaining targets */
                $remainingTargets = $checkTip['total_added_targets'] - $addedNewTargets;
                $this->common_model->updateRecords(TIP, array('total_added_targets' => $remainingTargets, 'updated_at' => date('Y-m-d H:i:s')), array('tip_id' => $tipId));
                
                /* Updating target numbers */
                $targets = $this->common_model->getAllRecordsById(TIP_TARGET, array('tt_tip_id' => $tipId));
                $remainingTargets = $remainingTargets - 1;

                for($i = 0; $i <= $remainingTargets; $i++){
                    $newNum = $i + 1;
                    $this->common_model->updateRecords(TIP_TARGET, array('target_number' => $newNum, 'updated_at' => date('Y-m-d H:i:s')), array('tt_id' => $targets[$i]['tt_id'], 'tt_tip_id' => $tipId));
                }
            }

            $this->session->set_flashdata('item_success', sprintf(ITEM_DELETE_SUCCESS, 'Tip Follow Up'));
            redirect($this->url.'/follow-ups/'.$tipId);
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/follow-ups/'.$tipId);
        }
    }

    public function loadinstruments() {
        $instruments = $symbols = array();
        if(empty($_POST['instrument_type_id'])) {
            $symbols[] = array('id' => '', 'text' => 'Please select stock market type first.');
        }
        if(!empty($_POST['instrument_type_id']) && !empty($_POST['exchange'])) {
            $typeId = $_POST['instrument_type_id'];
            $exchange = $_POST['exchange'];
            $symbol = $this->common_model->getSingleRecordById(STOCK_MARKET_TYPE, array('id' => $typeId));
            
            if(!empty($symbol)) {
                if($symbol['id'] == 3) {
                    $code = 'CDS-FUT';
                    $instruments = $this->common_model->getRecordsByOrderLikeConditions(INSTRUMENT, array('segment' => $code), 'tradingsymbol', $exchange, 'tradingsymbol', 'ASC');
                } elseif($symbol['id'] == 7) {
                    $code = 'CDS-OPT';
                    $instruments = $this->common_model->getRecordsByOrderLikeConditions(INSTRUMENT, array('segment' => $code), 'tradingsymbol', $exchange, 'tradingsymbol', 'ASC');
                } elseif($symbol['id'] == 4) {
                    $code = 'NFO-OPT';
                    $instruments = $this->common_model->getRecordsByOrderLikeConditions(INSTRUMENT, array('segment' => $code), 'tradingsymbol', $exchange, 'tradingsymbol', 'ASC');
                } elseif($symbol['id'] == 5) {
                    $code = 'NFO-FUT';
                    $instruments = $this->common_model->getRecordsByOrderLikeConditions(INSTRUMENT, array('segment' => $code), 'tradingsymbol', $exchange, 'tradingsymbol', 'ASC');
                } else {
                    $code = $symbol['exchange_code'];
                    $instruments = $this->common_model->getRecordsByOrderLikeConditions(INSTRUMENT, array('exchange' => $code), 'tradingsymbol', $exchange, 'tradingsymbol', 'ASC');
                }
            }

            if(!empty($instruments)) {
                foreach($instruments as $val) {
                    $symbols[] = array('id' => strtoupper($val['tradingsymbol']), 'text' => strtoupper($val['tradingsymbol']));
                }
            }
        }
        $returnData = array('total_count' => count($symbols), 'items' => $symbols);
        echo json_encode($returnData);
    }

    public function getLtp() {
        $returnData = array();
        $ltpArray = $_POST['data'];
        if(isset($ltpArray) && $ltpArray != null) {
            $data = (array) json_decode($ltpArray);
            if(!empty($data)) {
                $accessToken = get_option('kite_access_token');
                if(!empty($accessToken)) {
                    foreach($data as $key => $val) {
                        $url = KITE_API_URL."instruments/".$val[0]."/".$val[1]."?api_key=".ZERODHA_API_KEY."&access_token=".$accessToken;
                        $response = file_get_contents($url);
                        $response = json_decode($response);
                        if($response->status == 'success') {
                            $returnData[$key] = $response->data->last_price;
                        }
                    }
                }
            }
        }
        echo json_encode($returnData);
        exit();
    }

    public function loadTipTargets() {
        $data = array();
        $data['tip_id'] = $_POST['tipId'];
        $data['action'] = $_POST['action'];
        $data['targets'] = $this->common_model->getAllRecordsById(TIP_TARGET, array('tt_tip_id' => $_POST['tipId']));
        load_admin_view('ajaxInfo', $data, false, false, false);
    }
}