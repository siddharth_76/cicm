<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hawkeye extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->module = $this->router->fetch_module();
        $this->class = $this->router->fetch_class();
        $this->url = $this->module.'/'.$this->class;

        /* Load Excel Library */
        $this->load->library('EXcel');
    }

    /**
	* Index of hawkeye controller
    */
    public function index() {
    	is_logged_in($this->url.'/view-all');
    	redirect($this->url.'/view-all');
    	exit();
    }

    /**
    * Upload hawkEye Data
    * @param $_POST
    */
    public function upload() {
        is_logged_in($this->url.'/upload');
        is_have_access('can_upload_intraday_scanner_data');
        $data = array();
        $data['meta_title'] = 'Upload';
        $data['small_text'] = 'Intraday Scanner Data';
        $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'upload_intraday_scanner_data');
        $data['session_data'] = admin_session_data();
        $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
        $data['categories'] = $this->common_model->getAllRecordsById(INTRADAY_SCANNER_CATEGORY, array('status' => 1));

        if($this->input->post('submit')) { //p($_POST); p($_FILES); die;
            $this->form_validation->set_rules('category_id', 'Intraday Scanner Category', 'trim|required');
            $this->form_validation->set_rules('publish_date', 'Publish Date', 'trim|required');
            $this->form_validation->set_rules('attachment', 'Attachcment', 'callback__verify_attachment');

            if($this->form_validation->run() == true){
                /* Upload news attachment */
                $attachment = '';
                if(!empty($_FILES['attachment']['name'])) {
                    $config['file_name']     = time().$_FILES['attachment']['name']; 
                    $config['upload_path']   = './uploads/intradayScanner';
                    $config['allowed_types'] = 'xls|xlsx';
                    $config['max_size']      = '2474898';
                    $config['max_width']     = '100024';
                    $config['max_height']    = '768000';
                    $config['remove_spaces'] = true;
                    $config['field_name']    = 'attachment';

                    $picData = upload_file($_FILES['attachment'], $config);
                    if(isset($picData) && $picData['message'] == 'success') {
                        $attachment = $picData['upload_data']['upload_data']['full_path'];
                    }
                }
                
                if(!empty($attachment)) {
                    $objPHPExcel = PHPExcel_IOFactory::load($attachment);
                    $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

                    foreach ($cell_collection as $cell) {
                        $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                        $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                        $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

                        if ($row == 1) {
                            $header[$row][$column] = $data_value;
                        } else {
                            $arr_data[$row][$column] = $data_value;
                        }
                    }

                    if(!empty($arr_data)) {
                        foreach($arr_data as $val) {
                            $checkData = $this->common_model->getSingleRecordById(INTRADAY_SCANNER, ' intraday_category_id = '.$_POST['category_id'].' AND DATE(publish_date) = "'.$_POST['publish_date'].'" AND script_name = "'.$val['A'].'"');
                            if(!empty($checkData)) {
                                $updateData = array(
                                    'script_name' => $val['A'],
                                    'company_name' => $val['B'],
                                    'buy_above' => convertDBInt($val['C']),
                                    'buy_stoploss' => convertDBInt($val['K']),
                                    'buy_target_1' => convertDBInt($val['D']),
                                    'buy_target_2' => convertDBInt($val['E']),
                                    'buy_target_3' => (!empty($val['F'])) ? convertDBInt($val['F']) : 0,
                                    'sell_below' => convertDBInt($val['G']),
                                    'sell_stoploss' => convertDBInt($val['L']),
                                    'sell_target_1' => convertDBInt($val['H']),
                                    'sell_target_2' => convertDBInt($val['I']),
                                    'sell_target_3' => (!empty($val['J'])) ? convertDBInt($val['J']) : 0,
                                    'updated_at' => date('Y-m-d H:i:s')
                                );
                                $this->common_model->updateRecords(INTRADAY_SCANNER, $updateData, array('id' => $checkData['id']));
                            } else {
                                $addData = array(
                                    'intraday_category_id' => $_POST['category_id'],
                                    'script_name' => $val['A'],
                                    'company_name' => $val['B'],
                                    'buy_above' => convertDBInt($val['C']),
                                    'buy_stoploss' => convertDBInt($val['K']),
                                    'buy_target_1' => convertDBInt($val['D']),
                                    'buy_target_2' => convertDBInt($val['E']),
                                    'buy_target_3' => (!empty($val['F'])) ? convertDBInt($val['F']) : 0,
                                    'sell_below' => convertDBInt($val['G']),
                                    'sell_stoploss' => convertDBInt($val['L']),
                                    'sell_target_1' => convertDBInt($val['H']),
                                    'sell_target_2' => convertDBInt($val['I']),
                                    'sell_target_3' => (!empty($val['J'])) ? convertDBInt($val['J']) : 0,
                                    'status' => 1,
                                    'publish_date' => $_POST['publish_date'],
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'updated_at' => date('Y-m-d H:i:s')
                                );
                                $this->common_model->addRecords(INTRADAY_SCANNER, $addData);
                            }
                            unset($addData, $updateData);
                        }
                    }

                    $this->session->set_flashdata('item_success', sprintf(ITEM_ADD_SUCCESS, 'Intraday Scanner Data'));
                    redirect($this->url.'/view-all');
                } else {
                    $this->session->set_flashdata('general_error', GENERAL_ERROR);
                    redirect($this->url.'/view-all');
                }
            }
        }
        /* Load admin view */
        load_admin_view('upload-intraday-scanner-data', $data);
    }

    /**
    * File uploading call back to check for valid files
    * @param Array $_FILES
    */
    function _verify_attachment($data)
    {   
        if(!empty($_FILES['attachment']['name'])) {
            $valid_extension = array('xlx', 'xlsx');
            $type = $_FILES['attachment']['name'];
            $type = explode('.', $type);
            if(!in_array($type[1], $valid_extension)) {
                $this->form_validation->set_message('_verify_attachment', 'Please Select Vaild Excel FIle To Upload Data.');
                return FALSE;
            }
        }
    }

    /**
    * View All Intraday Scanner Data
    */
    public function viewAll() {
        is_logged_in($this->url.'/view-all');
        is_have_access('can_upload_intraday_scanner_data');
        $data = array();
        $data['meta_title'] = 'View All';
        $data['small_text'] = 'Feedback Forms';
        $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'view_all_intraday_scanner_data');
        $data['session_data'] = admin_session_data();
        $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
        $data['user_info'] = get_user($data['session_data']['user_id']);

        /* Fetch Data */
        $offset = 0;
        if(isset($_GET['per_page'])) {
            $offset = $_GET['per_page'];
        } else {
            $offset = $this->uri->segment(4);
        }

        $data['offset'] = $offset;
        $data['users'] = '';
        $data['pagination'] = '';
        
        $condition = '';
        $catArr = array();
        $cats = $this->common_model->getAllRecords(INTRADAY_SCANNER_CATEGORY);
        if(!empty($cats)) {
            foreach($cats as $val){
                $catArr[$val['id']] = $val['title'];
            }
        }
        $data['cats'] = $catArr;

        if(!empty($_GET['intraday_category_id'])) {
            $condition .= ' intraday_category_id = '.$_GET['intraday_category_id'];
        }

        if(!empty($_GET['publish_date'])) {
            $condition .= ' AND publish_date = "'.date('Y-m-d', strtotime($_GET['publish_date'])).'"';
        }

        $data['hawk'] = $this->common_model->getPaginateRecordsByOrderByLikeCondition(INTRADAY_SCANNER, (isset($_GET['s'])) ? array('script_name', 'company_name') : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', 'id', 'DESC', RESULT_PER_PAGE, $offset, $condition);
        if(count($data['hawk']) > 0) {
            /* Pagination records */
            $url = get_cms_url().$this->url.'/view-all';
            $total_records = $this->common_model->getTotalPaginateRecordsByOrderByLikeCondition(INTRADAY_SCANNER, (isset($_GET['s'])) ? array('script_name', 'company_name') : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', $condition);
            $data['pagination'] = custom_pagination($url, $total_records, RESULT_PER_PAGE, 'right', '', http_build_query($_GET, '', '&'));
        }

        /* Load admin view */
        load_admin_view('view-all-intraday-scanner-data', $data);
    }

    /**
    * Delete Data
    * @param $id
    */
    public function delete() {
        is_logged_in($this->url.'/view-all');
        is_have_access('can_upload_intraday_scanner_data');
        $id = $this->uri->segment(4);
        if($id) {
            /* Deleting Data */
            $this->common_model->deleteRecord(INTRADAY_SCANNER, array('id' => $id));

            $this->session->set_flashdata('item_success', sprintf(ITEM_DELETE_SUCCESS, 'Intraday Scanner Data'));
            redirect($this->url.'/view-all');
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/view-all');
        }
    }
}