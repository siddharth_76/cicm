<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plans extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->module = $this->router->fetch_module();
        $this->class = $this->router->fetch_class();
        $this->url = $this->module.'/'.$this->class;
    }

    /**
	* Index of plans controller
    */
    public function index() {
    	is_logged_in($this->url.'/view-all');
    	redirect($this->url.'/view-all');
    	exit();
    }

    /**
    * View All Plans
    */
    public function viewAll() {
        is_logged_in($this->url.'/view-all');
        is_have_access('can_see_membership_palns');
        $data = array();
        $data['meta_title'] = 'View Plans';
        $data['small_text'] = 'All Membership Plans';
        $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'view_all_membership_plans');
        $data['session_data'] = admin_session_data();
        $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
        $data['user_info'] = get_user($data['session_data']['user_id']);

        /* Fetch Data */
        $offset = 0;
        if(isset($_GET['per_page'])) {
            $offset = $_GET['per_page'];
        } else {
            $offset = $this->uri->segment(4);
        }

        $data['offset'] = $offset;
        $data['users'] = '';
        $data['pagination'] = '';
        
        $condition = '';

        $data['plans'] = $this->common_model->getPaginateRecordsByOrderByLikeCondition(MEMBERSHIP_PLAN, (isset($_GET['s'])) ? array('plan_id', 'plan_text', 'plan_description', 'plan_amount') : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', 'plan_id', 'DESC', RESULT_PER_PAGE, $offset, $condition);
        if(count($data['plans']) > 0) {
            /* Fetching Membership Plan Categories */
            $planCatsArr = array();
            $planCats = $this->common_model->getAllRecords(MEMBERSHIP_PLAN_CATEGORY);
            if(!empty($planCats)) {
                foreach($planCats as $val) {
                    $planCatsArr[$val['id']] = $val['title'];
                }
            }
            $data['plan_categories'] = $planCatsArr;

            /* Pagination records */
            $url = get_cms_url().$this->url.'/view-all';
            $total_records = $this->common_model->getTotalPaginateRecordsByOrderByLikeCondition(MEMBERSHIP_PLAN, (isset($_GET['s'])) ? array('plan_id', 'plan_text', 'plan_description', 'plan_amount') : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', $condition);
            $data['pagination'] = custom_pagination($url, $total_records, RESULT_PER_PAGE, 'right', '', http_build_query($_GET, '', '&'));
        }

        /* Load admin view */
        load_admin_view('view-all-plans', $data);
    }

    /**
    * Add new plan
    * @param $_POST
    */
    public function addNew() {
        is_logged_in($this->url.'/add-new');
        is_have_access('can_add_membership_palns');
        $data = array();
        $data['meta_title'] = 'Add New';
        $data['small_text'] = 'Plan';
        $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'add_new_plan');
        $data['session_data'] = admin_session_data();
        $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
        $data['plan_categories'] = $this->common_model->getAllRecords(MEMBERSHIP_PLAN_CATEGORY);
        $data['stock_market_types'] = $this->common_model->getAllRecordsOrderById(STOCK_MARKET_TYPE, 'display_order', 'ASC', array('status' => 1));
        $catArr = array();
        $cats = $this->common_model->getAllRecords(INTRADAY_SCANNER_CATEGORY);
        if(!empty($cats)) {
            foreach($cats as $val){
                $catArr[$val['id']] = $val['title'];
            }
        }
        $data['cats'] = $catArr;

        if($this->input->post('submit')) {
            $this->form_validation->set_rules('plan_category', 'Plan Category', 'trim|required');
            $this->form_validation->set_rules('plan_title', 'Plan Title', 'trim|required');
            $this->form_validation->set_rules('plan_amount', 'Plan Amount', 'trim|required|is_natural_no_zero');
            $this->form_validation->set_rules('plan_benifits_in_days', 'Plan benifits in days', 'trim|required|is_natural_no_zero');

            if($this->form_validation->run() == true){
                $addData = array(
                    'plan_created_by' => $data['session_data']['user_id'],
                    'plan_category' => $_POST['plan_category'],
                    'plan_title' => $_POST['plan_title'],
                    'plan_description' => $_POST['plan_description'],
                    'plan_amount' => $_POST['plan_amount'],
                    'plan_benifits_in_days' => $_POST['plan_benifits_in_days'],
                    'is_tips_allowed' => $_POST['is_tips_allowed'],
                    'is_intraday_scanner_allowed' => $_POST['is_intraday_scanner_allowed'],
                    'is_traders_clinic_allowed' => $_POST['is_traders_clinic_allowed'],
                    'tc_allowed_query' => ($_POST['is_traders_clinic_allowed'] == 1) ? $_POST['tc_allowed_query'] : 0,
                    'plan_status' => $_POST['plan_status'],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );

                /* Add record */
                $planId = $this->common_model->addRecords(MEMBERSHIP_PLAN, $addData);
                if($planId) {
                    /* Adding Plan Benefits */
                    if(!empty($_POST['stock_market_type'])) {
                        foreach($_POST['stock_market_type'] as $key => $val) {
                            $addData = array(
                                'benefit_plan_id' => $planId,
                                'stock_market_type' => $val,
                                'created_at' => date('Y-m-d H:i:s')
                            );
                            $this->common_model->addRecords(MEMBERSHIP_PLAN_TIP_BENEFIT, $addData);
                            unset($addData);
                        }
                    }

                    if(!empty($_POST['intrday_scanner_cats'])) {
                        foreach($_POST['intrday_scanner_cats'] as $key => $val) {
                            $addData = array(
                                'benefit_plan_id' => $planId,
                                'intrday_scanner_cat' => $val,
                                'created_at' => date('Y-m-d H:i:s')
                            );
                            $this->common_model->addRecords(MEMBERSHIP_PLAN_INTRADAY_BENEFIT, $addData);
                            unset($addData);
                        }
                    }

                    $this->session->set_flashdata('item_success', sprintf(ITEM_ADD_SUCCESS, 'Plan'));
                    redirect($this->url.'/view-all');
                } else {
                    $this->session->set_flashdata('general_error', GENERAL_ERROR);
                    redirect($this->url.'/add-new');
                }
            }
        }
        /* Load admin view */
        load_admin_view('add-new-plan', $data);
    }

    /**
    * Edit Plan
    * @param $_POST
    */
    public function edit() {
        is_logged_in($this->url.'/view-all');
        is_have_access('can_edit_membership_palns');
        $planId = $this->uri->segment(4);
        $checkPlan = $this->common_model->getSingleRecordById(MEMBERSHIP_PLAN, array('plan_id' => $planId));
        if(!empty($checkPlan)) {
            if($planId) {
                $data = array();
                $data['meta_title'] = 'Edit';
                $data['small_text'] = 'Plan';
                $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'edit_plan');
                $data['session_data'] = admin_session_data();
                $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
                $data['plan'] = $checkPlan;
                $data['plan_categories'] = $this->common_model->getAllRecords(MEMBERSHIP_PLAN_CATEGORY);
                $data['stock_market_types'] = $this->common_model->getAllRecordsOrderById(STOCK_MARKET_TYPE, 'display_order', 'ASC', array('status' => 1));
                
                $catArr = $stockMarArr = $intradayArr = array();

                $cats = $this->common_model->getAllRecords(INTRADAY_SCANNER_CATEGORY);
                if(!empty($cats)) {
                    foreach($cats as $val){
                        $catArr[$val['id']] = $val['title'];
                    }
                }
                $data['cats'] = $catArr;

                $tipBenefits = $this->common_model->getAllRecordsById(MEMBERSHIP_PLAN_TIP_BENEFIT, array('benefit_plan_id' => $planId));
                if(!empty($tipBenefits)) {
                    foreach($tipBenefits as $val) {
                        $stockMarArr[] = $val['stock_market_type'];
                    }
                }

                $intradayBenefits = $this->common_model->getAllRecordsById(MEMBERSHIP_PLAN_INTRADAY_BENEFIT, array('benefit_plan_id' => $planId));
                if(!empty($intradayBenefits)) {
                    foreach($intradayBenefits as $val) {
                        $intradayArr[] = $val['intrday_scanner_cat'];
                    }
                }

                $data['stockMarArr'] = $stockMarArr;
                $data['intradayArr'] = $intradayArr;

                if($this->input->post('submit')) {
                    $this->form_validation->set_rules('plan_category', 'Plan Category', 'trim|required');
                    $this->form_validation->set_rules('plan_title', 'Plan Title', 'trim|required');
                    $this->form_validation->set_rules('plan_amount', 'Plan Amount', 'trim|required|is_natural_no_zero');
                    $this->form_validation->set_rules('plan_benifits_in_days', 'Plan benifits in days', 'trim|required|is_natural_no_zero');

                    if($this->form_validation->run() == true){
                        $stock_market_type = $_POST['stock_market_type'];
                        $intrday_scanner_cats = $_POST['intrday_scanner_cats'];

                        if($_POST['is_traders_clinic_allowed'] == 1) {
                            $_POST['tc_allowed_query'] = $_POST['tc_allowed_query'];
                        } else {
                            $_POST['tc_allowed_query'] = 0;
                        }
                        $_POST['updated_at'] = date('Y-m-d H:i:s');

                        unset($_POST['submit'], $_POST['stock_market_type'], $_POST['intrday_scanner_cats']);

                        /* Updating plan data */
                        $this->common_model->updateRecords(MEMBERSHIP_PLAN, $_POST, array('plan_id' => $planId));

                        /* Updating plan benefit info */
                        $this->common_model->deleteRecord(MEMBERSHIP_PLAN_TIP_BENEFIT, array('benefit_plan_id' => $planId));
                        $this->common_model->deleteRecord(MEMBERSHIP_PLAN_INTRADAY_BENEFIT, array('benefit_plan_id' => $planId));
                        
                        if(!empty($stock_market_type)) {
                            foreach($stock_market_type as $key => $val) {
                                $addData = array(
                                    'benefit_plan_id' => $planId,
                                    'stock_market_type' => $val,
                                    'created_at' => date('Y-m-d H:i:s')
                                );
                                $this->common_model->addRecords(MEMBERSHIP_PLAN_TIP_BENEFIT, $addData);
                                unset($addData);
                            }
                        }

                        if(!empty($intrday_scanner_cats)) {
                            foreach($intrday_scanner_cats as $key => $val) {
                                $addData = array(
                                    'benefit_plan_id' => $planId,
                                    'intrday_scanner_cat' => $val,
                                    'created_at' => date('Y-m-d H:i:s')
                                );
                                $this->common_model->addRecords(MEMBERSHIP_PLAN_INTRADAY_BENEFIT, $addData);
                                unset($addData);
                            }
                        }
                        $this->session->set_flashdata('item_success', sprintf(ITEM_UPDATE_SUCCESS, 'Plan'));
                        redirect($this->url.'/view-all');
                    } 
                }

                /* Load admin view */
                load_admin_view('edit-plan', $data);
            } else {
                $this->session->set_flashdata('invalid_item', INVALID_ITEM);
                redirect($this->url.'/view-all');
            }
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/view-all');
        }
    }

    /**
    * Delete plan
    * @param $planId
    */
    public function delete() {
        is_logged_in($this->url.'/view-all');
        is_have_access('can_edit_membership_palns');
        $planId = $this->uri->segment(4);
        if($planId) {
            /* Deleting Plan */
            $this->common_model->deleteRecord(MEMBERSHIP_PLAN, array('plan_id' => $planId));
            $this->common_model->deleteRecord(MEMBERSHIP_PLAN_TIP_BENEFIT, array('benefit_plan_id' => $planId));
            $this->common_model->deleteRecord(MEMBERSHIP_PLAN_INTRADAY_BENEFIT, array('benefit_plan_id' => $planId));

            $this->session->set_flashdata('item_success', sprintf(ITEM_DELETE_SUCCESS, 'Plan'));
            redirect($this->url.'/view-all');
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/view-all');
        }
    }
}