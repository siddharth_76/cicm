<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->module = $this->router->fetch_module();
        $this->class = $this->router->fetch_class();
        $this->url = $this->module.'/'.$this->class;
    }

    /**
	* Index of news controller
    */
    public function index() {
    	is_logged_in($this->url.'/view-all');
    	redirect($this->url.'/view-all');
    	exit();
    }

    /**
    * View All News
    */
    public function viewAll() {
        is_logged_in($this->url.'/view-all');
        is_have_access('can_see_news');
        $data = array();
        $data['meta_title'] = 'View News';
        $data['small_text'] = 'All News';
        $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'view_all_news');
        $data['session_data'] = admin_session_data();
        $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
        $data['user_info'] = get_user($data['session_data']['user_id']);

        /* Fetch Data */
        $offset = 0;
        if(isset($_GET['per_page'])) {
            $offset = $_GET['per_page'];
        } else {
            $offset = $this->uri->segment(4);
        }

        $data['offset'] = $offset;
        $data['users'] = '';
        $data['pagination'] = '';
        
        $condition = '';

        $data['news'] = $this->common_model->getPaginateRecordsByOrderByLikeCondition(NEWS, (isset($_GET['s'])) ? array('news_id', 'title', 'description') : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', 'news_id', 'DESC', RESULT_PER_PAGE, $offset, $condition);
        if(count($data['news']) > 0) {
            /* Pagination records */
            $url = get_cms_url().$this->url.'/view-all';
            $total_records = $this->common_model->getTotalPaginateRecordsByOrderByLikeCondition(NEWS, (isset($_GET['s'])) ? array('news_id', 'title', 'description') : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', $condition);
            $data['pagination'] = custom_pagination($url, $total_records, RESULT_PER_PAGE, 'right', '', http_build_query($_GET, '', '&'));
        }

        /* Load admin view */
        load_admin_view('view-all-news', $data);
    }

    /**
    * Add new news
    * @param $_POST
    */
    public function addNew() {
        is_logged_in($this->url.'/add-new');
        is_have_access('can_post_news');
        $data = array();
        $data['meta_title'] = 'Add New';
        $data['small_text'] = 'News';
        $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'add_new_news');
        $data['session_data'] = admin_session_data();
        $data['permissions'] = get_user_permissions($data['session_data']['user_id']);

        if($this->input->post('submit')) {
            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            $this->form_validation->set_rules('status', 'Status', 'trim|required');
            $this->form_validation->set_rules('attachment', 'Attachcment', 'callback__verify_news_attachment');

            if($this->form_validation->run() == true){
                /* Upload news attachment */
                $attachment = '';
                if(!empty($_FILES['attachment']['name'])) {
                    $config['file_name']     = time().$_FILES['attachment']['name']; 
                    $config['upload_path']   = './uploads/news';
                    $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
                    $config['max_size']      = '2474898';
                    $config['max_width']     = '100024';
                    $config['max_height']    = '768000';
                    $config['remove_spaces'] = true;
                    $config['field_name']    = 'attachment';

                    $picData = upload_file($_FILES['attachment'], $config);
                    if(isset($picData) && $picData['message'] == 'success') {
                        $attachment = $picData['file_name'];
                    }
                }

                $addData = array(
                    'created_by' => $data['session_data']['user_id'],
                    'title' =>  $_POST['title'],
                    'description' => $_POST['description'],
                    'attachment' => $attachment,
                    'status' => $_POST['status'],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );

                /* Add record */
                $newsId = $this->common_model->addRecords(NEWS, $addData);
                if($newsId) {
                    /* Send Push Notification */
                    if($_POST['status'] == 1) {
                        $fcmData = array(
                            'title' => 'New News Posted',
                            'body' => 'New news has been posted on CICM, please have a look.',
                            'type' => 'new_news_posted',
                            'extra' => array('news_id' => $newsId)
                        );
                        send_fcm_notification($fcmData, get_all_device_tokens(1));
                    }

                    $this->session->set_flashdata('item_success', sprintf(ITEM_ADD_SUCCESS, 'News'));
                    redirect($this->url.'/view-all');
                } else {
                    $this->session->set_flashdata('general_error', GENERAL_ERROR);
                    redirect($this->url.'/add-new');
                }
            }
        }
        /* Load admin view */
        load_admin_view('add-new-news', $data);
    }

    /**
    * Edit News
    * @param $_POST
    */
    public function edit() {
        is_logged_in($this->url.'/view-all');
        is_have_access('can_edit_news');
        $newsId = $this->uri->segment(4);
        $checkNews = $this->common_model->getSingleRecordById(NEWS, array('news_id' => $newsId));
        if(!empty($checkNews)) {
            if($newsId) {
                $data = array();
                $data['meta_title'] = 'Edit';
                $data['small_text'] = 'News';
                $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'edit_news');
                $data['session_data'] = admin_session_data();
                $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
                $data['news'] = $checkNews;

                if($this->input->post('submit')) {
                    $this->form_validation->set_rules('title', 'Title', 'trim|required');
                    $this->form_validation->set_rules('status', 'Status', 'trim|required');
                    $this->form_validation->set_rules('attachment', 'Attachcment', 'callback__verify_news_attachment');

                    if($this->form_validation->run() == true){
                        /* Upload news attachment */
                        if(!empty($_FILES['attachment']['name'])) {
                            $config['file_name']     = time().$_FILES['attachment']['name']; 
                            $config['upload_path']   = './uploads/news';
                            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
                            $config['max_size']      = '2474898';
                            $config['max_width']     = '100024';
                            $config['max_height']    = '768000';
                            $config['remove_spaces'] = true;
                            $config['field_name']    = 'attachment';

                            $picData = upload_file($_FILES['attachment'], $config);
                            if(isset($picData) && $picData['message'] == 'success') {
                                $attachment = $picData['file_name'];
                            }
                        } else {
                            $attachment = (!empty($_POST['current_attachment'])) ? $_POST['current_attachment'] : '';
                        }

                        $updateData = array(
                            'title' =>  $_POST['title'],
                            'description' => $_POST['description'],
                            'attachment' => $attachment,
                            'status' => $_POST['status'],
                            'updated_at' => date('Y-m-d H:i:s')
                        );

                        /* Updating plan data */
                        $this->common_model->updateRecords(NEWS, $updateData, array('news_id' => $newsId));

                        $this->session->set_flashdata('item_success', sprintf(ITEM_UPDATE_SUCCESS, 'News'));
                        redirect($this->url.'/view-all');
                    } 
                }

                /* Load admin view */
                load_admin_view('edit-news', $data);
            } else {
                $this->session->set_flashdata('invalid_item', INVALID_ITEM);
                redirect($this->url.'/view-all');
            }
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/view-all');
        }
    }

    /**
    * Delete News
    * @param $newsId
    */
    public function delete() {
        is_logged_in($this->url.'/view-all');
        is_have_access('can_edit_news');
        $newsId = $this->uri->segment(4);
        if($newsId) {
            /* Deleting News */
            $this->common_model->deleteRecord(NEWS, array('news_id' => $newsId));

            $this->session->set_flashdata('item_success', sprintf(ITEM_DELETE_SUCCESS, 'News'));
            redirect($this->url.'/view-all');
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/view-all');
        }
    }

    /**
    * File uploading call back to check for valid files
    * @param Array $_FILES
    */
    function _verify_news_attachment($data)
    {   
        if(!empty($_FILES['attachment']['name'])) {
            $valid_extension = array('png', 'jpg', 'jpeg', 'bmp', 'gif');
            $type = $_FILES['attachment']['type'];
            $type = explode('/', $type);
            if(!in_array($type[1], $valid_extension)) {
                $this->form_validation->set_message('_verify_news_attachment', IMG_ERROR);
                return FALSE;
            }
        }
    }
}