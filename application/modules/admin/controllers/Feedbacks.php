<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedbacks extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->module = $this->router->fetch_module();
        $this->class = $this->router->fetch_class();
        $this->url = $this->module.'/'.$this->class;
    }

    /**
	* Index of feedback controller
    */
    public function index() {
    	is_logged_in($this->url.'/view-all');
    	redirect($this->url.'/view-all');
    	exit();
    }

    /**
    * View All Feedbacks Forms
    */
    public function viewAll() {
        is_logged_in($this->url.'/view-all');
        is_have_access('can_see_users_feedback');
        $data = array();
        $data['meta_title'] = 'View All';
        $data['small_text'] = 'Feedback Forms';
        $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'view_all_feedback_forms');
        $data['session_data'] = admin_session_data();
        $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
        $data['user_info'] = get_user($data['session_data']['user_id']);

        /* Fetch Data */
        $offset = 0;
        if(isset($_GET['per_page'])) {
            $offset = $_GET['per_page'];
        } else {
            $offset = $this->uri->segment(4);
        }

        $data['offset'] = $offset;
        $data['users'] = '';
        $data['pagination'] = '';
        
        $condition = '';

        $data['forms'] = $this->common_model->getPaginateRecordsByOrderByLikeCondition(FEEDBACK_FORM_ELEMENT, (isset($_GET['s'])) ? array() : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', 'id', 'DESC', RESULT_PER_PAGE, $offset, $condition);
        if(count($data['forms']) > 0) {
            /* Pagination records */
            $url = get_cms_url().$this->url.'/view-all';
            $total_records = $this->common_model->getTotalPaginateRecordsByOrderByLikeCondition(FEEDBACK_FORM_ELEMENT, (isset($_GET['s'])) ? array() : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', $condition);
            $data['pagination'] = custom_pagination($url, $total_records, RESULT_PER_PAGE, 'right', '', http_build_query($_GET, '', '&'));
        }

        /* Load admin view */
        load_admin_view('view-all-feedback-forms', $data);
    }

    /**
     * Create Feedback Form
     */
    public function create() {
        is_logged_in($this->url.'/create');
        is_have_access('can_access_feedback_forms');
        $data = array();
        $data['meta_title'] = 'Add New';
        $data['small_text'] = 'Feedback Form';
        $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'add_new_feedback_form');
        $data['session_data'] = admin_session_data();
        $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
        $data['user_info'] = get_user($data['session_data']['user_id']);

        if(isset($_POST['submit'])) {
            $addData = array(   
                'elements' => serialize($_POST['questions']),
                'show_comment_box' => $_POST['show_comment_box'],
                'status' => $_POST['status'],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
            /* Adding Records */
            $eleId = $this->common_model->addRecords(FEEDBACK_FORM_ELEMENT, $addData);
            if(!empty($eleId)){
                /* managing form publish state */
                $this->common_model->updateCustomSql('UPDATE `'.FEEDBACK_FORM_ELEMENT.'` SET status = 0 WHERE id != '.$eleId);

                /* Notify to users */
                if($_POST['status'] == 1) {
                    $fcmData = array(
                        'title' => 'New Feedback Form',
                        'body' => 'Dear CICM users, new feedback form has been posted. Please submit your feedback to us.',
                        'type' => 'new_feedback_form_posted',
                        'extra' => array('id' => $formId)
                    );
                    send_fcm_notification($fcmData, get_all_device_tokens(1));
                }

                $this->session->set_flashdata('item_success', sprintf(ITEM_ADD_SUCCESS, 'Feedback Form'));
                redirect($this->url.'/view-all');
            } else {
                $this->session->set_flashdata('general_error', GENERAL_ERROR);
                redirect($this->url.'/view-all');
            }
        }

        /* Load admin view */
        load_admin_view('add-new-feedback-form', $data);
    }

    /**
     * Edit Feedback Form
     */
    public function edit() {
        is_logged_in($this->url.'/create');
        is_have_access('can_access_feedback_forms');
        $formId = $this->uri->segment(4);
        $checkForm = $this->common_model->getSingleRecordById(FEEDBACK_FORM_ELEMENT, array('id' => $formId));
        if(!empty($formId) && !empty($checkForm)){
            $data = array();
            $data['meta_title'] = 'Edit';
            $data['small_text'] = 'Feedback Form';
            $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'edit_feedback_form');
            $data['session_data'] = admin_session_data();
            $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
            $data['user_info'] = get_user($data['session_data']['user_id']);
            $data['form'] = $checkForm;

            if(isset($_POST['submit'])) {
                $updateData = array(   
                    'elements' => serialize($_POST['questions']),
                    'show_comment_box' => $_POST['show_comment_box'],
                    'status' => $_POST['status'],
                    'updated_at' => date('Y-m-d H:i:s')
                );

                /* Updatin Data */
                $this->common_model->updateRecords(FEEDBACK_FORM_ELEMENT, $updateData, array('id' => $formId));
                
                /* managing form publish state */
                if($_POST['status'] == 1) {
                    $this->common_model->updateCustomSql('UPDATE `'.FEEDBACK_FORM_ELEMENT.'` SET status = 0 WHERE id != '.$formId);
                }

                $this->session->set_flashdata('item_success', sprintf(ITEM_UPDATE_SUCCESS, 'Feedback Form'));
                redirect($this->url.'/view-all');
            }
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/view-all');   
        }

        /* Load admin view */
        load_admin_view('edit-feedback-form', $data);
    }
    /**
     * Delete Form
     */
    public function deleteForm() {
        is_logged_in($this->url.'/view-all');
        is_have_access('can_access_feedback_forms');
        $formId = $this->uri->segment(4);
        if($formId) {
            /* Deleting Feedback Form */
            $this->common_model->deleteRecord(FEEDBACK_FORM_ELEMENT, array('id' => $formId));

            $this->session->set_flashdata('item_success', sprintf(ITEM_DELETE_SUCCESS, 'Feedback Form'));
            redirect($this->url.'/view-all');
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/view-all');
        }
    }

    /**
    * Manage All Feedbacks form users
    */
    public function manage() {
        is_logged_in($this->url.'/manage');
        is_have_access('can_see_users_feedback');
        $data = array();
        $data['meta_title'] = 'Manage';
        $data['small_text'] = 'Submitted Feedback Forms';
        $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'manage_submitted_feedback_forms');
        $data['session_data'] = admin_session_data();
        $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
        $data['user_info'] = get_user($data['session_data']['user_id']);

        /* Fetch Data */
        $offset = 0;
        if(isset($_GET['per_page'])) {
            $offset = $_GET['per_page'];
        } else {
            $offset = $this->uri->segment(4);
        }

        $data['offset'] = $offset;
        $data['users'] = '';
        $data['pagination'] = '';
        
        $condition = '';

        $data['submission'] = $this->common_model->getPaginateRecordsByOrderByLikeCondition(FEEDBACK_FORM_SUBMISSION, (isset($_GET['s'])) ? array() : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', 'id', 'DESC', RESULT_PER_PAGE, $offset, $condition);
        if(count($data['submission']) > 0) {
            /* Pagination records */
            $url = get_cms_url().$this->url.'/manage';
            $total_records = $this->common_model->getTotalPaginateRecordsByOrderByLikeCondition(FEEDBACK_FORM_SUBMISSION, (isset($_GET['s'])) ? array() : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', $condition);
            $data['pagination'] = custom_pagination($url, $total_records, RESULT_PER_PAGE, 'right', '', http_build_query($_GET, '', '&'));
        }

        /* Load admin view */
        load_admin_view('manage-submitted-feedback-forms', $data);
    }

    /**
    * View Feedback Submission
    */
    public function view() {
        is_logged_in($this->url.'/manage');
        is_have_access('can_see_users_feedback');
        $feedId = $this->uri->segment(4);
        $checkFeed = $this->common_model->getSingleRecordById(FEEDBACK_FORM_SUBMISSION, array('id' => $feedId));
        if(!empty($feedId) && !empty($checkFeed)) {
            $data = array();
            $data['meta_title'] = 'View';
            $data['small_text'] = 'Feedback Submission Data';
            $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'view_feedback_submission_data');
            $data['session_data'] = admin_session_data();
            $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
            $data['user_info'] = get_user($data['session_data']['user_id']);
            $data['feed'] = $checkFeed;
            $data['form'] = $this->common_model->getSingleRecordById(FEEDBACK_FORM_ELEMENT, array('id' => $checkFeed['form_id']));
            $data['user'] = get_user($checkFeed['user_id']);
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/manage');   
        }

        /* Load admin view */
        load_admin_view('view-feedback-submission-detail', $data);
    }
}