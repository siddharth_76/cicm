<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tradersclinic extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->module = $this->router->fetch_module();
        $this->class = $this->router->fetch_class();
        $this->url = $this->module.'/'.$this->class;

        /* Load Excel Library */
        $this->load->library('EXcel');
    }

    /**
	* Index of tradersclinic controller
    */
    public function index() {
    	is_logged_in($this->url.'/view-all');
    	redirect($this->url.'/view-all');
    	exit();
    }

    /**
    * View All Traders Clinic Data
    */
    public function viewAll() {
        is_logged_in($this->url.'/view-all');
        is_have_access('can_see_traders_clinic_enquiry');
        $data = array();
        $data['meta_title'] = 'View All';
        $data['small_text'] = 'Traders Clinic Data';
        $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'view_all_traders_clinic_data');
        $data['session_data'] = admin_session_data();
        $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
        $data['user_info'] = get_user($data['session_data']['user_id']);

        /* Fetch Data */
        $offset = 0;
        if(isset($_GET['per_page'])) {
            $offset = $_GET['per_page'];
        } else {
            $offset = $this->uri->segment(4);
        }

        $data['offset'] = $offset;
        $data['users'] = '';
        $data['pagination'] = '';
        
        $condition = '';

        $data['traders'] = $this->common_model->getPaginateRecordsByOrderByLikeCondition(TC_FORM_SUBMISSION, (isset($_GET['s'])) ? array('first_name', 'last_name', 'phone_number', 'comment') : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', 'id', 'DESC', RESULT_PER_PAGE, $offset, $condition);
        if(count($data['traders']) > 0) {
            $data['scripts'] = array('1' => 'Stock', '2' => 'F&O', '3' => 'Commodities', '4' => 'Currency');
            $data['vision'] = array('1' => 'Buy', '2' => 'Sell', '3' => 'Hold');

            /* Pagination records */
            $url = get_cms_url().$this->url.'/view-all';
            $total_records = $this->common_model->getTotalPaginateRecordsByOrderByLikeCondition(TC_FORM_SUBMISSION, (isset($_GET['s'])) ? array('first_name', 'last_name', 'phone_number', 'comment') : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', $condition);
            $data['pagination'] = custom_pagination($url, $total_records, RESULT_PER_PAGE, 'right', '', http_build_query($_GET, '', '&'));
        }

        /* Load admin view */
        load_admin_view('view-all-traders-clinic-data', $data);
    }

    /**
    * View
    */
    public function view() {
        is_logged_in($this->url.'/view');
        is_have_access('can_see_traders_clinic_conversations');
        $ticketId = $this->uri->segment(4);
        $checkTicket = $this->common_model->getSingleRecordById(TC_FORM_SUBMISSION, array('id' => $ticketId));

        if(!empty($ticketId) && !empty($checkTicket)) {
            $data = array();
            $data['meta_title'] = 'View';
            $data['small_text'] = 'Traders Clinic Conversation';
            $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'view_all_traders_clinic_data');
            $data['session_data'] = admin_session_data();
            $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
            $data['user_info'] = get_user($data['session_data']['user_id']);
            $data['ticket_id'] = $ticketId;
            $data['ticket'] = $checkTicket;
            $data['userData'] = get_user($checkTicket['user_id']);

            /* Fetch Data */
            $condition = array('tc_id' => $ticketId);
            $data['messages'] = $this->common_model->getAllRecordsOrderById(TC_FORM_MESSAGE, 'id', 'DESC', $condition);

            /* Load admin view */
            load_admin_view('view-traders-clinic-conversation', $data);
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/view-all');
        }
    }

    /**
     * Send Message
     */
    public function send() {
        is_logged_in($this->url.'/view-all');
        is_have_access('can_reply_on_traders_clinic');
        $ticketId = $this->uri->segment(4);
        $checkTicket = $this->common_model->getSingleRecordById(TC_FORM_SUBMISSION, array('id' => $ticketId));
        $sessionData = admin_session_data();

        if(!empty($ticketId) && !empty($checkTicket)) {
            if(isset($_POST['submit'])) {
                $userData = $this->common_model->getSingleRecordById(USER, array('id' => $checkTicket['user_id']));
                $addData = array(
                    'tc_id' => $ticketId,
                    'from_id' => $sessionData['user_id'],
                    'message' => $_POST['message'],
                    'status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );

                $msgId = $this->common_model->addRecords(TC_FORM_MESSAGE, $addData);
                if(!empty($msgId)) {
                    /* Send Push Notification */
                    if(!empty($userData['device_token'])) {
                        $fcmData = array(
                            'title' => 'New Traders Clinic Feedback',
                            'body' => $_POST['message'],
                            'type' => 'new_traders_clinic_message',
                            'extra' => array('tc_id' => $ticketId, 'message_id' => $msgId)
                        );
                        send_fcm_notification($fcmData, $userData['device_token']);
                    }

                    $this->session->set_flashdata('item_success', sprintf(ITEM_ADD_SUCCESS, 'Message'));
                    redirect($this->url.'/view/'.$ticketId);
                } else {
                    $this->session->set_flashdata('general_error', GENERAL_ERROR);
                    redirect($this->url.'/view/'.$ticketId);
                }
            }
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/view-all');
        }
    }

    public function updatestatus() {
        if(isset($_POST['tc_id']) && isset($_POST['tc_status'])) {
            $this->common_model->updateRecords(TC_FORM_SUBMISSION, array('status' => $_POST['tc_status']), array('id' => $_POST['tc_id']));
            $text = ($_POST['tc_status'] == 1) ? 'reopended' : 'closed';
            echo 'This conversation has been '.$text.' successfully.';
        } else {
            echo 'Unable to process request, please try again later.';
        }
    }
}