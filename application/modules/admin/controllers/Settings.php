<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->module = $this->router->fetch_module();
        $this->class = $this->router->fetch_class();
        $this->url = $this->module.'/'.$this->class;
    }

    /**
	* Index of setting controller
    */
    public function index() {
    	is_logged_in($this->url.'/general');
    	redirect($this->url.'/general');
    	exit();
    }

    /**
	* General Settings of CMS
    */
    public function general() {
    	is_logged_in($this->url.'/general');
    	is_have_access('can_change_settings');
		$data = array();
		$data['meta_title'] = 'General Settings';
		$data['small_text'] = '';
		$data['body_class'] = array('admin_dashboard', 'is_logged_in', 'general_settings');
		$data['session_data'] = admin_session_data();
		$data['permissions'] = get_user_permissions($data['session_data']['user_id']);
		$data['user_info'] = get_user($data['session_data']['user_id']);

		if($this->input->post('submit')) {
			$this->form_validation->set_rules('app_email', 'App Email', 'trim|valid_email|required');
			$this->form_validation->set_rules('app_logo', 'App Logo', 'callback__verify_logo');
			$this->form_validation->set_rules('app_favicon', 'App Favicon', 'callback__verify_favicon');

            if($this->form_validation->run() == true){
				/* Upload logo and favicon */
				if(!empty($_FILES['app_logo']['name'])) {
					$config1['file_name']     = time().$_FILES['app_logo']['name']; 
	                $config1['upload_path']   = './uploads/site';
	                $config1['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
	                $config1['max_size']      = '2474898';
	                $config1['max_width']     = '100024';
	                $config1['max_height']    = '768000';
	                $config1['remove_spaces'] = true;
	                $config1['field_name']	  = 'app_logo';

	                $logoData = upload_file($_FILES['app_logo'], $config1);
	                if(isset($logoData) && $logoData['message'] == 'success') {
	                	$_POST['app_logo'] = $logoData['file_path'];
	                }
				} else {
					$_POST['app_logo'] = $_POST['old_app_logo'];
				}

				if(!empty($_FILES['app_favicon']['name'])) {
					$config2['file_name']     = time().$_FILES['app_favicon']['name']; 
	                $config2['upload_path']   = './uploads/site';
	                $config2['allowed_types'] = 'ico|png|jpg|jpeg';
	                $config2['max_size']      = '2474898';
	                $config2['max_width']     = '100024';
	                $config2['max_height']    = '768000';
	                $config2['remove_spaces'] = true;
	                $config2['field_name']	  = 'app_favicon';

	                $faviconData = upload_file($_FILES['app_favicon'], $config2);
	                if(isset($faviconData) && $faviconData['message'] == 'success') {
	                	$_POST['app_favicon'] = $faviconData['file_path'];
	                }
				} else {
					$_POST['app_favicon'] = $_POST['old_church_favicon'];
				}

				$_POST['copyright_text'] = str_replace(PHP_EOL, '<br/>', $_POST['copyright_text']);
				$_POST['footer_name_address'] = str_replace(PHP_EOL, '<br/>', $_POST['footer_name_address']);
				
				/* Updating setting options */
				unset($_POST['old_app_logo'], $_POST['old_app_favicon'], $_POST['submit']);
				foreach($_POST as $key => $val) {
					update_option($key, $val);
				}

				/* Set session flashdata and redirect */
				$this->session->set_flashdata('setting_success', SETTING_SUCCESS);
				redirect(redirect($this->url.'/general'));
			}
		}

		/* Load admin view */
		load_admin_view('general-settings', $data);
    }

    /**
	* Admin Profile
	*/
	public function profile() {
		is_logged_in($this->url.'/profile');
		$data = array();
		$data['meta_title'] = 'Profile';
		$data['small_text'] = 'Edit Profile';
		$data['body_class'] = array('admin_dashboard', 'is_logged_in', 'edit_admin_profile');
		$data['session_data'] = admin_session_data();
		$data['permissions'] = get_user_permissions($data['session_data']['user_id']);
		$data['user_info'] = $this->common_model->getSingleRecordById(USER, array('id' => $data['session_data']['user_id']));
		$userId = $data['session_data']['user_id'];

		if($this->input->post('submit')) {
			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback__verify_email_address');
			$this->form_validation->set_rules('profile_pic', 'Profile Picture', 'callback__verify_profile_picture');

			$changePassword = 0;
			if(!empty($_POST['password']) && !empty($_POST['repeat_password'])) {
				$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
				$this->form_validation->set_rules('repeat_password', 'Confirm Password', 'trim|required|min_length[6]|matches[password]');
				$changePassword = 1;
			}

			if($this->form_validation->run() == true) {
				/* Upload profile picture */
				if(!empty($_FILES['profile_pic']['name'])) {
					$config['file_name']     = time().$_FILES['profile_pic']['name']; 
	                $config['upload_path']   = './uploads/users';
	                $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
	                $config['max_size']      = '2474898';
	                $config['max_width']     = '100024';
	                $config['max_height']    = '768000';
	                $config['remove_spaces'] = true;
	                $config['field_name']	 = 'profile_pic';

	                $picData = upload_file($_FILES['profile_pic'], $config);
	                if(isset($picData) && $picData['message'] == 'success') {
	                	$userData['profile_pic'] = $picData['file_name'];
	                }
				} else {
					$userData['profile_pic'] = (isset($_POST['current_profile_pic'])) ? $_POST['current_profile_pic'] : '';
				}

				/* Update user profile data */
				$wherreArr = array('id' => $userId);

				$userData = array(
					'name' => $_POST['name'],
					'email' => $_POST['email'],
					'profile_pic' => $userData['profile_pic']
				);

				if(!empty($_POST['password']) && !empty($_POST['repeat_password'])) {
					$userData['password'] = md5($_POST['password']);
				}

				$this->common_model->updateRecords(USER, $userData, $wherreArr);

				/* Redirect to login if password has been changed */
				if($changePassword == 1) {
					$this->session->unset_userdata('admin_session_data');
					$this->session->set_flashdata('change_password_success', LOGIN_TO_CONTINUE);
					redirect(get_cms_url('admin/login?logged_out=true'));
					exit();
				}

				/* Set flash data and redirect to the profile page */
				$this->session->set_flashdata('admin_profile_update_success', ADMIN_PROFILE_UPDATE_SUCCESS);
				redirect($this->url.'/profile');
			}
		}

		/* Load admin view */
		load_admin_view('profile', $data);
	}

	/**
	* Callback function to check email in users entity
	* @param Array $data
	*/
	function _verify_email_address($data){
        $email_address = $this->input->post('email');
		$sql = 'SELECT * FROM '.USER.' WHERE `email` = "'.$email_address.'" AND (`user_type_id` = '.SUPER_ADMIN_USER_TYPE.' OR `user_type_id` = '.SUB_ADMIN_USER_TYPE.')';
		$user_email_exists = $this->common_model->getCustomSqlCount($sql);

		if($user_email_exists > 1) {
			$this->form_validation->set_message('_verify_signup_email_address', EMAIL_EXISTS);
			return FALSE;
		}
	}

	/**
	* SEO Settings
    */
    public function seo() {
    	is_logged_in($this->url.'/seo');
    	is_have_access('can_change_settings');
		$data = array();
		$data['meta_title'] = 'SEO Settings';
		$data['small_text'] = 'Search Engine Optimization';
		$data['body_class'] = array('admin_dashboard', 'is_logged_in', 'seo_settings');
		$data['session_data'] = admin_session_data();
		$data['permissions'] = get_user_permissions($data['session_data']['user_id']);
		$data['user_info'] = get_user($data['session_data']['user_id']);

		if($this->input->post('submit')) {
			unset($_POST['submit']);
			foreach($_POST as $key => $val) {
				update_option($key, $val);
			}

			$this->session->set_flashdata('setting_success', SETTING_SUCCESS);
			redirect(redirect($this->url.'/seo'));
		}

		/* Load admin view */
		load_admin_view('seo-settings', $data);
    }

    /**
	* App Settings
    */
    public function app() {
    	is_logged_in($this->url.'/seo');
    	is_have_access('can_change_settings');
		$data = array();
		$data['meta_title'] = 'App Settings';
		$data['small_text'] = 'Application Configurations';
		$data['body_class'] = array('admin_dashboard', 'is_logged_in', 'app_settings');
		$data['session_data'] = admin_session_data();
		$data['permissions'] = get_user_permissions($data['session_data']['user_id']);
		$data['user_info'] = get_user($data['session_data']['user_id']);
		$shareImages = $this->common_model->getAllRecords(SHARE_IMAGE);
		$data['share_images'] = $shareImages;

		if($this->input->post('submit')) {
			unset($_POST['submit']);
			foreach($_POST as $key => $val) {
				update_option($key, $val);
			}

			$path = "uploads/site/";
			$img = 1;
			if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {
				$addData = $updateData = array();
				foreach ($_FILES['share_image']['name'] as $f => $name) {
		        	if(!empty($name)) {
		        		$file_name = time().'-share-'.$img++.$name;
			            if(move_uploaded_file($_FILES["share_image"]["tmp_name"][$f], $path.$file_name)) {
			            	$id = $f + 1;
			            	$checkImg = $this->common_model->getSingleRecordById(SHARE_IMAGE, array('id' => $id));
			            	if(!empty($checkImg)) {
			            		$updateData = array(
			            			'image' => $file_name,
			            			'created_at' => date('Y-m-d H:i:s'),
			            			'updated_at' => date('Y-m-d H:i:s')
		            			);
		            			$this->common_model->updateRecords(SHARE_IMAGE, $updateData, array('id' => $id));
			            	} else {
				            	$addData = array(
				            		'image' => $file_name,
				            		'created_at' => date('Y-m-d H:i:s'),
				            		'updated_at' => date('Y-m-d H:i:s')
			            		);
			            		$this->common_model->addRecords(SHARE_IMAGE, $addData);
			            	}
			            }
			        }
			        unset($addData, $updateData);
				}
			}

			$this->session->set_flashdata('setting_success', SETTING_SUCCESS);
			redirect(redirect($this->url.'/app'));
		}

		/* Load admin view */
		load_admin_view('app-settings', $data);
    }

    /**
	* Features Settings
    */
    public function feature() {
    	is_logged_in($this->url.'/seo');
    	is_have_access('can_change_settings');
		$data = array();
		$data['meta_title'] = 'Features Settings';
		$data['small_text'] = 'Application Configurations';
		$data['body_class'] = array('admin_dashboard', 'is_logged_in', 'features_settings');
		$data['session_data'] = admin_session_data();
		$data['permissions'] = get_user_permissions($data['session_data']['user_id']);
		$data['user_info'] = get_user($data['session_data']['user_id']);

		if($this->input->post('submit')) {
			update_option('active_features_for_membership_expired_users', serialize($_POST['features']));
			update_option('default_features_for_sub_admin_users', serialize($_POST['sub_admin_features']));
			update_option('default_features_for_staff_admin_users', serialize($_POST['staff_user_features']));

			$this->session->set_flashdata('setting_success', SETTING_SUCCESS);
			redirect(redirect($this->url.'/feature'));
		}

		/* Load admin view */
		load_admin_view('feature-settings', $data);
    }

    /**
	* File uploading call back to check for cvalid files
	* @param Array $_FILES
    */
    function _verify_logo($data)
	{	
		if(!empty($_FILES['app_logo']['name'])) {
			$valid_extension = array('png', 'jpg', 'jpeg', 'bmp', 'gif');
			$type = $_FILES['app_logo']['type'];
			$type = explode('/', $type);
			if(!in_array($type[1], $valid_extension)) {
				$this->form_validation->set_message('_verify_logo', LOGO_ERROR);
				return FALSE;
			}
		}
	}

	/**
	* File uploading call back to check for cvalid files
	* @param Array $_FILES
    */
    function _verify_profile_picture($data)
	{	
		if(!empty($_FILES['profile_picture']['name'])) {
			$valid_extension = array('png', 'jpg', 'jpeg', 'bmp', 'gif');
			$type = $_FILES['profile_picture']['type'];
			$type = explode('/', $type);
			if(!in_array($type[1], $valid_extension)) {
				$this->form_validation->set_message('_verify_profile_picture', IMG_ERROR);
				return FALSE;
			}
		}
	}

	/**
	* File uploading call back to check for cvalid files
	* @param Array $_FILES
    */
    function _verify_favicon($data)
	{	
		if(!empty($_FILES['church_favicon']['name'])) {
			$valid_extension = array('ico', 'png', 'jpg', 'jpeg');
			$type = $_FILES['church_favicon']['type'];
			$type = explode('/', $type);
			if(!in_array($type[1], $valid_extension)) {
				$this->form_validation->set_message('_verify_favicon', FAVICON_ERROR);
				return FALSE;
			}
		}
	}
}
