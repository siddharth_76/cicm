<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notifications extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->module = $this->router->fetch_module();
        $this->class = $this->router->fetch_class();
        $this->url = $this->module.'/'.$this->class;
    }

    /**
	* Index of notifications controller
    */
    public function index() {
    	is_logged_in($this->url.'/manage');
    	redirect($this->url.'/manage');
    	exit();
    }

    /**
    * Manage All Notifications
    */
    public function manage() {
        is_logged_in($this->url.'/manage');
        is_have_access('can_access_notifications');
        $data = array();
        $data['meta_title'] = 'Manage Notifications';
        $data['small_text'] = 'All Notifications';
        $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'view_all_notifications');
        $data['session_data'] = admin_session_data();
        $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
        $data['user_info'] = get_user($data['session_data']['user_id']);

        /* Fetch Data */
        $offset = 0;
        if(isset($_GET['per_page'])) {
            $offset = $_GET['per_page'];
        } else {
            $offset = $this->uri->segment(4);
        }

        $data['offset'] = $offset;
        $data['users'] = '';
        $data['pagination'] = '';
        
        $condition = '';
        if(!empty($_GET['type'])) {
            $condition .= ' type = "'.$_GET['type'].'"';
        }

        if(isset($_GET['is_seen']) && $_GET['is_seen'] >= 0) {
            $condition .= ' AND is_seen = '.$_GET['is_seen'];
        }

        $data['notifications'] = $this->common_model->getPaginateRecordsByOrderByLikeCondition(NOTIFICATION, (isset($_GET['s'])) ? array('id', 'type', 'text', 'extra') : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', 'id', 'DESC', RESULT_PER_PAGE, $offset, $condition);
        if(count($data['notifications']) > 0) {
            /* Pagination records */
            $url = get_cms_url().$this->url.'/manage';
            $total_records = $this->common_model->getTotalPaginateRecordsByOrderByLikeCondition(NOTIFICATION, (isset($_GET['s'])) ? array('id', 'type', 'text', 'extra') : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', $condition);
            $data['pagination'] = custom_pagination($url, $total_records, RESULT_PER_PAGE, 'right', '', http_build_query($_GET, '', '&'));
        }

        /* Load admin view */
        load_admin_view('view-all-notifications', $data);
    }

    /**
    * View notification detail
    */
    public function view() {
        is_logged_in($this->url.'/manage');
        is_have_access('can_access_notifications');
        $notificationId = $this->uri->segment(4);
        $checkNotification = $this->common_model->getSingleRecordById(NOTIFICATION, array('id' => $notificationId));
        if(!empty($checkNotification)) {
            $data = array();
            $data['meta_title'] = 'View Notification';
            $data['small_text'] = 'Detail';
            $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'view_notification_detail');
            $data['session_data'] = admin_session_data();
            $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
            $data['user_info'] = get_user($data['session_data']['user_id']);
            $data['notification'] = $checkNotification;

            /* Update notification seen status */
            $this->common_model->updateRecords(NOTIFICATION, array('is_seen' => 1), array('id' => $notificationId));

            /* Load admin view */
            load_admin_view('view-notification-detail', $data);
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/manage');
        }
    }

    /**
     * Reset User Device
     * @param $deviceId
     */
    public function resetUserDevice() {
        is_have_access('can_reset_user_device');
        $notificationId = $this->uri->segment(4);
        $deviceId = $this->uri->segment(5);
        $deviceType = $this->uri->segment(6);
        if(!empty($deviceId) && !empty($deviceType)) {
            $deviceId = $deviceId;
            $checkDevice = $this->common_model->getRecordCount(USER, array('device_id' => $deviceId, 'device_type' => $deviceType));
            if(!empty($checkDevice)) {
                $updateData = array(
                    'device_id' => '',
                    'device_token' => '',
                    'updated_at' => date('Y-m-d H:i:s')
                );
                $this->common_model->updateRecords(USER, $updateData, array('device_id' => $deviceId, 'device_type' => $deviceType));
                $this->session->set_flashdata('item_success', sprintf(ITEM_UPDATE_SUCCESS, 'User Device'));
                redirect($this->url.'/view/'.$notificationId);
            } else {
                $this->session->set_flashdata('general_error', 'This Device Is Not Exixts In Our System.');
                redirect($this->url.'/view/'.$notificationId);
            }
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/view/'.$notificationId);
        }
    }
}