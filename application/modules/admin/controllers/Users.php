<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->module = $this->router->fetch_module();
        $this->class = $this->router->fetch_class();
        $this->url = $this->module.'/'.$this->class;
    }

    /**
	* Index of users controller
    */
    public function index() {
    	is_logged_in($this->url.'/view-all');
        is_have_access('can_see_users');
    	redirect($this->url.'/view-all');
    	exit();
    }

    /**
     * Assign app users
     */
    public function assignusers() {
        is_logged_in($this->url.'/view-all');
        is_have_access('can_assign_user');
        if(isset($_POST['staffUser']) && !empty($_POST['staffUser'])) {
            $staffUser = $_POST['staffUser'];
            $appUsers = (isset($_POST['appUsers']) && !empty($_POST['appUsers'])) ? explode(',', $_POST['appUsers']) : array();
            
            foreach($appUsers as $val){
                $this->common_model->updateRecords(USER, array('assign_to' => $staffUser, 'updated_at' => date('Y-m-d H:i:s')), array('id' => $val));
            }
        }
        exit();
    }

    /**
     * View User Profile
     * @parm $userId
     */
    public function view() {
        is_logged_in($this->url.'/view');
        is_have_access('can_see_users');
        $userId = $this->uri->segment(4);
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $userId));
        $sessionInfo = admin_session_data();

        if(!empty($userId) && !empty($checkUser)) {
            if($sessionInfo['user_type'] == SUPER_ADMIN_USER_TYPE || $sessionInfo['user_type'] == SUB_ADMIN_USER_TYPE) {
                $canAccess = 1;
            } elseif($sessionInfo['user_id'] == $checkUser['assign_to']) {
                $canAccess = 1;
            } else {
                $canAccess = 0;
            }

            if($canAccess == 1) {
                $data = array();
                $data['meta_title'] = 'View';
                $data['small_text'] = 'User Profile';
                $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'view_user_history');
                $data['session_data'] = admin_session_data();
                $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
                $data['user'] = $checkUser;
                $data['follow_ups'] = $this->common_model->getAllRecordsOrderById(USER_FOLLOW_UP, 'updated_at', 'DESC', array('user_id' => $userId));
                $data['subscription'] = $this->common_model->getAllRecordsOrderById(USER_SUBSCRIPTION_HISTORY, 'created_at', 'DESC', array('user_id' => $userId));
                $data['payments'] = $this->common_model->getAllRecordsOrderById(PAYMENT, 'updated_at', 'DESC', array('user_id' => $userId));
                $data['sharedTips'] = $this->common_model->getAllRecordsOrderById(USER_SHARE_TIP, 'shared_at', 'DESC', array('user_id' => $userId));
                $data['membershipExtension'] = $this->common_model->getAllRecordsOrderById(USER_MEMBERSHIP_EXTENSION, 'created_at', 'DESC', array('user_id' => $userId));
                $data['feedbacks'] = $this->common_model->getAllRecordsOrderById(FEEDBACK_FORM_SUBMISSION, 'created_at', 'DESC', array('user_id' => $userId));
                $data['tradersclinic'] = $this->common_model->getAllRecordsOrderById(TC_FORM_SUBMISSION, 'created_at', 'DESC', array('user_id' => $userId));
                $data['scripts'] = array('1' => 'Stock', '2' => 'F&O', '3' => 'Commodities', '4' => 'Currency');
                $data['vision'] = array('1' => 'Buy', '2' => 'Sell', '3' => 'Hold');

                /* Load admin view */
                load_admin_view('view-user-profile', $data);
            } else {
                $this->session->set_flashdata('invalid_item', "You don't have permission to access the profile.");
                redirect($this->url.'/view-all');
            }
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/view-all');
        }
    }

    /**
    * Add new user
    * @param $_POST
    */
    public function addNew() {
        is_logged_in($this->url.'/add-new');
        is_have_access('can_create_users');
        $data = array();
        $data['meta_title'] = 'Add New';
        $data['small_text'] = 'User';
        $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'add_new_user');
        $data['session_data'] = admin_session_data();
        $data['permissions'] = get_user_permissions($data['session_data']['user_id']);

        if($data['session_data']['user_type'] == SUPER_ADMIN_USER_TYPE) {
            $data['allowed_user_type'] = array(SUB_ADMIN_USER_TYPE, STAFF_USER_TYPE, APP_USER_TYPE);
        } elseif($data['session_data']['user_type'] == SUB_ADMIN_USER_TYPE) {
            $data['allowed_user_type'] = array(STAFF_USER_TYPE, APP_USER_TYPE);
        } elseif($data['session_data']['user_type'] == STAFF_USER_TYPE) {
            $data['allowed_user_type'] = array(APP_USER_TYPE);
        }

        $data['user_types'] = $this->common_model->getPaginateRecordsByOrderByLikeCondition(USER_TYPE, '', '', '', 'type', 'ASC', '', '', ' id NOT IN('.SUPER_ADMIN_USER_TYPE.')');

        if($this->input->post('submit')) {
            $this->form_validation->set_rules('user_type_id', 'User Type', 'trim|required');
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback__verify_user_email');
            $this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|integer|callback__verify_user_phone_number');

            if($_POST['user_type_id'] != APP_USER_TYPE) {
                $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
            }

            if($_POST['user_type_id'] == APP_USER_TYPE) {
                $this->form_validation->set_rules('free_membership_days', 'Free Membership Days', 'trim|required|is_natural_no_zero');
            }

            if($this->form_validation->run() == true){
                $addData = array(
                    'user_type_id' => $_POST['user_type_id'],
                    'email' => strtolower($_POST['email']),
                    'name' => $_POST['name'],
                    'phone_number' => $_POST['phone_number'],
                    'created_by' => $data['session_data']['user_id'],
                    'status' => $_POST['status'],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );

                if($_POST['user_type_id'] != APP_USER_TYPE) {
                    $addData['password'] = md5($_POST['password']);
                }

                if($_POST['user_type_id'] == APP_USER_TYPE) {
                    $addData['membership_type'] = FREE_MEMBER;
                    $addData['membership_expire_on'] = date('Y-m-d', strtotime('+ '.$_POST['free_membership_days'].' days'));
                    $addData['membership_status'] = 1;
                }

                /* Add record */
                $userId = $this->common_model->addRecords(USER, $addData);
                if($userId) {
                    /* Adding user active features data & membership extension data  */
                    if($_POST['user_type_id'] == APP_USER_TYPE) {
                        $features = application_features();
                        $featureData = '';
                        foreach($features as $val) {
                            $featureData = array(
                                'user_id' => $userId,
                                'feature_title' => $val['label'],
                                'feature_prefix' => $val['cond'],
                                'is_active' => $_POST['features'][$val['cond']],
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s'),
                            );
                            $this->common_model->addRecords(USER_ACTIVE_FEATURES, $featureData);
                            unset($featureData);
                        }

                        /* Add record on user membership extension */
                        $postData = array(
                            'user_id' => $userId,
                            'membership_extended_by' => $data['session_data']['user_id'],
                            'membership_type' => FREE_MEMBER,
                            'membership_start_date' => date('Y-m-d'),
                            'membership_end_date' => date('Y-m-d', strtotime('+ '.$_POST['free_membership_days'].' days')),
                            'membership_extended_text' => 'Membership days entended on first time registration.',
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        );
                        $this->common_model->addRecords(USER_MEMBERSHIP_EXTENSION, $postData);
                    }

                    /* Adding sub admin & staff user permissions */
                    if($_POST['user_type_id'] == SUB_ADMIN_USER_TYPE || $_POST['user_type_id'] == STAFF_USER_TYPE) {
                        foreach($_POST['permission'] as $key => $val) {
                            $this->common_model->addRecords(USER_PERMISSION, array('user_id' => $userId, 'permission_label' => $key, 'permission_value' => $val, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')));
                        }
                    }

                    $this->session->set_flashdata('item_success', sprintf(ITEM_ADD_SUCCESS, 'User'));
                    redirect($this->url.'/view-all');
                } else {
                    $this->session->set_flashdata('general_error', GENERAL_ERROR);
                    redirect($this->url.'/add-new');
                }
            }
        }
        /* Load admin view */
        load_admin_view('add-new-user', $data);
    }

    /**
    * View all users
    */
    public function viewAll() {
        is_logged_in($this->url.'/view-all');
        is_have_access('can_see_users');
        $data = array();
        $data['meta_title'] = 'View All Users';
        $data['small_text'] = 'All User';
        $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'view_all_users');
        $data['session_data'] = admin_session_data();
        $data['user_info'] = get_user($data['session_data']['user_id']);
        $data['permissions'] = get_user_permissions($data['session_data']['user_id']);

        if($data['session_data']['user_type'] == SUPER_ADMIN_USER_TYPE) {
            $data['allowed_user_type'] = array(SUB_ADMIN_USER_TYPE, STAFF_USER_TYPE, APP_USER_TYPE);
        } elseif($data['session_data']['user_type'] == SUB_ADMIN_USER_TYPE) {
            $data['allowed_user_type'] = array(STAFF_USER_TYPE, APP_USER_TYPE);
        } elseif($data['session_data']['user_type'] == STAFF_USER_TYPE) {
            $data['allowed_user_type'] = array(APP_USER_TYPE);
        }

        /* Fetch Data */
        $offset = 0;
        if(isset($_GET['per_page'])) {
            $offset = $_GET['per_page'];
        } else {
            $offset = $this->uri->segment(4);
        }

        $data['offset'] = $offset;
        $data['users'] = '';
        $data['pagination'] = '';
        $data['user_types'] = $this->common_model->getPaginateRecordsByOrderByLikeCondition(USER_TYPE, '', '', '', 'type', 'ASC', '', '', ' id NOT IN ('.SUPER_ADMIN_USER_TYPE.')');
        
        $condition = ' is_user_deleted = 0';

        /* Fetching user as per the loggedin user type */
        if($data['session_data']['user_type'] == SUPER_ADMIN_USER_TYPE) {
            $condition .= ' AND user_type_id != '.SUPER_ADMIN_USER_TYPE;
        } elseif($data['session_data']['user_type'] == SUB_ADMIN_USER_TYPE) {
            $condition .= ' AND user_type_id IN('.STAFF_USER_TYPE.', '.APP_USER_TYPE.')';
        } elseif($data['session_data']['user_type'] == STAFF_USER_TYPE) {
            $condition .= ' AND assign_to = '.$data['session_data']['user_id'].' AND user_type_id IN('.APP_USER_TYPE.')';
        }

        if(!empty($_GET['user_type_id'])) {
            $condition .= ' AND user_type_id = '.$_GET['user_type_id'];
        }

        if(!empty($_GET['membership_type'])) {
            $condition .= ' AND membership_type = '.$_GET['membership_type'];
        }

        if(!empty($_GET['custom_text'])) {
            if(is_numeric($_GET['custom_text'])) {
                $condition .= ' AND phone_number = "'.$_GET['custom_text'].'"';
            } else {
                $condition .= ' AND (name LIKE "%'.$_GET['custom_text'].'%" OR email LIKE "%'.$_GET['custom_text'].'%" OR device_id LIKE "%'.$_GET['custom_text'].'%")';
            }
        }

        $data['users'] = $this->common_model->getPaginateRecordsByOrderByLikeCondition(USER, (isset($_GET['s']) && $data['session_data']['user_type'] == SUPER_ADMIN_USER_TYPE) ? array('name', 'phone_number', 'email', 'device_id') : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', 'id', 'DESC', RESULT_PER_PAGE, $offset, $condition);
        if(count($data['users']) > 0) {
            /* Get staff users */
            $data['staffUsers'] = $this->common_model->getAllRecordsOrderById(USER, 'name', 'ASC', array('user_type_id' => STAFF_USER_TYPE, 'status' => 1, 'is_user_deleted' => 0));
            
            /* Pagination records */
            $url = get_cms_url().$this->url.'/view-all';
            $total_records = $this->common_model->getTotalPaginateRecordsByOrderByLikeCondition(USER, (isset($_GET['s']) && $data['session_data']['user_type'] == SUPER_ADMIN_USER_TYPE) ? array('name', 'phone_number', 'email', 'device_id') : '', (isset($_GET['s'])) ? $_GET['s'] : '', 'OR', $condition);
            $data['pagination'] = custom_pagination($url, $total_records, RESULT_PER_PAGE, 'right', '', http_build_query($_GET, '', '&'));
        }

        /* Load admin view */
        load_admin_view('view-all-users', $data);
    }

    /**
    * Edit User
    * @param $_POST
    */
    public function edit() {
        is_logged_in($this->url.'/view-all');
        is_have_access('can_edit_user');
        $userId = $this->uri->segment(4);
        $userData = get_user($userId);
        if(!empty($userData)) {
            if($userId) {
                $data = array();
                $data['meta_title'] = 'Edit';
                $data['small_text'] = 'User';
                $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'edit_user');
                $data['session_data'] = admin_session_data();
                $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
                $data['user_info'] = get_user($data['session_data']['user_id']);
                $data['user'] = get_user($userId);
                $data['user_types'] = $this->common_model->getPaginateRecordsByOrderByLikeCondition(USER_TYPE, '', '', '', 'type', 'ASC', '', '', ' id NOT IN('.SUPER_ADMIN_USER_TYPE.')');
                $data['membership_plan_categories'] = $this->common_model->getAllRecordsById(MEMBERSHIP_PLAN_CATEGORY, array('status' => 1));

                if($userData['membership_type'] == PREMIUM_MEMBER) {
                    $data['plan'] = $this->common_model->getSingleRecordById(MEMBERSHIP_PLAN, array('plan_id' => $userData['current_active_plan']));
                    $data['planCat'] = $this->common_model->getSingleRecordById(MEMBERSHIP_PLAN_CATEGORY, array('id' => $data['plan']['plan_category']));
                }

                if($userData['user_type_id'] != APP_USER_TYPE) {
                    $permissions = $this->common_model->getAllRecordsById(USER_PERMISSION, array('user_id' => $userId));
                    foreach($permissions as $val) {
                        $permissionArr[$val['permission_label']] = $val['permission_value'];
                    }
                    $data['user_permissions'] = $permissionArr;
                }

                if($userData['user_type_id'] == APP_USER_TYPE) {
                    $activeFeatures = $this->common_model->getAllRecordsById(USER_ACTIVE_FEATURES, array('user_id' => $userId));
                    $data['active_features'] = $activeFeatures;
                    foreach($activeFeatures as $cv) {
                        $oldFeatures[$cv['feature_prefix']] = $cv['is_active'];
                    }
                }

                if($this->input->post('submit')) {
                    $this->form_validation->set_rules('name', 'Name', 'trim|required');
                    $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback__verify_edit_email');
                    $this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|integer|callback__verify_edit_phone_number');

                    if($userData['user_type_id'] != APP_USER_TYPE) {
                        /* Check for change password validation */
                        if(!empty($_POST['password']) || !empty($_POST['confirm_password'])) {
                            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|matches[confirm_password]');
                            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required');
                        }
                    }

                    if($this->form_validation->run() == true){
                        unset($_POST['submit']);

                        /* Updating user data */
                        $updateArr = array(
                            'email' => $_POST['email'],
                            'name' => $_POST['name'],
                            'phone_number' => $_POST['phone_number'],
                            'status' => $_POST['status'],
                            'updated_at' => date('Y-m-d H:i:s')
                        );

                        if($userData['user_type_id'] == APP_USER_TYPE) {
                            $updateArr['reward_points'] = convertDBInt($_POST['reward_points']);
                            if(!empty($_POST['extend_membership'])) {
                                if($_POST['free_membership_days'] > 0) {
                                    $membershipExpireOn = date('Y-m-d', strtotime($userData['membership_expire_on'].' + '.$_POST['free_membership_days'].' days'));
                                } else {
                                    $membershipExpireOn = date('Y-m-d', strtotime($_POST['free_membership_days'].' day', strtotime($userData['membership_expire_on'])));
                                }
                                $updateArr['membership_expire_on'] = $membershipExpireOn;
                                $updateArr['membership_status'] = 1;

                                /* Add record on user membership extension */
                                $postData = array(
                                    'user_id' => $userId,
                                    'membership_extended_by' => $data['session_data']['user_id'],
                                    'membership_type' => FREE_MEMBER,
                                    'membership_start_date' => date('Y-m-d'),
                                    'membership_end_date' => $membershipExpireOn,
                                    'membership_extended_text' => 'Membership days updated from CICM admin portal',
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'updated_at' => date('Y-m-d H:i:s')
                                );
                                $this->common_model->addRecords(USER_MEMBERSHIP_EXTENSION, $postData);
                            }
                        }

                        if($userData['user_type_id'] != APP_USER_TYPE) {
                            $updateArr['password'] = md5($_POST['password']);
                        }

                        $this->common_model->updateRecords(USER, $updateArr, array('id' => $userId));
                        
                        /* Updating users active features */
                        if($userData['user_type_id'] == APP_USER_TYPE) {
                            $features = application_features();
                            $featureData = '';
                            foreach($_POST['features'] as $key => $val) {
                                $checkFeature = $this->common_model->getRecordCount(USER_ACTIVE_FEATURES, array('user_id' => $userId, 'feature_prefix' => $key));
                                if(!empty($checkFeature)) {
                                    $this->common_model->updateRecords(USER_ACTIVE_FEATURES, array('is_active' => $val), array('user_id' => $userId, 'feature_prefix' => $key));
                                } else {
                                    $featureData = array(
                                        'user_id' => $userId,
                                        'feature_title' => str_replace('_', ' ', $key),
                                        'feature_prefix' => $key,
                                        'is_active' => $val,
                                        'created_at' => date('Y-m-d H:i:s'),
                                        'updated_at' => date('Y-m-d H:i:s'),
                                    );
                                    $this->common_model->addRecords(USER_ACTIVE_FEATURES, $featureData);
                                    unset($featureData);
                                }
                            }

                            /* Adding user history change records */
                            $historyData = array(
                                'user_id' => $userId,
                                'modified_by' => $data['session_data']['user_id'],
                                'old_active_features' => serialize($oldFeatures),
                                'new_active_features' => serialize($_POST['features']),
                                'created_at' => date('Y-m-d H:i:s')
                            );
                            $this->common_model->addRecords(USER_ACTIVE_FEATURES_CHANGE_HISTORY, $historyData);

                            /* Send Push Notification */
                            if(!empty($userData['device_token'])) {
                                if($userData['device_type'] == 1){
                                    $fcmData = array(
                                        'title' => 'CICM Application Features Updated',
                                        'body' => 'Dear '.ucfirst($userData['name']).', your CICM application active features has been updated, to get a benifit of all features please subscribe CICM subscription plan.',
                                        'type' => 'active_features_changed',
                                        'extra' => array('user_id' => $userId, 'active_features' => $_POST['features'])
                                    );
                                    send_fcm_notification($fcmData, $userData['device_token']);
                                }
                            }
                        }

                        /* Updating user permissions */
                        if($userData['user_type_id'] != APP_USER_TYPE) {
                            foreach($_POST['permission'] as $key => $val) {
                                $checkPer = $this->common_model->getRecordCount(USER_PERMISSION, array('permission_label' => $key, 'user_id' => $userId));
                                if($checkPer > 0) {
                                    $this->common_model->updateRecords(USER_PERMISSION, array('permission_value' => $val, 'updated_at' => date('Y-m-d H:i:s')), array('user_id' => $userId, 'permission_label' => $key));
                                } else {
                                    $this->common_model->addRecords(USER_PERMISSION, array('user_id' => $userId, 'permission_label' => $key, 'permission_value' => $val, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')));
                                }
                            }
                        }

                        $this->session->set_flashdata('item_success', sprintf(ITEM_UPDATE_SUCCESS, 'User'));
                        redirect($this->url.'/view-all');
                    } 
                }

                /* Load admin view */
                load_admin_view('edit-user', $data);
            } else {
                $this->session->set_flashdata('invalid_item', INVALID_ITEM);
                redirect($this->url.'/view-all');
            }
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/view-all');
        }
    }

    /**
    * Delete user
    * @param $userId
    */
    public function delete() {
        is_logged_in($this->url.'/view-all');
        is_have_access('can_edit_user');
        $userId = $this->uri->segment(4);
        if($userId) {
            $adminData = admin_session_data();
            $userData = $this->common_model->getSingleRecordById(USER, array('id' => $userId));

            /* Delete Records */
            $deleteData = array(
                'email' => '',
                'phone_number' => '',
                'device_type' => 0,
                'device_id' => '',
                'device_token' => '',
                'status' => 0,
                'is_user_deleted' => 1,
                'updated_at' => date('Y-m-d H:i:s')
            );
            $this->common_model->updateRecords(USER, $deleteData, array('id' => $userId));

            /* Adding deleted users entry */
            $addData = array(
                'user_id' => $userId,
                'user_type_id' => $userData['user_type_id'],
                'deleted_by' => $adminData['user_id'],
                'email' => $userData['email'],
                'phone_number' => $userData['phone_number'],
                'deleted_at' => date('Y-m-d H:i:s')
            );
            $this->common_model->addRecords(USER_DELETED, $addData);

            $this->session->set_flashdata('item_success', sprintf(ITEM_DELETE_SUCCESS, 'User'));
            redirect($this->url.'/view-all');
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/view-all');
        }
    }

    /**
    * user profile image callback function
    */
    function _verify_user_image($data) {
        if(!empty($_FILES['profile_picture']['name'])) {
            $valid_extension = array('png', 'jpg', 'jpeg');
            $size = $_FILES['profile_picture']['size'];
            $type = $_FILES['profile_picture']['type'];
            $type = explode('/', $type);

            /* Check for file type */
            if(!in_array($type[1], $valid_extension)) {
                $this->form_validation->set_message('_verify_user_image', IMG_ERROR);
                return FALSE;
            }
            /* Check for file size */
            if($size > 2097152) {
                $this->form_validation->set_message('_verify_user_image', IMG_SIZE_ERROR_FOR_2);
                return FALSE;
            }
        }
    }           

    /**
    * Check for edit user existing email address
    */
    function _verify_edit_email($data) {
        $userId = $this->uri->segment(4);
        $userData = get_user($userId);
        $checkEmail = $this->common_model->getCustomSqlCount('SELECT `email` FROM '.USER.' WHERE `id` != '.$userId.' AND `email` = "'.$_POST['email'].'" AND user_type_id = '.$userData['user_type_id'].' AND is_user_deleted = 0');
        if($checkEmail > 0) {
            $this->form_validation->set_message('_verify_edit_email', EMAIL_EXISTS);
            return FALSE;
        }
    }

    /**
    * Check for edit user existing phonr number
    */
    function _verify_edit_phone_number($data) {
        $userId = $this->uri->segment(4);
        $userData = get_user($userId);
        $checkEmail = $this->common_model->getCustomSqlCount('SELECT `phone_number` FROM '.USER.' WHERE `id` != '.$userId.' AND `phone_number` = "'.$_POST['phone_number'].'" AND user_type_id = '.$userData['user_type_id'].' AND is_user_deleted = 0');
        if($checkEmail > 0) {
            $this->form_validation->set_message('_verify_edit_phone_number', PHGONE_NUMBER_EXISTS);
            return FALSE;
        } 
    }

    /**
     * Get Membership Plan By Cat Id
     */
    public function getmembershipplansbycat() {
        $html = '';
        if(!empty($_POST['catId'])) {
            $plans = $this->common_model->getAllRecordsById(MEMBERSHIP_PLAN, array('plan_category' => $_POST['catId']));
            if(!empty($plans)) {
                $html .= '<option value="">Select Membership Plan</option>';
                foreach($plans as $val){
                    $html .= '<option value="'.$val['plan_id'].'">'.ucfirst($val['plan_title']).'</option>';
                }
            } else {
                $html .= '<option value="">No plans found.</option>';
            }
        } else {
            $html .= '<option value="">No plans found.</option>';
        }
        echo $html;
    }

    /**
    * Manage Membership
    */
    public function manageMembership() {
        is_logged_in($this->url.'/manage-membership');
        is_have_access('can_change_user_membership');
        $userId = $this->uri->segment(4);
        if($userId) {
            $data = array();
            $data['meta_title'] = 'Manage Membership';
            $data['small_text'] = 'Plans';
            $data['body_class'] = array('admin_dashboard', 'is_logged_in', 'manage_user_membership');
            $data['session_data'] = admin_session_data();
            $data['permissions'] = get_user_permissions($data['session_data']['user_id']);
            $data['user_info'] = get_user($data['session_data']['user_id']);
            $data['user'] = $userData = $this->common_model->getSingleRecordById(USER, array('id' => $userId));
            $data['userId'] = $userId;
            $data['plan_categories'] = $this->common_model->getAllRecords(MEMBERSHIP_PLAN_CATEGORY);

            $plans = array();
            if($data['user']['membership_type'] == PREMIUM_MEMBER) {
                $subsPlans = $this->common_model->getAllRecordsById(USER_SUBSCRIPTION_HISTORY, array('user_id' => $userId, 'status' => 1));
                if(!empty($subsPlans)) {
                    foreach($subsPlans as $val) {
                        $planInfo = $this->common_model->getSingleRecordById(MEMBERSHIP_PLAN, array('plan_id' => $val['subscribed_plan_id']));
                        $planCatInfo = $this->common_model->getSingleRecordById(MEMBERSHIP_PLAN_CATEGORY, array('id' => $planInfo['plan_category']));
                        $plans[] = array(
                            'plan' => $planInfo['plan_title'],
                            'category' => $planCatInfo['title'],
                            'start_date' => $val['subscription_start_date'],
                            'end_date' => $val['subscription_end_date']
                        );
                    }
                }
            }
            $data['plans'] = $plans;

            if(isset($_POST['submit'])) {
                if($_POST['membership_type'] == FREE_MEMBER) {
                    if($_POST['free_membership_days'] > 0) {
                        $membershipExpireOn = date('Y-m-d', strtotime($userData['membership_expire_on'].' + '.$_POST['free_membership_days'].' days'));
                    } else {
                        $membershipExpireOn = date('Y-m-d', strtotime($_POST['free_membership_days'].' day', strtotime($userData['membership_expire_on'])));
                    }

                    /* Add record on user membership extension */
                    $postData = array(
                        'user_id' => $userId,
                        'membership_extended_by' => $data['session_data']['user_id'],
                        'membership_type' => FREE_MEMBER,
                        'membership_start_date' => date('Y-m-d'),
                        'membership_end_date' => $membershipExpireOn,
                        'membership_extended_text' => 'Membership days updated from CICM admin portal',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    );
                    $this->common_model->addRecords(USER_MEMBERSHIP_EXTENSION, $postData);

                    $updateArr = array();
                    $updateArr['membership_type'] = FREE_MEMBER;
                    $updateArr['membership_expire_on'] = $membershipExpireOn;
                    $updateArr['membership_status'] = 1;
                    $this->common_model->updateRecords(USER, $updateArr, array('id' => $userId));

                    $this->session->set_flashdata('item_success', sprintf(ITEM_UPDATE_SUCCESS, 'User Membership'));
                    redirect($this->url.'/manage-membership/'.$userId);
                } elseif($_POST['membership_type'] == PREMIUM_MEMBER) {
                    $this->handlePaidMembership($userId, $userData, $_POST['membership_plan'], $_POST);
                }
            }

            /* Load admin view */
            load_admin_view('manage-user-membership', $data);
        } else {
            $this->session->set_flashdata('invalid_item', INVALID_ITEM);
            redirect($this->url.'/view-all');
        }
    }

    function handlePaidMembership($userId, $userData, $planId, $postData){        
        /* Getting Plan Data */
        $planData = $this->common_model->getSingleRecordById(MEMBERSHIP_PLAN, array('plan_id' => $planId));
        $isTipsAllowed = ($planData['is_tips_allowed'] == 1) ? 1 : 0;
        $isIntradayScannerAllowed = ($planData['is_intraday_scanner_allowed'] == 1) ? 1 : 0;
        $isTradersClinicAllowed = ($planData['is_traders_clinic_allowed'] == 1) ? 1 : 0;

        $userActiveFeatures = get_user_active_features($postData['user_id']);
        $features = application_features();
        foreach($features as $val) {
            if($val['cond'] == 'can_access_tips') {
                $isAllowed = $isTipsAllowed;
            } elseif($val['cond'] == 'can_access_intraday_scanner') {
                $isAllowed = $isIntradayScannerAllowed;
            } elseif($val['cond'] == 'can_access_traders_clinic') {
                $isAllowed = $isTradersClinicAllowed;
            } else {
                $isAllowed = $val['value'];
            }

            $appliedFeatures[$val['cond']] = $isAllowed; 
        }

        $startDate = date('Y-m-d');
        $endDate = date('Y-m-d', strtotime('+ '.$planData['plan_benifits_in_days'].' Days'));

        /* Adding user subscription history */
        $subscriptionData = array(
            'user_id' => $postData['user_id'],
            'payment_id' => 0,
            'subscribed_plan_id' => $planId,
            'subscription_start_date' => $startDate,
            'subscription_end_date' => $endDate,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        );
        $subscriptionId = $this->common_model->addRecords(USER_SUBSCRIPTION_HISTORY, $subscriptionData);
        if(!empty($subscriptionId)) {
            /* Adding Membership Extension Data */
            $membershipExtensionData = array(
                'user_id' => $postData['user_id'],
                'membership_extended_by' => ADMIN_USER_ID,
                'membership_type' => PREMIUM_MEMBER,
                'membership_start_date' => $startDate,
                'membership_end_date' => $endDate,
                'membership_extended_text' => 'Membership Days Extended By Subscription Plan '.ucfirst($planData['plan_title']).' Purchase.',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
            $membershipExtensionId = $this->common_model->addRecords(USER_MEMBERSHIP_EXTENSION, $membershipExtensionData);
            
            /* Updating user active features and features history */
            $featureData = '';
            foreach($appliedFeatures as $key => $val) {
                $checkFeature = $this->common_model->getRecordCount(USER_ACTIVE_FEATURES, array('user_id' => $postData['user_id'], 'feature_prefix' => $key));
                if(!empty($checkFeature)) {
                    $this->common_model->updateRecords(USER_ACTIVE_FEATURES, array('is_active' => $val), array('user_id' => $postData['user_id'], 'feature_prefix' => $key));
                } else {
                    $featureData = array(
                        'user_id' => $postData['user_id'],
                        'feature_title' => str_replace('_', ' ', $key),
                        'feature_prefix' => $key,
                        'is_active' => $val,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    );
                    $this->common_model->addRecords(USER_ACTIVE_FEATURES, $featureData);
                    unset($featureData);
                }
            }

            /* Adding user history change records */
            $historyData = array(
                'user_id' => $postData['user_id'],
                'modified_by' => ADMIN_USER_ID,
                'old_active_features' => serialize($userActiveFeatures),
                'new_active_features' => serialize($appliedFeatures),
                'created_at' => date('Y-m-d H:i:s')
            );
            $this->common_model->addRecords(USER_ACTIVE_FEATURES_CHANGE_HISTORY, $historyData);

            /* Updating User Membership Data */
            $this->common_model->updateRecords(USER, array('membership_type' => PREMIUM_MEMBER, 'membership_status' => 1, 'updated_at' => date('Y-m-d H:i:s')), array('id' => $postData['user_id']));

            /* Updating user traders clinic limits */
            if($planData['is_traders_clinic_allowed'] == 1) {
                $allowedLimits = $planData['tc_allowed_query'];
                $this->common_model->increaseUserTcFormLimit(USER, $allowedLimits, $postData['user_id']);
            }

            /* Send Push Notification */
            if(!empty($userData['device_token'])) {
                if($userData['device_type'] == 1){
                    $fcmData = array(
                        'title' => 'Subscription Plan',
                        'body' => 'Dear '.ucfirst($userData['name']).', You have successfully susbscribed for CICM plan.',
                        'type' => 'plan_subscribed',
                        'extra' => array('user_id' => $checkPayment['user_id'], 'plan_id' => $planId, 'active_features' => $appliedFeatures)
                    );
                    send_fcm_notification($fcmData, $userData['device_token']);
                }
            }

            $this->session->set_flashdata('item_success', sprintf(ITEM_UPDATE_SUCCESS, 'User Membership'));
        } else {
            $this->session->set_flashdata('general_error', 'Unable to update user membership, please try again.');
        }
        redirect($this->url.'/manage-membership/'.$userId);
    }

    function getMembershipPlans() {
        $html = '';
        $planCatId = $_POST['membership_category'];
        if(!empty($planCatId)) {
            $plans = $this->common_model->getAllRecordsById(MEMBERSHIP_PLAN, array('plan_category' => $planCatId, 'plan_status' => 1));
            if(!empty($plans) && count($plans) > 0) {
                $html .= '<option value="">Select Plan</option>';
                foreach($plans as $val) {
                    $html .= '<option value="'.$val['plan_id'].'">'.ucfirst($val['plan_title']).'</option>';
                }
            }
        } else {
            $html .= '<option value="">No Plans Found!!</option>';
        } 
        echo $html;
        exit();
    }

    function _verify_user_phone_number($data){   
        if(!empty($_POST['phone_number']) && !empty($_POST['user_type_id'])) {
            $check = $this->common_model->getRecordCount(USER, array('phone_number' => $_POST['phone_number'], 'user_type_id' => $_POST['user_type_id'], 'is_user_deleted' => 0));
            if($check > 0) {
                $this->form_validation->set_message('_verify_user_phone_number', 'Phone number is already in use.');
                return FALSE;
            }
        }
    }

    function _verify_user_email($data){   
        if(!empty($_POST['email']) && !empty($_POST['user_type_id'])) {
            $check = $this->common_model->getRecordCount(USER, array('email' => $_POST['email'], 'user_type_id' => $_POST['user_type_id'], 'is_user_deleted' => 0));
            if($check > 0) {
                $this->form_validation->set_message('_verify_user_email', 'Email is already in use.');
                return FALSE;
            }
        }
    }

    function getuserdefaultfeatures(){
        $html = '';
        if(!empty($_POST['user_type_id'])) {
            if($_POST['user_type_id'] == SUB_ADMIN_USER_TYPE) {
                $defaultFeatures = unserialize(get_option('default_features_for_sub_admin_users'));
            } elseif($_POST['user_type_id'] == STAFF_USER_TYPE) {
                $defaultFeatures = unserialize(get_option('default_features_for_staff_admin_users'));
            }

            $internalFeatures = internal_user_permissions();
            if(!empty($internalFeatures)) {
                foreach($internalFeatures as $val){
                    $name = ucfirst(str_replace('_', ' ', $val));
                    $checked = (!empty($defaultFeatures) && $defaultFeatures[$val] == 1) ? 'checked="checked"' : '';
                    $html .= '<div class="checkbox featureBoxes">
                        <label>
                            <input type="hidden" name="permission['.$val.']" value="0" />
                            <input type="checkbox" name="permission['.$val.']" value="1" '.$checked.'> '.$name.'
                        </label>
                    </div>';
                }
            }

            echo $html;
        }
    }

    function addfollowup() {
        $sessionData = admin_session_data();
        if(!empty($_POST['user_id']) && !empty($_POST['message'])) {
            $addData = array(
                'sent_by' => $sessionData['user_id'],
                'user_id' => $_POST['user_id'],
                'message' => $_POST['message'],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
            $followUpId = $this->common_model->addRecords(USER_FOLLOW_UP, $addData);
            if($followUpId) {
                echo 'Follow up added successfully.';
            } else {
                echo 'Some error occurred, please try again.';
            }
        } else {
            echo 'Some error occurred, please try again.';
        }
    }

    function sendsmstouser() {
        if(!empty($_POST['user_id']) && !empty($_POST['sms']) && !empty($_POST['phone_number'])) {
            send_sms($_POST['phone_number'], $_POST['sms']);
            echo 'Message sent successfully.';
        } else {
            echo 'Some error occurred, please try again.';
        }
    }
}
