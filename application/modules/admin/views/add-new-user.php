<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'add_user_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-md-12">
	        <!-- general form elements -->
	        <div class="box box-primary">
	          <!-- form start -->
	          <form role="form" action="<?php cms_url('admin/users/add-new'); ?>" method="post" enctype="multipart/form-data">
	            <div class="box-body">
	            	<!-- Validation error and flash data -->
		            <?php if(validation_errors() || $this->session->flashdata('general_error')) { ?>
		                <div class="alert alert-danger alert-dismissable">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                  <?php echo validation_errors(); ?>
		                  <?php echo $this->session->flashdata('general_error'); ?>
		                </div>
		            <?php } ?>

	            	<div class="col-md-6">
	            		<div class="form-group">
		                  <label for="user_type_id">Select User Type</label>
		                  <select name="user_type_id" class="form-control addNewUserTypeId" id="user_type_id">
		                  	<option value="">Select User Type</option>
		                  	<?php if(!empty($user_types)) { ?>
		                  		<?php 
		                  			foreach($user_types as $val) { 
		                  			$types = explode('_', $val['type']);
		                  			if(in_array($val['id'], $allowed_user_type)) {
	                  			?>
		                  				<option value="<?php echo $val['id']; ?>" <?php echo set_select('user_type_id', $val['id']); ?>><?php echo ucfirst(strtolower($types[0])).' '.ucfirst(strtolower($types[1])); ?></option>
	                  				<?php } ?>
	                  			<?php } ?>
                  			<?php } else { ?>
                  				<option value="">No user types found.</option>
              				<?php } ?>
		                  </select>
		                </div>
		                <div class="form-group">
		                  <label for="name">Name</label>
		                  <input type="text" name="name" class="form-control" id="name" placeholder="Enter Name" value="<?php echo set_value('name'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="name">Email</label>
		                  <input type="text" name="email" class="form-control" id="name" placeholder="Enter Email" value="<?php echo set_value('email'); ?>"/>
		                </div>
		                <div class="form-group usersPassword" style="display: none;">
		                  <label for="name">Password</label>
		                  <input type="password" disabled="disabled" name="password" class="form-control" id="password" placeholder="Enter Password" value="<?php echo set_value('password'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="phone_number">Phone Number</label>
		                  <input type="text" name="phone_number" class="form-control" id="phone_number" placeholder="Enter Phone Number" value="<?php echo set_value('phone_number'); ?>" />
		                </div>
		                <div class="form-group validForAppUser" style="display: none;">
		                  <label for="free_membership_days">Free Membership Trial Days</label>
		                  <input type="number" disabled="disabled" name="free_membership_days" class="form-control" id="free_membership_days" placeholder="Enter Free Membership Trial Days For New User" value="<?php echo get_option('free_trial_days_for_new_members'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="status">User Status</label>
		                  <select name="status" class="form-control" id="status">
                  			<option value="1" <?php set_select('status', 1, true); ?>>Enable</option>
                  			<option value="0" <?php set_select('status', 0, true); ?>>Disable</option>
		                  </select>
		                </div>
	              	</div><!-- .col-md-6 -->
              		<div class="col-md-6 subAdminPermissions" style="display: none;">
		                <h4>Select User Permissions</h4>
		                <hr/>
		                <div class="appendedUsersFeatures"></div>
              		</div><!-- .col-md-6 -->
              		<div class="col-md-6 appUserPermissions" style="display: none;">
		                <h4>Select Active Features For App User</h4>
		                <hr/>
              			<?php $features = application_features(); ?>
	              		<?php foreach($features as $val) { ?>
		              		<div class="checkbox">
		                      <label>
		                      	<input type="hidden" name="features[<?php echo $val['cond']; ?>]" value="0" />
		                        <input type="checkbox" name="features[<?php echo $val['cond']; ?>]" value="1" <?php if($val['value'] == 1) { ?>checked="checked"<?php } ?>> <?php echo $val['label']; ?>
		                      </label>
		                    </div>
		                <?php } ?>
              		</div><!-- .col-md-6 -->
	            </div><!-- .box-body -->
	            <div class="box-footer">
	              <input type="submit" name="submit" value="Add User" class="btn btn-primary"/>
	            </div>
	          </form>
	        </div><!-- /.box -->
      	</div><!--/.col (left) -->
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
$(document).ready(function(){
	<?php if(isset($_POST['user_type_id'])) { ?>
		var selectedType = $('select[name="user_type_id"]').val();
		if(selectedType != '' && selectedType != '<?php echo APP_USER_TYPE; ?>') {
			$('.bodyWrapper').fadeIn();
			$('input[type="submit"]').prop('disabled', true);
			$.ajax({
	            type: 'POST',
	            url: url+'admin/users/getuserdefaultfeatures',
	            data: 'user_type_id='+selectedType,
	            success:function(response) {
	                $('.appendedUsersFeatures').html(response);
	                $('input[type="submit"]').prop('disabled', false);
	                $('.bodyWrapper').hide();
	            }
	        });
		}
	<?php } ?>

	$('select[name="user_type_id"]').change(function(){
		if($(this).val() != '<?php echo APP_USER_TYPE; ?>') {
			$('.bodyWrapper').fadeIn();
			$('input[type="submit"]').prop('disabled', true);
			$.ajax({
                type: 'POST',
                url: url+'admin/users/getuserdefaultfeatures',
                data: 'user_type_id='+$(this).val(),
                success:function(response) {
                    $('.appendedUsersFeatures').html(response);
                    $('input[type="submit"]').prop('disabled', false);
                    $('.bodyWrapper').hide();
                }
            });
		}
	});
});
</script>
<?php if(isset($_POST['submit']) && $_POST['user_type_id'] == APP_USER_TYPE) { ?>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.validForAppUser').fadeIn();
			$('.validForAppUser input').removeAttr('disabled');

			$('.usersPassword').hide();
			$('.usersPassword input').attr('disabled', 'disabled');

			$('.subAdminPermissions').hide();
			$('.appUserPermissions').fadeIn();
		});
	</script>
<?php } ?>

<?php if(isset($_POST['submit']) && ($_POST['user_type_id'] == SUB_ADMIN_USER_TYPE || $_POST['user_type_id'] == STAFF_USER_TYPE)) { ?>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.validForAppUser').hide();
			$('.validForAppUser input').attr('disabled', 'disabled');

			$('.usersPassword').fadeIn();
			$('.usersPassword input').removeAttr('disabled');

			$('.appUserPermissions').hide();
			$('.subAdminPermissions').fadeIn();
		});
	</script>
<?php } ?>