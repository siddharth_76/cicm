<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'view_all_follow_ups_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-xs-12">
	      <div class="box box-primary">
	        <div class="box-header">
	          <h3 class="box-title"><?php echo sprintf(ALL_DATA, 'Follow Ups'); ?></h3>
	          <?php if(!empty($tip_id) && $permissions['can_post_follow_up'] == 1) { ?><a href="<?php cms_url('admin/tips/add-new-follow-up/'.$tip_id); ?>" class="addNewAdmin" title="Add New Follow Up">Add New</a><?php } ?>
	          <div class="box-tools">
	            <div class="input-group customInputGroups" style="width: 150px;">
	              <form action="<?php cms_url('admin/tips/follow-ups'); ?>" method="get">
		              <input type="text" name="s" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo $this->input->get('s'); ?>">
		              <div class="input-group-btn cusInputGrpbtn">
		                <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
		              </div>
	              </form>
	            </div>
	            <?php if($pagination) { echo $pagination; } ?>
	          </div>
	          <!-- flash data -->
            <?php if($this->session->flashdata('item_success')) { ?>
                <div class="alert alert-success alert-dismissable" style="margin-top:12px;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo $this->session->flashdata('item_success'); ?>
                </div>
            <?php } if($this->session->flashdata('invalid_item')) { ?>
            	<div class="alert alert-danger alert-dismissable" style="margin-top:12px;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo $this->session->flashdata('invalid_item'); ?>
                </div>
            <?php } ?>
	        </div><!-- /.box-header -->
	        <div class="box-body table-responsive no-padding">
	          <table class="table table-hover">
	            <tr>
	              <th>ID</th>
	              <th>Tip</th>
	              <th>Message</th>
	              <th>Is Trailed Stoploss</th>
	              <th>Is Trailed Targets</th>
	              <th>Date Created</th>
	              <?php if($session_data['user_type'] == SUPER_ADMIN_USER_TYPE || $permissions['can_edit_follow_up'] == 1) { ?>
	              	<th>Action</th>
	              <?php } ?>
	            </tr>
	            <?php
	            	if(!empty($follow_ups)) {
	            		$offset = $offset + 1;
	            		foreach($follow_ups as $val) {
	            ?>
	            	<tr>
		              <td><?php echo $offset++; ?></td>
		              <td>
		              	<a href="<?php cms_url('admin/tips/view-all?s='.$val['fu_tip_id']);?>" target="View Tip" target="_blank">#Tip<?php echo $val['fu_tip_id']; ?></a>	
		              </td>
		              <td><?php echo $val['fu_message']; ?></td>
		              <td>
		              	<?php if($val['is_trailed_stoploss'] == 1) { ?>
			              	<span class="label label-success">Yes</span>
			              <?php } else { ?>
			              	<span class="label label-info">No</span>
			              <?php } ?>
			          </td>
			          <td>
		              	<?php if($val['is_trailed_targets'] == 1) { ?>
			              	<span class="label label-success">Yes</span>
			              <?php } else { ?>
			              	<span class="label label-info">No</span>
			              <?php } ?>
			          </td>
		              <td><?php echo date('d/m/Y', strtotime($val['created_at'])); ?></td>
		              <?php if($session_data['user_type'] == SUPER_ADMIN_USER_TYPE || $permissions['can_edit_follow_up'] == 1) { ?>
			              <td>
			              	<?php if($permissions['can_edit_follow_up'] == 1) { ?>
				              	<a href="<?php cms_url('admin/tips/edit-follow-up/'.$tip_id.'/'.$val['fu_id']); ?>" title="Edit Follow Up">
				              		<i class="fa fa-pencil"></i> Edit
				              	</a>
				            <?php } ?>
			              	<?php if($session_data['user_type'] == SUPER_ADMIN_USER_TYPE) { ?>
				              	| <a href="<?php cms_url('admin/tips/deleteFollowUp/'.$tip_id.'/'.$val['fu_id']); ?>" title="Delete Follow Up" onclick="if(!confirm('Are you sure you want to delete this follow up?')) return false;">
				              		<i class="fa fa-trash"></i> Delete
				              	</a>
				            <?php } ?>
			              </td>
			            <?php } ?>
		            </tr>
	            <?php } /* End foreach */ ?>
	            <?php } else { ?>
	            	<tr>
	            		<td colspan="7" align="center"><?php echo sprintf(NO_RECORDS_FOUND, 'follow up') ?></td>
	            	</tr>
	            <?php } ?>
	          </table>
	        </div><!-- /.box-body -->
	        <div class="box-footer"><?php if($pagination) { echo $pagination; } ?></div>
	      </div><!-- /.box -->
	    </div>
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->