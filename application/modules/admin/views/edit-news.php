<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'edit_news_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-md-12">
	        <!-- general form elements -->
	        <div class="box box-primary">
	          <!-- form start -->
	          <form role="form" action="<?php cms_url('admin/news/edit/'.$news['news_id']); ?>" method="post" enctype="multipart/form-data">
	            <div class="box-body">
	            	<!-- Validation error and flash data -->
		            <?php if(validation_errors() || $this->session->flashdata('general_error')) { ?>
		                <div class="alert alert-danger alert-dismissable">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                  <?php echo validation_errors(); ?>
		                  <?php echo $this->session->flashdata('general_error'); ?>
		                </div>
		            <?php } ?>

	            	<div class="col-md-6">
		                <div class="form-group">
		                  <label for="title">News Title</label>
		                  <input type="text" name="title" class="form-control" id="title" placeholder="Enter News Title" value="<?php echo $news['title']; ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="description">News Description</label>
		                  <textarea style="height: 220px;" name="description" class="form-control" id="description" placeholder="Enter News Description"><?php echo $news['description']; ?></textarea>
		                </div>
		                <div class="form-group">
		                  <label for="attachment">News Attachment</label>
		                  <input type="file" name="attachment" class="form-control" id="attachment" />
		                  <?php if(!empty($news['attachment'])) { ?>
		                  	<input type="hidden" name="current_attachment" value="<?php echo $news['attachment']; ?>"/>
		                  	<img src="<?php echo base_url().NEWS_ATTACHMENT_PATH.$news['attachment']; ?>" class="viewAdminLogo"/>
		                  <?php } ?>
		                </div>
		                <div class="form-group">
		                  <label for="status">News Status</label>
		                  <select name="status" class="form-control" id="status">
                  			<option value="1" <?php if($news['status'] == 1) { echo 'selected="selected"'; } ?>>Publish</option>
                  			<option value="0" <?php if($news['status'] == 0) { echo 'selected="selected"'; } ?>>Pendig</option>
		                  </select>
		                </div>
	              	</div><!-- .col-md-6 -->
	            </div><!-- .box-body -->
	            <div class="box-footer">
	              <input type="submit" name="submit" value="save Changes" class="btn btn-primary"/>
	            </div>
	          </form>
	        </div><!-- /.box -->
      	</div><!--/.col (left) -->
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->