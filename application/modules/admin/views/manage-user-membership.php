<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'manage_user_membership_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-md-12">
	        <!-- general form elements -->
	        <div class="box box-primary">
	            <div class="box-body">
                <!-- Validation error and flash data -->
                <?php if($this->session->flashdata('general_error') || $this->session->flashdata('invalid_item')) { ?>
                    <div class="alert alert-danger alert-dismissable">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <?php echo $this->session->flashdata('invalid_item'); ?>
                      <?php echo $this->session->flashdata('general_error'); ?>
                    </div>
                <?php } ?>
                <?php if($this->session->flashdata('item_success')) { ?>
                    <div class="alert alert-success alert-dismissable">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <?php echo $this->session->flashdata('item_success'); ?>
                    </div>
                <?php } ?>
                <div class="col-md-6">
                    <div class="form-group">
                    <h4>User Info:</h4><hr/>
                    <p>
                      <label><i class="fa fa-user"></i> User: </label>
                      <?php echo ucfirst($user['name']); ?>
                    </p>
                    <p>
                      <label><i class="fa fa-envelope-o"></i> Email: </label>
                      <?php echo $user['email']; ?>
                    </p>
                    <p>
                      <label><i class="fa fa-phone"></i> Phone Number: </label>
                      <?php echo $user['phone_number']; ?>
                    </p>
                    <h4>Membership Info:</h4>
                    <p>
                      <label><i class="fa fa-bars"></i> Membership Type: </label>
                      <?php echo ($user['membership_type'] == PREMIUM_MEMBER) ? 'Paid' : 'Free'; ?>
                    </p>
                    <?php if($user['membership_type'] == PREMIUM_MEMBER && !empty($plans)) { ?>
                      <p><i class="fa fa-bar-chart"></i> <label>Subscribed Plans:</label></p>
                      <?php $i = 1; foreach($plans as $val) { ?>
                        <p>
                          Plan#<?php echo $i++; ?><br/>
                          <label>Plan Title: </label>
                          <?php echo $val['plan']; ?><br/>
                          <label>Plan Category: </label>
                          <?php echo $val['category']; ?><br/>
                          <label>Start Date: </label>
                          <?php echo date('d-m-Y', strtotime($val['start_date'])); ?><br/>
                          <label>End Date: </label>
                          <?php echo date('d-m-Y', strtotime($val['end_date'])); ?><br/>
                        </p>
                      <?php } ?>
                    <?php } else { ?>
                      <p>
                        <i class="fa fa-calendar"></i>
                        <label>Membership Benifits Expire On: </label>
                        <?php echo date('d-m-Y', strtotime($user['membership_expire_on'])); ?>
                      </p>
                    <?php } ?>
                    </div>
                </div><!-- .col-md-6 -->
                <div class="col-md-6">
                  <h4>Assign/Change Membership:</h4>
                  <hr/>
                  <form id="assignMembershipPlan" method="post" action="<?php cms_url('admin/users/manage-membership/'.$userId); ?>">
                    <div class="form-group">
                      <label for="membership_type">Select Membership Type</label>
                      <select name="membership_type" id="membership_type" class="form-control">
                        <option value="">Select</option>
                        <option value="<?php echo FREE_MEMBER; ?>">Free</option>
                        <option value="<?php echo PREMIUM_MEMBER; ?>">Paid</option>
                      </select>
                    </div>
                    <div class="form-group" id="isExtendMembership" style="display: none;">
                      <label for="free_membership_days">Enter number of days to extend the membership</label>
                      <input type="number" name="free_membership_days" class="form-control" id="free_membership_days" placeholder="Enter Free Membership Trial Days For New User" value="<?php echo get_option('free_trial_days_for_new_members'); ?>" />
                    </div>
                    <div class="form-group" id="paidMembershipPlansCat" style="display: none;">
                      <label for="membership_category">Select Membership Plan Category</label>
                      <select name="membership_category" id="membership_category" class="form-control" onchange="getMembershipPlans(this);">
                        <?php if(!empty($plan_categories)) { ?>
                          <option value="">Select</option>
                          <?php foreach($plan_categories as $pcat) { ?>
                            <option value="<?php echo $pcat['id']; ?>"><?php echo $pcat['title']; ?></option>
                          <?php } ?>
                        <?php } else { ?>
                          <option value="">Nothing Found</option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="form-group" id="paidMembershipPlans" style="display: none;">
                      <label for="membership_plan">Select Membership Plan</label>
                      <select name="membership_plan" id="membership_plan" class="form-control">
                      </select>
                    </div>
                    <div class="form-group">
                      <input type="hidden" name="user_id" value="<?php echo $userId; ?>"/>
                      <input type="submit" name="submit" value="Save Changes" class="btn btn-primary"/>
                    </div>
                  </form>
                </div><!-- .col-md-6 -->
	            </div><!-- .box-body -->	
	            <div class="box-footer"></div>
	        </div><!-- /.box -->
      	</div><!--/.col (left) -->
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
  function getMembershipPlans(obj) {
    var selectedPlanCat = $(obj).val();
    $('.bodyWrapper').fadeIn();
    $.ajax({
        type: 'POST',
        url: url+'admin/users/getMembershipPlans',
        data: 'membership_category='+selectedPlanCat,
        success:function(response) {
            $('#membership_plan').html(response);
            $('#paidMembershipPlans').fadeIn();
            setInterval(function () { $('.bodyWrapper').hide(); }, 500);
        }
    });
  }

  $(document).ready(function(){
    $('#membership_type').on('change', function(){
      if($(this).val() == '<?php echo PREMIUM_MEMBER; ?>') {
        $('#isExtendMembership').hide();
        $('#paidMembershipPlansCat').fadeIn();
      } else if($(this).val() == '<?php echo FREE_MEMBER; ?>') {
        $('#paidMembershipPlansCat, #paidMembershipPlans').hide();
        $('#isExtendMembership').fadeIn();
      } else {
        $('#paidMembershipPlansCat, #paidMembershipPlans, #isExtendMembership').hide();
      }
    });

    $('form#assignMembershipPlan').submit(function(e){
      var flag = 0;
      if($('#assignMembershipPlan select[name="membership_type"]').val() == '') {
        flag++;
        $('#assignMembershipPlan select[name="membership_type"]').addClass('textValidationError');
      } else {
        $('#assignMembershipPlan select[name="membership_type"]').removeClass('textValidationError');
      }

      if($('#assignMembershipPlan select[name="membership_type"]').val() != '' && $('input[name="free_membership_days"]').val() == '') {
          flag++;
          $('input[name="free_membership_days"]').addClass('textValidationError');
      } else {
        $('input[name="free_membership_days"]').removeClass('textValidationError');
      }

      if($('#assignMembershipPlan select[name="membership_type"]').val() == '<?php echo PREMIUM_MEMBER; ?>' && $('#assignMembershipPlan select[name="membership_category"]').val() == '') {
        flag++;
        $('#assignMembershipPlan select[name="membership_category"]').addClass('textValidationError');
      } else {
        $('#assignMembershipPlan select[name="membership_category"]').removeClass('textValidationError');
      }

      if($('#assignMembershipPlan select[name="membership_type"]').val() == '<?php echo PREMIUM_MEMBER; ?>' && $('#assignMembershipPlan select[name="membership_plan"]').val() == '') {
        flag++;
        $('#assignMembershipPlan select[name="membership_plan"]').addClass('textValidationError');
      } else {
        $('#assignMembershipPlan select[name="membership_plan"]').removeClass('textValidationError');
      }

      if(flag > 0) {
        return false;
      }

      if(confirm('Are you sure want to make these changes??')) {
        $(this).removeClass('textValidationError');
        return true;
      } else {
        return false;
      }
    });
  });
</script>