<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'edit_user_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-md-12">
	        <!-- general form elements -->
	        <div class="box box-primary">
	          <!-- form start -->
	          <form role="form" action="<?php cms_url('admin/users/edit/'.$user['id']); ?>" method="post" enctype="multipart/form-data">
	            <div class="box-body">
	            	<!-- Validation error and flash data -->
		            <?php if(validation_errors() || $this->session->flashdata('general_error')) { ?>
		                <div class="alert alert-danger alert-dismissable">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                  <?php echo validation_errors(); ?>
		                  <?php echo $this->session->flashdata('general_error'); ?>
		                </div>
		            <?php } ?>

	            	<div class="col-md-6">
	            		<div class="form-group">
		                  <label for="user_type_id">Select User Type</label>
		                  <select name="user_type_id" class="form-control" id="user_type_id" disabled="disabled">
		                  	<option value="">Select User Type</option>
		                  	<?php if(!empty($user_types)) { ?>
		                  		<?php 
		                  			foreach($user_types as $val) { 
		                  			$types = explode('_', $val['type']);
	                  			?>
		                  			<option value="<?php echo $val['id']; ?>" <?php if($val['id'] == $user['user_type_id']) { echo 'Selected="delected"'; } ?>><?php echo ucfirst(strtolower($types[0])).' '.ucfirst(strtolower($types[1])); ?></option>
	                  			<?php } ?>
                  			<?php } else { ?>
                  				<option value="">No user types found.</option>
              				<?php } ?>
		                  </select>
		                </div>
		                <div class="form-group">
		                  <label for="name">Name</label>
		                  <input type="text" name="name" class="form-control" id="name" placeholder="Enter Name" value="<?php echo $user['name']; ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="name">Email</label>
		                  <input type="text" name="email" class="form-control" id="name" placeholder="Enter Email" value="<?php echo $user['email']; ?>"/>
		                </div>
		                <div class="form-group">
		                  <label for="phone_number">Phone Number</label>
		                  <input type="text" name="phone_number" class="form-control" id="phone_number" placeholder="Enter Phone Number" value="<?php echo $user['phone_number']; ?>" />
		                </div>
		                <?php if($user['user_type_id'] == APP_USER_TYPE) { ?>
		                	<div class="form-group">
			                  <label for="membership_type">Membership Type</label>
			                  <select name="membership_type" class="form-control" id="membership_type" disabled="disabled">
	                  			<option value="1" <?php if($user['membership_type'] == 1) { echo 'Selected="selected"'; } ?>>Free</option>
	                  			<option value="2" <?php if($user['membership_type'] == 2) { echo 'Selected="selected"'; } ?>>Paid</option>
			                  </select>
			                </div>
			                <div class="form-group">
			                  <label for="membership_expire_on">Membership Expires On</label>
			                  <input type="text" disabled="disabled" name="membership_expire_on" class="form-control" id="membership_expire_on" placeholder="Enter Membership Expires On" value="<?php echo date('d/m/Y', strtotime($user['membership_expire_on'])); ?>" />
			                </div>
			                <?php if($permissions['can_extend_user_membership'] == 1) { ?>
				                <div class="checkbox">
			                      <label>
			                      	<input type="hidden" name="extend_membership" value="0"/>
			                        <input type="checkbox" name="extend_membership" value="1"> Extend membership days
			                      </label>
			                    </div>
			                    <div class="form-group isExtendMembership" style="display: none;">
				                  <label for="free_membership_days">Enter number of days to extend the membership</label>
				                  <input type="number" disabled="disabled" name="free_membership_days" class="form-control" id="free_membership_days" placeholder="Enter Free Membership Trial Days For New User" value="<?php echo get_option('free_trial_days_for_new_members'); ?>" />
				                </div>
				                <div class="form-group">
				                	<label>Reward Points</label>
				                	<input type="number" class="form-control" name="reward_points" id="reward_points" value="<?php echo revertDBInt($user['reward_points']); ?>"/>
				                </div>
				            <?php } ?>
			            <?php } ?>
		                <div class="form-group">
		                  <label for="status">User Status</label>
		                  <select name="status" class="form-control" id="status">
                  			<option value="1" <?php if($user['status'] == 1) { echo 'Selected="selected"'; } ?>>Enable</option>
                  			<option value="0" <?php if($user['status'] == 0) { echo 'Selected="selected"'; } ?>>Disable</option>
		                  </select>
		                </div>
		                <?php if($user['user_type_id'] != APP_USER_TYPE) { ?>
		                	<br/>
			                <h4>Change Password</h4>
			                <hr/>
			                <div class="form-group">
			                  <label for="password">Password</label>
			                  <input type="password" name="password" class="form-control" id="password" placeholder="Enter Password" />
			                </div>
			                <div class="form-group">
			                  <label for="confirm_password">Confirm Password</label>
			                  <input type="password" name="confirm_password" class="form-control" id="confirm_password" placeholder="Enter Confirm Password" />
			                </div>
		                <?php } ?>
	              	</div><!-- .col-md-6 -->
	              	<?php if($user['user_type_id'] != APP_USER_TYPE) { ?>
		              	<div class="col-md-6 subAdminPermissions">
			                <h4>Select User Permissions</h4>
			                <hr/>
	              			<?php $permissions = internal_user_permissions(); ?>
		              		<?php foreach($permissions as $val) { ?>
	              				<?php $name = ucfirst(str_replace('_', ' ', $val)); ?>
			              		<div class="checkbox">
			                      <label>
			                      	<input type="hidden" name="permission[<?php echo $val; ?>]" value="0" />
			                        <input type="checkbox" name="permission[<?php echo $val; ?>]" value="1" <?php if($user_permissions[$val] == 1) { echo 'checked="checked"'; } ?>> <?php echo $name; ?>
			                      </label>
			                    </div>
			                <?php } ?>
	              		</div><!-- .col-md-6 -->
	              	<?php } ?>
	              	<?php if($user['user_type_id'] == APP_USER_TYPE && $permissions['can_change_user_active_features'] == 1) { ?>
	              		<div class="col-md-6 appUserPermissions">
			                <h4>Active Features List For App User</h4>
			                <hr/>
	              			<?php $features = application_features(); ?>
		              		<?php foreach($features as $key => $val) { ?>
			              		<div class="checkbox">
			                      <label>
			                      	<input type="hidden" name="features[<?php echo $val['cond']; ?>]" value="0" />
			                        <input type="checkbox" name="features[<?php echo $val['cond']; ?>]" value="1" <?php if($active_features[$key]['is_active'] == 1) { ?> checked="checked" <?php } ?>> <?php echo $val['label']; ?>
			                      </label>
			                    </div>
			                <?php } ?>
	              		</div><!-- .col-md-6 -->
	              	<?php } ?>
	            </div><!-- .box-body -->	

	            <div class="box-footer">
	              <input type="submit" name="submit" value="Save Changes" class="btn btn-primary"/>
	            </div>
	          </form>
	        </div><!-- /.box -->
      	</div><!--/.col (left) -->
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
	$(document).ready(function(){
		$('input[name="extend_membership"]').click(function(){
			if($(this).is(':checked')) {
				$('.isExtendMembership').fadeIn();
				$('.isExtendMembership input').removeAttr('disabled');
			} else {
				$('.isExtendMembership').hide();
				$('.isExtendMembership input').attr('disabled', 'disabled');
			}
		});

		$('form').submit(function(e){
			if(confirm('Are you sure want to make these changes??')) {
				return true;
			} else {
				return false;
			}
		});
	});
</script>