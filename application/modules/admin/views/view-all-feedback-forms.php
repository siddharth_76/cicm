<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'view_all_feedback_forms_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-xs-12">
	      <div class="box box-primary">
	        <div class="box-header">
	          <h3 class="box-title"><?php echo sprintf(ALL_DATA, 'Feedback Forms'); ?></h3>
	          <?php if($permissions['can_access_feedback_forms'] == 1) { ?>
	          	<a href="<?php cms_url('admin/feedbacks/create'); ?>" class="addNewAdmin" title="Add New Feedback Form">Add New</a>
	          <?php } ?>
	          <div class="box-tools">
	            <?php if($pagination) { echo $pagination; } ?>
	          </div>
	          <!-- flash data -->
            <?php if($this->session->flashdata('item_success')) { ?>
                <div class="alert alert-success alert-dismissable" style="margin-top:12px;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo $this->session->flashdata('item_success'); ?>
                </div>
            <?php } if($this->session->flashdata('invalid_item')) { ?>
            	<div class="alert alert-danger alert-dismissable" style="margin-top:12px;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo $this->session->flashdata('invalid_item'); ?>
                </div>
            <?php } ?>
	        </div><!-- /.box-header -->
	        <div class="box-body table-responsive no-padding">
	          <table class="table table-hover">
	            <tr>
	              <th>ID</th>
	              <th>Status</th>
	              <?php if($permissions['can_access_feedback_forms'] == 1) { ?><th>Action</th><?php } ?>
	              <th>Date Created</th>
	              <?php if($session_data['user_type'] == SUPER_ADMIN_USER_TYPE) { ?><th>Delete</th><?php } ?>
	            </tr>
	            <?php
	            	if(!empty($forms)) {
	            		$offset = $offset + 1;
	            		foreach($forms as $val) {
	            ?>
	            	<tr>
		              <td><?php echo $offset++; ?></td>
		              <td>
		              	<?php if($val['status'] == 1) { ?>
		              		<span class="label label-success">Published</span>
		              	<?php } else { ?>
	              			<span class="label label-warning">Pending</span>
		              	<?php } ?>
		              </td>
		              <?php if($permissions['can_access_feedback_forms'] == 1) { ?>
			              <td>
			              	<a href="<?php cms_url('admin/feedbacks/edit/'.$val['id']); ?>" title="Edit Feedback Form">
			              		<i class="fa fa-pencil"></i> Edit
			              	</a>
			              </td>
			          <?php } ?>
		              <td><?php echo date('d/m/Y', strtotime($val['created_at'])); ?></td>
		              <?php if($session_data['user_type'] == SUPER_ADMIN_USER_TYPE) { ?>
			              <td>
			              	<a href="<?php if($val['status'] != 1) { cms_url('admin/feedbacks/deleteForm/'.$val['id']); } else { echo 'javascript:void(0);'; } ?>" title="Delete Form" <?php if($val['status'] != 1) { ?>onclick="if(!confirm('Are you sure you want to delete this form?')) return false;"<?php } else { ?>onclick="alert('You cannot delete this published feedback form.');"<?php } ?>>
			              		<i class="fa fa-trash"></i> Delete
			              	</a>
			              </td>
			           <?php } ?>
		            </tr>
	            <?php } /* End foreach */ ?>
	            <?php } else { ?>
	            	<tr>
	            		<td colspan="5" align="center"><?php echo sprintf(NO_RECORDS_FOUND, 'feedback forms') ?></td>
	            	</tr>
	            <?php } ?>
	          </table>
	        </div><!-- /.box-body -->
	        <div class="box-footer"><?php if($pagination) { echo $pagination; } ?></div>
	      </div><!-- /.box -->
	    </div>
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->