<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'view_traders_clinic_coversation_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-xs-12">
        <div class="box box-primary direct-chat direct-chat-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Send reply to (<i class="fa fa-user"></i> <?php echo ucfirst($userData['name']); ?>, <?php echo $userData['phone_number']; ?>)</h3>
              <div class="box-tools pull-right">
                <?php if($permissions['can_see_traders_clinic_conversations'] == 1 && $permissions['can_reply_on_traders_clinic'] == 1) { ?>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="tc_status" id="tc_status" value="<?php echo $ticket['status']; ?>" <?php if($ticket['status'] != 1) { echo 'checked="checked"'; } ?>>Do you want to close this conversation??
                    </label>
                  </div>
                <?php } ?>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body">
              <!-- Validation error and flash data -->
              <?php if(validation_errors() || $this->session->flashdata('general_error')) { ?>
                  <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo validation_errors(); ?>
                    <?php echo $this->session->flashdata('general_error'); ?>
                  </div>
              <?php } ?>

              <?php if($this->session->flashdata('item_success')) { ?>
                  <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('item_success'); ?>
                  </div>
              <?php } ?>
              <!-- Conversations are loaded here -->
              <div class="supportChatMsgs direct-chat-messages">
                <?php if(!empty($messages)) { ?>
                  <?php foreach($messages as $val) { 
                    $user = get_user($val['from_id']);
                  ?>
                      <!-- Message. Default to the left -->
                      <?php if($val['from_id'] == $ticket['user_id']) { ?>
                        <div class="direct-chat-msg">
                          <div class="direct-chat-info clearfix">
                            <span class="direct-chat-name pull-left"><?php echo ucfirst($user['name']); ?></span>
                            <span class="direct-chat-timestamp pull-right"><?php echo date('d M Y, h:i A', strtotime($val['created_at'])); ?></span>
                          </div><!-- /.direct-chat-info -->
                          <img class="direct-chat-img" src="<?php echo (!empty($user['profile_pic'])) ? $user['profile_pic'] : base_url().'assets/admin/dist/img/avatar5.png'; ?>" alt="message user image"><!-- /.direct-chat-img -->
                          <div class="direct-chat-text"><?php echo ucfirst($val['message']); ?></div><!-- /.direct-chat-text -->
                        </div><!-- /.direct-chat-msg -->
                      <?php } else { ?>
                        <div class="direct-chat-msg right">
                          <div class="direct-chat-info clearfix">
                            <span class="direct-chat-name pull-right"><?php echo ucfirst($user['name']); ?></span>
                            <span class="direct-chat-timestamp pull-left"><?php echo date('d M Y, h:i A', strtotime($val['created_at'])); ?></span>
                          </div><!-- /.direct-chat-info -->
                          <img class="direct-chat-img" src="<?php echo (!empty($user['profile_pic'])) ? $user['profile_pic'] : base_url().'assets/admin/dist/img/avatar5.png'; ?>" alt="message user image"><!-- /.direct-chat-img -->
                          <div class="direct-chat-text"><?php echo ucfirst($val['message']); ?></div><!-- /.direct-chat-text -->
                        </div><!-- /.direct-chat-msg -->
                      <?php } ?>

                      <?php } ?>
                <?php } ?>
              </div><!--/.direct-chat-messages-->
            </div><!-- /.box-body -->
            <div class="box-footer">
              <?php if($permissions['can_see_traders_clinic_conversations'] == 1 && $permissions['can_reply_on_traders_clinic'] == 1) { ?>
                <form id="supportReplyFrm" action="<?php cms_url('admin/tradersclinic/send/'.$ticket['id']); ?>" method="post">
                  <div class="input-group">
                    <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                    <span class="input-group-btn">
                      <input type="submit" class="btn btn-primary btn-flat" name="submit" value="Send">
                    </span>
                  </div>
                </form>
              <?php } ?>
            </div><!-- /.box-footer-->
          </div><!--/.direct-chat -->
	    </div>
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
  $(document).ready(function(){
    $('#supportReplyFrm').submit(function(){
      if(confirm('Are you sure want to send this message??')) {
        return true;
      } else {
        return false;
      }
    });

    $('input[name="tc_status"]').click(function(){
      var tcStatus = '';
      if($(this).is(':checked')) {
        tcStatus = 0;
      } else {
        tcStatus = 1;
      }

      if(confirm('Are you sure want to close this conversation??')){
        $('.bodyWrapper').fadeIn();
        $.ajax({
            type: 'POST',
            url: url+'admin/tradersclinic/updatestatus',
            data: 'tc_id=<?php echo $ticket["id"]; ?>&'+'tc_status='+tcStatus,
            success:function(response) {
                $('.bodyWrapper').hide();
                alert(response);
            }
        });
      } else {
        $(this).prop('checked', false);
      }
    });
  });
</script>