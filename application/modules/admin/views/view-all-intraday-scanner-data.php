<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'view_all_news_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-xs-12">
    	  <!-- Search Filters -->
	      	<div class="box box-primary">
	      		<form action="<?php cms_url('admin/hawkeye/view-all'); ?>" method="get">
		        	<div class="box-header">
		          		<h3 class="box-title">Filter Intraday Scanner Data</h3>
		          		<div class="box-tools pull-right">
		                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		                </div>
	          		</div><!-- /.box-header -->
		        	<div class="box-body">
		        		<div class="col-md-6">
		        			<div class="form-group">
			                  <label for="intraday_category_id">Select Intraday Scanner Category</label>
			                  <select name="intraday_category_id" class="form-control" id="intraday_category_id" required="required">
			                  	<?php if(!empty($cats)) { ?>
			                  		<option value="">Select Intraday Scanner Category</option>
			                  		<?php foreach($cats as $key => $val) { ?>
			                  			<option value="<?php echo $key; ?>" <?php if(isset($_GET['intraday_category_id']) && $_GET['intraday_category_id'] == $key) { echo 'selected="selected"'; } ?>><?php echo ucfirst($val); ?></option>
		                  			<?php } ?>
		                  		<?php } else { ?>
		                  			<option value="">Nothing Found</option>
	                  			<?php } ?>
			                  </select>
			                </div>
		        		</div>
		        		<div class="col-md-6">
		        			<div class="form-group">
			                  <label for="publish_date">Select Publish Date</label>
			                  <input type="text" name="publish_date" class="form-control datepicker" placeholder="Select Publish Date" value="<?php if(isset($_GET['publish_date'])) { echo $_GET['publish_date']; } ?>" />
			                </div>
		        		</div>
		        		<div class="col-md-6">
		        			<button type="submit" class="btn btn-primary">Filter Results</button>
		              		<a href="<?php cms_url('admin/hawkeye/view-all'); ?>" class="btn btn-primary">Reset Filter</a>
		        		</div>
		        	</div><!-- /.box-body -->
	            </form>
	      	</div>
	      <!-- Search Filters End Here -->
	      <div class="box box-primary">
	        <div class="box-header">
	          <h3 class="box-title"><?php echo sprintf(ALL_DATA, 'Intraday Scanner Data'); ?></h3>
	          <a href="<?php cms_url('admin/hawkeye/upload'); ?>" class="addNewAdmin" title="Upload Data">Upload Data</a>
	          <div class="box-tools">
	            <div class="input-group customInputGroups" style="width: 150px;">
	              <form action="<?php cms_url('admin/hawkeye/upload'); ?>" method="get">
		              <input type="text" name="s" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo $this->input->get('s'); ?>">
		              <div class="input-group-btn cusInputGrpbtn">
		                <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
		              </div>
	              </form>
	            </div>
	            <?php if($pagination) { echo $pagination; } ?>
	          </div>
	          <!-- flash data -->
            <?php if($this->session->flashdata('item_success')) { ?>
                <div class="alert alert-success alert-dismissable" style="margin-top:12px;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo $this->session->flashdata('item_success'); ?>
                </div>
            <?php } if($this->session->flashdata('invalid_item')) { ?>
            	<div class="alert alert-danger alert-dismissable" style="margin-top:12px;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo $this->session->flashdata('invalid_item'); ?>
                </div>
            <?php } ?>
	        </div><!-- /.box-header -->
	        <div class="box-body table-responsive no-padding">
	          <table class="table table-hover">
	            <tr>
	              <th>ID</th>
	              <th>Script Name</th>
	              <th>Company Name</th>
	              <th>Intraday Category</th>
	              <th>Publish Date</th>
	              <th>Date Created</th>
	              <?php if($session_data['user_type'] == SUPER_ADMIN_USER_TYPE) { ?>
	              	<th>Delete</th>
	              <?php } ?>
	            </tr>
	            <?php
	            	if(!empty($hawk)) {
	            		$offset = $offset + 1;
	            		foreach($hawk as $val) {
	            ?>
	            	<tr>
		              <td><?php echo $offset++; ?></td>
		              <td>
		              	<?php echo ucfirst($val['script_name']); ?>	
		              </td>
		              <td>
		              	<?php echo ucfirst($val['company_name']); ?>	
		              </td>
		              <td>
		              	<?php echo ucfirst($cats[$val['intraday_category_id']]); ?>
		              </td>
		              <td><?php echo date('d/m/Y', strtotime($val['publish_date'])); ?></td>
		              <td><?php echo date('d/m/Y', strtotime($val['created_at'])); ?></td>
		              <?php if($session_data['user_type'] == SUPER_ADMIN_USER_TYPE) { ?>
			              <td>
			              	<a href="<?php cms_url('admin/hawkeye/delete/'.$val['id']); ?>" title="Delete Entry" onclick="if(!confirm('Are you sure you want to delete this entry?')) return false;">
			              		<i class="fa fa-trash"></i> Delete
			              	</a>
			              </td>
			           <?php } ?>
		            </tr>
	            <?php } /* End foreach */ ?>
	            <?php } else { ?>
	            	<tr>
	            		<td colspan="7" align="center"><?php echo sprintf(NO_RECORDS_FOUND, 'intraday scanner') ?></td>
	            	</tr>
	            <?php } ?>
	          </table>
	        </div><!-- /.box-body -->
	        <div class="box-footer"><?php if($pagination) { echo $pagination; } ?></div>
	      </div><!-- /.box -->
	    </div>
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
	$(document).ready(function(){
		$('.datepicker').datepicker({
		    format: 'dd-mm-yyyy',
		    startDate: '-3d'
		});
	});
</script>