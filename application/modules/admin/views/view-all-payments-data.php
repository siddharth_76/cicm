<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'view_all_payments_data_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-xs-12">
    	  <!-- Search Filters -->
	      	<div class="box box-primary">
	      		<form action="<?php cms_url('admin/payments/view-all'); ?>" method="get">
		        	<div class="box-header">
		          		<h3 class="box-title">Filter Payment Data</h3>
		          		<div class="box-tools pull-right">
		                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		                </div>
	          		</div><!-- /.box-header -->
		        	<div class="box-body">
		        		<div class="col-md-4">
		        			<div class="form-group">
			                  <label for="user_id">Select User</label>
			                  <select name="user_id" class="form-control" id="user_id">
			                  		<?php if(!empty($users)) { ?>
			                  			<option value="">Select User</option>
			                  			<?php foreach($users as $val) { ?>
			                  				<option <?php if(isset($_GET['user_id']) && $_GET['user_id'] == $val['id']) { echo 'selected="selected"'; } ?> value="<?php echo $val['id']; ?>"><?php echo ucfirst($val['name']); ?></option>
			                  			<?php } ?>
		                  			<?php } else { ?>
		                  				<option value="">No User found</option>
		                  			<?php } ?>
			                  </select>
			                </div>
		        		</div>
		        		<div class="col-md-4">
		        			<div class="form-group">
			                  <label for="is_reward_points_used">Is Reward Points Used</label>
			                  <select name="is_reward_points_used" class="form-control" id="is_reward_points_used">
		                  			<option value="">Select</option>
		                  			<option <?php if(isset($_GET['is_reward_points_used']) && $_GET['is_reward_points_used'] == 0) { echo 'selected="selected"'; } ?> value="0">No</option>
		                  			<option <?php if(isset($_GET['is_reward_points_used']) && $_GET['is_reward_points_used'] == 1) { echo 'selected="selected"'; } ?> value="1">Yes</option>
			                  </select>
			                </div>
		        		</div>
		        		<div class="col-md-4">
		        			<div class="form-group">
			                  <label for="custom_text">Enter Text</label>
			                  <input type="text" name="custom_text" class="form-control" placeholder="Enter Text" value="<?php if(isset($_GET['custom_text'])) { echo $_GET['custom_text']; } ?>" />
			                </div>
		        		</div>
		        		<div class="col-md-6">
		        			<button type="submit" class="btn btn-primary">Filter Results</button>
		              		<a href="<?php cms_url('admin/payments/view-all'); ?>" class="btn btn-primary">Reset Filter</a>
		        		</div>
		        	</div><!-- /.box-body -->
	            </form>
	      	</div>
	      <!-- Search Filters End Here -->
	      <div class="box box-primary">
	        <div class="box-header">
	          <h3 class="box-title"><?php echo sprintf(ALL_DATA, 'Payments Data'); ?></h3>
	          <div class="box-tools">
	            <?php if($pagination) { echo $pagination; } ?>
	          </div>
	          <!-- flash data -->
            <?php if($this->session->flashdata('item_success')) { ?>
                <div class="alert alert-success alert-dismissable" style="margin-top:12px;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo $this->session->flashdata('item_success'); ?>
                </div>
            <?php } if($this->session->flashdata('invalid_item')) { ?>
            	<div class="alert alert-danger alert-dismissable" style="margin-top:12px;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo $this->session->flashdata('invalid_item'); ?>
                </div>
            <?php } ?>
	        </div><!-- /.box-header -->
	        <div class="box-body table-responsive no-padding">
	          <table class="table table-hover">
	            <tr>
	              <th>ID</th>
	              <th>User</th>
	              <th>Txn Id</th>
	              <th>Status</th>
	              <th>Is Reward Points Used</th>
	              <th>Used Reward Points</th>
	              <th>Total Amount</th>
	              <th>Final Amount</th>
	              <?php if($permissions['can_access_payment'] == 1) { ?><th>Action</th><?php } ?>
	              <th>Date Created</th>
	            </tr>
	            <?php
	            	if(!empty($payments)) {
	            		$offset = $offset + 1;
	            		foreach($payments as $val) {
	            			$userData = get_user($val['user_id']);
	            ?>
	            	<tr>
	            		<td><?php echo $offset++; ?></td>
		            	<td>
		            		<a href="<?php cms_url('admin/userts/view-all?s='.$userData['name']); ?>" target="_blank" title="<?php echo ucfirst($userData['name']); ?>"><?php echo ucfirst($userData['name']); ?></a>
		            	</td>
		            	<td><?php echo $val['txn_id']; ?></td>
		            	<td>
		            		<?php if($val['payment_status'] == 'init') { ?>
		            			<span class="label label-warning"><?php echo ucfirst($val['payment_status']); ?></span>	
		            		<?php } else { ?>
		            			<span class="label label-success"><?php echo ucfirst($val['payment_status']); ?></span>	
		            		<?php } ?>
	            		</td>
		            	<td>
		            		<?php if($val['is_reward_points_used'] == 1) { ?>
		            			<span class="label label-success">Yes</span>
		            		<?php } else { ?>
		            			<span class="label label-warning">No</span>
		            		<?php } ?>
		            	</td>
		            	<td><?php echo revertDBInt($val['used_reward_points']); ?></td>
		            	<td><i class="fa fa-inr"></i> <?php echo revertDBInt($val['total_amount']); ?></td>
		            	<td><i class="fa fa-inr"></i> <?php echo revertDBInt($val['final_amount']); ?></td>
		            	<?php if($permissions['can_access_payment'] == 1) { ?>
			            	<td>
			            		<a href="<?php cms_url('admin/payments/view/'.$val['id']); ?>" title="View payment Detail"><i class="fa fa-eye"></i> View</a>
			            	</td>
			           	<?php } ?>
		            	<td>
		            		<?php echo date('m/d/Y h:i A', strtotime($val['created_at'])); ?>
		            	</td>
		            </tr>
	            <?php } /* End foreach */ ?>
	            <?php } else { ?>
	            	<tr>
	            		<td colspan="10" align="center"><?php echo sprintf(NO_RECORDS_FOUND, 'payments data') ?></td>
	            	</tr>
	            <?php } ?>
	          </table>
	        </div><!-- /.box-body -->
	        <div class="box-footer"><?php if($pagination) { echo $pagination; } ?></div>
	      </div><!-- /.box -->
	    </div>
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->