<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'view_all_plans_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-xs-12">
	      <div class="box box-primary">
	        <div class="box-header">
	          <h3 class="box-title"><?php echo sprintf(ALL_DATA, 'Plans'); ?></h3>
	          <?php if($permissions['can_add_membership_palns'] == 1) { ?>
	          	<a href="<?php cms_url('admin/plans/add-new'); ?>" class="addNewAdmin" title="Add New Plan">Add New</a>
	          <?php } ?>
	          <div class="box-tools">
	            <div class="input-group customInputGroups" style="width: 150px;">
	              <form action="<?php cms_url('admin/plans/view-all'); ?>" method="get">
		              <input type="text" name="s" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo $this->input->get('s'); ?>">
		              <div class="input-group-btn cusInputGrpbtn">
		                <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
		              </div>
	              </form>
	            </div>
	            <?php if($pagination) { echo $pagination; } ?>
	          </div>
	          <!-- flash data -->
            <?php if($this->session->flashdata('item_success')) { ?>
                <div class="alert alert-success alert-dismissable" style="margin-top:12px;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo $this->session->flashdata('item_success'); ?>
                </div>
            <?php } if($this->session->flashdata('invalid_item')) { ?>
            	<div class="alert alert-danger alert-dismissable" style="margin-top:12px;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo $this->session->flashdata('invalid_item'); ?>
                </div>
            <?php } ?>
	        </div><!-- /.box-header -->
	        <div class="box-body table-responsive no-padding">
	          <table class="table table-hover">
	            <tr>
	              <th>ID</th>
	              <th>Plan Title</th>
	              <th>Plan Category</th>
	              <th>Plan Amount</th>
	              <th>Plan Benifits In Days</th>
	              <th>Tips Allowed</th>
	              <th>Intraday Scanner Allowed</th>
	              <th>Traders Clinic Allowed</th>
	              <th>Plan Status</th>
	              <?php if($permissions['can_edit_membership_palns'] == 1) { ?>
	              	<th>Action</th>
	              <?php } ?>
	              <th>Date Created</th>
	              <?php if($session_data['user_type'] == SUPER_ADMIN_USER_TYPE) { ?>
	              	<th>Delete</th>
	              <?php } ?>
	            </tr>
	            <?php
	            	if(!empty($plans)) {
	            		$offset = $offset + 1;
	            		foreach($plans as $val) {
	            ?>
	            	<tr>
		              <td><?php echo $offset++; ?></td>
		              <td>
		              	<?php echo ucfirst($val['plan_title']); ?>	
		              </td>
		              <td>
		              	<span class="label label-success">
		              		<?php echo ucfirst($plan_categories[$val['plan_category']]); ?>
		              	</span>
		              </td>
		              <td><i class="fa fa-inr"></i> <?php echo $val['plan_amount']; ?></td>
		              <td><i class="fa fa-clock-o"></i> <?php echo $val['plan_benifits_in_days']; ?></td>
		              <td>
		              	<?php if($val['is_tips_allowed'] == 1) { ?>
		              		<span class="label label-success">Yes</span>
		              	<?php } else { ?>
	              			<span class="label label-danger">No</span>
		              	<?php } ?>
		              </td>
		              <td>
		              	<?php if($val['is_intraday_scanner_allowed'] == 1) { ?>
		              		<span class="label label-success">Yes</span>
		              	<?php } else { ?>
	              			<span class="label label-danger">No</span>
		              	<?php } ?>
		              </td>
		              <td>
		              	<?php if($val['is_traders_clinic_allowed'] == 1) { ?>
		              		<span class="label label-success">Yes</span>
		              	<?php } else { ?>
	              			<span class="label label-danger">No</span>
		              	<?php } ?>
		              </td>
		              <td>
		              	<?php if($val['plan_status'] == 1) { ?>
		              		<span class="label label-success">Enable</span>
		              	<?php } else { ?>
	              			<span class="label label-warning">Disable</span>
		              	<?php } ?>
		              </td>
		              <?php if($permissions['can_edit_membership_palns'] == 1) { ?>
			              <td>
			              	<a href="<?php cms_url('admin/plans/edit/'.$val['plan_id']); ?>" title="Edit Plan">
			              		<i class="fa fa-pencil"></i> Edit
			              	</a>
			              </td>
			          <?php } ?>
		              <td><?php echo date('d/m/Y', strtotime($val['created_at'])); ?></td>
		              <?php if($session_data['user_type'] == SUPER_ADMIN_USER_TYPE) { ?>
			              <td>
			              	<a href="<?php cms_url('admin/plans/delete/'.$val['plan_id']); ?>" title="Delete Plan" onclick="if(!confirm('Are you sure you want to delete this plan?')) return false;">
			              		<i class="fa fa-trash"></i> Delete
			              	</a>
			              </td>
			           <?php } ?>
		            </tr>
	            <?php } /* End foreach */ ?>
	            <?php } else { ?>
	            	<tr>
	            		<td colspan="9" align="center"><?php echo sprintf(NO_RECORDS_FOUND, 'plans') ?></td>
	            	</tr>
	            <?php } ?>
	          </table>
	        </div><!-- /.box-body -->
	        <div class="box-footer"><?php if($pagination) { echo $pagination; } ?></div>
	      </div><!-- /.box -->
	    </div>
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->