<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'add_new_tip_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-md-12">
	        <!-- general form elements -->
	        <div class="box box-primary">
	          <!-- form start -->
	          <form role="form" action="<?php cms_url('admin/tips/add-new'); ?>" method="post">
	            <div class="box-body">
	            	<!-- Validation error and flash data -->
		            <?php if(validation_errors() || $this->session->flashdata('general_error')) { ?>
		                <div class="alert alert-danger alert-dismissable">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                  <?php echo validation_errors(); ?>
		                  <?php echo $this->session->flashdata('general_error'); ?>
		                </div>
		            <?php } ?>

	            	<div class="col-md-6">
	            		<div class="form-group">
		                  <label for="stock_market_type">Select Stock Market Type</label>
		                  <select name="stock_market_type" class="form-control" id="stock_market_type">
		                  	<option value="">Select Stock Market Type</option>
		                  	<?php 
		                  		if(!empty($stock_market_types)) {
		                  			foreach($stock_market_types as $val){
	                  		?>
	                  			<option value="<?php echo $val['id']; ?>" <?php set_select('stock_market_type', $val['id']); ?>><?php echo ucfirst($val['market_type_title']); ?></option>
	                  			<?php } ?>
	                  		<?php } else { ?>
	                  			<option value="">No Stock Market Types Found</option>
	                  		<?php } ?>
		                  </select>
		                </div>
		                <div class="form-group">
		                  <label for="tip_type">Select Tip Type</label>
		                  <select name="tip_type" class="form-control" id="tip_type">
		                  	<option value="">Select Tip Type</option>
		                  	<?php 
		                  		$tipTypes = get_tip_type();
		                  		if(!empty($tipTypes)) {
		                  			foreach($tipTypes as $key => $val){
	                  		?>
	                  			<option value="<?php echo $key; ?>" <?php set_select('tip_type', $key); ?>><?php echo ucfirst($val); ?></option>
	                  			<?php } ?>
	                  		<?php } else { ?>
	                  			<option value="">No Tip Type Found</option>
	                  		<?php } ?>
		                  </select>
		                </div>
		                <div class="form-group">
		                  <label for="exchange">Enter Tip Exchange Name (Stock Symbol)</label>
		                  <select name="exchange" class="form-control select2" id="exchange">
		                  	<option value="">Select Exchange Name</option>
		                  </select>
		                </div>
		                <div class="form-group">
		                  <label for="exchange_manually">Enter Tip Exchange Name Manually (Stock Symbol)</label>
		                  <input type="text" name="exchange_manually" class="form-control" id="exchange_manually" placeholder="Enter Tip Exchange Name Manually" value="<?php echo set_value('exchange_manually'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="market_type">Select Exchange Market Type</label>
		                  <select name="market_type" class="form-control" id="market_type">
		                  	<option value="">Select Exchange Market Type</option>
		                  	<?php 
		                  		$marketTypes = get_tip_exchange_market_type();
		                  		if(!empty($marketTypes)) {
		                  			foreach($marketTypes as $key => $val){
	                  		?>
	                  			<option value="<?php echo $key; ?>" <?php set_select('market_type', $key); ?>><?php echo ucfirst($val); ?></option>
	                  			<?php } ?>
	                  		<?php } else { ?>
	                  			<option value="">No Exchange Market Type Found</option>
	                  		<?php } ?>
		                  </select>
		                </div>
		                <div class="form-group">
		                  <label for="exchange_tip_type">Select Exchange Tip Type</label>
		                  <select name="exchange_tip_type" class="form-control" id="exchange_tip_type">
		                  	<option value="">Select Exchange Tip Type</option>
		                  	<?php 
		                  		$marketTypes = get_tip_exchange_type();
		                  		if(!empty($marketTypes)) {
		                  			foreach($marketTypes as $key => $val){
	                  		?>
	                  			<option value="<?php echo $key; ?>" <?php set_select('exchange_tip_type', $key); ?>><?php echo ucfirst($val); ?></option>
	                  			<?php } ?>
	                  		<?php } else { ?>
	                  			<option value="">No Exchange Tip Type Found</option>
	                  		<?php } ?>
		                  </select>
		                </div>
		                <div class="form-group">
		                  <label for="tip_publish_status">Select Tip Publish Status</label>
		                  <select name="tip_publish_status" class="form-control" id="tip_publish_status">
                  			<option value="1" <?php set_select('tip_publish_status', 1); ?>>Publish Now</option>
                  			<option value="0" <?php set_select('tip_publish_status', 0); ?>>Publish Later</option>
		                  </select>
		                </div>
		                <div class="form-group tipPublishDate" style="display:none;">
		                  <label for="tip_publish_date">Enter Tip Publish Date</label>
		                  <input type="text" disabled="disabled" name="tip_publish_date" class="form-control datepicker" id="tip_publish_date" placeholder="Enter Tip Publish Date" value="<?php echo set_value('tip_publish_date'); ?>" />
		                </div>
		                <div class="checkbox">
	                      <label>
	                      	<input type="hidden" name="is_automization_on" value="0" />
	                        <input type="checkbox" name="is_automization_on" id="is_automization_on" value="1" checked="checked"'> Do you want to allow to zerodha automization for this tip??
	                      </label>
	                    </div>
	              	</div><!-- .col-md-6 -->
	              	<div class="col-md-6">
	              		<div class="form-group">
		                  <label for="tip_price">Enter Tip Estimated Price</label>
		                  <input type="text" name="tip_price" class="form-control" id="tip_price" placeholder="First Price-Second Price" value="<?php echo set_value('tip_price'); ?>" />
		                  <small><strong>Note: </strong>For multiple tip price use Eg: 100-200</small>
		                </div>
		                <div class="form-group">
		                  <label for="stoploss">Enter Tip Stoploss</label>
		                  <input type="text" name="stoploss" class="form-control" id="stoploss" placeholder="Enter Tip Stoploss" value="<?php echo set_value('stoploss'); ?>" />
		                </div>
		                <h4>Tip Targets:</h4>
		                <div class="row">
		                	<div class="col-md-6">
				                <div class="form-group tip_target1">
				                  <label for="tip_targets[1]">Target 1</label>
				                  <input type="text" name="tip_targets[1]" class="form-control tippTargets" id="tip_target1" placeholder="Enter Tip Target" value="<?php echo set_value('tip_target[1]'); ?>" />
				                </div>
				            </div>
				            <div class="col-md-6">
				                <div class="form-group stoploss_traling1">
				                  <label for="stoploss_traling[1]">Target 1 Stoploss Trail</label>
				                  <input type="text" name="stoploss_traling[1]" class="form-control tippTargets" id="stoploss_traling1" placeholder="Enter Stoploss Trailing" value="<?php echo set_value('stoploss_traling[1]'); ?>" />
				                </div>
				            </div>
			            </div>
			            <div class="row">
		                	<div class="col-md-6">
				                <div class="form-group tip_target2">
				                  <label for="tip_targets[2]">Target 2</label>
				                  <input type="text" name="tip_targets[2]" class="form-control tippTargets" id="tip_target2" placeholder="Enter Tip Target" value="<?php echo set_value('tip_target[2]'); ?>" />
				                </div>
				            </div>
				            <div class="col-md-6">
				                <div class="form-group stoploss_traling2">
				                  <label for="stoploss_traling[2]">Target 2 Stoploss Trail</label>
				                  <input type="text" name="stoploss_traling[2]" class="form-control tippTargets" id="stoploss_traling2" placeholder="Enter Stoploss Trailing" value="<?php echo set_value('stoploss_traling[2]'); ?>" />
				                </div>
				            </div>
			            </div>
			            <div class="row">
		                	<div class="col-md-6">
				                <div class="form-group tip_target3">
				                  <label for="tip_targets[3]">Target 3</label>
				                  <input type="text" name="tip_targets[3]" class="form-control tippTargets" id="tip_target3" placeholder="Enter Tip Target" value="<?php echo set_value('tip_target[3]'); ?>" />
				                </div>
				            </div>
				            <div class="col-md-6">
				                <div class="form-group stoploss_traling3">
				                  <label for="stoploss_traling[3]">Target 3 Stoploss Trail</label>
				                  <input type="text" name="stoploss_traling[3]" class="form-control tippTargets" id="stoploss_traling3" placeholder="Enter Stoploss Trailing" value="<?php echo set_value('stoploss_traling[3]'); ?>" />
				                </div>
				            </div>
			            </div>
		                <div class="form-group appendNewTargets">
		                	<input type="hidden" name="total_added_targets" value="3"/>
		                	<a href="javascript:void(0);" class="addNewMoreTargets"><i class="fa fa-plus"></i> Add More Targets</a>
		                </div>
	              	</div><!-- .col-md-6 -->
	            </div><!-- .box-body -->
	            <div class="box-footer">
	              <input type="submit" name="submit" value="Add Tip" class="btn btn-primary"/>
	            </div>
	          </form>
	        </div><!-- /.box -->
      	</div><!--/.col (left) -->
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
	function removeTargetElement(ele) {
		var totalAddedTargets = $('input[name="total_added_targets"]').val();
		totalAddedTargets--;

		$('#tipTarStoploss'+ele).remove();
		$('input[name="total_added_targets"]').val(totalAddedTargets);
	}

	$(document).ready(function(){
		$('select[name="stock_market_type"]').change(function(){
			$('select[name="exchange"]').html('');
			$('.select2-selection__rendered').text('Select Exchange Name');
		});

		/* Load instruments list */
        $(".select2").select2({
            ajax:{
                url: url+"admin/tips/loadinstruments",
                type: 'POST',
			    dataType: 'json',
			    delay: 250,
			    data: function (params) {
			      return {
			        exchange: params.term,
			        instrument_type_id: $('select[name="stock_market_type"]').val(),
			        page: params.page
			      };
			    },
			    processResults: function (data, params) {
			      params.page = params.page || 1;

			      return {
			        results: data.items,
			        pagination: {
			          more: (params.page * 30) < data.total_count
			        }
			      };
			    },
			    cache: true
            }
        });

		$('#tip_publish_status').change(function(){
			if($(this).val() == 0) {
				$('.tipPublishDate input[name="tip_publish_date"]').removeAttr('disabled');
				$('.tipPublishDate').fadeIn();
			} else {
				$('.tipPublishDate input[name="tip_publish_date"]').attr('disabled', 'disabled');
				$('.tipPublishDate').hide();
			}
		});

		$('.datepicker').datepicker({
		    format: 'dd-mm-yyyy',
		    startDate: '-3d'
		});

		$('.addNewMoreTargets').click(function(){
			var totalAddedTargets = $('input[name="total_added_targets"]').val();
			totalAddedTargets++;

			var html = '<div class="row appendedTarStoploss" id="tipTarStoploss'+totalAddedTargets+'"><a href="javascript:void(0);" class="removeTargetEle" onclick="removeTargetElement('+totalAddedTargets+');"><i class="fa fa-times"></i> Remove</a><div class="col-md-6"><div class="form-group tip_target'+totalAddedTargets+'"><label for="tip_targets['+totalAddedTargets+']">Target '+totalAddedTargets+'</label><input type="text" name="tip_targets['+totalAddedTargets+']" class="form-control tippTargets" id="tip_target'+totalAddedTargets+'" placeholder="Enter Tip Target"/></div></div><div class="col-md-6"><div class="form-group stoploss_traling'+totalAddedTargets+'"><label for="stoploss_traling['+totalAddedTargets+']">Target '+totalAddedTargets+' Stoploss Trail</label><input type="text" name="stoploss_traling['+totalAddedTargets+']" class="form-control tippTargets" id="stoploss_traling'+totalAddedTargets+'" placeholder="Enter Stoploss Trailing" value=""/></div></div></div>';
			
			$('.appendNewTargets').before(html);
			$('input[name="total_added_targets"]').val(totalAddedTargets);
		});

		$('form').submit(function(event){
			var isError = 0;
			var isTargetError = 0;

			if($('#tip_publish_status').val() == 0 && $('input[name="tip_publish_date"]').val() == ''){
				isError = 1;
				$('input[name="tip_publish_date"]').focus();
				alert('Please Select Tip Publish Date');
			}

			$('.tippTargets').each(function(){
				if($(this).val() == '') {
					isError = 1;
					isTargetError = 1;
				}
			});

			if(isError == 1) {
				if(isTargetError == 1) {
					alert('Please Enter Tip Targets');
				}

				event.preventDefault();
				return false;
			} else {
				if(confirm('Are you sure want to to add this tip??')) {
					return true;
				} else {
					return false;
				}
			}
		});
	});
</script>