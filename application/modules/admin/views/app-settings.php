<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'app_settings_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-md-12">
	        <!-- general form elements -->
	        <div class="box box-primary">
	          <!-- form start -->
	          <form role="form" action="<?php cms_url('admin/settings/app'); ?>" method="post" enctype="multipart/form-data">
	            <div class="box-body">
	            	<!-- Validation error and flash data -->
		            <?php if(validation_errors()) { ?>
		                <div class="alert alert-danger alert-dismissable">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                  <?php echo validation_errors(); ?>
		                </div>
		            <?php } if($this->session->flashdata('setting_success')) { ?>
		                <div class="alert alert-success lert-dismissable">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                  <?php echo $this->session->flashdata('setting_success'); ?>
		                </div>
		            <?php } ?>

	            	<div class="col-md-6">
		                <div class="form-group">
		                  <label for="free_trial_days_for_new_members">Free Trial Days (For new registered members)</label>
		                  <input type="number" name="free_trial_days_for_new_members" class="form-control" id="free_trial_days_for_new_members" placeholder="Enter free trial days for newly registered members" value="<?php echo get_option('free_trial_days_for_new_members'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="verification_otp_length">Verification OTP Length</label>
		                  <input type="number" name="verification_otp_length" class="form-control" id="verification_otp_length" placeholder="Enter verification OTP lenght" value="<?php echo get_option('verification_otp_length'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="free_tc_form_submission_limit">Free Traders Clinic Form Submission Limit</label>
		                  <input type="number" name="free_tc_form_submission_limit" class="form-control" id="free_tc_form_submission_limit" placeholder="Enter free traders clinic form submission limit" value="<?php echo get_option('free_tc_form_submission_limit'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="payu_money_merchant_id">PayU Money Merchant ID</label>
		                  <input type="text" name="payu_money_merchant_id" class="form-control" id="payu_money_merchant_id" placeholder="Enter PayU Money Merchant ID" value="<?php echo get_option('payu_money_merchant_id'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="payu_money_merchant_key">PayU Money Merchant Key</label>
		                  <input type="text" name="payu_money_merchant_key" class="form-control" id="payu_money_merchant_key" placeholder="Enter PayU Money Merchant Key" value="<?php echo get_option('payu_money_merchant_key'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="payu_money_salt">PayU Money Salt</label>
		                  <input type="text" name="payu_money_salt" class="form-control" id="payu_money_salt" placeholder="Enter PayU Money Salt" value="<?php echo get_option('payu_money_salt'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="feedback_form_rating_numbers">Feedback Form Rating Numbers</label>
		                  <input type="number" name="feedback_form_rating_numbers" class="form-control" id="feedback_form_rating_numbers" placeholder="Enter feedback form maximum rating number" value="<?php echo get_option('feedback_form_rating_numbers'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="feedback_comment_field_label">Feedback Comment Field label</label>
		                  <input type="text" name="feedback_comment_field_label" class="form-control" id="feedback_comment_field_label" placeholder="Enter feedback comment field label" value="<?php echo get_option('feedback_comment_field_label'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="about_us_content">About Us Content</label>
		                  <textarea style="height: 220px;" name="about_us_content" class="form-control" id="about_us_content" placeholder="Enter about us content."><?php echo get_option('about_us_content'); ?></textarea>
		                </div>
		                <div class="form-group">
		                  <label for="disclaimer_content">Disclaimer Content</label>
		                  <textarea style="height: 220px;" name="disclaimer_content" class="form-control" id="disclaimer_content" placeholder="Enter disclaimer content."><?php echo get_option('disclaimer_content'); ?></textarea>
		                </div>
		                <h3>Error Messages</h3>
		                <hr/>
		                <div class="form-group">
		                  <label for="welcome_user_text_message">Welcome User Text Message</label>
		                  <textarea name="welcome_user_text_message" class="form-control" id="welcome_user_text_message" placeholder="Enter welcome user text message."><?php echo get_option('welcome_user_text_message'); ?></textarea>
		                </div>
		                <div class="form-group">
		                  <label for="verification_otp_message">Verification OTP Message</label>
		                  <textarea name="verification_otp_message" class="form-control" id="verification_otp_message" placeholder="Enter verification OTP message."><?php echo get_option('verification_otp_message'); ?></textarea>
		                </div>
		                <div class="form-group">
		                  <label for="device_exists_message">Device Exists Message</label>
		                  <textarea name="device_exists_message" class="form-control" id="device_exists_message" placeholder="Enter device exists error message."><?php echo get_option('device_exists_message'); ?></textarea>
		                </div>
		                <div class="form-group">
		                  <label for="membership_expire_message">Membership Expire Message</label>
		                  <textarea name="membership_expire_message" class="form-control" id="membership_expire_message" placeholder="Enter membership expire message."><?php echo get_option('membership_expire_message'); ?></textarea>
		                </div>
		                <div class="form-group">
		                  <label for="tc_over_limit_message">Traders Clinic Over Limit Message</label>
		                  <textarea name="tc_over_limit_message" class="form-control" id="tc_over_limit_message" placeholder="Enter traders clinic over limit error message."><?php echo get_option('tc_over_limit_message'); ?></textarea>
		                </div>
		                <div class="form-group">
		                  <label for="paid_membership_join_message">Paid Membership Subscription Message</label>
		                  <textarea name="paid_membership_join_message" class="form-control" id="paid_membership_join_message" placeholder="Enter paid membership subscription message."><?php echo get_option('paid_membership_join_message'); ?></textarea>
		                </div>
		                <div class="form-group">
		                  <label for="paid_membership_expire_message">Paid Membership Expire Message</label>
		                  <textarea name="paid_membership_expire_message" class="form-control" id="paid_membership_expire_message" placeholder="Enter paid membership expire message."><?php echo get_option('paid_membership_expire_message'); ?></textarea>
		                </div>
		                <h3>Sharing Images:</h3>
		                <hr/>
		                <div class="form-group">
		                  <label for="tip_sharing_heading">Sharing Heading</label>
		                  <input type="text" name="tip_sharing_heading" class="form-control" id="tip_sharing_heading" placeholder="Enter tip sharing heading" value="<?php echo get_option('tip_sharing_heading'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="tip_sharing_message">Sharing Message</label>
		                  <textarea name="tip_sharing_message" class="form-control" id="tip_sharing_message" placeholder="Enter tip sharing message"><?php echo get_option('tip_sharing_message'); ?></textarea>
		                </div>
		                <div class="form-group">
		                  <label for="reward_point_on_tip_share">Enter earn reward point on tip share</label>
		                  <input type="text" name="reward_point_on_tip_share" class="form-control" id="reward_point_on_tip_share" placeholder="Enter earn reward point on tip share" value="<?php echo get_option('reward_point_on_tip_share'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="reward_point_actual_value">Enter tip share reward points actual value (1 reward point = ??)</label>
		                  <input type="text" name="reward_point_actual_value" class="form-control" id="reward_point_actual_value" placeholder="Enter tip share reward points actual value" value="<?php echo get_option('reward_point_actual_value'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="share_image1">Share Image#1</label>
		                  <input type="file" name="share_image[]" class="form-control" id="share_image1"/>
		                  <?php if(!empty($share_images[0]['image'])) { ?>
		                  	<img src="<?php echo base_url().'uploads/site/'.$share_images[0]['image']; ?>" class="viewAdminLogo"/>
		                  <?php } ?>
		                </div>
		                <div class="form-group">
		                  <label for="share_image2">Share Image#2</label>
		                  <input type="file" name="share_image[]" class="form-control" id="share_image2"/>
		                  <?php if(!empty($share_images[1]['image'])) { ?>
		                  	<img src="<?php echo base_url().'uploads/site/'.$share_images[1]['image']; ?>" class="viewAdminLogo"/>
		                  <?php } ?>
		                </div>
		                <div class="form-group">
		                  <label for="share_image3">Share Image#3</label>
		                  <input type="file" name="share_image[]" class="form-control" id="share_image3"/>
		                  <?php if(!empty($share_images[2]['image'])) { ?>
		                  	<img src="<?php echo base_url().'uploads/site/'.$share_images[2]['image']; ?>" class="viewAdminLogo"/>
		                  <?php } ?>
		                </div>
		                <h3>Investing.com HTML Codes:</h3>
		                <hr/>
		                <div class="form-group">
		                  <label for="economic_calendar_html_code">Investing.com Economic Calendar HTML Code</label>
		                  <textarea name="economic_calendar_html_code" class="form-control" id="economic_calendar_html_code" placeholder="Enter investing.com economic calendar HTML code."><?php echo get_option('economic_calendar_html_code'); ?></textarea>
		                </div>
		                <div class="form-group">
		                  <label for="earning_calendar_html_code">Investing.com Earning Calendar HTML Code</label>
		                  <textarea name="earning_calendar_html_code" class="form-control" id="earning_calendar_html_code" placeholder="Enter investing.com earning calendar HTML code."><?php echo get_option('earning_calendar_html_code'); ?></textarea>
		                </div>
		                <h3>Zerodha API:</h3>
		                <hr/>
		                <div class="checkbox">
	                      <label>
	                      	<input type="hidden" name="is_zerodha_api_active" value="0" />
	                        <input type="checkbox" name="is_zerodha_api_active" id="is_zerodha_api_active" value="1" <?php if(get_option('is_zerodha_api_active') == 1) { echo 'checked="checked"'; } ?>> Is zerodha API active??
	                      </label>
	                    </div>
	              	</div><!-- .col-md-6 -->
	              	<div class="col-md-6">
	              		<h3>Update Instruments List Manually</h3>
	              		<hr/>
	              		<p><a href="<?php cms_url('api/cron/updateinstrumentlist/NSE'); ?>" title="Update NSE Instruments" onclick="if(!confirm('Are you sure you want to update NSE instruments list manually?')) return false;" class="btn btn-primary" target="_blank">NSE Instruments</a></p>
	              		<p><a href="<?php cms_url('api/cron/updateinstrumentlist/NFO'); ?>" title="Update NFO Instruments" onclick="if(!confirm('Are you sure you want to update NFO instruments list manually?')) return false;" class="btn btn-primary" target="_blank">NFO Instruments</a></p>
	              		<p><a href="<?php cms_url('api/cron/updateinstrumentlist/CDS'); ?>" title="Update CDS Instruments" onclick="if(!confirm('Are you sure you want to update CDS instruments list manually?')) return false;" class="btn btn-primary" target="_blank">CDS Instruments</a></p>
	              		<p><a href="<?php cms_url('api/cron/updateinstrumentlist/MCX'); ?>" title="Update MCX Instruments" onclick="if(!confirm('Are you sure you want to update MCX instruments list manually?')) return false;" class="btn btn-primary" target="_blank">MCX Instruments</a></p>
	              		<h3>Download Zerodha Instruments List</h3>
	              		<hr/>
	              		<p><a href="<?php echo KITE_API_URL."instruments/NSE?api_key=".ZERODHA_API_KEY; ?>" title="Download NSE Instruments List" class="btn btn-primary" target="_blank">NSE Instruments</a></p>
	              		<p><a href="<?php echo KITE_API_URL."instruments/NFO?api_key=".ZERODHA_API_KEY; ?>" title="Download NFO Instruments List" class="btn btn-primary" target="_blank">NFO Instruments</a></p>
	              		<p><a href="<?php echo KITE_API_URL."instruments/CDS?api_key=".ZERODHA_API_KEY; ?>" title="Download CDS Instruments List" class="btn btn-primary" target="_blank">CDS Instruments</a></p>
	              		<p><a href="<?php echo KITE_API_URL."instruments/MCX?api_key=".ZERODHA_API_KEY; ?>" title="Download MCX Instruments List" class="btn btn-primary" target="_blank">MCX Instruments</a></p>
	              		<h3>RSS News Feeds</h3>
	              		<hr/>
	              		<div class="form-group">
		                  <label for="rss_news_links">Enter RSS Feeds News Link</label>
		                  <textarea name="rss_news_links" class="form-control" id="rss_news_links" placeholder="Enter RSS Feeds news link by comma seprated."><?php echo get_option('rss_news_links'); ?></textarea>
		                  <small>Note: RSS Links Eg: (http://example.com,http://example.com)</small>
		                </div>
	              	</div><!-- .col-md-6 -->
	            </div><!-- .box-body -->	

	            <div class="box-footer">
	              <input type="submit" name="submit" value="Save Changes" class="btn btn-primary"/>
	            </div>
	          </form>
	        </div><!-- /.box -->
      	</div><!--/.col (left) -->
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->