<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'view_all_users_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-xs-12">
    	  <!-- Search Filters -->
	      	<div class="box box-primary">
	      		<form action="<?php cms_url('admin/users/view-all'); ?>" method="get">
		        	<div class="box-header">
		          		<h3 class="box-title">Filter Users</h3>
		          		<div class="box-tools pull-right">
		                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		                </div>
	          		</div><!-- /.box-header -->
		        	<div class="box-body">
		        		<div class="col-md-4">
		        			<div class="form-group">
			                  <label for="user_type_id">Select User Type</label>
			                  <select name="user_type_id" class="form-control" id="user_type_id">
			                  	<?php if(!empty($user_types)) { ?>
			                  		<?php if($session_data['user_type'] != STAFF_USER_TYPE){ ?><option value="">Select User Type</option><?php } ?>
			                  		<?php foreach($user_types as $val) { 
			                  			$types = explode('_', $val['type']);
			                  			if(in_array($val['id'], $allowed_user_type)) {
		                  			?>
			                  				<option value="<?php echo $val['id']; ?>" <?php if(isset($_GET['user_type_id']) && $_GET['user_type_id'] == $val['id']) { echo 'selected="selected"'; } ?>><?php echo ucfirst(strtolower($types[0])).' '.ucfirst(strtolower($types[1])); ?></option>
		                  				<?php } ?>
		                  			<?php } ?>
		                  		<?php } else { ?>
		                  			<option value=""></option>
	                  			<?php } ?>
			                  </select>
			                </div>
		        		</div>
		        		<div class="col-md-4">
		        			<div class="form-group">
			                  <label for="user_type_id">Select Membership Type</label>
			                  <select name="membership_type" class="form-control" id="membership_type">
			                  	<?php if(!empty($user_types)) { ?>
			                  		<option value="">Select Membership Type</option>
			                  		<option value="<?php echo FREE_MEMBER; ?>" <?php if(isset($_GET['membership_type']) && $_GET['membership_type'] == FREE_MEMBER) { echo 'selected="selected"'; } ?>>Free</option>
			                  		<option value="<?php echo PREMIUM_MEMBER; ?>" <?php if(isset($_GET['membership_type']) && $_GET['membership_type'] == PREMIUM_MEMBER) { echo 'selected="selected"'; } ?>>Paid</option>
		                  		<?php } else { ?>
		                  			<option value=""></option>
	                  			<?php } ?>
			                  </select>
			                </div>
		        		</div>
		        		<div class="col-md-4">
		        			<div class="form-group">
			                  <label for="custom_text">Enter Text</label>
			                  <input type="text" name="custom_text" class="form-control" placeholder="Enter Text" value="<?php if(isset($_GET['custom_text'])) { echo $_GET['custom_text']; } ?>" />
			                </div>
		        		</div>
		        		<div class="col-md-6">
		        			<button type="submit" class="btn btn-primary">Filter Results</button>
		              		<a href="<?php cms_url('admin/users/view-all'); ?>" class="btn btn-primary">Reset Filter</a>
		        		</div>
		        	</div><!-- /.box-body -->
	            </form>
	      	</div>
	      <!-- Search Filters End Here -->
	      <div class="box box-primary">
	        <div class="box-header">
	          <h3 class="box-title"><?php echo sprintf(ALL_DATA, 'Users'); ?></h3>
	          <?php if($permissions['can_create_users'] == 1) { ?>
	          	<a href="<?php cms_url('admin/users/add-new'); ?>" class="addNewAdmin" title="Add New User">Add New</a>
	          <?php } ?>
	          <?php if($permissions['can_assign_user'] == 1) { ?>
	          	<a href="javascript:void(0);" class="addNewAdmin assignUser" title="Assign to CICM Staff User">Assign User</a>
	          <?php } ?>
	          <div class="box-tools">
	            <div class="input-group customInputGroups" style="width: 150px;">
	              <form action="<?php cms_url('admin/users/view-all'); ?>" method="get">
		              <input type="text" name="s" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo $this->input->get('s'); ?>">
		              <div class="input-group-btn cusInputGrpbtn">
		                <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
		              </div>
	              </form>
	            </div>
	            <?php if($pagination) { echo $pagination; } ?>
	          </div>
	          <!-- flash data -->
            <?php if($this->session->flashdata('item_success')) { ?>
                <div class="alert alert-success alert-dismissable" style="margin-top:12px;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo $this->session->flashdata('item_success'); ?>
                </div>
            <?php } if($this->session->flashdata('invalid_item')) { ?>
            	<div class="alert alert-danger alert-dismissable" style="margin-top:12px;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo $this->session->flashdata('invalid_item'); ?>
                </div>
            <?php } ?>
	        </div><!-- /.box-header -->
	        <div class="box-body table-responsive no-padding">
	          <table class="table table-hover sortable">
	            <thead>
		            <tr>
		              <?php if(!empty($permissions) && $permissions['can_assign_user'] == 1) { ?><th data-defaultsort="disabled">#</th><?php } ?>
		              <th>ID</th>
		              <th>Name</th>
		              <th>User Type</th>
		              <th>Membership</th>
		              <th>Membership Status</th>
		              <th>Membership Expire On</th>
		              <th>Reward Points</th>
		              <th>Status</th>
		              <?php if($session_data['user_type'] != STAFF_USER_TYPE) { ?><th>Assigned Staff</th><?php } ?>
		              <?php if($permissions['can_change_user_membership'] == 1) { ?><th data-defaultsort="disabled">Membership</th><?php } ?>
		              <?php if($permissions['can_see_user_history'] == 1) { ?><th data-defaultsort="disabled">View</th><?php } ?>
		              <th>Last Login At</th>
		              <?php if($permissions['can_edit_user'] == 1) { ?><th data-defaultsort="disabled">Action</th><?php } ?>
		              <th>Date Created</th>
		              <?php if($session_data['user_type'] == SUPER_ADMIN_USER_TYPE) { ?><th data-defaultsort="disabled">Delete</th><?php } ?>
		            </tr>
	            </thead>
	            <tbody>
		            <?php
		            	if(!empty($users)) {
		            		$offset = $offset + 1;
		            		foreach($users as $val) {
		            			if($val['user_type_id'] != SUPER_ADMIN_USER_TYPE){
		            ?>
		            	<tr>
		            	  <?php if(!empty($permissions) && $permissions['can_assign_user'] == 1) { ?>
		            	  	<td>
		            	  		<?php if($val['user_type_id'] == APP_USER_TYPE) { ?>
		            	  			<input type="checkbox" name="assign_to[]" value="<?php echo $val['id']; ?>"/>
		            	  		<?php } else { ?>
		            	  			<input type="checkbox" name="assign_to[]" value="<?php echo $val['id']; ?>" disabled="disabled"/>
		            	  		<?php } ?>
		            	  	</td>
		            	  <?php } ?>
			              <td><?php echo $offset++; ?></td>
			              <td>
			                <?php echo ucfirst($val['name']); ?>
		                	<br/><i class="fa fa-phone"></i> <?php echo $val['phone_number']; ?><br/>
		                	<i class="fa fa-envelope-o"></i>
		                	<a href="<?php cms_url('admin/users/edit/'.$val['id']); ?>" title="Edit User">
		                		<?php echo $val['email']; ?>
			                </a>	
			              </td>
			              <td>
			              	<span class="label label-success">
			              		<?php echo get_user_type_name($val['user_type_id']); ?>
			              	</span>
		              	  </td>
			              <td>
			              	<?php if($val['user_type_id'] == APP_USER_TYPE) { ?>
				              	<?php if($val['membership_type'] == 1) { ?>
				              		<span class="label label-warning">Free</span>
				              	<?php } elseif($val['membership_type'] == 2) { ?>
				              		<span class="label label-success">Paid</span>
				              	<?php } ?>
				              <?php } else { ?>
				              	<span>N/A</span>
				              <?php } ?>
			              </td>
			              <td>
			              	<?php if($val['user_type_id'] == APP_USER_TYPE) { ?>
			              		<?php if($val['membership_status'] == 1) { ?>
				              		<span class="label label-success">Active</span>
				              	<?php } else { ?>
				              		<span class="label label-warning">Expired</span>
				              	<?php } ?>
		              		<?php } else { ?>
		              			<span>N/A</span>
		              		<?php } ?>
			              </td>
			              <td>
			              	<?php if($val['user_type_id'] == APP_USER_TYPE) { ?>
			              		<?php if($val['membership_type'] == FREE_MEMBER) { ?>
			              			<span class="label label-warning"><i class="fa fa-clock-o"></i> <?php echo date('d M, Y', strtotime($val['membership_expire_on'])); ?></span>
		              			<?php } else { ?>
			              			<?php if($val['membership_status'] == 1) { ?>
			              				<span class="label label-success"><i class="fa fa-clock-o"></i> <?php echo (get_paid_user_membership_expiration_date($val['id']) != null) ? date('d M, Y', strtotime(get_paid_user_membership_expiration_date($val['id']))) : 'N/A'; ?></span>
			              			<?php } else { ?>
			              			<span class="label label-success"><i class="fa fa-clock-o"></i> N/A</span>
			              			<?php } ?>
			              		<?php } ?>
			              	<?php } else { ?>
			              		<span>N/A</span>
			              	<?php } ?>			
	              		  </td>
			              <td>
			              	<?php if($val['user_type_id'] == APP_USER_TYPE) { ?>
			              		<?php echo revertDBInt($val['reward_points']); ?>
			              	<?php } else { ?>
			              		<span>N/A</span>
			              	<?php } ?>			
	              		  </td>
			              <td>
			              	<?php if($val['is_user_deleted'] == 1) { ?>
			              		<span class="label label-danger">Deleted</span>
			              	<?php } else { ?>
			              		<?php if($val['status'] == 1) { ?>
			              			<span class="label label-success">Enable</span>
		              			<?php } elseif($val['status'] == 2) { ?>
		              				<span class="label label-info">Pending</span>
		              			<?php } else { ?>
		              				<span class="label label-warning">Disable</span>
		              			<?php } ?>
			              	<?php } ?>
			              </td>
			              <?php if($session_data['user_type'] != STAFF_USER_TYPE) { ?>
			              	<td>
			              		<?php
			              			if(!empty($val['assign_to'])) {
			              				$staffData = get_user($val['assign_to']);
			              				if(!empty($staffData)) {
			              					echo '<a href="'.get_cms_url('admin/users/view-all?s='.$staffData['name']).'" target="_blank"><i class="fa fa-user"></i> '.ucfirst($staffData['name']).'</a>';
			              				} else {
			              					echo '<span>N/A</span>';
			              				}
			              			} else {
			              				echo '<span>N/A</span>';
			              			} 
		              			?>
		              		</td>
			              <?php } ?>
			              <?php if($permissions['can_change_user_membership'] == 1) { ?>
				              <td>
				              	<?php if($val['user_type_id'] == APP_USER_TYPE) { ?>
				              		<a href="<?php cms_url('admin/users/manage-membership/'.$val['id']); ?>" title="Manage Membership"><i class="fa fa-bars"></i> Manage</a>
				              	<?php } else { ?>
				              		<span>N/A</span>
				              	<?php } ?>
				              </td>
				          <?php } ?>
				          <?php if($permissions['can_see_user_history'] == 1) { ?>
				          	<td>
				              	<?php if($val['is_user_deleted'] == 0) { ?>
				              		<?php if($val['user_type_id'] == APP_USER_TYPE) { ?>
						              	<a href="<?php cms_url('admin/users/view/'.$val['id']); ?>" title="View User Profile" target="_blank">
						              		<i class="fa fa-eye"></i> Profile
						              	</a>
						            <?php } else { ?>
					              		<span>N/A</span>
					              	<?php } ?>
					            <?php } else { ?>
					            	<span>N/A</span>
					            <?php } ?>
			              </td>
				          <?php } ?>
				          <td>
				          	<?php 
				          		if($val['user_type_id'] == APP_USER_TYPE) {
				          			echo date('d/m/Y h:i A', strtotime($val['last_login_date']));
				          		} else {
				          			echo 'N/A';
				          		}
		          			?>
	          			  </td>
			              <?php if($permissions['can_edit_user'] == 1) { ?>
				              <td>
				              	<?php if($val['is_user_deleted'] == 0) { ?>
					              	<a href="<?php cms_url('admin/users/edit/'.$val['id']); ?>" title="Edit User">
					              		<i class="fa fa-pencil"></i> Edit
					              	</a>
					            <?php } ?>
				              </td>
				           <?php } ?>
			              <td><?php echo date('d/m/Y', strtotime($val['created_at'])); ?></td>
			              <?php if($session_data['user_type'] == SUPER_ADMIN_USER_TYPE) { ?>
				              <td>
				              	<?php if($val['is_user_deleted'] == 0) { ?>
					              	<a href="<?php cms_url('admin/users/delete/'.$val['id']); ?>" title="Delete User" onclick="if(!confirm('Are you sure you want to delete this user?')) return false;">
					              		<i class="fa fa-trash"></i> Delete
					              	</a>
					             <?php } ?>
				              </td>
				            <?php } ?>
			            </tr>
			            <?php } ?>
		            <?php } /* End foreach */ ?>
		            <?php } else { ?>
		            	<tr>
		            		<td colspan="16" align="center"><?php echo sprintf(NO_RECORDS_FOUND, 'users') ?></td>
		            	</tr>
		            <?php } ?>
	            </tbody>
	          </table>
	        </div><!-- /.box-body -->
	        <div class="box-footer"><?php if($pagination) { echo $pagination; } ?></div>
	      </div><!-- /.box -->
	    </div>
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<div id="assignUserPopUpModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Select CICM Staff User</h4>
      </div>
      <div class="modal-body">
      	<form id="assignuserForm" action="" method="post">
	        <div class="form-group">
	        	<label>Select Staff User</label>
	        	<br/><select name="staff_user" class="form-control" style="width: 250px;">
	        		<?php if(!empty($staffUsers)) { ?>
	        			<option value="">Select Staff User</option>
	        			<?php foreach($staffUsers as $val) { ?>
	        				<option value="<?php echo $val['id']; ?>"><?php echo ucfirst($val['name']); ?> (<?php echo $val['phone_number']; ?>)</option>
	        			<?php } ?>
	    			<?php } else { ?>
	    				<option value="">Nothing Found</option>
	    			<?php } ?>
	        	</select>
	        </div>
	        <div class="form-group">
	        	<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
	        </div>
	    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('select[name="staff_user"]').select2();
		$('.assignUser').on('click', function(){
			if($('input[name="assign_to[]"]:checked').length > 0){
				var users = $('input[name="assign_to[]"]:checked').val();
				$('#assignUserPopUpModal').modal();
			} else {
				alert('Please select assignee user first.');
			}
		});

		$('#assignuserForm').submit(function(e){
			e.preventDefault();
			if($('select[name="staff_user"]').val() != '') {
				var staffUser = $('select[name="staff_user"]').val();
				var appUsers = $('input[name="assign_to[]"]:checked').map(function() {return this.value;}).get().join(',');
				if(staffUser != '') {
					$('#assignuserForm input[type="submit"]').prop('disabled', true);
					$.ajax({
		                type: 'POST',
		                url: url+'admin/users/assignusers',
		                data: 'staffUser='+staffUser+'&appUsers='+appUsers,
		                success:function(response) {
		                    $('#assignuserForm input[type="submit"]').prop('disabled', false);
		                    alert('User assigned successfully.');
		                    window.location.reload();
		                }
		            });
				}
			} else {
				alert('Please select staff user.');
			}
		});
	});
</script>