<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'edit_plan_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-md-12">
	        <!-- general form elements -->
	        <div class="box box-primary">
	          <!-- form start -->
	          <form role="form" action="<?php cms_url('admin/plans/edit/'.$plan['plan_id']); ?>" method="post">
	            <div class="box-body">
	            	<!-- Validation error and flash data -->
		            <?php if(validation_errors() || $this->session->flashdata('general_error')) { ?>
		                <div class="alert alert-danger alert-dismissable">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                  <?php echo validation_errors(); ?>
		                  <?php echo $this->session->flashdata('general_error'); ?>
		                </div>
		            <?php } ?>

	            	<div class="col-md-6">
	            		<div class="form-group">
		                  <label for="plan_category">Plan Category</label>
		                  <select name="plan_category" class="form-control" id="plan_category">
		                  	<option value="">Select Plan Category</option>
		                  	<?php 
		                  		if(!empty($plan_categories)) {
		                  			foreach($plan_categories as $val){
	                  		?>
	                  			<option value="<?php echo $val['id']; ?>" <?php if($val['id'] == $plan['plan_category']) { echo 'selected="selected"'; } ?>><?php echo ucfirst($val['title']); ?></option>
	                  			<?php } ?>
	                  		<?php } else { ?>
	                  			<option value="">No Plan Categories Found</option>
	                  		<?php } ?>
		                  </select>
		                </div>
		                <div class="form-group">
		                  <label for="plan_title">Plan Title</label>
		                  <input type="text" name="plan_title" class="form-control" id="plan_title" placeholder="Enter Plan Title" value="<?php echo $plan['plan_title']; ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="plan_description">Plan Description</label>
		                  <textarea name="plan_description" class="form-control" id="plan_description" placeholder="Enter Plan Description"><?php echo $plan['plan_description']; ?></textarea>
		                </div>
		                <div class="form-group">
		                  <label for="plan_amount">Plan Amount</label>
		                  <input type="number" name="plan_amount" class="form-control" id="plan_amount" placeholder="Enter Plan Amount" value="<?php echo $plan['plan_amount']; ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="plan_benifits_in_days">Plan Benefits Duration</label>
		                  <select name="plan_benifits_in_days" class="form-control" id="plan_benifits_in_days">
		                  	<option value="">Select Plan Benefits Duration</option>
		                  	<?php 
		                  		$durations = plan_durations();
		                  		foreach($durations as $key => $val) { 
	                  		?>
	                  			<option value="<?php echo $key; ?>" <?php if($key == $plan['plan_benifits_in_days']) { echo 'selected="selected"'; } ?>><?php echo $val.' ('.$key.' Days)'; ?></option>
	                  		<?php } ?>
		                  </select>
		                </div>
		                <div class="form-group">
		                  <label for="plan_status">Plan Status</label>
		                  <select name="plan_status" class="form-control" id="plan_status">
                  			<option value="1" <?php if($plan['plan_status'] == 1) { echo 'Selected="selected"'; } ?>>Enable</option>
                  			<option value="0" <?php if($plan['plan_status'] == 0) { echo 'Selected="selected"'; } ?>>Disable</option>
		                  </select>
		                </div>
	              	</div><!-- .col-md-6 -->
	              	<div class="col-md-6">
	              		<h4>Select Plan Benefits:</h4><hr/>
	              		<div class="checkbox">
	                      <label>
	                      	<input type="hidden" name="is_tips_allowed" value="0" />
	                        <input type="checkbox" name="is_tips_allowed" id="is_tips_allowed" value="1" <?php if($plan['is_tips_allowed'] == 1){ echo 'checked="checked"'; } ?>/> Is Tips Allowed??
	                      </label>
	                    </div>
	                    <div class="form-group selectMarketType" <?php if($plan['is_tips_allowed'] != 1) { ?>style="display: none;"<?php } ?>>
		                  <label for="stock_market_type[]">Select allowed segments for this plan</label>
		                  <br/>
		                  <select name="stock_market_type[]" class="form-control select2" id="stock_market_type" multiple="multiple" data-placeholder="Select Stock Market Type">
		                  	<?php 
		                  		if(!empty($stock_market_types)) {
		                  			foreach($stock_market_types as $val){
	                  		?>
	                  			<option <?php if(in_array($val['id'], $stockMarArr)) { echo 'selected="selected"'; } ?> value="<?php echo $val['id']; ?>"><?php echo ucfirst($val['market_type_title']); ?></option>
	                  			<?php } ?>
	                  		<?php } ?>
		                  </select>
		                </div>
	                    <div class="checkbox">
	                      <label>
	                      	<input type="hidden" name="is_intraday_scanner_allowed" value="0" />
	                        <input type="checkbox" name="is_intraday_scanner_allowed" id="is_intraday_scanner_allowed" value="1" <?php if($plan['is_intraday_scanner_allowed'] == 1){ echo 'checked="checked"'; } ?>/> Is Intraday Scanner Allowed??
	                      </label>
	                    </div>
	                    <div class="form-group selectIntradayCats" <?php if($plan['is_intraday_scanner_allowed'] != 1) { ?>style="display: none;"<?php } ?>>
		                  <label for="intrday_scanner_cats[]">Select allowed segments for this plan</label>
		                  <br/>
		                  <select name="intrday_scanner_cats[]" class="form-control select2" id="intrday_scanner_cats" multiple="multiple" data-placeholder="Select Intraday Scanner Categories">
		                  	<?php 
		                  		if(!empty($cats)) {
		                  			foreach($cats as $ckey => $cval){
	                  		?>
	                  			<option <?php if(in_array($ckey, $intradayArr)) { echo 'selected="selected"'; } ?> value="<?php echo $ckey; ?>"><?php echo ucfirst($cval); ?></option>
	                  			<?php } ?>
	                  		<?php } ?>
		                  </select>
		                </div>
	                    <div class="checkbox">
	                      <label>
	                      	<input type="hidden" name="is_traders_clinic_allowed" value="0" />
	                        <input type="checkbox" name="is_traders_clinic_allowed" id="is_traders_clinic_allowed" value="1" <?php if($plan['is_traders_clinic_allowed'] == 1){ echo 'checked="checked"'; }?>/> Is Traders Clinic Allowed??
	                      </label>
	                    </div>
	                    <div class="form-group tcAllowedQuery" <?php if($plan['is_traders_clinic_allowed'] != 1) { ?>style="display: none;"<?php } ?>>
		                  <label for="tc_allowed_query">Enter traders clinic number of query allowed in this plan</label>
		                  <input type="number" name="tc_allowed_query" class="form-control" id="tc_allowed_query" placeholder="Enter number of queries allowed in this plan for trders clinic." value="<?php echo $plan['tc_allowed_query']; ?>" />
		                </div>
	              	</div><!-- .col-md-6 -->
	            </div><!-- .box-body -->
	            <div class="box-footer">
	              <input type="submit" name="submit" value="Save Changes" class="btn btn-primary"/>
	            </div>
	          </form>
	        </div><!-- /.box -->
      	</div><!--/.col (left) -->
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
	$(document).ready(function(){
		$('.select2').select2();
		$('span.select2').css({
			width: '444px'
		});
		$('.select2-search, .select2-search__field').css({
			width: '100%'
		});

		if($('input[name="is_traders_clinic_allowed"]').is(':checked')){
			$('.tcAllowedQuery').fadeIn();
		} else {
			$('.tcAllowedQuery').hide();
		}
		
		$('input[name="is_traders_clinic_allowed"]').click(function(){
			if($(this).is(':checked')){
				$('.tcAllowedQuery').fadeIn();
			} else {
				$('.tcAllowedQuery').hide();
			}
		});

		$('input[name="is_traders_clinic_allowed"]').click(function(){
			if($(this).is(':checked')){
				$('.tcAllowedQuery').fadeIn();
			} else {
				$('.tcAllowedQuery').hide();
			}
		});

		if($('input[name="is_tips_allowed"]').is(':checked')){
			$('.selectMarketType').fadeIn();
		} else {
			$('.selectMarketType').hide();
		}

		$('input[name="is_tips_allowed"]').click(function(){
			if($(this).is(':checked')){
				$('.selectMarketType').fadeIn();
			} else {
				$('.selectMarketType').hide();
			}
		});

		if($('input[name="is_intraday_scanner_allowed"]').is(':checked')){
			$('.selectIntradayCats').fadeIn();
		} else {
			$('.selectIntradayCats').hide();
		}

		$('input[name="is_intraday_scanner_allowed"]').click(function(){
			if($(this).is(':checked')){
				$('.selectIntradayCats').fadeIn();
			} else {
				$('.selectIntradayCats').hide();
			}
		});

		$('form').submit(function(){
			if($('input[name="is_traders_clinic_allowed"]').is(':checked') && $('input[name="tc_allowed_query"]').val() == ''){
				$('input[name="tc_allowed_query"]').focus();
				alert('Please enter number of queries allowed in this plan for trders clinic.');
				return false;
			} else {
				if(confirm('Are you sure want to make changes on this plan because some users are already enroll this plan??')) {
					return true;
				} else {
					return false;
				}
			}
		});
	});
</script>