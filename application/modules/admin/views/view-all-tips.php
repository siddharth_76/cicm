<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'view_all_tips_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-xs-12">
      	    <!-- Search Filters -->
	      	<div class="box box-primary">
	      		<form action="<?php cms_url('admin/tips/view-all'); ?>" method="get">
		        	<div class="box-header">
		          		<h3 class="box-title">Filter Tips</h3>
		          		<div class="box-tools pull-right">
		                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		                </div>
	          		</div><!-- /.box-header -->
		        	<div class="box-body">
		        		<div class="col-md-4">
		        			<div class="form-group">
			                  <label for="tip_type">Select Tip Type</label>
			                  <select name="tip_type" class="form-control" id="tip_type">
			                  	<option value="">Select Tip Type</option>
			                  	<?php 
			                  		$tipTypes = get_tip_type();
			                  		if(!empty($tipTypes)) {
			                  			foreach($tipTypes as $key => $val){
		                  		?>
		                  			<option value="<?php echo $key; ?>" <?php if(isset($_GET['tip_type']) && $_GET['tip_type'] == $key) { echo 'selected="selected"'; } ?>><?php echo ucfirst($val); ?></option>
		                  			<?php } ?>
		                  		<?php } else { ?>
		                  			<option value="">No Tip Type Found</option>
		                  		<?php } ?>
			                  </select>
			                </div>
		        		</div>
		        		<div class="col-md-4">
		        			<div class="form-group">
			                  <label for="stock_market_type">Select Stock Market Type</label>
			                  <select name="stock_market_type" class="form-control" id="stock_market_type">
			                  	<option value="">Select Stock Market Type</option>
			                  	<?php 
			                  		if(!empty($stock_markets)) {
			                  			foreach($stock_markets as $key => $val){
		                  		?>
		                  			<option value="<?php echo $key; ?>" <?php if(isset($_GET['stock_market_type']) && $_GET['stock_market_type'] == $key) { echo 'selected="selected"'; } ?>><?php echo ucfirst($val); ?></option>
		                  			<?php } ?>
		                  		<?php } else { ?>
		                  			<option value="">No Stock Market Types Found</option>
		                  		<?php } ?>
			                  </select>
			                </div>
		        		</div>
		        		<div class="col-md-4">
		        			<div class="form-group">
			                  <label for="market_type">Select Exchange Market Type</label>
			                  <select name="market_type" class="form-control" id="market_type">
			                  	<option value="">Select Exchange Market Type</option>
			                  	<?php 
			                  		$marketTypes = get_tip_exchange_market_type();
			                  		if(!empty($marketTypes)) {
			                  			foreach($marketTypes as $key => $val){
		                  		?>
		                  			<option value="<?php echo $key; ?>" <?php if(isset($_GET['market_type']) && $_GET['market_type'] == $key){ echo 'selected="selected"'; } ?>><?php echo ucfirst($val); ?></option>
		                  			<?php } ?>
		                  		<?php } else { ?>
		                  			<option value="">No Exchange Market Type Found</option>
		                  		<?php } ?>
			                  </select>
			                </div>
		        		</div>
		        		<div class="col-md-4">
		        			<div class="form-group">
			                  <label for="exchange_tip_type">Select Exchange Tip Type</label>
			                  <select name="exchange_tip_type" class="form-control" id="exchange_tip_type">
			                  	<option value="">Select Exchange Tip Type</option>
			                  	<?php 
			                  		$marketTypes = get_tip_exchange_type();
			                  		if(!empty($marketTypes)) {
			                  			foreach($marketTypes as $key => $val){
		                  		?>
		                  			<option value="<?php echo $key; ?>" <?php if(isset($_GET['exchange_tip_type']) && $_GET['exchange_tip_type'] == $key) { echo 'selected="selected"'; } ?>><?php echo ucfirst($val); ?></option>
		                  			<?php } ?>
		                  		<?php } else { ?>
		                  			<option value="">No Exchange Tip Type Found</option>
		                  		<?php } ?>
			                  </select>
			                </div>
		        		</div>
		        		<div class="col-md-4">
		        			<label for="status">Select Tip Status</label>
			                 <select name="status" class="form-control" id="status">
			                 	<option <?php if(isset($_GET['status']) && $_GET['status'] == '-1') { echo 'selected="selected"'; } ?> value="-1">Select Status</option>
			                 	<option <?php if(isset($_GET['status']) && $_GET['status'] == 1) { echo 'selected="selected"'; } ?> value="1">Published</option>
			                 	<option <?php if(isset($_GET['status']) && $_GET['status'] == 2) { echo 'selected="selected"'; } ?> value="2">Pending</option>
			                 	<option <?php if(isset($_GET['status']) && $_GET['status'] == 0) { echo 'selected="selected"'; } ?> value="0">Disabled</option>
			                 	<option <?php if(isset($_GET['status']) && $_GET['status'] == 3) { echo 'selected="selected"'; } ?> value="3">Closed</option>
			                 </select>
		        		</div>
		        		<div class="col-md-12">
		        			<input type="hidden" name="is_search" value="1"/>
		        			<button type="submit" class="btn btn-primary">Filter Results</button>
		              		<a href="<?php cms_url('admin/tips/view-all'); ?>" class="btn btn-primary">Reset Filter</a>
		        		</div>
		        	</div><!-- /.box-body -->
	            </form>
	      	</div>
	      <!-- Search Filters End Here -->
	      <div class="box box-primary">
	        <div class="box-header">
	          <h3 class="box-title"><?php echo sprintf(ALL_DATA, 'Tips'); ?></h3>
	          <?php if($permissions['can_post_tips'] == 1) { ?>
	          	<a href="<?php cms_url('admin/tips/add-new'); ?>" class="addNewAdmin" title="Add New Tip">Add New</a>
	          <?php } ?>
	          <div class="box-tools">
	            <div class="input-group customInputGroups" style="width: 150px;">
	              <form action="<?php cms_url('admin/tips/view-all'); ?>" method="get">
		              <input type="text" name="s" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo $this->input->get('s'); ?>">
		              <div class="input-group-btn cusInputGrpbtn">
		                <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
		              </div>
	              </form>
	            </div>
	            <?php if($pagination) { echo $pagination; } ?>
	          </div>
	          <!-- flash data -->
            <?php if($this->session->flashdata('item_success')) { ?>
                <div class="alert alert-success alert-dismissable" style="margin-top:12px;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo $this->session->flashdata('item_success'); ?>
                </div>
            <?php } if($this->session->flashdata('invalid_item')) { ?>
            	<div class="alert alert-danger alert-dismissable" style="margin-top:12px;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo $this->session->flashdata('invalid_item'); ?>
                </div>
            <?php } ?>
	        </div><!-- /.box-header -->
	        <div class="box-body table-responsive no-padding">
	          <table class="table table-hover sortable">
	            <thead>
		            <tr>
		              <th>ID</th>
		              <th>Type</th>
		              <th>Stock Market</th>
		              <th>Exchange</th>
		              <th>Market Type</th>
		              <th>Exchange Tip Type</th>
		              <th>Price</th>
		              <th>Stoploss</th>
		              <th>Live LTP</th>
		              <th>Total Targets</th>
		              <th data-defaultsort="disabled">All Targets</th>
		              <th>Total Acheived</th>
		              <th>Status</th>
		              <?php if($permissions['can_see_follow_up'] == 1) { ?><th data-defaultsort="disabled">Follow Up's</th><?php } ?>
		              <th>Date Created</th>
		              <?php if($session_data['user_type'] == SUPER_ADMIN_USER_TYPE || $permissions['can_edit_tips'] == 1) { ?>
		              	<th width="70" data-defaultsort="disabled">Action</th>
		              <?php } ?>
		            </tr>
	            </thead>
	            <tbody>
	            <?php
	            	$ltpArray = array();
	            	if(!empty($tips)) {
	            		$offset = $offset + 1;
	            		foreach($tips as $val) {
	            			if($val['status'] == 1) {
	            				$ltpArray[$val['tip_id']] = array(strtoupper($val['stock_market_code']), strtoupper($val['exchange']));
	            			}
	            			$ltpPrice = ($val['status'] == 1) ? get_tip_ltp(strtoupper($val['stock_market_code']), strtoupper($val['exchange'])) : 0;
	            ?>
	            	<tr>
		              <td><?php echo $offset++; ?></td>
		              <td>
		              	<?php echo get_tip_type($val['tip_type']); ?>	
		              </td>
		              <td>
		              	<?php echo $stock_markets[$val['stock_market_type']]; ?>
		              </td>
		              <td><?php echo strtoupper($val['exchange']); ?></td>
		              <td><?php echo get_tip_exchange_market_type($val['market_type']); ?></td>
		              <td><?php echo get_tip_exchange_type($val['exchange_tip_type']); ?></td>
		              <td>
		              	<i class="fa fa-inr"></i>
		              	<?php 
		              		echo revertDBInt($val['price_one']); 
		              		if(!empty($val['price_two'])) {
		              			echo ' - ';
		              			echo revertDBInt($val['price_two']);
		              		}
	              		?>
		              </td>
		              <td><i class="fa fa-inr"></i> <?php echo ($val['acheived_target'] > 0) ? revertDBInt($val['trailed_stoploss']) : revertDBInt($val['stoploss']); ?></td>
		              <td id="tip<?php echo $val['tip_id']; ?>" ltp="<?php echo $ltpPrice; ?>">
		              	<?php if($val['status'] == 1) { ?>
		              		<span class="label label-success"><i class="fa fa-inr"></i> <?php echo $ltpPrice; ?> <i class="fa fa-arrow-up"></i></span>
		              	<?php } else { echo '...'; } ?>
	              	  </td>
		              <td>
		              	<span class="label label-info">
		              		<?php echo $val['total_added_targets']; ?>
		              	</span>
		              </td>
		              <td>
		              	<?php  
		              		$allTargets = get_tip_targets($val['tip_id']);
		              		$allTargetArr = array();
		              		if(!empty($allTargets)) {
		              			foreach($allTargets as $tval) {
		              				$allTargetArr[$tval['target_number']] = revertDBInt($tval['target_amount']); 
		              			}
		              		}
		              		if(!empty($allTargetArr)) {
		              			echo implode(', ', $allTargetArr);
		              		}
		              	?>
		              	<br/><a href="javascript:void(0);" onclick="loadTipTargets('<?php echo $val["tip_id"]; ?>');"><i class="fa fa-eye"></i> View</a>
		              </td>
		              <td>
		              	<?php
		              		$aClass = ($val['acheived_target'] > 0) ? 'success' : 'warning';
		              	?>
		              	<span class="label label-<?php echo $aClass; ?>">
		              		<?php echo $val['acheived_target']; ?>
		              	</span>
		              </td>
		              <td>
		              	<?php if($val['status'] == 1) { ?>
		              		<span class="label label-success">Published</span>
		              	<?php } elseif($val['status'] == 2) { ?>
	              			<span class="label label-warning">Pending</span>
		              	<?php } elseif($val['status'] == 0) { ?>
		              		<span class="label label-danger">Disabled</span>
		              	<?php } elseif($val['status'] == 3) { ?>
		              		<span class="label label-info">Closed</span>
		              	<?php } ?>
		              	</td>
		              </td>
		              <?php if($permissions['can_see_follow_up'] == 1) { ?>
			              <td>
			              	<a href="<?php cms_url('admin/tips/follow-ups/'.$val['tip_id']); ?>" title="View All Follow Up's" target="_blank"><i class="fa fa-eye"></i> View</a>
			              </td>
			          <?php } ?>
		              <td><?php echo date('d/m/Y', strtotime($val['created_at'])); ?></td>
		              <?php if($session_data['user_type'] == SUPER_ADMIN_USER_TYPE || $permissions['can_edit_tips'] == 1) { ?>
			              <td width="70">
			              	<?php if($permissions['can_edit_tips'] == 1) { ?>
				              	<a href="<?php cms_url('admin/tips/edit/'.$val['tip_id']); ?>" title="Edit Tip">
				              		<i class="fa fa-pencil"></i> Edit
				              	</a>
				            <?php } ?>
			              	<?php if($session_data['user_type'] == SUPER_ADMIN_USER_TYPE) { ?>
				              	<br/>
				              	<a href="<?php cms_url('admin/tips/delete/'.$val['tip_id']); ?>" title="Delete Tip" onclick="if(!confirm('Are you sure you want to delete this tip?')) return false;">
				              		<i class="fa fa-trash"></i> Delete
				              	</a>
				            <?php } ?>
			              </td>
			            <?php } ?>
		            </tr>
	            <?php } /* End foreach */ ?>
	            <?php } else { ?>
	            	<tr>
	            		<td colspan="16" align="center"><?php echo sprintf(NO_RECORDS_FOUND, 'tips') ?></td>
	            	</tr>
	            <?php } ?>
	            </tbody>
	          </table>
	        </div><!-- /.box-body -->
	        <div class="box-footer"><?php if($pagination) { echo $pagination; } ?></div>
	      </div><!-- /.box -->
	    </div>
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<div class="modal" id="tipTargetsModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tip Targets:</h4>
      </div>
      <div class="modal-body">
        <img class="targetsLoader" src="<?php admin_assets(); ?>dist/img/ajax-loader.gif"/>
      </div>
      <div class="modal-footer">
      	<input type="hidden" name="loadedTipId" value=""/>
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
	function loadTipTargets(tipId) {
		$('#tipTargetsModal').modal();
		$('input[name="loadedTipId"]').val(tipId);
	}

	function getTipLtpData() {
		var tipArray = '<?php echo json_encode($ltpArray); ?>';
        $.ajax({
            type: 'POST',
            url: url+'admin/tips/getLtp',
            data: 'data='+tipArray,
            success:function(response) {
                var obj = jQuery.parseJSON(response);
				$.each(obj, function(key,value) {
					var ltp = $('#tip'+key).attr('ltp');
					$('#tip'+key).attr('ltp', value);
					if(parseFloat(value) >= parseFloat(ltp)) {
						$('td#tip'+key).html('<span class="label label-success"><i class="fa fa-inr"></i> '+value+' <i class="fa fa-arrow-up"></i></span>');
					} else {
						$('td#tip'+key).html('<span class="label label-danger"><i class="fa fa-inr"></i> '+value+' <i class="fa fa-arrow-down"></i></span>');
					}
				});
				setTimeout(function(){
					getTipLtpData();
				}, 500); 
            }
        });
	}

	$(document).ready(function(){
		$('#tipTargetsModal').on('shown.bs.modal', function (e) {
			setTimeout(function(){
				var tipId = $('input[name="loadedTipId"]').val();
				$.ajax({
	                type: 'POST',
	                url: url+'admin/tips/loadTipTargets',
	                data: 'tipId='+tipId+'&action=loadTipTargets',
	                success:function(response) {
	 					$('#tipTargetsModal .modal-body').html(response);
	                }
	            });
            }, 500);
		});

		$('#tipTargetsModal').on('hidden.bs.modal', function() {
			$('input[name="loadedTipId"]').val('');
			$('#tipTargetsModal .modal-body').html('<img class="targetsLoader" src="<?php admin_assets(); ?>dist/img/ajax-loader.gif"/>');
		});

		<?php if(!empty($ltpArray)) { ?>
			setTimeout(function(){ getTipLtpData(); }, 5000);
        <?php } ?>
	});
</script>