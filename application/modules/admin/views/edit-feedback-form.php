<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'edit_feedback_form_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-md-12">
	        <!-- general form elements -->
	        <div class="box box-primary">
	          <!-- form start -->
	          <form role="form" action="<?php cms_url('admin/feedbacks/edit/'.$form['id']); ?>" method="post">
	            <div class="box-body">
	            	<!-- Validation error and flash data -->
		            <?php if(validation_errors() || $this->session->flashdata('general_error')) { ?>
		                <div class="alert alert-danger alert-dismissable">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                  <?php echo validation_errors(); ?>
		                  <?php echo $this->session->flashdata('general_error'); ?>
		                </div>
		            <?php } ?>

		            <?php if($this->session->flashdata('item_success')) { ?>
		                <div class="alert alert-success alert-dismissable">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                  <?php echo $this->session->flashdata('item_success'); ?>
		                </div>
		            <?php } ?>

	            	<div class="col-md-6">
	            		<?php 
	            			$elements = unserialize($form['elements']);
	            			if(!empty($elements)) {
	            				$id = 1;
	            				foreach($elements as $val) {
	            		?>
				                <div class="form-group">
				                  <label for="questions[]">Enter Question</label>
				                  <input type="text" name="questions[]" class="form-control" id="question<?php echo $id++; ?>" placeholder="Enter Question" value="<?php echo $val; ?>" />
				                </div>
				            <?php } ?>
			            <?php } ?>
		                <div class="form-group appendFormSections">
		                	<input type="hidden" name="total_elements" value="<?php echo count($elements); ?>"/>
		                	<a href="javascript:void(0);" class="addMoreSections"><i class="fa fa-plus"></i> Add More</a>
		                </div>
		                <div class="checkbox">
	                      <label>
	                      	<input type="hidden" name="show_comment_box" value="0" />
	                        <input type="checkbox" name="show_comment_box" value="1" <?php if($form['show_comment_box'] == 1) { echo 'checked="checked"'; } ?>> Show Comment Box??
	                      </label>
	                    </div>
		                <div class="form-group">
		                  <label for="status">Form Status</label>
		                  <select name="status" class="form-control" id="status">
                  			<option value="1" <?php if($form['status'] == 1) { echo 'selected="selected"'; } ?>>Publish</option>
                  			<option value="0" <?php if($form['status'] == 0) { echo 'selected="selected"'; } ?>>Pending</option>
		                  </select>
		                </div>
	              	</div><!-- .col-md-6 -->
	            </div><!-- .box-body -->
	            <div class="box-footer">
	              <input type="submit" name="submit" value="Save Changes" class="btn btn-primary"/>
	            </div>
	          </form>
	        </div><!-- /.box -->
      	</div><!--/.col (left) -->
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
	function removeEle(id) {
		if(confirm('Are you sure want to delete this question??')) {
			var id = $('input[name="total_elements"]').val();
			$('#ele'+id).remove();
			id--;
			$('input[name="total_elements"]').val(id);
		}
	}

	$(document).ready(function(){
		$('.addMoreSections').click(function(){
			var id = $('input[name="total_elements"]').val();
			id++;
			var html = '<div class="form-group" id="ele'+id+'">';
          	html += '<label for="fields[question]">Enter Question</label>';
          	html += '<a href="javascript:void(0);" onclick="removeEle('+id+');" class="removeQues"><i class="fa fa-times"></i> Remove</a>';
		    html += '<input type="text" name="questions[]" class="form-control" id="question'+id+'" placeholder="Enter Question" value="" />';
		    html += '</div>';
			$('.appendFormSections').before(html);
			$('input[name="total_elements"]').val(id);
		});

		$('form').submit(function(){
			var isError = 0;
			$('input[name="questions[]"]').each(function(){
				if($(this).val() == '') {
					isError = 1;
				}
			});

			if(isError == 1) {
				alert('Please Fill All Question Fields.');
				return false;
			}
		});
	});
</script>