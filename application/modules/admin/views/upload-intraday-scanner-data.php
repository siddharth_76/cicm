<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'upload_intraday_scanner_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-md-12">
	        <!-- general form elements -->
	        <div class="box box-primary">
	          <!-- form start -->
	          <form role="form" action="<?php cms_url('admin/hawkeye/upload'); ?>" method="post" enctype="multipart/form-data">
	            <div class="box-body">
	            	<!-- Validation error and flash data -->
		            <?php if(validation_errors() || $this->session->flashdata('general_error')) { ?>
		                <div class="alert alert-danger alert-dismissable">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                  <?php echo validation_errors(); ?>
		                  <?php echo $this->session->flashdata('general_error'); ?>
		                </div>
		            <?php } ?>

	            	<div class="col-md-6">
		                <div class="form-group">
		                  <label for="category_id">Select Intraday Scanner Category</label>
		                  <select name="category_id" class="form-control" id="category_id">
                  			<?php if(!empty($categories)) { ?>
                  				<option value="">Select Intraday Scanner Category</option>
                  			`<?php foreach($categories as $val) { ?>
                  				<option value="<?php echo $val['id']; ?>"><?php echo ucfirst($val['title']); ?></option>
                  				<?php } ?>
                  			<?php } else { ?>
                  				<option value="">No Categories Found</option>
                  			<?php } ?>
		                  </select>
		                </div>
		                <div class="form-group">
		                  <label for="publish_date">Select Publish Date</label>
		                  <input type="text" name="publish_date" class="form-control datepicker" id="publish_date" value="<?php echo date('Y-m-d'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="attachment">Select File</label>
		                  <input type="file" name="attachment" class="form-control" id="attachment" />
		                </div>
	              	</div><!-- .col-md-6 -->
	            </div><!-- .box-body -->
	            <div class="box-footer">
	              <input type="submit" name="submit" value="Upload Data" class="btn btn-primary"/>
	            </div>
	          </form>
	        </div><!-- /.box -->
      	</div><!--/.col (left) -->
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
	$(document).ready(function(){
		$('.datepicker').datepicker({
		    format: 'yyyy-mm-dd',
		    startDate: '-3d'
		});
	});
</script>