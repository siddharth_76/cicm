<?php 
	if($action = 'loadTipTargets') {
		if(!empty($targets)) {
?>
	<table class="table">
		<thead>
			<tr>
				<th>Target No.</th>
				<th>Target Amount</th>
				<th>Is Acheived</th>
			</tr>
		</thead>
		<?php foreach($targets as $val) { ?>
			<tr>
				<td><?php echo $val['target_number']; ?></td>
				<td><?php echo revertDBInt($val['target_amount']); ?></td>
				<td>
					<?php if($val['is_achieved'] == 1) { ?>
						<span class="label label-success">Yes</span>
					<?php } else { ?>
						<span class="label label-danger">No</span>
					<?php } ?>
				</td>
			</tr>
		<?php } ?>
	</table>
 	<?php } ?>
<?php } ?>