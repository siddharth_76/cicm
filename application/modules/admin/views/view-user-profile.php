<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'view_user_profile_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-xs-12">
	      <div class="box box-primary">
	        <div class="box-header">
	          <h3 class="box-title">View User Profile : <i class="fa fa-user"></i> <?php echo ucfirst($user['name']); ?>, <i class="fa fa-phone"></i> <?php echo $user['phone_number']; ?></h3>
	          <div class="box-tools"></div>
	        </div><!-- /.box-header -->
	        <div class="box-body table-responsive no-padding">
	        	<div class="col-md-12">
	        		<input type="hidden" name="user_id" value="<?php echo $user['id']; ?>"/>
	        		<h3>Profile Snapshot:</h3><hr/>
	        		<div class="col-md-6">
		        		<p><label><i class="fa fa-user"></i> Name : <?php echo ucfirst($user['name']); ?></label></p>
		        		<p><label><i class="fa fa-phone"></i> Phone Number : <?php echo $user['phone_number']; ?></label></p>
		        		<p><label><i class="fa fa-email"></i> Email : <a href="mailto:<?php echo $user['email']; ?>"><?php echo $user['email']; ?></a></label></p>
		        		<p><label><i class="fa fa-calendar"></i> Joined On : </label> <?php echo date('m/d/Y', strtotime($user['created_at'])); ?></p>
	        		</div>
	        		<div class="col-md-6">
	        			<p><label><i class="fa fa-bars"></i> Membership Type : <?php echo ($user['membership_type'] == FREE_MEMBER) ? 'Free' : 'Paid'; ?></label></p>
		        		<p><label><i class="fa fa-bars"></i> Membership Status : <?php echo ($user['membership_status'] == 1) ? 'Active' : 'Expired'; ?></label></p>
		        		<?php if($user['membership_status'] != 1) { ?>
		        			<p><label><i class="fa fa-calendar"></i> Membership Expired On : <?php echo date('m/d/Y', strtotime($user['membership_expire_on'])) ?></label></p>
		        		<?php } else { ?>
		        			<p><label><i class="fa fa-calendar"></i> Membership Expiring On : <?php echo date('m/d/Y', strtotime($user['membership_expire_on'])) ?></label></p>
		        		<?php } ?>
		        		<p><label><i class="fa fa-money"></i> Reward Points : </label> <?php echo revertDBInt($user['reward_points']); ?></p>
	        		</div>
	        	</div><!-- .col-md-12 -->
	        	<div class="col-md-12">
              	  <!-- Custom Tabs -->
	              <div class="nav-tabs-custom">
	                <ul class="nav nav-tabs">
	                  <?php if($permissions['can_log_user_followup_info'] == 1) { ?>
	                  	<li class="active"><a href="#tab_1" data-toggle="tab">Write Follow Up</a></li>
	                  <?php } ?>
	                  <?php if($permissions['can_see_user_subscription_history'] == 1) { ?>
	                  	<li><a href="#tab_2" data-toggle="tab">Subscription History</a></li>
	                  <?php } ?>
	                  <?php if($permissions['can_see_user_membership_extension'] == 1) { ?>
	                  	<li><a href="#tab_3" data-toggle="tab">Membership Extension</a></li>
	                  <?php } ?>
	                  <?php if($permissions['can_see_user_traders_clinic_enquiry'] == 1) { ?>
	                  	<li><a href="#tab_4" data-toggle="tab">Traders Clinic Enquiry</a></li>
	                  <?php } ?>
	                  <?php if($permissions['can_see_user_feedbacks'] == 1) { ?>
	                  	<li><a href="#tab_5" data-toggle="tab">Feedbacks</a></li>
	                  <?php } ?>
	                  <?php if($permissions['can_see_user_shared_tips'] == 1) { ?>
	                  	<li><a href="#tab_6" data-toggle="tab">Shared Tips</a></li>
	                  <?php } ?>
	                  <?php if($permissions['can_see_user_payment_history'] == 1) { ?>
	                  	<li><a href="#tab_7" data-toggle="tab">Payment History</a></li>
	                  <?php } ?>
	                  <?php if($permissions['can_send_sms_to_user'] == 1) { ?>
	                  	<li><a href="#tab_8" data-toggle="tab">Send SMS</a></li>
	                  <?php } ?>
	                  <?php if($permissions['can_log_user_followup_info'] == 1) { ?>
	                  	<li><a href="#tab_9" data-toggle="tab">Follow Up Timeline</a></li>
	                  <?php } ?>
	                </ul>
	                <div class="tab-content">
	                  <?php if($permissions['can_log_user_followup_info'] == 1) { ?>
	                  	<div class="tab-pane active" id="tab_1">
	                  		<div class="row">
		                  		<div class="col-md-12">
		                  			<div class="form-group">
		                  				<label>Write Follow Up Message:</label>
		                  				<textarea style="height: 200px;" name="message" class="form-control" placeholder="Write follow up message here."></textarea>
		                  			</div>
		                  			<div class="form-group">
		                  				<a href="javascript:void(0);" class="submitFollowUp btn btn-primary">Submit</a>
		                  			</div>
		                  		</div>
		                  	</div>
	                  	</div><!-- /.tab-pane -->
	                  <?php } ?>
	                  <?php if($permissions['can_see_user_subscription_history'] == 1) { ?>
	                  	<div class="tab-pane" id="tab_2">
	                  		<div class="row">
	                  			<div class="col-md-12">
	                  				<?php if(!empty($subscription)) { ?>
	                  					<ul class="timeline">
	                  					<?php 
	                  						foreach($subscription as $val) { 
	                  							$planData = $this->common_model->getSingleRecordById(MEMBERSHIP_PLAN, array('plan_id' => $val['subscribed_plan_id']));
	                  							$planCat = $this->common_model->getSingleRecordById(MEMBERSHIP_PLAN_CATEGORY, array('id' => $planData['plan_category']));
	                  					?>
	                  							<li class="time-label">
								                  <span class="bg-red"><?php echo date('d M, Y',strtotime($val['created_at'])); ?></span>
								                </li>
								                <!-- /.timeline-label -->
								                <!-- timeline item -->
								                <li>
								                  <i class="fa fa-bars bg-blue"></i>
								                  <div class="timeline-item">
								                    <span class="time"><i class="fa fa-clock-o"></i> <?php echo date('h:i A',strtotime($val['created_at'])); ?></span>
								                    <h3 class="timeline-header">Membership Plan <a href="javascript:void(0);"><?php echo ucfirst($planData['plan_title']); ?></a> Subscribed By <i class="fa fa-user"></i> <?php echo ucfirst($user['name']); ?></h3>
								                    <div class="timeline-body">
								                    	<p><label>Plan Info:</label></p>
								                    	<div class="row">
									                    	<div class="col-md-6">
										                    	<p><label>Plan Name: <?php echo $planData['plan_title']; ?> (<?php echo ucfirst($planCat['title']); ?>)</label></p>
										                    	<p><label>Benefits Days: <?php echo $planData['plan_benifits_in_days']; ?></label></p>
										                    	<p><label>Amount: <i class="fa fa-inr"></i> <?php echo $planData['plan_amount']; ?></label></p>
										                    	<p><label>Membership Status: <?php echo ($val['status'] == 1) ? 'Active' : 'Expired'; ?></label></p>
										                    </div>
										                    <div class="col-md-6">
										                    	<p><label>Is Tips Allowed: <?php echo ($planData['is_tips_allowed'] == 1) ? 'Yes' : 'No'; ?></label></p>
										                    	<p><label>Is Intraday Scanner Allowed: <?php echo ($planData['is_intraday_scanner_allowed']) ? 'Yes' : 'No'; ?></label></p>
										                    	<p><label>Is traders Clinic Allowed: <?php echo ($planData['is_traders_clinic_allowed']) ? 'Yes' : 'No'; ?></label></p>
										                    	<?php if($planData['is_traders_clinic_allowed'] == 1) { ?>
										                    		<p><label>Allowed Traders Clinic Enquiry: <?php echo $planData['tc_allowed_query']; ?></label></p>
										                    	<?php } ?>
										                    </div>
										                </div>
								                    </div>
								                    <div class="timeline-footer"></div>
								                  </div>
								                </li>
	                  					<?php } ?>
		                  					<li>
							                  <i class="fa fa-clock-o bg-gray"></i>
							                </li>
						            	</ul><!-- .timeline -->
	                  				<?php } else { ?>
	                  					<p><label>Nothing Found.</label></p>
	                  				<?php } ?>
	                  			</div>
	                  		</div>
	                  	</div><!-- /.tab-pane -->
	                  <?php } ?>
	                  <?php if($permissions['can_see_user_membership_extension'] == 1) { ?>
	                  	<div class="tab-pane" id="tab_3">
	                  		<div class="row">
	                  			<div class="col-md-12">
	                  				<?php if(!empty($membershipExtension)) { ?>
	                  					<ul class="timeline">
		                  					<?php foreach($membershipExtension as $val) { ?>
		                  						<li class="time-label">
								                  <span class="bg-red"><?php echo date('d M, Y',strtotime($val['created_at'])); ?></span>
								                </li>
								                <!-- /.timeline-label -->
								                <!-- timeline item -->
								                <li>
								                  <i class="fa fa-user bg-blue"></i>
								                  <div class="timeline-item">
								                    <span class="time"><i class="fa fa-clock-o"></i> <?php echo date('h:i A',strtotime($val['created_at'])); ?></span>
								                    <h3 class="timeline-header">Membership Extension</h3>
								                    <div class="timeline-body">
								                    	<p><label>Membership Start Date: <?php echo date('m/d/Y', strtotime($val['membership_start_date'])); ?></label></p>
								                    	<p><label>Membership End Date: <?php echo date('m/d/Y', strtotime($val['membership_end_date'])); ?></label></p>
								                    	<p><label>Membership Type: <?php echo ($val['membership_type'] == 1) ? 'Free' : 'Paid'; ?></label></p>
								                    	<p><?php echo $val['membership_extended_text']; ?><p>
							                    	</div>
								                    <div class="timeline-footer"></div>
								                  </div>
								                </li>
		                  					<?php } ?>
		                  					<li>
							                  <i class="fa fa-clock-o bg-gray"></i>
							                </li>
						            	</ul><!-- .timeline -->
	                  				<?php } else { ?>
	                  					<p><label>Nothing Found.</label></p>
	                  				<?php } ?>
	                  			</div>
	                  		</div>
	                  	</div><!-- /.tab-pane -->
	                  <?php } ?>
	                  <?php if($permissions['can_see_user_traders_clinic_enquiry'] == 1) { ?>
		                  <div class="tab-pane" id="tab_4">
		                  	<div class="row">
	                  			<div class="col-md-12">
	                  				<table class="table table-hover">
							            <tr>
							              <th>ID</th>
							              <th>Name</th>
							              <th>Phone</th>
							              <th>Script</th>
							              <th>Market</th>
							              <th>Quantity</th>
							              <th>Price</th>
							              <th>Position</th>
							              <th>Vision</th>
							              <th>Comment</th>
							              <th>Status</th>
							              <th>Date Created</th>
							              <?php if($permissions['can_see_traders_clinic_conversations'] == 1) { ?><th>Chat</th><?php } ?>
							            </tr>
							            <?php
							            	if(!empty($tradersclinic)) {
							            		$i = 1;
							            		foreach($tradersclinic as $val) {
							            ?>
							            	<tr>
								              <td><?php echo $i++; ?></td>
								              <td>
								              	<?php echo ucfirst($val['first_name']).' '.ucfirst($val['last_name']); ?>	
								              </td>
								              <td>
								              	<?php echo ucfirst($val['phone_number']); ?>	
								              </td>
								              <td><?php echo $val['script_name']; ?></td>
								              <td>
								              	<?php  
								              		$scr = explode(',', $val['market_type']);
								              		if(!empty($scr)) {
								              			foreach($scr as $v) {
								              	?>
								              		<?php $scNames[] = $scripts[$v]; ?>
								              		<?php } ?>
								              	<?php echo implode(', ', $scNames); } ?>
								              </td>
								              <td><?php echo $val['quantity']; ?></td>
								              <td><i class="fa fa-inr"></i> <?php echo revertDBInt($val['price']); ?></td>
								              <td><?php echo ($val['position'] == 1) ? 'Long' : 'Short'; ?></td>
								              <td><?php echo $vision[$val['vision']]; ?></td>
								              <td><?php echo $val['comment']; ?></td>
								              <td>
								              	<?php if($val['status'] == 1) { ?>
								              		<span class="label label-success">Active</span>
								              	<?php } else { ?>
							              			<span class="label label-warning">Closed</span>
								              	<?php } ?>
								              </td>
								              <td><?php echo date('d/m/Y', strtotime($val['created_at'])); ?></td>
								              <?php if($permissions['can_see_traders_clinic_conversations'] == 1) { ?>
									              <td>
									              	<a href="<?php cms_url('admin/tradersclinic/view/'.$val['id']); ?>" title="View Detail">
									              		<i class="fa fa-external-link"></i> View
									              	</a>
									              </td>
									           <?php } ?>
								            </tr>
							            <?php unset($scNames); } /* End foreach */ ?>
							            <?php } else { ?>
							            	<tr>
							            		<td colspan="13" align="center"><?php echo sprintf(NO_RECORDS_FOUND, 'traders clinic data') ?></td>
							            	</tr>
							            <?php } ?>
							          </table>
	                  			</div>
	                  		</div>
		                  </div><!-- /.tab-pane -->
		              <?php } ?>
		              <?php if($permissions['can_see_user_feedbacks'] == 1) { ?>
		                  <div class="tab-pane" id="tab_5">
		                  	<div class="row">
	                  			<div class="col-md-12">
	                  				<?php if(!empty($feedbacks)) { ?>
	                  					<ul class="timeline">
		                  					<?php 
		                  						foreach($feedbacks as $val) { 
		                  							$form = $this->common_model->getSingleRecordById(FEEDBACK_FORM_ELEMENT, array('id' => $val['form_id']));
	                  						?>
		                  						<li class="time-label">
								                  <span class="bg-red"><?php echo date('d M, Y',strtotime($val['created_at'])); ?></span>
								                </li>
								                <!-- /.timeline-label -->
								                <!-- timeline item -->
								                <li>
								                  <i class="fa fa-envelope-o bg-blue"></i>
								                  <div class="timeline-item">
								                    <span class="time"><i class="fa fa-clock-o"></i> <?php echo date('h:i A',strtotime($val['created_at'])); ?></span>
								                    <h3 class="timeline-header">Feedback</h3>
								                    <div class="timeline-body">
								                    	<?php 
									                      $questions = unserialize($form['elements']);
									                      $answers = explode(',', $val['answers']);
									                      foreach($questions as $k => $v) {
									                    ?>
									                      <div class="form-group">
									                        <p>
									                          <label for="question">Topic:</label>
									                          <?php echo ucfirst($v); ?>
									                        </p>
									                        <p>
									                          <label for="answer">Rating:</label>
									                          <?php echo $answers[$k]; ?>
									                        </p>
									                      </div>
									                    <?php } ?>
								                    	<p><label>Comment: <?php echo $val['comment']; ?></label></p>
							                    	</div>
								                    <div class="timeline-footer"></div>
								                  </div>
								                </li>
		                  					<?php } ?>
		                  					<li>
							                  <i class="fa fa-clock-o bg-gray"></i>
							                </li>
						            	</ul><!-- .timeline -->
	                  				<?php } else { ?>
	                  					<p><label>Nothing Found.</label></p>
	                  				<?php } ?>
	                  			</div>
	                  		</div>
		                  </div><!-- /.tab-pane -->
		              <?php } ?>
		              <?php if($permissions['can_see_user_shared_tips'] == 1) { ?>
		                  <div class="tab-pane" id="tab_6">
		                  	<div class="row">
	                  			<div class="col-md-12">
	                  				<?php if(!empty($sharedTips)) { ?>
	                  				<ul class="timeline">
	                  					<?php 
	                  						foreach($sharedTips as $val) { 
	                  							$tip = $this->common_model->getSingleRecordById(TIP, array('tip_id' => $val['tip_id']));
	                  							if(!empty($tip['price_one']) && !empty($tip['price_two'])) {
								            		$price = revertDBInt($tip['price_one']).'-'.revertDBInt($tip['price_two']);
								            	} else {
								            		$price = revertDBInt($tip['price_one']);
								            	}
              							?>
	                  						<li class="time-label">
							                  <span class="bg-red"><?php echo date('d M, Y',strtotime($val['shared_at'])); ?></span>
							                </li>
							                <!-- /.timeline-label -->
							                <!-- timeline item -->
							                <li>
							                  <i class="fa fa-lightbulb-o bg-blue"></i>
							                  <div class="timeline-item">
							                    <span class="time"><i class="fa fa-clock-o"></i> <?php echo date('h:i A',strtotime($val['shared_at'])); ?></span>
							                    <h3 class="timeline-header">Shared Tip</h3>
							                    <div class="timeline-body">
							                    	<p><label>Tip: <?php echo get_tip_type($tip['tip_type']).' - '.get_tip_exchange_market_type($tip['market_type']).' '.$tip['exchange'].' '.get_tip_exchange_type($tip['exchange_tip_type']).' @ '.$price.' @ '.revertDBInt($tip['stoploss']); ?></label></p>
							                    	<p><label>Is Earned Reward Points: <?php echo ($val['is_earned_points'] == 1) ? 'Yes' : 'No'; ?></label></p>
							                    </div>
							                    <div class="timeline-footer"></div>
							                  </div>
							                </li>
	                  					<?php } ?>
	                  					<li>
						                  <i class="fa fa-clock-o bg-gray"></i>
						                </li>
	                  				</ul>
	                  				<?php } else { ?>
	                  					<p><label>Nothing Found.</label></p>
	                  				<?php } ?>
	                  			</div>
	                  		</div>
		                  </div><!-- /.tab-pane -->
		              <?php } ?>
		              <?php if($permissions['can_see_user_payment_history'] == 1) { ?>
		                  <div class="tab-pane" id="tab_7">
		                  	<div class="row">
	                  			<div class="col-md-12">
	                  				<?php if(!empty($payments)) { ?>
	                  					<ul class="timeline">
		                  					<?php foreach($payments as $val) { ?>
		                  						<li class="time-label">
								                  <span class="bg-red"><?php echo date('d M, Y',strtotime($val['created_at'])); ?></span>
								                </li>
								                <!-- /.timeline-label -->
								                <!-- timeline item -->
								                <li>
								                  <i class="fa fa-money bg-blue"></i>
								                  <div class="timeline-item">
								                    <span class="time"><i class="fa fa-clock-o"></i> <?php echo date('h:i A',strtotime($val['created_at'])); ?></span>
								                    <h3 class="timeline-header">Payment Info</h3>
								                    <div class="timeline-body">
								                    	<p><label>Transaction ID: <?php echo $val['txn_id']; ?></label></p>
								                    	<p><label>Amount: <i class="fa fa-inr"></i> <?php echo revertDBInt($val['total_amount']); ?></label></p>
								                    	<p><label>Is Reward Points used: <?php echo ($val['is_reward_points_used'] == 1) ? 'Yes' : 'No'; ?></label></p>
								                    	<?php if($val['is_reward_points_used'] == 1) { ?>
								                    		<p><label>Used Reward Points: <?php echo revertDBInt($val['used_reward_points']); ?></label></p>
								                    	<?php } ?>
								                    	<p><label>Payment Status: <?php echo strtoupper($val['payment_status']); ?></label></p>
								                    </div>
								                    <div class="timeline-footer"></div>
								                  </div>
								                </li>
		                  					<?php } ?>
		                  					<li>
							                  <i class="fa fa-clock-o bg-gray"></i>
							                </li>
						            	</ul><!-- .timeline -->
	                  				<?php } else { ?>
	                  					<p><label>Nothing Found.</label></p>
	                  				<?php } ?>
	                  			</div>
	                  		</div>
		                  </div><!-- /.tab-pane -->
		              <?php } ?>
		              <?php if($permissions['can_send_sms_to_user'] == 1) { ?>
		                  <div class="tab-pane" id="tab_8">
		                  	<div class="row">
		                  		<div class="col-md-12">
		                  			<div class="form-group">
		                  				<label>Send SMS:</label>
		                  				<textarea style="height: 200px;" name="sms" class="form-control" placeholder="Write message here."></textarea>
		                  			</div>
		                  			<div class="form-group">
		                  				<a href="javascript:void(0);" class="submitSms btn btn-primary">Submit</a>
		                  			</div>
		                  		</div>
		                  	</div>
		                  </div><!-- /.tab-pane -->
		              <?php } ?>
		              <?php if($permissions['can_log_user_followup_info'] == 1) { ?>
		                  <div class="tab-pane" id="tab_9">
		                  	<div class="row">
					            <div class="col-md-12">
					            <?php if(!empty($follow_ups)) { ?>
					            	<ul class="timeline">
						            	<?php foreach($follow_ups as $val) { ?>
						            		<?php $userData = get_user($val['sent_by']); ?>
						            		<li class="time-label">
							                  <span class="bg-red"><?php echo date('d M, Y',strtotime($val['created_at'])); ?></span>
							                </li>
							                <!-- /.timeline-label -->
							                <!-- timeline item -->
							                <li>
							                  <i class="fa fa-envelope bg-blue"></i>
							                  <div class="timeline-item">
							                    <span class="time"><i class="fa fa-clock-o"></i> <?php echo date('h:i A',strtotime($val['created_at'])); ?></span>
							                    <h3 class="timeline-header">Follow Up By : <i class="fa fa-user"></i> <?php echo ucfirst($userData['name']); ?></h3>
							                    <div class="timeline-body"><?php echo $val['message']; ?></div>
							                    <div class="timeline-footer"></div>
							                  </div>
							                </li>
						            	<?php } ?>
						            	<li>
						                  <i class="fa fa-clock-o bg-gray"></i>
						                </li>
					            	</ul><!-- .timeline -->
					            <?php } else { ?>
					            	<p><label>Nothing has been added yet.</label></p>
					            <?php } ?>
					            </div><!-- /.col -->
					          </div><!-- /.row -->
		                  </div><!-- #tab_9 -->
		                <?php } ?>
	                </div><!-- /.tab-content -->
	              </div><!-- nav-tabs-custom -->
            	</div><!-- .col-md-12 -->
	        </div><!-- /.box-body -->
	        <div class="box-footer"></div>
	      </div><!-- /.box -->
	    </div>
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
	$(document).ready(function(){
		var userId = $('input[name="user_id"]').val();
		$('.submitFollowUp').click(function(){
			var ele = $('textarea[name="message"]');
			if(ele.val() == ''){
				$(ele).css('border','1px solid #ff0000');
			} else {
				$('.bodyWrapper').fadeIn();
				$(ele).css('border', '1px solid #d2d6de');
				$.ajax({
					type: 'POST',
                    url: url+'admin/users/addfollowup',
                    data: 'user_id='+userId+'&message='+$(ele).val(),
                    success:function(response) {
                		$('.bodyWrapper').hide();
                		$(ele).val('');
                    	alert(response);
                    	window.location.reload();
                    }
				});
			}
		});

		$('.submitSms').click(function(){
			if(confirm('Are you sure want to send sms to <?php echo $user["name"]; ?>')) {
				var ele = $('textarea[name="sms"]');
				if(ele.val() == ''){
					$(ele).css('border','1px solid #ff0000');
				} else {
					$('.bodyWrapper').fadeIn();
					$(ele).css('border', '1px solid #d2d6de');
					$.ajax({
						type: 'POST',
	                    url: url+'admin/users/sendsmstouser',
	                    data: 'user_id='+userId+'&sms='+$(ele).val()+'&phone_number='+'<?php echo $user['phone_number']; ?>',
	                    success:function(response) {
	                		$('.bodyWrapper').hide();
	                		$(ele).val('');
	                    	alert(response);
	                    }
					});
				}
			}
		});
	});
</script>