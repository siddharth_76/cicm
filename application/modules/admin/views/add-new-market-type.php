<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'add_new_market_type_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-md-12">
	        <!-- general form elements -->
	        <div class="box box-primary">
	          <!-- form start -->
	          <form role="form" action="<?php cms_url('admin/tips/add-new-market-type'); ?>" method="post">
	            <div class="box-body">
	            	<!-- Validation error and flash data -->
		            <?php if(validation_errors() || $this->session->flashdata('general_error')) { ?>
		                <div class="alert alert-danger alert-dismissable">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                  <?php echo validation_errors(); ?>
		                  <?php echo $this->session->flashdata('general_error'); ?>
		                </div>
		            <?php } ?>

	            	<div class="col-md-6">
		                <div class="form-group">
		                  <label for="market_type_title">Market Type Title</label>
		                  <input type="text" name="market_type_title" class="form-control" id="market_type_title" placeholder="Enter Market Type Title" value="<?php echo set_value('market_type_title'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="exchange_code">Market Type Exchange Code</label>
		                  <input type="text" name="exchange_code" class="form-control" id="exchange_code" placeholder="Enter Market Type Exchange Code" value="<?php echo set_value('exchange_code'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="status">Status</label>
		                  <select name="status" class="form-control" id="status">
                  			<option value="1" <?php set_select('status', 1, true); ?>>Enable</option>
                  			<option value="0" <?php set_select('status', 0, true); ?>>Disable</option>
		                  </select>
		                </div>
	              	</div><!-- .col-md-6 -->
	            </div><!-- .box-body -->
	            <div class="box-footer">
	              <input type="submit" name="submit" value="Add Market Type" class="btn btn-primary"/>
	            </div>
	          </form>
	        </div><!-- /.box -->
      	</div><!--/.col (left) -->
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->