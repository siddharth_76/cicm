<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'view_notification_detail_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-md-12">
	        <!-- general form elements -->
	        <div class="box box-primary">
	            <div class="box-body">
                <!-- Validation error and flash data -->
                <?php if($this->session->flashdata('general_error') || $this->session->flashdata('invalid_item')) { ?>
                    <div class="alert alert-danger alert-dismissable">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <?php echo $this->session->flashdata('invalid_item'); ?>
                      <?php echo $this->session->flashdata('general_error'); ?>
                    </div>
                <?php } ?>
                <?php if($this->session->flashdata('item_success')) { ?>
                    <div class="alert alert-success alert-dismissable">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <?php echo $this->session->flashdata('item_success'); ?>
                    </div>
                <?php } ?>
                <div class="col-md-12">
                    <div class="form-group">
                      <label for="notification_type">Notification Type</label>
                      <p><?php echo $notification['type']; ?></p>
                    </div>
                    <div class="form-group">
                      <label for="notification_message">Notification Message</label>
                      <p><?php echo $notification['text']; ?></p>
                    </div>
                    <?php if(!empty($notification['extra'])) { ?>

                      <?php if($notification['type'] == 'device_exists') { ?>
                        <?php 
                          $extraData = unserialize($notification['extra']); 
                          $checkDevice = $this->common_model->getRecordCount(USER, array('device_type' => $extraData['device_type'], 'device_id' => $extraData['device_id']));
                        ?>
                        <div class="form-group">
                          <label for="notification_extra_attributes">Notification Extra Attributes</label>
                            <?php foreach($extraData as $key => $val){ ?>
                              <p><?php echo ucwords(str_replace('_', ' ', $key)).' : '.$val.'<br/>'; ?></p>
                            <?php } ?>
                        </div>
                      <?php } ?>

                      <?php if($notification['type'] == 'features_changed') { ?>
                        <?php $extraData = unserialize($notification['extra']); ?>
                        <div class="form-group">
                          <label for="notification_extra_attributes">Notification Extra Attributes</label><br/>
                            <?php foreach($extraData as $key => $val){ ?>
                              <div class="col-md-6" style="padding: 0px; margin-top: 12px;">
                                <strong><?php echo ucwords(str_replace('_', ' ', $key)).':<br/>'; ?></strong>
                                <ul>
                                <?php foreach($val as $ikey => $ival){ ?>
                                  <li><?php echo ucwords(str_replace('_', ' ', $ikey)); ?> : <?php echo ($ival == 1) ? 'Yes' : 'No'; ?></li>
                                <?php } ?>
                                </ul>
                              </div>
                            <?php } ?>
                        </div>
                      <?php } ?>

                    <?php } ?>

                    <?php if($permissions['can_reset_user_device'] == 1 && $notification['type'] == 'device_exists' && !empty($extraData['device_id']) && $checkDevice > 0) { ?>
                      <div class="form-group">
                        <a href="<?php cms_url('admin/notifications/resetUserDevice/'.$notification['id'].'/'.$extraData['device_id'].'/'.$extraData['device_type']); ?>" title="Reset User Device" onclick="if(!confirm('Are you sure you want to reset device?')) return false;"><span class="label label-info">Reset User Device</span></a>
                      </div>
                    <?php } ?>

                </div><!-- .col-md-6 -->
	            </div><!-- .box-body -->	
	            <div class="box-footer"></div>
	        </div><!-- /.box -->
      	</div><!--/.col (left) -->
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->