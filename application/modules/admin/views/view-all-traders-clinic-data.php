<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'view_all_news_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-xs-12">
	      <div class="box box-primary">
	        <div class="box-header">
	          <h3 class="box-title"><?php echo sprintf(ALL_DATA, 'Traders Clinic Data'); ?></h3>
	          <div class="box-tools">
	            <div class="input-group customInputGroups" style="width: 150px;">
	              <form action="<?php cms_url('admin/hawkeye/upload'); ?>" method="get">
		              <input type="text" name="s" class="form-control input-sm pull-right" placeholder="Search" value="<?php echo $this->input->get('s'); ?>">
		              <div class="input-group-btn cusInputGrpbtn">
		                <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
		              </div>
	              </form>
	            </div>
	            <?php if($pagination) { echo $pagination; } ?>
	          </div>
	          <!-- flash data -->
            <?php if($this->session->flashdata('item_success')) { ?>
                <div class="alert alert-success alert-dismissable" style="margin-top:12px;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo $this->session->flashdata('item_success'); ?>
                </div>
            <?php } if($this->session->flashdata('invalid_item')) { ?>
            	<div class="alert alert-danger alert-dismissable" style="margin-top:12px;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo $this->session->flashdata('invalid_item'); ?>
                </div>
            <?php } ?>
	        </div><!-- /.box-header -->
	        <div class="box-body table-responsive no-padding">
	          <table class="table table-hover">
	            <tr>
	              <th>ID</th>
	              <th>Name</th>
	              <th>Phone</th>
	              <th>Script</th>
	              <th>Market</th>
	              <th>Quantity</th>
	              <th>Price</th>
	              <th>Position</th>
	              <th>Vision</th>
	              <th>Comment</th>
	              <th>Status</th>
	              <th>Date Created</th>
	              <?php if($permissions['can_see_traders_clinic_conversations'] == 1) { ?><th>Chat</th><?php } ?>
	            </tr>
	            <?php
	            	if(!empty($traders)) {
	            		$offset = $offset + 1;
	            		foreach($traders as $val) {
	            ?>
	            	<tr>
		              <td><?php echo $offset++; ?></td>
		              <td>
		              	<?php echo ucfirst($val['first_name']).' '.ucfirst($val['last_name']); ?>	
		              </td>
		              <td>
		              	<?php echo ucfirst($val['phone_number']); ?>	
		              </td>
		              <td><?php echo $val['script_name']; ?></td>
		              <td>
		              	<?php  
		              		$scr = explode(',', $val['market_type']);
		              		if(!empty($scr)) {
		              			foreach($scr as $v) {
		              	?>
		              		<?php $scNames[] = $scripts[$v]; ?>
		              		<?php } ?>
		              	<?php echo implode(', ', $scNames); } ?>
		              </td>
		              <td><?php echo $val['quantity']; ?></td>
		              <td><i class="fa fa-inr"></i> <?php echo revertDBInt($val['price']); ?></td>
		              <td><?php echo ($val['position'] == 1) ? 'Long' : 'Short'; ?></td>
		              <td><?php echo $vision[$val['vision']]; ?></td>
		              <td><?php echo $val['comment']; ?></td>
		              <td>
		              	<?php if($val['status'] == 1) { ?>
		              		<span class="label label-success">Active</span>
		              	<?php } else { ?>
	              			<span class="label label-warning">Closed</span>
		              	<?php } ?>
		              </td>
		              <td><?php echo date('d/m/Y', strtotime($val['created_at'])); ?></td>
		              <?php if($permissions['can_see_traders_clinic_conversations'] == 1) { ?>
			              <td>
			              	<a href="<?php cms_url('admin/tradersclinic/view/'.$val['id']); ?>" title="View Detail">
			              		<i class="fa fa-external-link"></i> View
			              	</a>
			              </td>
			           <?php } ?>
		            </tr>
	            <?php unset($scNames); } /* End foreach */ ?>
	            <?php } else { ?>
	            	<tr>
	            		<td colspan="13" align="center"><?php echo sprintf(NO_RECORDS_FOUND, 'traders clinic') ?></td>
	            	</tr>
	            <?php } ?>
	          </table>
	        </div><!-- /.box-body -->
	        <div class="box-footer"><?php if($pagination) { echo $pagination; } ?></div>
	      </div><!-- /.box -->
	    </div>
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->