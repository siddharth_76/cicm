<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'app_settings_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-md-12">
	        <!-- general form elements -->
	        <div class="box box-primary">
	          <!-- form start -->
	          <form role="form" action="<?php cms_url('admin/settings/feature'); ?>" method="post">
	            <div class="box-body">
	            	<!-- Validation error and flash data -->
		            <?php if(validation_errors()) { ?>
		                <div class="alert alert-danger alert-dismissable">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                  <?php echo validation_errors(); ?>
		                </div>
		            <?php } if($this->session->flashdata('setting_success')) { ?>
		                <div class="alert alert-success lert-dismissable">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                  <?php echo $this->session->flashdata('setting_success'); ?>
		                </div>
		            <?php } ?>

	            	<div class="col-md-6">
	            		<!-- Active feature for app user after membership expiration -->
	            		<div class="form-group">
	            			<label>Select active features for membership expired users</label>
	            			<br/><small>Note: (These features will be automatically activated when user membership is expired, you can also change the active features for selected user from users management.)</small>
            			</div>
		                <?php
		                	$activeFeatures = unserialize(get_option('active_features_for_membership_expired_users'));
		                	$features = application_features();
		                	foreach($features as $val) { 
		                ?>
			                <div class="checkbox">
		                      <label>
		                      	<input type="hidden" name="features[<?php echo $val['cond']; ?>]" value="0" />
		                        <input type="checkbox" name="features[<?php echo $val['cond']; ?>]" <?php if($activeFeatures[$val['cond']] == 1) { echo 'checked="checked"'; } ?> value="1"> <?php echo ucfirst($val['label']); ?>
		                      </label>
		                    </div>
	                    <?php } ?>

	                    <!-- Default enabled features for staff & sub admin users -->
	                    <div class="form-group"></div>
	                    <div class="form-group">
	            			<label>Select default active features for sub admin users</label>
            			</div>
		                <?php
		                	$sunAdminFeatures = unserialize(get_option('default_features_for_sub_admin_users'));
		                	$features = internal_user_permissions();
		                	foreach($features as $val) { 
		                ?>
			                <div class="checkbox">
		                      <label>
		                      	<input type="hidden" name="sub_admin_features[<?php echo $val; ?>]" value="0" />
		                        <input type="checkbox" name="sub_admin_features[<?php echo $val; ?>]" <?php if($sunAdminFeatures[$val] == 1) { echo 'checked="checked"'; } ?> value="1"> <?php echo ucfirst(str_replace('_', ' ', $val)); ?>
		                      </label>
		                    </div>
	                    <?php } ?>

	                    <div class="form-group"></div>
	                    <div class="form-group">
	            			<label>Select default active features for cicm staff users</label>
            			</div>
		                <?php
		                	$staffUserFeatures = unserialize(get_option('default_features_for_staff_admin_users'));
		                	$features = internal_user_permissions();
		                	foreach($features as $val) { 
		                ?>
			                <div class="checkbox">
		                      <label>
		                      	<input type="hidden" name="staff_user_features[<?php echo $val; ?>]" value="0" />
		                        <input type="checkbox" name="staff_user_features[<?php echo $val; ?>]" <?php if($staffUserFeatures[$val] == 1) { echo 'checked="checked"'; } ?> value="1"> <?php echo ucfirst(str_replace('_', ' ', $val)); ?>
		                      </label>
		                    </div>
	                    <?php } ?>
	              	</div><!-- .col-md-6 -->
	            </div><!-- .box-body -->	

	            <div class="box-footer">
	              <input type="submit" name="submit" value="Save Changes" class="btn btn-primary"/>
	            </div>
	          </form>
	        </div><!-- /.box -->
      	</div><!--/.col (left) -->
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->