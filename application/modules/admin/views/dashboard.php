<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'admin_dashboard'); ?>

  <!-- Main content -->
  <section class="content">
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->