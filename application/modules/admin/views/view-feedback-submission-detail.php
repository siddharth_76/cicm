<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'view_feedback_submission_detail_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-md-12">
	        <!-- general form elements -->
	        <div class="box box-primary">
	            <div class="box-body">
                <!-- Validation error and flash data -->
                <?php if($this->session->flashdata('general_error') || $this->session->flashdata('invalid_item')) { ?>
                    <div class="alert alert-danger alert-dismissable">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <?php echo $this->session->flashdata('invalid_item'); ?>
                      <?php echo $this->session->flashdata('general_error'); ?>
                    </div>
                <?php } ?>
                <?php if($this->session->flashdata('item_success')) { ?>
                    <div class="alert alert-success alert-dismissable">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <?php echo $this->session->flashdata('item_success'); ?>
                    </div>
                <?php } ?>
                <div class="col-md-6">
                    <div class="form-group">
                      <label for="notification_type">User Info:</label>
                      <p>
                        <i class="fa fa-user"></i> <?php echo ucfirst($user['name']); ?>
                        <br/><i class="fa fa-phone"></i> <?php echo $user['phone_number']; ?> 
                        <br/><i class="fa fa-ticket"></i> Membership: <?php echo ($user['membership_type'] == 2) ? 'Paid' : 'Free'; ?>   
                        <br/><i class="fa fa-ticket"></i> Msembership Status: <?php echo ($user['membership_status'] == 1) ? 'Active' : 'Expired'; ?>
                        <br/><i class="fa fa-calendar"></i> Membership Exipration Date: <?php echo date('d/m/Y',strtotime($user['membership_expire_on'])); ?>
                      </p>
                    </div>
                    <div class="form-group">
                      <label for="notification_message">Feedback Information:</label>
                    </div>
                    <?php 
                      $questions = unserialize($form['elements']);
                      $answers = explode(',', $feed['answers']);
                      foreach($questions as $key => $val) {
                    ?>
                      <div class="form-group">
                        <p>
                          <label for="question">Topic:</label>
                          <?php echo ucfirst($val); ?>
                        </p>
                        <p>
                          <label for="answer">Rating:</label>
                          <?php echo $answers[$key]; ?>
                        </p>
                      </div>
                    <?php } ?>
                    <div class="form-group">
                      <label for="comment">Comment: </label>
                      <?php echo $feed['comment']; ?>
                    </div>
                    <div class="form-group">
                      <label for="created_at">Submission Date:</label>
                      <?php echo date('d/m/Y',strtotime($feed['created_at'])); ?>
                    </div>
                </div><!-- .col-md-6 -->
	            </div><!-- .box-body -->	
	            <div class="box-footer"></div>
	        </div><!-- /.box -->
      	</div><!--/.col (left) -->
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->