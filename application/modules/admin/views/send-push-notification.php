<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'send_push_notification_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-md-12">
	        <!-- general form elements -->
	        <div class="box box-primary">
	          <!-- form start -->
	          <form id="pushForm" role="form" action="<?php cms_url('admin/send-push-notification'); ?>" method="post">
	            <div class="box-body">
	            	<!-- Validation error and flash data -->
		            <?php if(validation_errors() || $this->session->flashdata('general_error')) { ?>
		                <div class="alert alert-danger alert-dismissable">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                  <?php echo validation_errors(); ?>
		                  <?php echo $this->session->flashdata('general_error'); ?>
		                </div>
		            <?php } ?>

		            <?php if($this->session->flashdata('item_success')) { ?>
		                <div class="alert alert-success alert-dismissable">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                  <?php echo $this->session->flashdata('item_success'); ?>
		                </div>
		            <?php } ?>

	            	<div class="col-md-6">
		                <div class="form-group">
		                  <label for="subject">Enter Notification Subject</label>
		                  <input type="text" name="subject" class="form-control" id="name" placeholder="Enter Notification Message" value="<?php echo set_value('subject'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="message">Enter Notification Message</label>
		                  <textarea name="message" class="form-control" id="message" placeholder="Enter Notification Message"><?php echo set_value('message'); ?></textarea>
		                </div>
              		</div><!-- .col-md-6 -->
	            </div><!-- .box-body -->	

	            <div class="box-footer">
	              <input type="submit" name="submit" value="Send Notification" class="btn btn-primary"/>
	            </div>
	          </form>
	        </div><!-- /.box -->
	        <div class="box box-primary">
		        <div class="box-header">
		          <h3 class="box-title"><?php echo sprintf(ALL_DATA, 'Push Notification'); ?></h3>
		          <div class="box-tools">
		            <?php if($pagination) { echo $pagination; } ?>
		          </div>
		        </div><!-- /.box-header -->
		        <div class="box-body table-responsive no-padding">
		          <table class="table table-hover">
		            <tr>
		              <th>ID</th>
		              <th>Sent By</th>
		              <th>Subject</th>
		              <th>Messge</th>
		              <th>Date Created</th>
		            </tr>
		            <?php
		            	if(!empty($notifications)) {
		            		$offset = $offset + 1;
		            		foreach($notifications as $val) {
		            			$userData = get_user($val['sent_by']);
		            ?>
		            	<tr>
		            		<td><?php echo $offset++; ?></td>
			            	<td>
			            		<a href="<?php cms_url('admin/users/view-all?s='.$userData['name']); ?>" target="_blank" title="<?php echo ucfirst($userData['name']); ?>"><?php echo ucfirst($userData['name']); ?></a>
			            	</td>
			            	<td><?php echo $val['subject']; ?></td>
			            	<td><?php echo $val['message']; ?></td>
			            	<td>
			            		<?php echo date('m/d/Y h:i A', strtotime($val['created_at'])); ?>
			            	</td>
			            </tr>
		            <?php } /* End foreach */ ?>
		            <?php } else { ?>
		            	<tr>
		            		<td colspan="5" align="center"><?php echo sprintf(NO_RECORDS_FOUND, 'push notifications') ?></td>
		            	</tr>
		            <?php } ?>
		          </table>
		        </div><!-- /.box-body -->
		        <div class="box-footer"><?php if($pagination) { echo $pagination; } ?></div>
	      </div><!-- /.box -->
      	</div><!--/.col (left) -->
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
	$(document).ready(function(){
		$('#pushForm').submit(function(){
			if(confirm('Are you sure to sent this push notification??')) {
				return true;
			} else {
				return false;
			}
		});
	});
</script>