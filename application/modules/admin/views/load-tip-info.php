<?php
	$url = base_url().'api/tips/gettip';
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POST, true);

	$data = array(
	    'tip_id' => $tip['tip_id'],
	    'user_id' => ADMIN_USER_ID
	);

	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
	$contents = curl_exec($ch);
	curl_close($ch);

	$ltpArray = array();
	$tipData = json_decode($contents);
	if(!empty($tipData) && $tipData->code == SUCCESS) {
		$tipInfo = $tipData->response;
		$ltpArray[$tipInfo->tip_data->tip_id] = array(strtoupper($tipInfo->tip_data->stock_market_code), strtoupper($tipInfo->tip_data->exchange));
		$ltpPrice = get_tip_ltp(strtoupper($tipInfo->tip_data->stock_market_code), strtoupper($tipInfo->tip_data->exchange));
?>
	<div class="form-group">
		<p><label for="">Live LTP: </label> <text id="tip<?php echo $tipInfo->tip_data->tip_id; ?>" ltp="<?php echo $ltpPrice; ?>"><span class="label label-success"><i class="fa fa-inr"></i> <?php echo $ltpPrice; ?> <i class="fa fa-arrow-up"></i></span></text></p>

  		<p><label for="">Tip Type: </label> <?php echo $tipInfo->tip_data->tip_type; ?></p>

  		<p><label for="">Tip Market Type: </label> <?php echo $tipInfo->tip_data->market_type; ?></p>

  		<p><label for="">Tip Exchange: </label> <?php echo $tipInfo->tip_data->exchange; ?></p>

  		<p><label for="">Tip Exchange Type: </label> <?php echo $tipInfo->tip_data->exchange_tip_type; ?></p>

  		<p><label for="">Total Targets: </label> <?php echo $tipInfo->tip_data->total_target; ?></p>

  		<p><label for="">Acheived Targets: </label> <?php echo $tipInfo->tip_data->acheived_target; ?></p>

  		<p><label for="">Tip Price: </label> <i class="fa fa-inr"></i> <?php echo $tipInfo->tip_data->tip_price; ?></p>

  		<p><label for="">Stoploss: </label> <i class="fa fa-inr"></i> <?php echo $tipInfo->tip_data->stoploss; ?></p>

  		<?php if($tipInfo->tip_data->acheived_target > 0) { ?>
  			<p><label for="">Trailed Stoploss: </label> <i class="fa fa-inr"></i> <?php echo $tipInfo->tip_data->trailed_stoploss; ?></p>
  		<?php } ?>

  		<p><label>Target Data:</label></p>
  		
	  	<?php foreach($tipInfo->tip_data->targets as $val) { ?>
	  		<p>
	  			<label>Target <?php echo $val->target_number; ?>: </label> <i class="fa fa-inr"></i> <?php echo $val->target; ?>
	  		</p>
	  	<?php } ?>
  	</div>
<?php } ?>
<script type="text/javascript">
	function getTipLtpData() {
		var tipArray = '<?php echo json_encode($ltpArray); ?>';
        $.ajax({
            type: 'POST',
            url: url+'admin/tips/getLtp',
            data: 'data='+tipArray,
            success:function(response) {
                var obj = jQuery.parseJSON(response);
				$.each(obj, function(key,value) {
					var ltp = $('#tip'+key).attr('ltp');
					$('#tip'+key).attr('ltp', value);
					if(parseFloat(value) >= parseFloat(ltp)) {
						$('#tip'+key).html('<span class="label label-success"><i class="fa fa-inr"></i> '+value+' <i class="fa fa-arrow-up"></i></span>');
					} else {
						$('#tip'+key).html('<span class="label label-danger"><i class="fa fa-inr"></i> '+value+' <i class="fa fa-arrow-down"></i></span>');
					}
				});
				setTimeout(function(){
					getTipLtpData();
				}, 500); 
            }
        });
	}
	$(document).ready(function(){
		<?php if(!empty($ltpArray)) { ?>
			setTimeout(function(){ getTipLtpData(); }, 5000);
        <?php } ?>
	});
</script>