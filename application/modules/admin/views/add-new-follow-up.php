<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'add_new_follow_up_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-md-12">
	        <!-- general form elements -->
	        <div class="box box-primary">
	          <!-- form start -->
	          <form role="form" action="<?php cms_url('admin/tips/add-new-follow-up/'.$tip_id); ?>" method="post">
	            <div class="box-body">
	            	<!-- Validation error and flash data -->
		            <?php if(validation_errors() || $this->session->flashdata('general_error')) { ?>
		                <div class="alert alert-danger alert-dismissable">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                  <?php echo validation_errors(); ?>
		                  <?php echo $this->session->flashdata('general_error'); ?>
		                </div>
		            <?php } ?>

	            	<div class="col-md-6">
	            		<div class="form-group">
		                  <label for="fu_message">Enter Follow Up Message</label>
		                  <textarea style="height: 120px;" name="fu_message" class="form-control" id="fu_message" placeholder="Enter Tip Follow Up Message"><?php echo set_value('fu_message'); ?></textarea>
		                </div>
		                <div class="checkbox">
	                      <label>
	                      	<input type="hidden" name="trail_stoploss" value="0" />
	                        <input type="checkbox" name="trail_stoploss" id="trail_stoploss" value="1"> Do you want to trail stoploss??
	                      </label>
	                    </div>
	                    <div class="form-group tarilStoploss" style="display: none;">
		                  <label for="trailed_stoploss">Enter Trailed Stoploss</label>
		                  <input disabled="disabled" type="text" name="trailed_stoploss" class="form-control" id="trailed_stoploss" placeholder="Enter New Trailed Stoploss" value="<?php echo set_value('trailed_stoploss'); ?>" />
		                </div>
	                    <div class="checkbox">
	                      <label>
	                      	<input type="hidden" name="trail_targets" value="0" />
	                        <input type="checkbox" name="trail_targets" id="trail_targets" value="1"> Do you want to add more targets??
	                      </label>
	                    </div>
	                    <div class="form-group totalPresenttargets" style="display: none;">
	                		<label>Total present targets: <?php echo $tip['total_added_targets']; ?></label>
	                	</div>
	                    <div class="form-group appendNewTargets" style="display: none;">
		                	<input type="hidden" name="total_added_targets" value="<?php echo $tip['total_added_targets']; ?>"/>
		                	<a href="javascript:void(0);" class="addNewMoreTargets"><i class="fa fa-plus"></i> Add More Targets</a>
		                </div>
		                <div class="form-group">
		                  <label for="tip_status">Tip Status</label>
		                  <select name="tip_status" class="form-control" id="tip_status">
                  			<option value="1" <?php if($tip['status'] == 1) { echo 'selected="selected"'; } ?>>Published</option>
                  			<option value="3" <?php if($tip['status'] == 3) { echo 'selected="selected"'; } ?>>Closed</option>
		                  </select>
		                </div>
	              	</div><!-- .col-md-6 -->
	              	<div class="col-md-6">
	              		<h3>Tip Info:</h3>
	              		<?php include 'load-tip-info.php'; ?>
	              	</div>
	            </div><!-- .box-body -->
	            <div class="box-footer">
	              <input type="submit" name="submit" value="Add Follow Up" class="btn btn-primary"/>
	            </div>
	          </form>
	        </div><!-- /.box -->
      	</div><!--/.col (left) -->
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
	function removeTargetElement(ele) {
		var totalAddedTargets = $('input[name="total_added_targets"]').val();
		totalAddedTargets--;

		$('#tipTarStoploss'+ele).remove();
		$('input[name="total_added_targets"]').val(totalAddedTargets);
	}

	$(document).ready(function(){
		$('#trail_stoploss').click(function(){
			if($(this).is(':checked')) {
				$('.tarilStoploss input').removeAttr('disabled');
				$('.tarilStoploss').fadeIn();
			} else {
				$('.tarilStoploss input').attr('disabled', 'disabled');
				$('.tarilStoploss').hide();
			}
		});

		$('#trail_targets').click(function(){
			if($(this).is(':checked')) {
				$('.appendNewTargets, .totalPresenttargets').fadeIn();
			} else {
				$('.appendNewTargets, .totalPresenttargets').hide();
			}
		});

		$('.addNewMoreTargets').click(function(){
			var totalAddedTargets = $('input[name="total_added_targets"]').val();
			totalAddedTargets++;

			var html = '<div class="row appendedTarStoploss" id="tipTarStoploss'+totalAddedTargets+'"><a href="javascript:void(0);" class="removeTargetEle" onclick="removeTargetElement('+totalAddedTargets+');"><i class="fa fa-times"></i> Remove</a><div class="col-md-6"><div class="form-group tip_target'+totalAddedTargets+'"><label for="tip_targets['+totalAddedTargets+']">Target '+totalAddedTargets+'</label><input type="text" name="tip_targets['+totalAddedTargets+']" class="form-control tippTargets" id="tip_target'+totalAddedTargets+'" placeholder="Enter Tip Target"/></div></div><div class="col-md-6"><div class="form-group stoploss_traling'+totalAddedTargets+'"><label for="stoploss_traling['+totalAddedTargets+']">Target '+totalAddedTargets+' Stoploss Trail</label><input type="text" name="stoploss_traling['+totalAddedTargets+']" class="form-control tippTargets" id="stoploss_traling'+totalAddedTargets+'" placeholder="Enter Stoploss Trailing" value=""/></div></div></div>';
						
			$('.appendNewTargets').before(html);
			$('input[name="total_added_targets"]').val(totalAddedTargets);
		});

		$('form').submit(function(event){
			var isError = 0;
			var isTargetError = 0;

			if($('#trail_stoploss').is(':checked')) {
				if($('input[name="trailed_stoploss"]').val() == '') {
					isError = 1;
					alert('Please Enter New Trailed Target');

					event.preventDefault();
					return false;
				}
			}

			$('.tippTargets').each(function(){
				if($(this).val() == '') {
					isError = 1;
					isTargetError = 1;
				}
			});

			if(isError == 1) {
				if(isTargetError == 1) {
					alert('Please Enter Tip Targets');
				}

				event.preventDefault();
				return false;
			} else {
				if(confirm('Are you sure want to add this tip follow up??')) {
					return true;
				} else {
					return false;
				}
			}
		});
	});
</script>