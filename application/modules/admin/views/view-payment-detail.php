<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'view_payment_detail_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-xs-12">
	      <div class="box box-primary">
	        <div class="box-header">
	          <h3 class="box-title">View Payment Detail</h3>
	          <div class="box-tools"></div>
	        </div><!-- /.box-header -->
	        <div class="box-body table-responsive no-padding">
	        	<div class="col-md-6">
	        		<div class="form-group">
	        			<label>User : </label>
	        			<i class="fa fa-user"></i>
	        			<a href="<?php cms_url('admin/users/view-all?s='.$userData['name']); ?>" target="_blank" title="<?php echo ucfirst($userData['name']); ?>">
	        				<?php echo ucfirst($userData['name']); ?>
	        			</a>
	        		</div>
	        		<div class="form-group">
	        			<label>Transaction ID : </label>
	        			<?php echo $payment['txn_id']; ?>
	        		</div>
	        		<div class="form-group">
	        			<label>Payment Status : </label>
	        			<?php echo ucfirst($payment['payment_status']); ?>
	        		</div>
	        		<div class="form-group">
	        			<label>Is Reward Points Used : </label>
	        			<?php echo ($payment['is_reward_points_used'] == 1) ? 'Yes'  : 'No'; ?>
	        		</div>
	        		<div class="form-group">
	        			<label>Used Reward Points : </label>
	        			<i class="fa fa-trophy"></i>
	        			<?php echo revertDBInt($payment['used_reward_points']); ?>
	        		</div>
	        		<div class="form-group">
	        			<label>Total Amount : </label>
	        			<i class="fa fa-inr"></i>
	        			<?php echo revertDBInt($payment['total_amount']); ?>
	        		</div>
	        		<div class="form-group">
	        			<label>Final Amount : </label>
	        			<i class="fa fa-inr"></i>
	        			<?php echo revertDBInt($payment['final_amount']); ?>
	        		</div>
	        		<div class="form-group">
	        			<label><i class="fa fa-calendar"></i> Payment Date : </label>
	        			<?php echo date('m/d/Y h :i A', strtotime($payment['created_at'])); ?>
	        		</div>
	        		<div class="form-group">
	        			<label>Subscribed Plans :</label>
	        			<?php if(!empty($subscription)) { ?>
	        				<?php
	        					$i = 1; 
	        					foreach($subscription as $val) {
	        					$plan = $this->common_model->getSingleRecordByid(MEMBERSHIP_PLAN, array('plan_id' => $val['subscribed_plan_id'])); 
    						?>
    							<p>
    								<label>Plan#<?php echo $i++; ?> : </label>
    								<a href="<?php cms_url('admin/plans/view-all?s='.$val['subscribed_plan_id']); ?>" target="_blank" title="<?php echo ucfirst($plan['plan_title']); ?>">
    									<?php echo ucfirst($plan['plan_title']); ?>
    								</a>
    							</p>
    							<p>
    								<label>Subscription Start Date : </label>
    								<?php echo date('m/d/Y',strtotime($val['subscription_start_date'])); ?>
    							</p>
    							<p>
    								<label>Subscription End Date : </label>
    								<?php echo date('m/d/Y',strtotime($val['subscription_end_date'])); ?>
    							</p>
    							<p>
    								<label>Subscription Status : </label>
    								<?php if($val['status'] == 1) { ?>
				            			<span class="label label-success">Active</span>
				            		<?php } else { ?>
				            			<span class="label label-danger">Expired</span>
				            		<?php } ?>
    							</p>
	        				<?php } ?>
	        			<?php } ?>
	        		</div>
	        		<div class="form-group">
	        			<label></label>
	        		</div>
	        		<div class="form-group">
	        			<label></label>
	        		</div>
	        		<div class="form-group">
	        			<label></label>
	        		</div>
	        	</div><!-- .col-md-6 -->
	        	<div class="col-md-6">
	        		<div class="form-group">
	        			<label>PayuMoney Api Response : </label>
	        			<?php if(!empty($payment['api_response'])) { ?>
	        				<?php 
	        					$apiData = (array) json_decode($payment['api_response']);
	        					foreach($apiData as $key => $val) {
	        				?>
	        					<p style="margin-left: 12px;">
	        						<label><?php echo $key; ?> : </label>
	        						<?php echo $val; ?>
	        					</p>
	        				<?php } ?>
	        			<?php } ?>
	        		</div>
	        	</div><!-- .col-md-6 -->
	        </div><!-- /.box-body -->
	        <div class="box-footer"></div>
	      </div><!-- /.box -->
	    </div>
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->