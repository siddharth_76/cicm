<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'manage_submitted_feedback_forms_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-xs-12">
	      <div class="box box-primary">
	        <div class="box-header">
	          <h3 class="box-title"><?php echo sprintf(ALL_DATA, 'Submitted Feedbacks'); ?></h3>
	          <div class="box-tools">
	            <?php if($pagination) { echo $pagination; } ?>
	          </div>
	          <!-- flash data -->
            <?php if($this->session->flashdata('item_success')) { ?>
                <div class="alert alert-success alert-dismissable" style="margin-top:12px;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo $this->session->flashdata('item_success'); ?>
                </div>
            <?php } if($this->session->flashdata('invalid_item')) { ?>
            	<div class="alert alert-danger alert-dismissable" style="margin-top:12px;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo $this->session->flashdata('invalid_item'); ?>
                </div>
            <?php } ?>
	        </div><!-- /.box-header -->
	        <div class="box-body table-responsive no-padding">
	          <table class="table table-hover">
	            <tr>
	              <th>ID</th>
	              <th>User</th>
	              <th>Form ID</th>
	              <th>Comment</th>
	              <th>Action</th>
	              <th>Date Created</th>
	            </tr>
	            <?php
	            	if(!empty($submission)) {
	            		$offset = $offset + 1;
	            		foreach($submission as $val) {
	            			$userData = get_user($val['user_id']);
	            ?>
	            	<tr>
		              <td><?php echo $offset++; ?></td>
		              <td>
		              	<?php echo ucfirst($userData['name']); ?>
		              	<br/><i class="fa fa-phone"></i> (<?php echo $userData['phone_number']; ?>)
		              	<br/><i class="fa fa-ticket"></i> Membership: <?php echo ($userData['membership_type'] == 2) ? 'Paid' : 'Free'; ?>
		              </td>
		              <td>
		              	<i class="fa fa-table"></i> <?php echo $val['form_id']; ?>
		              </td>
		              <td><i class="fa fa-comment"></i> <?php echo $val['comment']; ?></td>
		              <td>
		              	<a href="<?php cms_url('admin/feedbacks/view/'.$val['id']); ?>" title="View Detail">
		              		<i class="fa fa-external-link"></i> View
		              	</a>
		              </td>
		              <td><?php echo date('d/m/Y', strtotime($val['created_at'])); ?></td>
		            </tr>
	            <?php } /* End foreach */ ?>
	            <?php } else { ?>
	            	<tr>
	            		<td colspan="6" align="center"><?php echo sprintf(NO_RECORDS_FOUND, 'feedback form submission') ?></td>
	            	</tr>
	            <?php } ?>
	          </table>
	        </div><!-- /.box-body -->
	        <div class="box-footer"><?php if($pagination) { echo $pagination; } ?></div>
	      </div><!-- /.box -->
	    </div>
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->