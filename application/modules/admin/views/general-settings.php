<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'general_settings_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-md-12">
	        <!-- general form elements -->
	        <div class="box box-primary">
	          <!-- form start -->
	          <form role="form" action="<?php cms_url('admin/settings/general'); ?>" method="post" enctype="multipart/form-data">
	            <div class="box-body">
	            	<!-- Validation error and flash data -->
		            <?php if(validation_errors()) { ?>
		                <div class="alert alert-danger alert-dismissable">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                  <?php echo validation_errors(); ?>
		                </div>
		            <?php } if($this->session->flashdata('setting_success')) { ?>
		                <div class="alert alert-success lert-dismissable">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                  <?php echo $this->session->flashdata('setting_success'); ?>
		                </div>
		            <?php } ?>

	            	<div class="col-md-6">
		                <div class="form-group">
		                  <label for="app_name">App Name</label>
		                  <input type="text" name="app_name" class="form-control" id="app_name" placeholder="Enter App Name" value="<?php echo get_option('app_name'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="app_description">App Description</label>
		                  <textarea name="app_description" class="form-control" id="app_description" placeholder="Enter App Description"><?php echo get_option('app_description'); ?></textarea>
		                </div>
	              	</div><!-- .col-md-6 -->
	            </div><!-- .box-body -->	

	            <div class="box-header with-border">
		            <h3 class="box-title"><?php echo LOGO_FAVICON_SETTING_TEXT; ?></h3>
	          	</div><!-- /.box-header -->

	          	<div class="box-body">
	            	<div class="col-md-6">
		                <div class="form-group">
		                  <label for="app_logo">App Logo</label>
		                  <input type="file" name="app_logo" class="form-control" id="app_logo"/>
		                  <?php $logo = get_option('app_logo'); if(!empty($logo)) { ?>
		                  	<input type="hidden" name="old_app_logo" value="<?php echo get_option('app_logo'); ?>"/>
		                  	<img src="<?php echo get_option('app_logo'); ?>" class="viewAdminLogo"/>
		                  <?php } ?>
		                </div>
		                <div class="form-group">
		                  <label for="app_favicon">App Favicon</label>
		                  <input type="file" name="app_favicon" class="form-control" id="app_favicon" />
		                  <?php $favicon = get_option('app_favicon'); if(!empty($favicon)) { ?>
		                  	<input type="hidden" name="old_app_favicon" value="<?php echo get_option('app_favicon'); ?>"/>
		                  	<img src="<?php echo get_option('app_favicon'); ?>" class="viewAdminfavicon"/>
		                  <?php } ?>
		                </div>
	              	</div><!-- .col-md-6 -->
	            </div><!-- .box-body -->

	            <div class="box-header with-border">
		            <h3 class="box-title"><?php echo APP_CONTACT; ?></h3>
	          	</div><!-- /.box-header -->

	            <div class="box-body">
	              <div class="col-md-6">
	                <div class="form-group">
	                  <label for="app_email">App Email</label>
	                  <input type="text" name="app_email" class="form-control" id="app_email" placeholder="Enter App Email" value="<?php echo get_option('app_email'); ?>" />
	                </div>
	                <div class="form-group">
	                  <label for="customer_care_number">Customer Care Number</label>
	                  <input type="text" name="customer_care_number" class="form-control" id="customer_care_number" placeholder="Enter Customer Care Number" value="<?php echo get_option('customer_care_number'); ?>" />
	                </div>
	                <div class="form-group">
	                  <label for="sales_contact_number">Sales Contact Number</label>
	                  <input type="text" name="sales_contact_number" class="form-control" id="sales_contact_number" placeholder="Enter Sales Contact Number" value="<?php echo get_option('sales_contact_number'); ?>" />
	                </div>
	              </div><!-- .col-md-6 -->
	            </div><!-- /.box-body -->

	            <div class="box-header with-border">
		            <h3 class="box-title"><?php echo SOCIAL_MEDIA_SETTINGS_TEXT; ?></h3>
	          	</div><!-- /.box-header -->

	            <div class="box-body">
	            	<div class="col-md-6">
		                <div class="form-group">
		                  <label for="facebook">Facebook</label>
		                  <input type="text" name="facebook_link" class="form-control" id="facebook" placeholder="Enter Facebook Link" value="<?php echo get_option('facebook_link'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="twitter">Twitter</label>
		                  <input type="text" name="twitter_link" class="form-control" id="twitter" placeholder="Enter Twitter Link" value="<?php echo get_option('twitter_link'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="googlePlus">Google+</label>
		                  <input type="text" name="google_plus_link" class="form-control" id="googlePlus" placeholder="Enter Google+ Link" value="<?php echo get_option('google_plus_link'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="google_map_link">Google Map</label>
		                  <input type="text" name="google_map_link" class="form-control" id="google_map_link" placeholder="Enter Google map Link" value="<?php echo get_option('google_map_link'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="linkedin">Linkedin</label>
		                  <input type="text" name="linkedin_link" class="form-control" id="linkedin" placeholder="Enter Linkedin Link" value="<?php echo get_option('linkedin_link'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="instagram">Instagram</label>
		                  <input type="text" name="instagram_link" class="form-control" id="instagram" placeholder="Enter Instagram Link" value="<?php echo get_option('instagram_link'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="pinterest">Pinterest</label>
		                  <input type="text" name="pinterest_link" class="form-control" id="pinterest" placeholder="Enter Pinterest Link" value="<?php echo get_option('pinterest_link'); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="youtube">Youtube</label>
		                  <input type="text" name="youtube_link" class="form-control" id="youtube" placeholder="Enter Youtube Link" value="<?php echo get_option('youtube_link'); ?>" />
		                </div>
	              	</div><!-- .col-md-6 -->
	            </div><!-- .box-body -->

	            <div class="box-header with-border">
		            <h3 class="box-title"><?php echo FOOTER_CONTENT_SETTINGS_TEXT; ?></h3>
	          	</div><!-- /.box-header -->

	            <div class="box-body">
	            	<div class="col-md-6">
		                <div class="form-group">
		                  <label for="copyright_text">Copyright Text</label>
		                  <textarea style="height:150px; resize: none;" name="copyright_text" class="form-control" id="copyright_text" placeholder="Enter Copyright Text"><?php echo str_replace('<br/>', PHP_EOL, get_option('copyright_text')); ?></textarea>
		                </div>
		                <div class="form-group">
		                  <label for="footer_name_address">Name & Address</label>
		                  <textarea style="height:150px; resize: none;" name="footer_name_address" class="form-control" id="footer_name_address" placeholder="Enter Address"><?php echo str_replace('<br/>', PHP_EOL, get_option('footer_name_address')); ?></textarea>
		                </div>
		            </div><!-- .col-md-6 -->
		        </div><!-- .box-body -->

	            <div class="box-footer">
	              <input type="submit" name="submit" value="Save Changes" class="btn btn-primary"/>
	            </div>
	          </form>
	        </div><!-- /.box -->
      	</div><!--/.col (left) -->
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->