<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <?php admin_content_header($meta_title, $small_text, 'edit_tip_header'); ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">
    	<div class="col-md-12">
	        <!-- general form elements -->
	        <div class="box box-primary">
	          <!-- form start -->
	          <form role="form" action="<?php cms_url('admin/tips/edit/'.$tip['tip_id']); ?>" method="post">
	            <div class="box-body">
	            	<!-- Validation error and flash data -->
		            <?php if(validation_errors() || $this->session->flashdata('general_error')) { ?>
		                <div class="alert alert-danger alert-dismissable">
		                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                  <?php echo validation_errors(); ?>
		                  <?php echo $this->session->flashdata('general_error'); ?>
		                </div>
		            <?php } ?>

	            	<div class="col-md-6">
	            		<div class="form-group">
		                  <label for="stock_market_type">Select Stock Market Type</label>
		                  <select name="stock_market_type" class="form-control" id="stock_market_type">
		                  	<option value="">Select Stock Market Type</option>
		                  	<?php 
		                  		if(!empty($stock_market_types)) {
		                  			foreach($stock_market_types as $val){
	                  		?>
	                  			<option value="<?php echo $val['id']; ?>" <?php if($tip['stock_market_type'] == $val['id']) { echo 'selected="selected"'; } ?>><?php echo ucfirst($val['market_type_title']); ?></option>
	                  			<?php } ?>
	                  		<?php } else { ?>
	                  			<option value="">No Stock Market Types Found</option>
	                  		<?php } ?>
		                  </select>
		                </div>
		                <div class="form-group">
		                  <label for="tip_type">Select Tip Type</label>
		                  <select name="tip_type" class="form-control" id="tip_type">
		                  	<option value="">Select Tip Type</option>
		                  	<?php 
		                  		$tipTypes = get_tip_type();
		                  		if(!empty($tipTypes)) {
		                  			foreach($tipTypes as $key => $val){
	                  		?>
	                  			<option value="<?php echo $key; ?>" <?php if($tip['tip_type'] == $key) { echo 'selected="selected"'; } ?>><?php echo ucfirst($val); ?></option>
	                  			<?php } ?>
	                  		<?php } else { ?>
	                  			<option value="">No Tip Type Found</option>
	                  		<?php } ?>
		                  </select>
		                </div>
		                <div class="form-group">
		                  <label for="exchange">Enter Tip Exchange Name (Stock Symbol)</label>
		                  <input type="text" name="exchange" class="form-control" id="exchange" placeholder="Enter Tip Exchange Name" value="<?php echo strtoupper($tip['exchange']); ?>" />
		                </div>
		                <div class="form-group">
		                  <label for="market_type">Select Exchange Market Type</label>
		                  <select name="market_type" class="form-control" id="market_type">
		                  	<option value="">Select Exchange Market Type</option>
		                  	<?php 
		                  		$marketTypes = get_tip_exchange_market_type();
		                  		if(!empty($marketTypes)) {
		                  			foreach($marketTypes as $key => $val){
	                  		?>
	                  			<option value="<?php echo $key; ?>" <?php if($tip['market_type'] == $key){ echo 'selected="selected"'; } ?>><?php echo ucfirst($val); ?></option>
	                  			<?php } ?>
	                  		<?php } else { ?>
	                  			<option value="">No Exchange Market Type Found</option>
	                  		<?php } ?>
		                  </select>
		                </div>
		                <div class="form-group">
		                  <label for="exchange_tip_type">Select Exchange Tip Type</label>
		                  <select name="exchange_tip_type" class="form-control" id="exchange_tip_type">
		                  	<option value="">Select Exchange Tip Type</option>
		                  	<?php 
		                  		$marketTypes = get_tip_exchange_type();
		                  		if(!empty($marketTypes)) {
		                  			foreach($marketTypes as $key => $val){
	                  		?>
	                  			<option value="<?php echo $key; ?>" <?php if($tip['exchange_tip_type'] == $key) { echo 'selected="selected"'; } ?>><?php echo ucfirst($val); ?></option>
	                  			<?php } ?>
	                  		<?php } else { ?>
	                  			<option value="">No Exchange Tip Type Found</option>
	                  		<?php } ?>
		                  </select>
		                </div>
		                <div class="form-group">
		                  <?php 
		                  	$publishDate = strtotime($tip['publish_date']);
		                  	$currentDate = strtotime(date('Y-m-d'));
		                  ?>
		                  <label for="tip_publish_status">Select Tip Publish Status</label>
		                  <select name="tip_publish_status" class="form-control" id="tip_publish_status">
		                    <?php if($currentDate >= $publishDate) { ?>
		                    	<option value="1" <?php if($tip['status'] == 1) { echo 'selected="selected"'; } ?>>Published</option>
		                    <?php } else { ?>
                  				<option value="1" <?php if($tip['status'] == 1) { echo 'selected="selected"'; } ?>>Publish Now</option>
		                    <?php } ?>
                  			<?php if($publishDate > $currentDate) { ?>
                  				<option value="2" <?php if($tip['status'] == 2) { echo 'selected="selected"'; } ?>>Publish Later</option>
                  			<?php } ?>
                  			<option value="0" <?php if($tip['status'] == 0) { echo 'selected="selected"'; } ?>>Disabled</option>
                  			<option value="3" <?php if($tip['status'] == 3) { echo 'selected="selected"'; } ?>>Closed</option>
		                  </select>
		                </div>
		                <div class="form-group tipPublishDate" <?php if($tip['status'] != 2) { ?> style="display:none;" <?php } ?>>
		                  <label for="tip_publish_date">Enter Tip Publish Date</label>
		                  <input type="text" <?php if($tip['status'] != 2) { ?> disabled="disabled" <?php } ?> name="tip_publish_date" class="form-control datepicker" id="tip_publish_date" placeholder="Enter Tip Publish Date" value="<?php echo date('d-m-Y', strtotime($tip['publish_date'])); ?>" />
		                </div>
		                <div class="checkbox">
	                      <label>
	                      	<input type="hidden" name="is_shared" value="0" />
	                        <input type="checkbox" name="is_shared" id="is_shared" value="1" <?php if($tip['is_shared'] == 1) { echo 'checked="checked"'; } ?>> Do you want to allow to share this tip??
	                      </label>
	                    </div>
	                    <div class="form-group tip_share_earning" <?php if($tip['is_shared'] != 1) { ?>style="display: none;"<?php } ?>>
		                  <label for="tip_share_earning">Enter Tip Earning</label>
		                  <input type="text" name="tip_share_earning" class="form-control" id="tip_share_earning" placeholder="Enter tip earning for sharing message" value="<?php echo revertDBInt($tip['tip_share_earning']); ?>" />
		                </div>
		                <div class="form-group tip_share_earning_days" <?php if($tip['is_shared'] != 1) { ?>style="display: none;"<?php } ?>>
		                  <label for="tip_share_earning_days">Enter Tip Earning Days</label>
		                  <input type="text" name="tip_share_earning_days" class="form-control" id="tip_share_earning_days" placeholder="Enter tip earning days for sharing message" value="<?php echo revertDBInt($tip['tip_share_earning_days']); ?>" />
		                </div>
		                <div class="checkbox">
	                      <label>
	                      	<input type="hidden" name="is_automization_on" value="0" />
	                        <input type="checkbox" name="is_automization_on" id="is_automization_on" value="1" <?php if($tip['is_automization_on'] == 1) { echo 'checked="checked"'; } ?>> Do you want to allow to zerodha automization for this tip??
	                      </label>
	                    </div>
	              	</div><!-- .col-md-6 -->
	              	<div class="col-md-6">
	              		<div class="form-group">
		                  <label for="tip_price">Enter Tip Estimated Price</label>
		                  <input type="text" name="tip_price" class="form-control" id="tip_price" placeholder="First Price-Second Price" value="<?php echo revertDBInt($tip['price_one']); ?><?php echo (!empty($tip['price_two'])) ? '-'.revertDBInt($tip['price_two']) : ''; ?>" />
		                  <small><strong>Note: </strong>For multiple tip price use Eg: 100-200</small>
		                </div>
		                <div class="form-group">
		                  <label for="stoploss">Enter Tip Stoploss</label>
		                  <input type="text" name="stoploss" class="form-control" id="stoploss" placeholder="Enter Tip Stoploss" value="<?php echo revertDBInt($tip['stoploss']); ?>" />
		                  <small>(Note: This stoploss is initial stoploss of tip, stoploss will be trailed by tip follow up section.)</small>
		                </div>
		                <h4>Tip Targets:</h4>
		                <?php for($i = 1; $i <= $tip['total_added_targets']; $i++) { ?>
			                <div class="form-group tip_target<?php echo $i; ?>">
			                  <label for="tip_targets[<?php echo $i; ?>]">Target <?php echo $i; ?></label>
			                  <input type="text" name="tip_targets[<?php echo $i; ?>]" class="form-control tippTargets" id="tip_target<?php echo $i; ?>" placeholder="Enter Tip Target" value="<?php echo revertDBInt($targets[$i]); ?>"/>
			                </div>
			                <div class="form-group">
			                  <label for="is_acheived[<?php echo $i; ?>]">Target <?php echo $i; ?> Acheive Status</label>
			                  <select name="is_acheived[<?php echo $i; ?>]" class="form-control" id="is_acheived<?php echo $i; ?>">
			                  	<option <?php if($tipAcheived[$i] == 0) { echo 'selected="selected"'; } ?> value="0">No</option>
			                  	<option <?php if($tipAcheived[$i] == 1) { echo 'selected="selected"'; } ?>value="1">Yes</option>
			                  </select>
			                </div>
			                <div class="form-group stoploss_traling<?php echo $i; ?>">
			                  <label for="stoploss_traling[<?php echo $i; ?>]">Target <?php echo $i; ?> Stoploss Trail</label>
			                  <input type="text" name="stoploss_traling[<?php echo $i; ?>]" class="form-control tippTargets" id="stoploss_traling<?php echo $i; ?>" placeholder="Enter Stoploss Trailing" value="<?php echo revertDBInt($stoplossTrail[$i]); ?>"/>
			                </div>
		                <?php } ?>
		                <!-- <div class="form-group appendNewTargets">
		                	<input type="hidden" name="total_added_targets" value="<?php echo $tip['total_added_targets'] ?>"/>
		                	<a href="javascript:void(0);" class="addNewMoreTargets"><i class="fa fa-plus"></i> Add More Targets</a>
		                </div> -->
	              	</div><!-- .col-md-6 -->
	            </div><!-- .box-body -->
	            <div class="box-footer">
	              <input type="submit" name="submit" value="Save Changes" class="btn btn-primary"/>
	            </div>
	          </form>
	        </div><!-- /.box -->
      	</div><!--/.col (left) -->
    </div><!-- .row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
	function removeTargetElement(ele) {
		var totalAddedTargets = $('input[name="total_added_targets"]').val();
		totalAddedTargets--;

		$('.tip_target'+ele).remove();
		$('input[name="total_added_targets"]').val(totalAddedTargets);
	}

	$(document).ready(function(){
		$('input[name="is_shared"]').click(function(){
			if($(this).is(':checked')){
				$('.tip_share_earning, .tip_share_earning_days').fadeIn();
			} else {
				$('.tip_share_earning, .tip_share_earning_days').hide();
			}
		});

		$('#tip_publish_status').change(function(){
			if($(this).val() == 2) {
				$('.tipPublishDate input[name="tip_publish_date"]').removeAttr('disabled');
				$('.tipPublishDate').fadeIn();
			} else {
				$('.tipPublishDate input[name="tip_publish_date"]').attr('disabled', 'disabled');
				$('.tipPublishDate').hide();
			}
		});

		$('.datepicker').datepicker({
		    format: 'dd-mm-yyyy',
		    startDate: '-3d'
		});

		$('.addNewMoreTargets').click(function(){
			var totalAddedTargets = $('input[name="total_added_targets"]').val();
			totalAddedTargets++;

			var html = '<div class="form-group tip_target'+totalAddedTargets+'"><label for="tip_targets['+totalAddedTargets+']">Target '+totalAddedTargets+'</label><a href="javascript:void(0);" class="removeTargetEle" onclick="removeTargetElement('+totalAddedTargets+');"><i class="fa fa-times"></i> Remove</a><input type="text" name="tip_targets['+totalAddedTargets+']" class="form-control tippTargets" id="tip_target'+totalAddedTargets+'" placeholder="Enter Tip Target"/></div>';
			
			$('.appendNewTargets').before(html);
			$('input[name="total_added_targets"]').val(totalAddedTargets);
		});

		$('form').submit(function(event){
			var isError = 0;
			var isTargetError = 0;

			if($('#tip_publish_status').val() == 0 && $('input[name="tip_publish_date"]').val() == ''){
				isError = 1;
				$('input[name="tip_publish_date"]').focus();
				alert('Please Select Tip Publish Date');
			}

			$('.tippTargets').each(function(){
				if($(this).val() == '') {
					isError = 1;
					isTargetError = 1;
				}
			});

			if(isError == 1) {
				if(isTargetError == 1) {
					alert('Please Enter Tip Targets');
				}

				event.preventDefault();
				return false;
			} else {
				if(confirm('Are you sure want to to update this tip??')) {
					return true;
				} else {
					return false;
				}
			}
		});
	});
</script>