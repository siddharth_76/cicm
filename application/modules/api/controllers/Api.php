<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends REST_Controller {

	public function __construct() {
		header('Access-Control-Allow-Origin: *');
        parent::__construct();
    }

    /**
     * Get System Prefrences
     * @return $resp
     */
    public function getsystempreferences_get() {
        $settings = $this->common_model->getAllRecords(SYSTEM_PREFERENCE);
        if(!empty($settings)) {
            foreach($settings as $val) {
                if($val['preference_key'] == 'active_features_for_membership_expired_users') {
                    $prefs[$val['preference_key']] = unserialize($val['preference_value']);
                } else {
                    $prefs[$val['preference_key']] = $val['preference_value'];
                } 
            }
            unset($prefs['verification_otp_message'], $prefs['verification_otp_length'], $prefs['device_exists_message'], $prefs['membership_expire_message']);
            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('system_preferences' => $prefs));
        } else {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'NOTHING_FOUND', 'error_label' => 'Nothing found.'));
        }
        $this->response($resp);
    }

    /**
     * Get all countries
     * @return $resp
     */
    public function getcountries_get() {
    	$countries = $this->common_model->getAllRecordsByOrder(COUNTRY, 'country_name', 'ASC');
    	$resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('countries' => $countries));
    	$this->response($resp);
    }

    /**
     * Get states by country
     * @param $object_info
     * @return $resp
     */
    public function getstates_post() {
    	$object_info = $_POST;
    	if(!empty($object_info['country_id'])) {
    		$states = $this->common_model->getAllRecordsOrderById(STATE, 'state_name', 'ASC', array('state_country_id' => $object_info['country_id']));
    	} else {
    		$states = $this->common_model->getAllRecordsByOrder(STATE, 'state_name', 'ASC');
    	}
    	$resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('states' => $states));
    	$this->response($resp);
    }

    /**
     * Get cities by state country
     * @param $object_info
     * @return $resp
     */
    public function getcities_post() {
    	$object_info = $_POST;
    	if(!empty($object_info['state_id']) && !empty($object_info['country_id'])) {
    		$cities = $this->common_model->getAllRecordsOrderById(CITY, 'city_name', 'ASC', array('city_state_id' => $object_info['state_id'], 'city_country_id' => $object_info['country_id']));
    	} else {
    		$cities = $this->common_model->getAllRecordsByOrder(CITY, 'city_name', 'ASC');
    	}
    	$resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('cities' => $cities));
    	$this->response($resp);
    }

    /**
     * Get economic calendar
     */
    public function geteconomiccalendar_get() {
        if(!empty(get_option('economic_calendar_html_code'))) {
            $returnData = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('html_code' => get_option('economic_calendar_html_code')));
        } else {
            $returnData = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('html_code' => ''));
        }

        /* Return Response */
        $this->response($returnData);
    }

    /**
     * Get earning calendar
     */
    public function getearningcalendar_get() {
        if(!empty(get_option('earning_calendar_html_code'))) {
            $returnData = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('html_code' => get_option('earning_calendar_html_code')));
        } else {
            $returnData = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('html_code' => ''));
        }

        /* Return Response */
        $this->response($returnData);
    }
}