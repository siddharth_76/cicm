<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TradersClinic extends REST_Controller {

	public function __construct() {
		header('Access-Control-Allow-Origin: *');
        parent::__construct();
    }

    /**
     * Traders Clinic Form Elements
     * @return $resp
     */
    public function formelements_post(){
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id', 'limit', 'offset');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }
        
        /* Check user have access of traders clinic */
        $features = get_user_active_features($object_info['user_id']);
        if(empty($features) || $features['can_access_traders_clinic'] != 1) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'TRADERS_CLINIC_FEATURE_IS_INACTIVE', 'error_label' => 'You cannot access traders clinic feature on this membership, to access it please subscribe our membership plan and if you have any query then contact to CICM support.'));
            $this->response($resp);
        }

        /* Get total filled form count by user */
        $tcData = get_user_tc_form_history($object_info['user_id']);
        $filledTcForm = $tcData['total_fill_up_tc_form'];
        $tcFormLimit = $tcData['tc_form_limit'];
        $isAbleToFillTcForm = $tcData['is_able_to_fill_tc_form'];


        /* Get Filled Traders Form Data */
        $tcFormData = $this->common_model->getPaginateRecordsByOrderByCondition(TC_FORM_SUBMISSION, 'created_at', 'DESC', $object_info['limit'], $object_info['offset'], array('user_id' => $object_info['user_id']));
        if(!empty($tcFormData)) {
            /* Check for pagination data info */
            $result = $object_info['limit'];
            $offset = $object_info['offset'];
            $checkNext = $result + $offset;
            $totalForms = $this->common_model->getRecordCount(TC_FORM_SUBMISSION, array('user_id' => $object_info['user_id']));

            /* Check for hasNext */
            if($checkNext >= $totalForms) {
                $hasNext = FALSE;
            } else {
                $hasNext = TRUE;
            }

            foreach($tcFormData as $val) {
                $val['user_data'] = array(
                    'name' => $checkUser['name'],
                    'email' => $checkUser['email'],
                    'profile_pic' => (!empty($checkUser['profile_pic'])) ? base_url().USER_PROFILE_PIC_PATH.$checkUser['profile_pic'] : ''
                );
                $val['tc_id'] = $val['id'];
                $val['display_date'] = date('D d M, Y h:i A', strtotime($val['created_at']));
                $val['price'] = revertDBInt($val['price']);
                $tcDataArr[] = $val;
            }

            $dataInfo = array(
                'hasNext' => $hasNext,
                'limit' => $object_info['limit'],
                'offset' => $object_info['offset'],
                'total_records' => $totalForms
            );

            $tcFormResp = array('data' => $tcDataArr, 'dataInfo' => $dataInfo);
        } else {
            $tcFormResp = array('error' => 'NOTHING_FOUND', 'error_label' => 'Nothing has been submitted by user.');
        }

        /* Traders Clinic Form Elements */
        $elements = array(
            array(
                'field_id' => '1',
                'field_label' => 'First Name',
                'field_small_label' => '',
                'field_code' => 'first_name',
                'field_type' => 'text',
                'select_options' => array(),
                'radio_options' => array()
            ),
            array(
                'field_id' => '2',
                'field_label' => 'Last Name',
                'field_small_label' => '',
                'field_code' => 'last_name',
                'field_type' => 'text',
                'select_options' => array(),
                'radio_options' => array()
            ),
            array(
                'field_id' => '3',
                'field_label' => 'Phone Number',
                'field_small_label' => '',
                'field_code' => 'phone_number',
                'field_type' => 'number',
                'select_options' => array(),
                'radio_options' => array()
            ),
            array(
                'field_id' => '4',
                'field_label' => 'Name Of Script',
                'field_small_label' => '',
                'field_code' => 'script_name',
                'field_type' => 'text',
                'select_options' => array(),
                'radio_options' => array()
            ),
            array(
                'field_id' => '5',
                'field_label' => 'Market Type',
                'field_small_label' => '',
                'field_code' => 'market_type',
                'field_type' => 'multiple-select',
                'select_options' => array(
                    array(
                        'id' =>'1',
                        'label' => 'Stock'
                    ),
                    array(
                        'id' => '2',
                        'label' => 'F&O'
                    ),
                    array( 
                        'id' => '3',
                        'label' => 'Commodities'
                    ),
                    array( 
                        'id' => '4',
                        'label' => 'Currency'
                    )
                ),
                'radio_options' => array()
            ),
            array(
                'field_id' => '6',
                'field_label' => 'Quantity',
                'field_small_label' => '',
                'field_code' => 'quantity',
                'field_type' => 'number',
                'select_options' => array(),
                'radio_options' => array()
            ),
            array(
                'field_id' => '7',
                'field_label' => 'Price',
                'field_small_label' => '',
                'field_code' => 'price',
                'field_type' => 'number',
                'select_options' => array(),
                'radio_options' => array()
            ),
            array(
                'field_id' => '8',
                'field_label' => 'Position',
                'field_small_label' => 'Your current position.',
                'field_code' => 'position',
                'field_type' => 'radio',
                'select_options' => array(),
                'radio_options' => array(
                    array(
                        'id' => '1',
                        'label' => 'Long'
                    ), array(
                        'id' => '2',
                        'label' => 'Short'
                    )
                )
            ),
            array(
                'field_id' => '9',
                'field_label' => 'Vision',
                'field_small_label' => 'Your current vision.',
                'field_code' => 'vision',
                'field_type' => 'radio',
                'select_options' => array(),
                'radio_options' => array(
                    array(
                        'id' => '1',
                        'label' => 'Buy'
                    ), array(
                        'id' => '2',
                        'label' => 'Sell'
                    ), array(
                        'id' => '3',
                        'label' => 'Hold'
                    )
                )
            ),
            array(
                'field_id' => '10',
                'field_label' => 'Comment',
                'field_small_label' => '.',
                'field_code' => 'comment',
                'field_type' => 'textarea',
                'select_options' => array(),
                'radio_options' => array()
            ),
        );
    
        /* Prepairing Response */
        $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('total_filled_tc_forms' => $filledTcForm, 'tc_form_limit' => $tcFormLimit, 'is_able_to_fill_tc_form' => $isAbleToFillTcForm, 'tc_form_data' => $tcFormResp, 'form_elements' => $elements));

        /* Return Response */
        $this->response($resp);
    }

    /**
     * Add Traders Clinic Form Data
     * @param $_POST
     * @return $resp
     */
    public function addtradersclinicformdata_post(){
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id', 'first_name', 'last_name', 'phone_number', 'script_name', 'market_type', 'quantity', 'price', 'position', 'vision');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }

        /* Check user have access of traders clinic */
        $features = get_user_active_features($object_info['user_id']);
        if(empty($features) || $features['can_access_traders_clinic'] != 1) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'TRADERS_CLINIC_FEATURE_IS_INACTIVE', 'error_label' => 'You cannot access traders clinic feature on this membership, to access it please subscribe our membership plan and if you have any query then contact to CICM support.'));
            $this->response($resp);
        }

        /* Check for form submission limitation */
        $tcDataHistory = get_user_tc_form_history($object_info['user_id']);
        $checkLimit = $tcDataHistory['total_fill_up_tc_form'];
        $freeLimit = $tcDataHistory['tc_form_limit'];

        if($tcDataHistory['is_able_to_fill_tc_form'] != 1) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ONLY_FOUR_TC_CAN_SUBMIT_IN_A_MONTH', 'error_label' => get_option('tc_over_limit_message')));
            $this->response($resp);
        }

        /* Process Request */
        $_POST['price'] = convertDBInt($_POST['price']);
        $_POST['status'] = 1;
        $_POST['created_at'] = $_POST['updated_at'] = date('Y-m-d H:i:s');

        $tcFormId = $this->common_model->addRecords(TC_FORM_SUBMISSION, $_POST);
        if($tcFormId) {
            /* Updating filled tc form in db for paid members */
            if($checkUser['membership_type'] == PREMIUM_MEMBER) {
                $this->common_model->increaseUserTcFormFilledLimit(USER, 1, $object_info['user_id']);
                $this->common_model->decreaseUserTcFormFilledLimit(USER, 1, $object_info['user_id']);
            }

            /* Send Notification to portal */
            send_web_notification('Traders Clinic form has been filled by user '.ucfirst($checkUser['name']).', please have a look.', 'New Traders Clinic Enquiry', get_cms_url('admin/tradersclinic/view-all'), array());

            /* Send Push Notification */
            if($checkUser['device_type'] == 1 && !empty($checkUser['device_token'])) {
                 $fcmData = array(
                    'title' => 'Traders Clinic Enquiry Submitted Successfully.',
                    'body' => 'Dear '.ucfirst($checkUser['name']).', Your Traders Clinic enquiry has been submitted successfully, our representative will connect with you as soon as possible.',
                    'type' => 'new_traders_form',
                    'extra' => array('tc_id' => $tcFormId, 'user_id' => $object_info['user_id'])
                );
                send_fcm_notification($fcmData, $checkUser['device_token']);
            }

            $returnData = $_POST;
            $returnData['id'] =  $tcFormId;
            $returnData['tc_ticket_id'] = $tcFormId;
            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('success' => 'TRADERS_CLINIC_DATA_SUBMITTED_SUCCESSFULLY', 'success_label' => 'Your Traders Clinic enquiry has been submitted successfully, our representative will connect with you as soon as possible.', 'data' => $returnData));
        } else {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'UNABLE_TO_SUBMIT_FORM', 'error_label' => 'Some error occurred, please try again.'));
        }

        /* Return Response */
        $this->response($resp);
    }

    /**
     * Add Traders Clinic Threads Message
     * @param $_POST
     * @return $resp
     */
    public function addtradersclinicmessage_post(){
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id', 'tc_id', 'message');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid tc */
        $checkTc = $this->common_model->getSingleRecordById(TC_FORM_SUBMISSION, array('id' => $object_info['tc_id'], 'user_id' => $object_info['user_id']));
        if(empty($checkTc)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_TC', 'error_label' => 'This form is not exists in our system.'));
            $this->response($resp);
        }

        if($checkTc['status'] != 1) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'TC_CLOSED', 'error_label' => 'This thread has been closed, please submit new traders clinic form.'));
            $this->response($resp);
        }

        /* Check user have access of traders clinic */
        $features = get_user_active_features($object_info['user_id']);
        if(empty($features) || $features['can_access_traders_clinic'] != 1) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'TRADERS_CLINIC_FEATURE_IS_INACTIVE', 'error_label' => 'You cannot access traders clinic feature on this membership, to access it please subscribe our membership plan and if you have any query then contact to CICM support.'));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }

        /* Process Request */
        $_POST['from_id'] = $_POST['user_id'];
        $_POST['status'] = 1;
        $_POST[ 'created_at'] = $_POST['updated_at'] = date('Y-m-d H:i:s');
        unset($_POST['user_id']);
        
        $tcMsgId = $this->common_model->addRecords(TC_FORM_MESSAGE, $_POST);
        if($tcMsgId) {
            $returnData = $_POST;
            $returnData['id'] =  $tcMsgId;
            $returnData['tc_msg_id'] =  $tcMsgId;
            $returnData['tc_ticket_id'] = $_POST['tc_id'];

            $returnData['user_data'] = array(
                'name' => $checkUser['name'],
                'email' => $checkUser['email'],
                'profile_pic' => (!empty($checkUser['profile_pic'])) ? base_url().USER_PROFILE_PIC_PATH.$checkUser['profile_pic'] : ''
            );

            /* Send notification to portal */
            send_web_notification('New message on traders clinic thread by user '.ucfirst($checkUser['name']).', please have a look.', 'New Traders Clinic Message', get_cms_url('admin/tradersclinic/view/'.$object_info['tc_id']), array());

            /* Send Push Notification */
            if($checkUser['device_type'] == 1 && !empty($checkUser['device_token'])) {
                 $fcmData = array(
                    'title' => 'Traders Clinic Enquiry Submitted Successfully.',
                    'body' => 'Dear '.ucfirst($checkUser['name']).', Your Traders Clinic enquiry has been submitted successfully, our representative will connect with you as soon as possible.',
                    'type' => 'new_traders_form_message',
                    'extra' => array('id' => $tcMsgId, 'tc_id' => $_POST['tc_id'], 'user_id' => $object_info['user_id'])
                );
                send_fcm_notification($fcmData, $checkUser['device_token']);
            }

            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('success' => 'TRADERS_CLINIC_MESSAGE_SUBMITTED_SUCCESSFULLY', 'success_label' => 'Traders clinic message submitted successfully.', 'data' => $returnData));
        } else {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'UNABLE_TO_SUBMIT_DATA', 'error_label' => 'Some error occurred, please try again.'));
        }

        /* Return Response */
        $this->response($resp);
    }

     /**
     * Get All Traders Clinic Threads Message
     * @param $_POST
     * @return $resp
     */
    public function gettradersclinicthreadsmessage_post(){
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('tc_id', 'user_id', 'limit', 'offset');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid tc */
        $checkTc = $this->common_model->getSingleRecordById(TC_FORM_SUBMISSION, array('id' => $object_info['tc_id'], 'user_id' => $object_info['user_id']));
        if(empty($checkTc)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_TC', 'error_label' => 'This form is not exists in our system.'));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }

        /* Check user have access of traders clinic */
        $features = get_user_active_features($object_info['user_id']);
        if(empty($features) || $features['can_access_traders_clinic'] != 1) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'TRADERS_CLINIC_FEATURE_IS_INACTIVE', 'error_label' => 'You cannot access traders clinic feature on this membership, to access it please subscribe our membership plan and if you have any query then contact to CICM support.'));
            $this->response($resp);
        }
        
        /* Process Request */
        $messages = $this->common_model->getPaginateRecordsByOrderByCondition(TC_FORM_MESSAGE, 'created_at', 'DESC', $object_info['limit'], $object_info['offset'], array('tc_id' => $object_info['tc_id'], 'status' => 1));
        if($messages) {
            /* Check for pagination data info */
            $result = $object_info['limit'];
            $offset = $object_info['offset'];
            $checkNext = $result + $offset;
            $totalMessages = $this->common_model->getRecordCount(TC_FORM_MESSAGE, array('tc_id' => $object_info['tc_id'], 'status' => 1));

            /* Check for hasNext */
            if($checkNext >= $totalMessages) {
                $hasNext = FALSE;
            } else {
                $hasNext = TRUE;
            }

            foreach($messages as $val) {
                if($val['from_id'] == $object_info['user_id']) {
                    $val['user_data'] = array(
                        'name' => $checkUser['name'],
                        'email' => $checkUser['email'],
                        'profile_pic' => (!empty($checkUser['profile_pic'])) ? base_url().USER_PROFILE_PIC_PATH.$checkUser['profile_pic'] : ''
                    );
                } else {
                    $fromUser = get_user($val['from_id']);
                    $val['user_data'] = array(
                        'name' => $fromUser['name'],
                        'email' => $fromUser['email'],
                        'profile_pic' => $fromUser['profile_pic']
                    );
                }

                $val['display_date'] = date('D d M, Y h:i A', strtotime($val['created_at']));
                $msgArr[] = $val;
            }

            $dataInfo = array(
                'hasNext' => $hasNext,
                'limit' => $object_info['limit'],
                'offset' => $object_info['offset'],
                'total_records' => $totalMessages
            );

            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('tc_status' => $checkTc['status'], 'data' => $msgArr, 'dataInfo' => $dataInfo));
        } else {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'NO_MESSAGES_FOUND', 'error_label' => 'No messages are found in this thread'));
        }


        /* Return Response */
        $this->response($resp);
    }
}