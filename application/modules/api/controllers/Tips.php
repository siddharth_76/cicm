<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tips extends REST_Controller {

	public function __construct() {
		header('Access-Control-Allow-Origin: *');
        parent::__construct();
    }

    /**
     * Get all tips
     */
    public function getalltips_post() {
    	/* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id', 'limit', 'offset');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }

        /* Check user have access of tip */
        $features = get_user_active_features($object_info['user_id']);
        if(empty($features) || $features['can_access_tips'] != 1) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'TIP_FEATURE_IS_INACTIVE', 'error_label' => 'You cannot access tips feature on this membership, to access it please subscribe our membership plan and if you have any query then contact to CICM support.'));
            $this->response($resp);
        }

        /* Process Request */
        $segments = get_user_subscribed_tip_segments($object_info['user_id']);
        if($segments == '-1') {
            $tips = $this->common_model->getPaginateRecordsByOrderByCondition(TIP, 'updated_at', 'DESC', $object_info['limit'],  $object_info['offset'], array('is_published' => 1));
        } else {
            if(is_array($segments)) {
                $fetchSegments = implode(',', $segments);
                $sql = 'SELECT * FROM '.TIP.' WHERE is_published = 1 AND stock_market_type IN('.$fetchSegments.') ORDER BY updated_at DESC LIMIT '.$object_info['limit'].' OFFSET '.$object_info['offset'];
                $tips = $this->common_model->getCustomSqlResult($sql);
            }
        }

    	if(!empty($tips)) {
    		/* Check for pagination data info */
            $result = $object_info['limit'];
            $offset = $object_info['offset'];
            $checkNext = $result + $offset;
            $totalTips = $this->common_model->getRecordCount(TIP, array('is_published' => 1));

            /* Check for hasNext */
            if($checkNext >= $totalTips) {
                $hasNext = FALSE;
            } else {
                $hasNext = TRUE;
            }

            foreach($tips as $val) {
            	if(!empty($val['price_one']) && !empty($val['price_two'])) {
            		$price = revertDBInt($val['price_one']).'-'.revertDBInt($val['price_two']);
            	} else {
            		$price = revertDBInt($val['price_one']);
            	}

            	$targets = $this->common_model->getAllRecordsById(TIP_TARGET, array('tt_tip_id' => $val['tip_id']));
            	if(!empty($targets)) {
            		foreach($targets as $tval) {
	            		$targetArr[] = array(	
	            			'target_id' => $tval['tt_id'],
	            			'target' => revertDBInt($tval['target_amount']),
	            			'target_number' => $tval['target_number'],
                            'is_achieved' => $tval['is_achieved'],
	            			'status' => 1
	        			);
	            	}
            	} else {
            		$targetArr = array();
            	}

            	$followUps = $this->common_model->getAllRecordsOrderById(TIP_FOLLOW_UP, 'created_at', 'DESC', array('fu_tip_id' => $val['tip_id'], 'fu_status' => 1));
            	if(!empty($followUps)) {
                    $displayTime = date('M d Y, h:i A', strtotime($followUps[0]['created_at']));
            		foreach($followUps as $fval) {
            			$followArr[] = array(
            				'id' => $fval['fu_id'],
            				'message' => $fval['fu_message'],
            				'display_date' => date('M d Y, h:i A', strtotime($fval['created_at']))
        				);
            		}
            	} else {
                    $displayTime = date('M d Y, h:i A',strtotime($val['created_at']));
            		$followArr = array();
            	}

            	$checkFav = $this->common_model->getSingleRecordById(USER_FAVORITE_TIP, array('tip_id' => $val['tip_id'], 'user_id' => $object_info['user_id']));

            	$tipsArr[] = array(
            		'tip_id' => $val['tip_id'],
	    			'tip_type' => get_tip_type($val['tip_type']),
	    			'market_type' => get_tip_exchange_market_type($val['market_type']),
	    			'exchange' => $val['exchange'],
	    			'exchange_tip_type' => get_tip_exchange_type($val['exchange_tip_type']),
                    'stock_market_code' => $val['stock_market_code'],
	    			'total_target' => $val['total_added_targets'],
	    			'acheived_target' => $val['acheived_target'],
	    			'targets' => $targetArr,
					'stoploss' => revertDBInt($val['stoploss']),
                    'trailed_stoploss' => revertDBInt($val['trailed_stoploss']),
					'tip_price' => $price,
					'display_date' => $displayTime,
					'tip_label' => strtoupper($val['exchange']).' '.get_tip_exchange_market_type($val['market_type']).' '.get_tip_exchange_type($val['exchange_tip_type']),
					'is_favorite' => (!empty($checkFav)) ? $checkFav['is_favorite'] : 0,
					'follow_ups' => $followArr,
					'is_published' => $val['is_published'],
					'status' => $val['status'],
                    'is_shared' => get_tip_share_status($object_info['user_id'], $val),
                    'created_at' => $val['created_at'],
                    'updated_at' => $val['updated_at']
        		);
        		unset($targetArr, $followArr);
            }

            $dataInfo = array(
                'hasNext' => $hasNext,
                'limit' => $object_info['limit'],
                'offset' => $object_info['offset'],
                'total_records' => $totalTips
            );

            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('tips' => $tipsArr, 'dataInfo' => $dataInfo));
    	} else {
    		$resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'NO_TIPS_FOUND', 'error_label' => 'No tips found'));
    	}

		/* Return Response */
		$this->response($resp);
    }

    /**
     * Get tip detail
     */
    public function gettip_post() {
    	/* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id', 'tip_id');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }

        /* Check user have access of tip */
        $features = get_user_active_features($object_info['user_id']);
        if($checkUser['user_type_id'] == APP_USER_TYPE && (empty($features) || $features['can_access_tips'] != 1)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'TIP_FEATURE_IS_INACTIVE', 'error_label' => 'You cannot access tips feature on this membership, to access it please subscribe our membership plan and if you have any query then contact to CICM support.'));
            $this->response($resp);
        }

        /* Check For Valid Tip */
        $checkTip = $this->common_model->getSingleRecordById(TIP, array('tip_id' => $object_info['tip_id']));
        if(empty($checkTip)) {
        	$resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_TIP', 'error_label' => 'Invalid tip, please try again later.'));
        	$this->response($resp);
        }

        /* Process Request */
        if(!empty($checkTip['price_one']) && !empty($checkTip['price_two'])) {
    		$price = revertDBInt($checkTip['price_one']).'-'.revertDBInt($checkTip['price_two']);
    	} else {
    		$price = revertDBInt($checkTip['price_one']);
    	}

    	$targets = $this->common_model->getAllRecordsById(TIP_TARGET, array('tt_tip_id' => $checkTip['tip_id']));
    	if(!empty($targets)) {
    		foreach($targets as $tval) {
        		$targetArr[] = array(	
        			'target_id' => $tval['tt_id'],
        			'target' => revertDBInt($tval['target_amount']),
        			'target_number' => $tval['target_number'],
                    'is_achieved' => $tval['is_achieved'],
        			'status' => 1
    			);
        	}
    	} else {
    		$targetArr = array();
    	}

    	$followUps = $this->common_model->getAllRecordsOrderById(TIP_FOLLOW_UP, 'created_at', 'DESC', array('fu_tip_id' => $checkTip['tip_id'], 'fu_status' => 1));
    	if(!empty($followUps)) {
    		foreach($followUps as $fval) {
    			$followArr[] = array(
    				'id' => $fval['fu_id'],
    				'message' => $fval['fu_message'],
    				'display_date' => date('M d Y, h:i A', strtotime($fval['created_at']))
				);
    		}
    	} else {
    		$followArr = array();
    	}

    	$checkFav = $this->common_model->getSingleRecordById(USER_FAVORITE_TIP, array('tip_id' => $checkTip['tip_id'], 'user_id' => $object_info['user_id']));

    	$tips = array(
    		'tip_id' => $checkTip['tip_id'],
			'tip_type' => get_tip_type($checkTip['tip_type']),
			'market_type' => get_tip_exchange_market_type($checkTip['market_type']),
			'exchange' => $checkTip['exchange'],
			'exchange_tip_type' => get_tip_exchange_type($checkTip['exchange_tip_type']),
            'stock_market_code' => $checkTip['stock_market_code'],
			'total_target' => $checkTip['total_added_targets'],
			'acheived_target' => $checkTip['acheived_target'],
			'targets' => $targetArr,
			'stoploss' => revertDBInt($checkTip['stoploss']),
            'trailed_stoploss' => revertDBInt($checkTip['trailed_stoploss']),
			'tip_price' => $price,
			'display_date' => date('M d Y, h:i A',strtotime($checkTip['created_at'])),
			'tip_label' => strtoupper($checkTip['exchange']).' '.get_tip_exchange_market_type($checkTip['market_type']).' '.get_tip_exchange_type($checkTip['exchange_tip_type']),
			'is_favorite' => (!empty($checkFav)) ? $checkFav['is_favorite'] : 0,
			'follow_ups' => $followArr,
			'is_published' => $checkTip['is_published'],
			'status' => $checkTip['status'],
            'is_shared' => get_tip_share_status($object_info['user_id'], $checkTip),
            'created_at' => $checkTip['created_at'],
            'updated_at' => $checkTip['updated_at']
		);

		$resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('tip_data' => $tips));
		
		/* Return Response */
		$this->response($resp);
    }

    /**
     * Mark Favourite Tip
     * @param $_POST
     * @return $resp
     */
    public function markfavoritetip_post(){
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id', 'tip_id', 'is_favorite');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }

        /* Check user have access of tip */
        $features = get_user_active_features($object_info['user_id']);
        if(empty($features) || $features['can_access_tips'] != 1) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'TIP_FEATURE_IS_INACTIVE', 'error_label' => 'You cannot access tips feature on this membership, to access it please subscribe our membership plan and if you have any query then contact to CICM support.'));
            $this->response($resp);
        }

        /* Check For Valid Tip */
        $checkTip = $this->common_model->getSingleRecordById(TIP, array('tip_id' => $object_info['tip_id']));
        if(empty($checkTip)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_TIP', 'error_label' => 'Invalid tip, please try again later.'));
            $this->response($resp);
        }

        /* Process Request */
        $checkFav = $this->common_model->getSingleRecordById(USER_FAVORITE_TIP, array('user_id' => $object_info['user_id'], 'tip_id' => $object_info['tip_id']));
        if(!empty($checkFav)){
            $this->common_model->updateRecords(USER_FAVORITE_TIP, array('is_favorite' => $object_info['is_favorite'], 'updated_at' => date('Y-m-d H:i:s')), array('user_id' => $object_info['user_id'], 'tip_id' => $object_info['tip_id']));
            $resp= array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('success' => 'TIP_UPDATED_SUCCESSFULLY', 'success_label' => 'Tip updated successfully.'));
        } else {
            $addData = array(
                'user_id' => $object_info['user_id'],
                'tip_id' => $object_info['tip_id'],
                'is_favorite' => $object_info['is_favorite'],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );

            $favId = $this->common_model->addRecords(USER_FAVORITE_TIP, $addData);
            if($favId) {
                $resp= array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('success' => 'TIP_UPDATED_SUCCESSFULLY', 'success_label' => 'Tip updated successfully.'));
            } else {
                $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'SOME_ERROR_OCCURRED', 'error_label' => 'Some error occurred, please try again later.'));
            }
        }

        /* Return Response */
        $this->response($resp);
    }

    /**
     * Get Favourite Tips
     * @param $_POST
     * @return $resp
     */
    public function getfaouritetips_post(){
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id', 'limit', 'offset');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }

        /* Check user have access of tip */
        $features = get_user_active_features($object_info['user_id']);
        if(empty($features) || $features['can_access_tips'] != 1) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'TIP_FEATURE_IS_INACTIVE', 'error_label' => 'You cannot access tips feature on this membership, to access it please subscribe our membership plan and if you have any query then contact to CICM support.'));
            $this->response($resp);
        }

        /* Process Request */
        $favTips = $this->common_model->getPaginateRecordsByOrderByCondition(USER_FAVORITE_TIP, 'created_at', 'DESC', $object_info['limit'], $object_info['offset'], array('user_id' => $object_info['user_id'], 'is_favorite' => 1));
        if(!empty($favTips)){
            /* Check for pagination data info */
            $result = $object_info['limit'];
            $offset = $object_info['offset'];
            $checkNext = $result + $offset;
            $totalTips = $this->common_model->getRecordCount(USER_FAVORITE_TIP, array('user_id' => $object_info['user_id'], 'is_favorite' => 1));

            /* Check for hasNext */
            if($checkNext >= $totalTips) {
                $hasNext = FALSE;
            } else {
                $hasNext = TRUE;
            }

            $tipsArr = array();
            foreach($favTips as $val) {
                $tipData = $this->common_model->getSingleRecordById(TIP, array('tip_id' => $val['tip_id'], 'is_published' => 1));
                if(!empty($tipData)) {
                    if(!empty($tipData['price_one']) && !empty($tipData['price_two'])) {
                        $price = revertDBInt($tipData['price_one']).'-'.revertDBInt($tipData['price_two']);
                    } else {
                        $price = revertDBInt($tipData['price_one']);
                    }

                    $targets = $this->common_model->getAllRecordsById(TIP_TARGET, array('tt_tip_id' => $tipData['tip_id']));
                    if(!empty($targets)) {
                        foreach($targets as $tval) {
                            $targetArr[] = array(   
                                'target_id' => $tval['tt_id'],
                                'target' => revertDBInt($tval['target_amount']),
                                'target_number' => $tval['target_number'],
                                'is_achieved' => $tval['is_achieved'],
                                'status' => 1
                            );
                        }
                    } else {
                        $targetArr = array();
                    }

                    $followUps = $this->common_model->getAllRecordsOrderById(TIP_FOLLOW_UP, 'created_at', 'DESC', array('fu_tip_id' => $tipData['tip_id'], 'fu_status' => 1));
                    if(!empty($followUps)) {
                        $displayTime = date('M d Y, h:i A', strtotime($followUps[0]['created_at']));
                        foreach($followUps as $fval) {
                            $followArr[] = array(
                                'id' => $fval['fu_id'],
                                'message' => $fval['fu_message'],
                                'display_date' => date('M d Y, h:i A', strtotime($fval['created_at']))
                            );
                        }
                    } else {
                        $displayTime = date('M d Y, h:i A', strtotime($val['created_at']));
                        $followArr = array();
                    }

                    $checkFav = $this->common_model->getSingleRecordById(USER_FAVORITE_TIP, array('tip_id' => $tipData['tip_id'],'user_id' => $object_info['user_id']));

                    $tipsArr[] = array(
                        'tip_id' => $tipData['tip_id'],
                        'tip_type' => get_tip_type($tipData['tip_type']),
                        'market_type' => get_tip_exchange_market_type($tipData['market_type']),
                        'exchange' => $tipData['exchange'],
                        'exchange_tip_type' => get_tip_exchange_type($tipData['exchange_tip_type']),
                        'stock_market_code' => $tipData['stock_market_code'],
                        'total_target' => $tipData['total_added_targets'],
                        'acheived_target' => $tipData['acheived_target'],
                        'targets' => $targetArr,
                        'stoploss' => revertDBInt($tipData['stoploss']),
                        'trailed_stoploss' => revertDBInt($tipData['trailed_stoploss']),
                        'tip_price' => $price,
                        'display_date' => $displayTime,
                        'tip_label' => strtoupper($tipData['exchange']).' '.get_tip_exchange_market_type($tipData['market_type']).' '.get_tip_exchange_type($tipData['exchange_tip_type']),
                        'is_favorite' => (!empty($checkFav)) ? $checkFav['is_favorite'] : 0,
                        'follow_ups' => $followArr,
                        'is_published' => $tipData['is_published'],
                        'status' => $tipData['status'],
                        'is_shared' => get_tip_share_status($object_info['user_id'], $tipData),
                        'created_at' => $tipData['created_at'],
                        'updated_at' => $tipData['updated_at']
                    );
                    unset($tipData, $targetArr, $followArr);
                }
            }

            $dataInfo = array(
                'hasNext' => $hasNext,
                'limit' => $object_info['limit'],
                'offset' => $object_info['offset'],
                'total_records' => $totalTips
            );

            if(!empty($tipsArr)) {
                $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('tips' => $tipsArr, 'dataInfo' => $dataInfo));
            } else {
                $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'NO_FAVOURITE_TIPS_FOUND', 'error_label' => 'No favourite tips found.'));
            }
        } else {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'NO_FAVOURITE_TIPS_FOUND', 'error_label' => 'No favourite tips found.'));
        }

        /* Return Response */
        $this->response($resp);
    }

    /**
     * Share Tips
     */
    public function sharetip_post() {
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id', 'tip_id');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }

        /* Check user have access of tip */
        $features = get_user_active_features($object_info['user_id']);
        if(empty($features) || $features['can_access_tips'] != 1) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'TIP_FEATURE_IS_INACTIVE', 'error_label' => 'You cannot access tips feature on this membership, to access it please subscribe our membership plan and if you have any query then contact to CICM support.'));
            $this->response($resp);
        }

        /* Check For Valid Tip */
        $checkTip = $this->common_model->getSingleRecordById(TIP, array('tip_id' => $object_info['tip_id']));
        if(empty($checkTip)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_TIP', 'error_label' => 'Invalid tip, please try again later.'));
            $this->response($resp);
        }

        if($checkTip['is_shared'] != 1) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'CANNOT_SHARE_THIS_TIP', 'error_label' => 'Sorry, You cannot share this tip.'));
            $this->response($resp);
        }

        /* Check for alredy shared tip */
        $checkShare = $this->common_model->getRecordCount(USER_SHARE_TIP, array('user_id' => $object_info['user_id'], 'tip_id' => $object_info['tip_id']));
        if($checkShare > 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ALREADY_SHRED_THIS_TIP', 'error_label' => 'You have already shared this tip.'));
            $this->response($resp);
        }

        /* Process Request */
        $shareImage = $this->common_model->getSingleRecordByOrder(SHARE_IMAGE, 'image', 'RANDOM');
        if(empty($shareImage)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'CANNOT_SHARE_THIS_TIP', 'error_label' => 'Sorry, You cannot share this tip.'));
            $this->response($resp);
        }

        $img = base_url().'uploads/site/'.$shareImage['image'];

        /* Add User Share Tip Info */
        $addData = array(
            'user_id' => $object_info['user_id'],
            'tip_id' => $object_info['tip_id'],
            'share_image' => $shareImage['image'],
            'shared_at' => date('Y-m-d H:i:s')
        );
        $shareId = $this->common_model->addRecords(USER_SHARE_TIP, $addData);
        if($shareId) {
            /* Fetching Tip Info */
            if(!empty($checkTip['price_one']) && !empty($checkTip['price_two'])) {
                $price = revertDBInt($checkTip['price_one']).'-'.revertDBInt($checkTip['price_two']);
            } else {
                $price = revertDBInt($checkTip['price_one']);
            }

            $targets = $this->common_model->getAllRecordsById(TIP_TARGET, array('tt_tip_id' => $checkTip['tip_id']));
            if(!empty($targets)) {
                foreach($targets as $tval) {
                    $targetArr[] = array(   
                        'target_id' => $tval['tt_id'],
                        'target' => revertDBInt($tval['target_amount']),
                        'target_number' => $tval['target_number'],
                        'is_achieved' => $tval['is_achieved'],
                        'status' => 1
                    );
                }
            } else {
                $targetArr = array();
            }

            $followUps = $this->common_model->getAllRecordsOrderById(TIP_FOLLOW_UP, 'created_at', 'DESC', array('fu_tip_id' => $checkTip['tip_id'], 'fu_status' => 1));
            if(!empty($followUps)) {
                foreach($followUps as $fval) {
                    $followArr[] = array(
                        'id' => $fval['fu_id'],
                        'message' => $fval['fu_message'],
                        'display_date' => date('M d Y, h:i A', strtotime($fval['created_at']))
                    );
                }
            } else {
                $followArr = array();
            }

            $checkFav = $this->common_model->getSingleRecordById(USER_FAVORITE_TIP, array('tip_id' => $checkTip['tip_id'], 'user_id' => $object_info['user_id']));

            $tips = array(
                'tip_id' => $checkTip['tip_id'],
                'tip_type' => get_tip_type($checkTip['tip_type']),
                'market_type' => get_tip_exchange_market_type($checkTip['market_type']),
                'exchange' => $checkTip['exchange'],
                'exchange_tip_type' => get_tip_exchange_type($checkTip['exchange_tip_type']),
                'stock_market_code' => $checkTip['stock_market_code'],
                'total_target' => $checkTip['total_added_targets'],
                'acheived_target' => $checkTip['acheived_target'],
                'targets' => $targetArr,
                'stoploss' => revertDBInt($checkTip['stoploss']),
                'trailed_stoploss' => revertDBInt($checkTip['trailed_stoploss']),
                'tip_price' => $price,
                'display_date' => date('M d Y, h:i A',strtotime($checkTip['created_at'])),
                'tip_label' => strtoupper($checkTip['exchange']).' '.get_tip_exchange_market_type($checkTip['market_type']).' '.get_tip_exchange_type($checkTip['exchange_tip_type']),
                'is_favorite' => (!empty($checkFav)) ? $checkFav['is_favorite'] : 0,
                'follow_ups' => $followArr,
                'is_published' => $checkTip['is_published'],
                'status' => $checkTip['status'],
                'is_shared' => $checkTip['is_shared'],
                'created_at' => $checkTip['created_at'],
                'updated_at' => $checkTip['updated_at']
            );

            $shareHeading = get_option('tip_sharing_heading');
            $shareMessage = sprintf(get_option('tip_sharing_message'), revertDBInt($checkTip['tip_share_earning']).'%', revertDBInt($checkTip['tip_share_earning_days']), strtoupper($checkTip['exchange']));

            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('share_id' => $shareId, 'share_image' => $img, 'share_heading' => $shareHeading, 'share_message' => $shareMessage, 'tip_data' => $tips));
        } else {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'UNABLE_TO_SHARE_TIP', 'error_label' => 'Unable to share tip, please try again later.'));
        }
        
        /* Return Response */
        $this->response($resp);
    }

    /**
     * Add rewar points on user profile.
     * @return $resp
     */
    public function addrewardpoint_post(){
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id', 'tip_id', 'share_id');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }

        /* Check For Valid Tip */
        $checkTip = $this->common_model->getSingleRecordById(TIP, array('tip_id' => $object_info['tip_id']));
        if(empty($checkTip)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_TIP', 'error_label' => 'Invalid tip, please try again later.'));
            $this->response($resp);
        }

        if($checkTip['is_shared'] != 1) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'CANNOT_SHARE_THIS_TIP', 'error_label' => 'Sorry, You cannot share this tip.'));
            $this->response($resp);
        }

        /* Check For Valid share request */
        $checkShare = $this->common_model->getSingleRecordById(USER_SHARE_TIP, array('id' => $object_info['share_id']));
        if(empty($checkShare)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_SHARE_TIP', 'error_label' => 'Invalid share tip, please try again later.'));
            $this->response($resp);
        }

        /* Process Request */
        $rewardPoints = get_option('reward_point_on_tip_share');
        $rewardPointActualValue = get_option('reward_point_actual_value');

        /* Updating user reward points */
        $this->common_model->updateUserRewardPoints(USER, $rewardPoints, $object_info['user_id']);

        /* Adding user earned reward points history */
        $addData = array(
            'user_id' => $object_info['user_id'],
            'share_id' => $object_info['share_id'],
            'tip_id' => $object_info['tip_id'],
            'earn_points' => convertDBInt($rewardPoints),
            'per_points_real_value' => convertDBInt($rewardPointActualValue),
            'created_at' => date('Y-m-d H:i:s')
        );

        $id = $this->common_model->addRecords(USER_EARNED_REWARD_POINTS, $addData);

        if($id) {
            /* Updating share tip data */
            $updateData = array(
                'is_earned_points' => 1,
                'earned_reward_points' => convertDBInt($rewardPoints)
            );
            $this->common_model->updateRecords(USER_SHARE_TIP, $updateData, array('id' => $object_info['share_id']));
            
            /* Send Notitification */
            $userData = get_user($object_info['user_id']);
            $fcmData = array(
                'title' => 'Reward Points Received',
                'body' => 'You have received '.$rewardPoints. ' reward points in your account.',
                'type' => 'reward_point_received',
                'extra' => array('user_id' => $object_info['user_id'], 'received_reward_point' => $rewardPoints, 'total_reward_points' => revertDBInt($userData['reward_points']))
            );
            send_fcm_notification($fcmData, $userData['device_token']);

            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('success' => 'REWARD_POINTS_ADDED_SUCCESSFULLY', 'success_label' => 'Reward points added successfully.'));
        } else {
            /* Updating share tip data */
            $updateData = array(
                'is_earned_points' => 0,
                'earned_reward_points' => 0
            );
            $this->common_model->updateRecords(USER_SHARE_TIP, $updateData, array('id' => $object_info['share_id']));
            
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'UNABLE_TO_ADD_REWARD_POINTS', 'success_label' => 'Unable to add reward points.'));
        }

        /* Return response */
        $this->response($resp);
    } 
}