<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class IntradayScanner extends REST_Controller {

	public function __construct() {
		header('Access-Control-Allow-Origin: *');
        parent::__construct();
    }

    /**
     * Get all categories
     */
    public function getintradayscannercategories_get() {
    	$cats = $this->common_model->getAllRecords(INTRADAY_SCANNER_CATEGORY);
    	if(!empty($cats)){
    		foreach($cats as $val) {
    			$catsArr[] = array(
    				'id' => $val['id'],
    				'label' => strtoupper($val['title'])
				);
    		}
    		$resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('data' => $catsArr));
    	} else {
    		$resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'NOTHING_FOUND', 'error_label' => 'No categories found.'));
    	}

    	/* Return Response */
    	$this->response($resp);
    }

    /**
     * Get Intraday Data
     * @param $_POST
     * @return $resp
     */
    public function getintradaydata_post(){
    	/* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id', 'intraday_category_id', 'limit', 'offset');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }

        /* Check user have access of intraday scanner */
        $features = get_user_active_features($object_info['user_id']);
        if(empty($features) || empty($features) || $features['can_access_intraday_scanner'] != 1) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INTRADAY_SCANNER_FEATURE_IS_INACTIVE', 'error_label' => 'You cannot access intraday scanner feature on this membership, to access it please subscribe our membership plan and if you have any query then contact to CICM support.'));
            $this->response($resp);
        }

        /* Check for valid intraday scanner category */
        $checkCat = $this->common_model->getSingleRecordById(INTRADAY_SCANNER_CATEGORY, array('id' => $object_info['intraday_category_id']));
        if(empty($checkCat)) {
        	$resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'CATEGORY_NOT_FOUND', 'error_label' => 'Invalid intraday scanner category, please check it again.'));
        }

        /* Process Request */
        $isReady = 0;
        $segments = get_user_subscribed_intraday_scanner_segments($object_info['user_id']);
        if($segments == '-1') {
            $isReady = 1;
        } else {
            if(is_array($segments) && in_array($object_info['intraday_category_id'], $segments)){
                $isReady = 1;
            }
        }

        $condition = 'intraday_category_id = '.$object_info['intraday_category_id'].' AND DATE(publish_date) = "'.date('Y-m-d').'" AND status = 1';

        if(!empty($object_info['keyword'])) {
        	$condition .= ' AND(script_name LIKE "%'.$object_info['keyword'].'%" OR company_name LIKE "%'.$object_info['keyword'].'%")';
        }

        $intradayData = ($isReady == 1) ? $this->common_model->getPaginateRecordsByConditions(INTRADAY_SCANNER, $object_info['limit'], $object_info['offset'], $condition) : array();
    	if(!empty($intradayData)) {
    		/* Check for pagination data info */
            $result = $object_info['limit'];
            $offset = $object_info['offset'];
            $checkNext = $result + $offset;
            $totalData = $this->common_model->getRecordCount(INTRADAY_SCANNER, $condition);

            /* Check for hasNext */
            if($checkNext >= $totalData) {
                $hasNext = FALSE;
            } else {
                $hasNext = TRUE;
            }

            foreach($intradayData as $val) {
            	$val['display_date'] = date('d M Y', strtotime($val['publish_date']));
            	$val['display_time'] = '08:00 AM';
            	$val['intraday_category_label'] = strtoupper($checkCat['title']);
                $val['buy_above'] = revertDBInt($val['buy_above']);
                $val['buy_stoploss'] = revertDBInt($val['buy_stoploss']);
                $val['buy_target_1'] = revertDBInt($val['buy_target_1']);
                $val['buy_target_2'] = revertDBInt($val['buy_target_2']);
                $val['buy_target_3'] = revertDBInt($val['buy_target_3']);
                $val['sell_below'] = revertDBInt($val['sell_below']);
                $val['sell_stoploss'] = revertDBInt($val['sell_stoploss']);
                $val['sell_target_1'] = revertDBInt($val['sell_target_1']);
                $val['sell_target_2'] = revertDBInt($val['sell_target_2']);
                $val['sell_target_3'] = revertDBInt($val['sell_target_3']);
            	$dataAr[] = $val;
            }

            $dataInfo = array(
                'hasNext' => $hasNext,
                'limit' => $object_info['limit'],
                'offset' => $object_info['offset'],
                'total_records' => $totalData
            );

            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('data' => $dataAr, 'dataInfo' => $dataInfo));
    	} else {
    		$resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'NOTHING_FOUND', 'error_label' => 'Market closed, wait for next update.'));
    	}

    	/* Return Response */
    	$this->response($resp);
    }

    /**
     * Get Intraday Data By Id
     * @param $_POST;
     * @return $resp
     */
    public function getintradaydatabyid_post(){
    	/* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id', 'id');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }

        /* Check user have access of intraday scanner */
        $features = get_user_active_features($object_info['user_id']);
        if(empty($features) || $features['can_access_intraday_scanner'] != 1) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INTRADAY_SCANNER_FEATURE_IS_INACTIVE', 'error_label' => 'You cannot access intraday scanner feature on this membership, to access it please subscribe our membership plan and if you have any query then contact to CICM support.'));
            $this->response($resp);
        }

        /* Process Request */
        $intradayData = $this->common_model->getSingleRecordById(INTRADAY_SCANNER, array('id' => $object_info['id'], 'status' => 1));
        if(!empty($intradayData)) {
        	/* Getting category data */
        	$catData = $this->common_model->getSingleRecordById(INTRADAY_SCANNER_CATEGORY, array('id' => $intradayData['intraday_category_id']));
        	$intradayData['display_date'] = date('d M Y', strtotime($intradayData['publish_date']));
        	$intradayData['display_time'] = '08:00 AM';
        	$intradayData['intraday_category_label'] = strtoupper($catData['title']);
            $intradayData['buy_above'] = revertDBInt($intradayData['buy_above']);
            $intradayData['buy_stoploss'] = revertDBInt($intradayData['buy_stoploss']);
            $intradayData['buy_target_1'] = revertDBInt($intradayData['buy_target_1']);
            $intradayData['buy_target_2'] = revertDBInt($intradayData['buy_target_2']);
            $intradayData['buy_target_3'] = revertDBInt($intradayData['buy_target_3']);
            $intradayData['sell_below'] = revertDBInt($intradayData['sell_below']);
            $intradayData['sell_stoploss'] = revertDBInt($intradayData['sell_stoploss']);
            $intradayData['sell_target_1'] = revertDBInt($intradayData['sell_target_1']);
            $intradayData['sell_target_2'] = revertDBInt($intradayData['sell_target_2']);
            $intradayData['sell_target_3'] = revertDBInt($intradayData['sell_target_3']);

        	$resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('data' => $intradayData));
        } else {
        	$resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'NOTHING_FOUND', 'error_label' => 'Market closed, wait for next update.'));
        }

        /* Return Response */
        $this->response($resp);
    }
}