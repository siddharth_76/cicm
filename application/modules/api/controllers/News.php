<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends REST_Controller {

	public function __construct() {
		header('Access-Control-Allow-Origin: *');
        parent::__construct();
    }

    /**
	 * Get All News
     * @param $_POST
     * @return $resp
     */
    public function getallnews_post() {
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id', 'limit', 'offset');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }

        /* Process Request */
        $news = $this->common_model->getPaginateRecordsByOrderByCondition(NEWS, 'created_at', 'DESC', $object_info['limit'], $object_info['offset'], array('status' => 1));
    	if(!empty($news)) {
            /* Check for pagination data info */
            $result = $object_info['limit'];
            $offset = $object_info['offset'];
            $checkNext = $result + $offset;
            $totalNews = $this->common_model->getRecordCount(NEWS, array('status' => 1));

            /* Check for hasNext */
            if($checkNext >= $totalNews) {
                $hasNext = FALSE;
            } else {
                $hasNext = TRUE;
            }

            foreach($news as $val) {
                $newsData[] = array(
                    'news_id' => $val['news_id'], 
                    'title' => $val['title'],
                    'description' => $val['description'],
                    'attachment' => base_url().NEWS_ATTACHMENT_PATH.$val['attachment'],
                    'is_read_more' => (!empty($val['description'])) ? '1' : '0',
                    'status' => $val['status'],
                    'display_date' => date('D d M, Y h:i A', strtotime($val['created_at'])),
                    'news_date' => date('d M', strtotime($val['created_at'])),
                    'news_time' => date('h:i A', strtotime($val['created_at']))
                );
            }

            $dataInfo = array(
                'hasNext' => $hasNext,
                'limit' => $object_info['limit'],
                'offset' => $object_info['offset'],
                'total_records' => $totalNews
            );

            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('news' => $newsData, 'dataInfo' => $dataInfo));
        } else {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'NO_NEWS_FOUND', 'error_label' => 'No news found.'));
        }
        
        /* Return Response */
		$this->response($resp);
    }

    /**
	 * Get News Detail
     * @param $_POST
     * @return $resp
     */
    public function getnews_post() {
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id', 'news_id');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }

        /* Check News */
        $checkNews = $this->common_model->getSingleRecordById(NEWS, array('news_id' => $object_info['news_id']));
        if(!empty($checkNews)) {
            $news = array(
                'news_id' => $checkNews['news_id'], 
                'title' => $checkNews['title'],
                'description' => $checkNews['description'],
                'attachment' => base_url().NEWS_ATTACHMENT_PATH.$checkNews['attachment'],
                'is_read_more' => (!empty($checkNews['description'])) ? '1' : '0',
                'status' => $checkNews['status'],
                'display_date' => date('D d M, Y h:i A', strtotime($checkNews['created_at'])),
                'news_date' => date('d M', strtotime($checkNews['created_at'])),
                'news_time' => date('h:i A', strtotime($checkNews['created_at']))
            );

            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('news_data' => $news));
        } else {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'NOTHING_FOUND', 'error_label' => 'Unable to load news detail, please try again.'));
        }
    	
        /* Return Response */
		$this->response($resp);
    }
}