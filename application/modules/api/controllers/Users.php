<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends REST_Controller {

	public function __construct() {
		header('Access-Control-Allow-Origin: *');

        $this->AppUserType = APP_USER_TYPE;
        $this->freeMember = FREE_MEMBER;
        $this->premiumMember = PREMIUM_MEMBER;

        parent::__construct();
    }

    /**
     * Get all user types
     * @return $resp
     */
    public function getallusertypes_get(){
        $userTypes = $this->common_model->getAllRecords(USER_TYPE);
        if(!empty($userTypes)) {
            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('data' => $userTypes));
        } else {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'NO_USER_TYPES_FOUND', 'error_label' => 'No user types found.'));
        }

        /* Return Response */
        $this->response($resp);
    }

    /**
     * Create user
     * @param Request $request
     * @return $resp
     */
    public function createuser_post() {
        /* Fetching request param */
        $object_info = $_POST;
        $deviceType = ($object_info['device_type'] == 1) ? 'Android' : 'IOS';
        $otp = random_numbers(get_option('verification_otp_length'));
        $alertMessage = '';

        /* Check for required value */
        $required_parameter = array('name', 'email', 'phone_number', 'device_type', 'device_id', 'device_token');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid device */
        $checkDevice = $this->common_model->getRecordCount(USER, array('user_type_id' => $this->AppUserType, 'email' => $object_info['email'], 'phone_number' => $object_info['phone_number'], 'device_type' => $object_info['device_type'], 'device_id' => $object_info['device_id']));
        if($checkDevice > 0) {
            /* Add Notification */
            $notificationMsg = 'Someone trying to register account by using '.$deviceType. ' device with device id: '.$object_info['device_id'];
            $noData = array(
                'type' => 'device_exists',
                'text' => $notificationMsg,
                'extra' => serialize($object_info),
                'created_at' => date('Y-m-d H:i:s')
            );
            $notificationId = $this->common_model->addRecords(NOTIFICATION, $noData);

            /* Send Web Notification */
            $heading = 'New Device Reset Request';
            $message = $notificationMsg;
            send_web_notification($message, $heading, get_cms_url('admin/notifications/view/'.$notificationId));

            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'DEVICE_IS_ALREADY_REGISTERED', 'error_label' => get_option('device_exists_message')));
            $this->response($resp);
        }

        /* Check for already registered user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('user_type_id' => $this->AppUserType, 'email' => $object_info['email'], 'phone_number' => $object_info['phone_number']));
        if(!empty($checkUser)) {
            /* Updating user data */
            $updateData = array(
                'verification_otp' => $otp,
                'device_type' => $object_info['device_type'],
                'device_id' => $object_info['device_id'],
                'device_token' => $object_info['device_token'],
                'last_login_date' => date('Y-m-d H:i:s'),
                'last_login_ip' => $_SERVER['REMOTE_ADDR'],
                'updated_at' => date('Y-m-d H:i:s')
            );
            $this->common_model->updateRecords(USER, $updateData, array('id' => $checkUser['id']));

            /* Getting user information */
            $userData = get_user($checkUser['id']);
            $userData['reward_points'] = revertDBInt($userData['reward_points']);

            /* Getting total filled up traders clinic form by user */
            $userData['tc_data'] = get_user_tc_form_history($checkUser['id']);
            
            /* Getting user subscription data */
            $userData['subscription_data'] = $this->getUserMembershipInfo($checkUser['id']);

            if($userData['membership_status'] == 0) {
                $alertMessage = sprintf(get_option('membership_expire_message'), $userData['name']);
            }

            /* Send Verification OTP */
            $textMessage = sprintf(get_option('verification_otp_message'), ucfirst($userData['name']), $otp);
            send_sms($userData['phone_number'], $textMessage);

            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('success' => 'USER_CREATED_SUCCESSFULLY', 'success_label' => 'User created successfully.', 'user_data' => $userData, 'alert_message' => $alertMessage));
        } else {
            /* Check for valid email */
            $checkEmail = $this->common_model->getRecordCount(USER, array('user_type_id' => $this->AppUserType, 'email' => $object_info['email'], 'is_user_deleted' => 0));
            if($checkEmail > 0) {
                $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'EMAIL_IS_ALREADY_REGISTERED', 'error_label' => 'This email is already registered.'));
                $this->response($resp);
            }

            /* Check for valid phone */
            $checkPhone = $this->common_model->getRecordCount(USER, array('user_type_id' => $this->AppUserType, 'phone_number' => $object_info['phone_number'], 'is_user_deleted' => 0));
            if($checkPhone > 0) {
                $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'PHONE_NUMBER_IS_ALREADY_REGISTERED', 'error_label' => 'This phone number is already registered.'));
                $this->response($resp);
            }

            /* Process Request */
            $object_info['user_type_id'] = $this->AppUserType;
            $object_info['email'] = strtolower($object_info['email']);
            $object_info['verification_otp'] = $otp;
            $object_info['membership_type'] = $this->freeMember;
            $object_info['membership_expire_on'] = date('Y-m-d', strtotime('+ '.get_option('free_trial_days_for_new_members').' days'));
            $object_info['membership_status'] = 1;
            $object_info['status'] = 2;
            $object_info['created_at'] = $object_info['updated_at'] = date('Y-m-d H:i:s');

            $userId = $this->common_model->addRecords(USER, $object_info);
            if(!empty($userId)) { 
                /* Add record on user membership extension */
                $postData = array(
                    'user_id' => $userId,
                    'membership_extended_by' => ADMIN_USER_ID,
                    'membership_type' => FREE_MEMBER,
                    'membership_start_date' => date('Y-m-d'),
                    'membership_end_date' => date('Y-m-d', strtotime('+ '.get_option('free_trial_days_for_new_members').' days')),
                    'membership_extended_text' => 'Membership days entended on first time registration.',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );

                $extensionId = $this->common_model->addRecords(USER_MEMBERSHIP_EXTENSION, $postData);
                if(!empty($extensionId)) {
                    /* Manage User Features */
                    $this->manageUserFeatures($userId);

                    /* Fetch User Details */
                    $userData = get_user($userId);
                    $userData['reward_points'] = revertDBInt($userData['reward_points']);

                    /* Getting total filled up traders clinic form by user */
                    $userData['tc_data'] = get_user_tc_form_history($userId);

                    /* Getting user subscription data */
                    $userData['subscription_data'] = $this->getUserMembershipInfo($userId);

                    /* Send Email */
                    send_mail(sprintf(NEW_USER_EMAIL_MSG, $userData['name'], $userData['email'], $userData['phone_number']), NEW_USER_EMAIL_SUBJECT, $userData['email']);

                    /* Send Verification OTP */
                    $textMessage = sprintf(get_option('verification_otp_message'), ucfirst($userData['name']), $otp);
                    send_sms($userData['phone_number'], $textMessage);

                    $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('success' => 'USER_CREATED_SUCCESSFULLY', 'success_label' => 'User created successfully.', 'user_data' => $userData, 'alert_message' => $alertMessage));
                } else {
                    $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'UNABLE_TO_CREATE_USER', 'error_label' =>  'Unable to create user, please try again later.'));
                }
            } else {
                $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'UNABLE_TO_CREATE_USER', 'error_label' =>  'Unable to create user, please try again later.'));
            }
        }

        /* Return response */
        $this->response($resp);
    }

    /**
     * Reset User Device
     * @param $_POST
     * @return $resp
     */
    public function resetuserdevice_post() {
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('device_type', 'device_id');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid device */
        $checkDevice = $this->common_model->getRecordCount(USER, array('user_type_id' => $this->AppUserType, 'device_type' => $object_info['device_type'], 'device_id' => $object_info['device_id']));
        if($checkDevice > 0) {
            $sql = 'UPDATE `'.USER.'` SET `device_id` = "" WHERE `device_type` = '.$object_info['device_type'].' AND `user_type_id` = '.$this->AppUserType.' AND `device_id` = "'.$object_info['device_id'].'"';
            $this->common_model->updateCustomSql($sql);
            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('success' => 'DEVICE_RESET_SUCCESSFULLY', 'success_label' => 'Device reset successfully, now you can create your account.'));
        } else {
            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('error' => 'DEVICE_NOT_FOUND', 'error_label' => 'Device not found.'));
        }

        /* Return Response */
        $this->response($resp);
    }

    /**
     * manage user features
     * @param $userId
     */
    public function manageUserFeatures($userId) {
        if(!empty($userId)) {
            $features = application_features();
            $postData = '';
            foreach($features as $val) {
                $postData = array(
                    'user_id' => $userId,
                    'feature_title' => $val['label'],
                    'feature_prefix' => $val['cond'],
                    'is_active' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                );
                $this->common_model->addRecords(USER_ACTIVE_FEATURES, $postData);
                unset($postData);
            }
        }
    }

    /**
     * Add user profile pic
     * @param $object_info
     * @return $resp
     */
    public function adduserprofilepic_post(){
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('id');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }

        /* Uploading Profile Picture */
        if(!empty($_FILES['profile_pic']['name'])) {
            $config['file_name']     = time().$_FILES['profile_pic']['name']; 
            $config['upload_path']   = './uploads/users';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '2474898';
            $config['max_width']     = '100024';
            $config['max_height']    = '768000';
            $config['remove_spaces'] = true;
            $config['field_name']    = 'profile_pic';

            $imageData = upload_file($_FILES['profile_pic'], $config);
            if(isset($imageData) && $imageData['message'] == 'success') {
                $this->common_model->updateRecords(USER, array('profile_pic' => $imageData['file_name'], 'updated_at' => date('Y-m-d H:i:s')), array('id' => $object_info['id']));
                $profilePic = $imageData['file_path'];
                $resp = array('code' => SUCCESS, 'message' => SUCCESS, 'response' => array('success' => 'PROFILE_PICTURE_UPLOADED_SUCCESSFULLY', 'success_label' => 'Profile picture uploaded successfully.', 'file_path' => $profilePic));
            } else {
                $resp = array('code' => SERVER_ERROR, 'message' => SERVER_MSG, 'response' => array('error' => 'SERVER_ERROR_OCCURED', 'error_label' => 'Some error occured, please try again.'));
            }
        } else {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'PLEASE_SELECT_PROFILE_PICTURE', 'error_label' => 'Please select profile picture to upload.'));
        }

        /* Return Response */
        $this->response($resp);
    }

    /**
     * Get user
     * @param $object_info
     * @return $resp
     */
    public function getuser_post() {
        /* Fetching request param */
        $object_info = $_POST;
        $alertMessage = '';

        /* Check for required value */
        $required_parameter = array('id');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getRecordCount(USER, array('id' => $object_info['id'], 'is_user_deleted' => 0));
        if($checkUser == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        /* Process Request */
        $user = get_user($object_info['id']);
        if(!empty($user)) {
            $user['reward_points'] = revertDBInt($user['reward_points']);

            /* Getting total filled up traders clinic form by user */
            $user['tc_data'] = get_user_tc_form_history($user['id']);
            
            /* Getting user subscription data */
            $user['subscription_data'] = $this->getUserMembershipInfo($user['id']);

            /* Check for user membership status */
            if($user['membership_status'] == 0) {
                $alertMessage = sprintf(get_option('membership_expire_message'), $user['name']);
            }

            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('user_data' => $user, 'alert_message' => $alertMessage));
        } else {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'USER_NOT_FOUND', 'error_label' => 'User not found.'));
        }

        /* Return Response */
        $this->response($resp);
    }

    /**
     * Edit user
     * @param $object_info
     * @return $resp
     */
    public function edituser_post() {
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkFromUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkFromUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkFromUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }

        /* Check for valid phone number */
        $checkPhone = $this->common_model->getCustomSqlCount('SELECT * FROM `'.USER.'` WHERE phone_number = "'.$object_info['phone_number'].'" AND id != '.$object_info['user_id']);
        if($checkPhone > 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'PHONE_NUMBER_IS_ALREADY_REGISTERED', 'error_label' => 'This phone number is already registered.'));
            $this->response($resp);
        }

        /* Process Request */
        $updateData = $object_info;
        $updateData['updated_at'] = date('Y-m-d H:i:s');

        unset($updateData['user_id']);
        $this->common_model->updateRecords(USER, $updateData, array('id' => $object_info['user_id']));

        $user = get_user($object_info['user_id']);
        $user['reward_points'] = revertDBInt($user['reward_points']);

        /* Getting total filled up traders clinic form by user */
        $user['tc_data'] = get_user_tc_form_history($user['id']);

        /* Getting user subscription data */
        $user['subscription_data'] = $this->getUserMembershipInfo($user['id']);

        $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('success' => 'USER_UPDATED_SUCCESSFULLY', 'success_label' => 'User profile updated successfully.', 'user_data' => $user));

        /* Return Response */
        $this->response($resp);
    }

    /**
     * Update user device info
     * @param $object_info
     * @return $resp
     */
    public function updateuserdivceinfo_post() {
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id', 'device_type', 'device_id', 'device_token');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'USER_IS_NOT_EXISTS', 'error_label' => 'Sorry, user is not exists in our system.'));
            $this->response($resp);
        }

        /* Precess Request */
        $updateData = array(
            'device_id' => $object_info['device_id'],
            'device_type' => $object_info['device_type'],
            'device_token' => $object_info['device_token'],
            'last_login_date' => date('Y-m-d H:i:s'),
            'last_login_ip' => $_SERVER['REMOTE_ADDR'],
            'updated_at' => date('Y-m-d H:i:s')
        );
        $this->common_model->updateRecords(USER, $updateData, array('id' => $object_info['user_id']));

        $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('success' => 'USER_DEVICE_INFO_UPDATED_SUCCESSFULLY', 'success_label' => 'User device info updated successfully.'));
        $this->response($resp);
    }

    /**
     * Resend OTP
     * @param $_POST
     * @return $resp
     */
    public function resendotp_post(){
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'USER_IS_NOT_EXISTS', 'error_label' => 'Sorry, user is not exists in our system.'));
            $this->response($resp);
        }

        /* Process Request */
        $userData = get_user($object_info['user_id']);
        $otp = random_numbers(get_option('verification_otp_length'));

        $this->common_model->updateRecords(USER, array('verification_otp' => $otp, 'updated_at' => date('Y-m-d H:i:s')), array('id' => $object_info['user_id']));
        
        /* Send Verification OTP */
        $textMessage = sprintf(get_option('verification_otp_message'), ucfirst($userData['name']), $otp);
        send_sms($userData['phone_number'], $textMessage);

        /* Send Response */
        $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('success' => 'OTP_SEND_SUCCESSFULLY', 'success_label' => 'OTP resend successfully.', 'otp' => $otp));
        $this->response($resp);
    }

    /**
     * Verify User
     * @param $object_info
     * @return $resp
     */
    public function verifyuser_post() {
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id', 'verification_otp');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'USER_IS_NOT_EXISTS', 'error_label' => 'Sorry, user is not exists in our system.'));
            $this->response($resp);
        }

        /* Check for valid OTP */
        $checkOtp = $this->common_model->getRecordCount(USER, array('id' => $object_info['user_id'], 'verification_otp' => $object_info['verification_otp']));
        if($checkOtp == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_OTP', 'error_label' => 'Invalid OTP.'));
            $this->response($resp);
        }

        /* Precess Request */
        $updateData = array(
            'verification_otp' => '',
            'status' => 1,
            'is_phone_verified' => 1,
            'last_login_date' => date('Y-m-d H:i:s'),
            'last_login_ip' => $_SERVER['REMOTE_ADDR'],
            'updated_at' => date('Y-m-d H:i:s')
        );
        $this->common_model->updateRecords(USER, $updateData, array('id' => $object_info['user_id']));

        /* Send welcome message to user */
        if($checkUser['is_phone_verified'] != 1) {
            $message = sprintf(get_option('welcome_user_text_message'), ucfirst($checkUser['name']), get_option('free_trial_days_for_new_members'));
            send_sms($checkUser['phone_number'], $message);
        }

        $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('success' => 'USER_VERIFIED_SUCCESSFULLY', 'success_label' => 'User verified successfully.'));
        $this->response($resp);
    }

    /**
     * Get membership plan categories
     * @return $resp
     */
    public function getmembershipcategories_get() {
        $categories = $this->common_model->getAllRecordsOrderById(MEMBERSHIP_PLAN_CATEGORY, 'id', 'ASC', array('status' => 1));
        if(!empty($categories)) {
            foreach($categories as $val) {
                $catsArr[] = array(
                    'category_id' => $val['id'],
                    'category_title' => $val['title'],
                    'category_image' => (!empty($val['image'])) ? base_url().'assets/'.$val['image'] : ''
                );
            }
            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('categories' => $catsArr));
        } else {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'NOTHING_FOUND', 'error_label' => 'No membership categories found.'));
        }

        /* Return Response */
        $this->response($resp);
    }

    /**
     * Get all membership plans
     * @return $resp
     */
    public function getallmembershipplans_get() {
        $planCats = $this->common_model->getAllRecordsOrderById(MEMBERSHIP_PLAN_CATEGORY, 'id', 'ASC', array('status' => 1));
        if(!empty($planCats)) {
            foreach($planCats as $val) {
                $plan_category_info = array('category_id' => $val['id'], 'category_title' => $val['title'], 'category_image' => (!empty($val['image'])) ? base_url().'assets/'.$val['image'] : '');
                $plans = $this->common_model->getAllRecordsOrderById(MEMBERSHIP_PLAN, 'plan_benifits_in_days', 'ASC', array('plan_category' => $val['id'], 'plan_status' => 1));

                if(!empty($plans)) {
                    foreach($plans as $pval) {
                        $pval['plan_category_label'] = $val['title'];
                        $plan_data[] = $pval;
                    }
                } else {
                    $plan_data = array();
                }

                $planSArr[] = array(
                    'plan_category_info' => $plan_category_info,
                    'plan_data' => $plan_data
                );
                unset($plan_data);
            }
            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('plans' => $planSArr));
        } else {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'NOTHING_FOUND', 'error_label' => 'No membership plans found.'));
        }

        /* Return Response */
        $this->response($resp);
    }

    /**
     * Get membership plans by category
     * @param $_POST
     * @return $resp
     */
    public function getmembershipplansbycategory_post() {
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id', 'category_id');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }

        /* Check for valid category */
        $checkCategory = $this->common_model->getSingleRecordById(MEMBERSHIP_PLAN_CATEGORY, array('id' => $object_info['category_id']));
        if(empty($checkCategory)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_CATEGORy', 'error_label' => 'This category is not exists in our system.'));
            $this->response($resp);
        }

        /* Process Request */
        $plans = $this->common_model->getAllRecordsOrderById(MEMBERSHIP_PLAN, 'plan_benifits_in_days', 'ASC', array('plan_category' => $object_info['category_id'], 'plan_status' => 1));
        if(!empty($plans)) {
            $planCategory = array(
                'category_id' => $checkCategory['id'],
                'category_title' => $checkCategory['title'],
                'category_image' => (!empty($checkCategory['image'])) ? base_url().'assets/'.$checkCategory['image'] : ''
            );

            foreach($plans as $val) {
                $val['plan_category_label'] = $checkCategory['title'];
                $planSArr[] = $val;
            }
            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('plan_category' => $planCategory, 'plans' => $planSArr));
        } else {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'NOTHING_FOUND', 'error_label' => 'No membership plans found.'));
        }

        /* Return Response */
        $this->response($resp);
    }

     /**
     * Get membership plan detail
     * @param $_POST
     * @return $resp
     */
    public function getmembershipplanbyid_post() {
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id', 'plan_id');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }

        /* Process Request */
        $plan = $this->common_model->getSingleRecordById(MEMBERSHIP_PLAN, array('plan_id' => $object_info['plan_id'], 'plan_status' => 1));
        if(!empty($plan)) {
            $checkCategory = $this->common_model->getSingleRecordById(MEMBERSHIP_PLAN_CATEGORY, array('id' => $plan['plan_category']));
            $planCategory = array(
                'category_id' => $checkCategory['id'],
                'category_title' => $checkCategory['title'],
                'category_image' => (!empty($checkCategory['image'])) ? base_url().'assets/'.$checkCategory['image'] : ''
            );
            $plan['plan_category_label'] = $checkCategory['title'];
            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('plan_category' => $planCategory, 'plan' => $plan));
        } else {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'NOTHING_FOUND', 'error_label' => 'No membership plans found.'));
        }

        /* Return Response */
        $this->response($resp);
    }

    /**
     * Get user active features
     * @param $object_info
     * @return $resp
     */
    public function getuseractivefeatures_post(){
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }

        /* Process request */
        $activeFeatures = get_user_active_features($object_info['user_id']);
        $returnData = array(
            'user_id' => $object_info['user_id'],
            'active_features' => $activeFeatures
        );

        $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('data' => $returnData));
        
        /* Return Response */
        $this->response($resp);
    }

    /**
     * Get user membership info
     * @param $userId
     */
    public function getUserMembershipInfo($userId) {
        $membershipData = array();
        if(!empty($userId)) {
            $userData = $this->common_model->getSingleRecordById(USER, array('id' => $userId));
            if(!empty($userData)) {
                $membershipType = $userData['membership_type'];
                if($membershipType == FREE_MEMBER) {
                    $diff = abs(strtotime($userData['membership_expire_on']) - strtotime(date('Y-m-d')));
                    $years = floor($diff / (365*60*60*24)); 
                    $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));  
                    $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
                    $membershipData = array(
                        'membership_type' => 'Free',
                        'membership_expire_on' => date('d/m/Y', strtotime($userData['membership_expire_on'])),
                        'membership_expire_on_display_date' => date('d M, Y', strtotime($userData['membership_expire_on'])),
                        'days_left' => ($userData['membership_status'] == 1 && $days > 0) ? $days : 0,
                        'membership_status' => ($userData['membership_status'] == 1 && $days > 0) ? 'Active' : 'Expired',
                        'subscription_data' => array()
                    );
                } elseif($membershipType == PREMIUM_MEMBER) {
                    $subsArr = array();
                    $subscribedPlans = $this->common_model->getAllRecordsOrderById(USER_SUBSCRIPTION_HISTORY, 'created_at', 'DESC', array('user_id' => $userData['id'], 'status' => 1));
                    if(!empty($subscribedPlans)) {
                        foreach($subscribedPlans as $val) {
                            $planData = $this->common_model->getSingleRecordById(MEMBERSHIP_PLAN, array('plan_id' => $val['subscribed_plan_id']));
                            $subsArr[] = array(
                                'plan_id' => $planData['plan_id'],
                                'plan_title' => $planData['plan_title'],
                                'plan_membership_status' => ($val['status'] == 1) ? 'Active' : 'Expired',
                                'plan_subscription_start_date' => date('d M, Y', strtotime($val['subscription_start_date'])),
                                'plan_subscription_end_date' => date('d M, Y', strtotime($val['subscription_end_date']))
                            );
                        }
                    }

                    $membershipData = array(
                        'membership_type' => 'Paid',
                        'membership_expire_on' => '',
                        'membership_expire_on_display_date' => '',
                        'days_left' => 0,
                        'membership_status' => ($userData['membership_status'] == 1) ? 'Active' : 'Expired',
                        'subscription_data' => $subsArr
                    );
                }
            }
        }
        return $membershipData;
    }
}