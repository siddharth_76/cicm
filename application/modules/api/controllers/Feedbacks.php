<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedbacks extends REST_Controller {

	public function __construct() {
		header('Access-Control-Allow-Origin: *');
        parent::__construct();
    }

    /**
     * Add Feedback Message
     * @param $_POST
     * @return $resp
     */
    public function addfeedbackmessage_post(){
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id', 'message');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }

        /* Process Request */
        $_POST['receiver_id'] = ADMIN_USER_ID;
        $_POST['sender_id'] = $_POST['init_user_id'] = $_POST['user_id'];
        $_POST['status'] = 1;
        $_POST['created_at'] = $_POST['updated_at'] = date('Y-m-d H:i:s');
        unset($_POST['user_id']);

        $feedbackId = $this->common_model->addRecords(FEEDBACK, $_POST);
        if($feedbackId) {
            $returnData = $_POST;
            $returnData['id'] =  $feedbackId;
            $returnData['display_date'] = date('D d M, Y h:i A');
            $returnData['user_data'] = array(
                'name' => $checkUser['name'],
                'email' => $checkUser['email'],
                'profile_pic' => (!empty($checkUser['profile_pic'])) ? base_url().USER_PROFILE_PIC_PATH.$checkUser['profile_pic'] : ''
            );

            /* Send notification to portal */
            send_web_notification('New feedback message by user '.ucfirst($checkUser['name']).', please have a look.', 'New Feedback Message', '');

            /* Send Push Notification */
            if($checkUser['device_type'] == 1 && !empty($checkUser['device_token'])) {
                $fcmData = array(
                    'title' => 'Feedback Submitted',
                    'body' => 'Dear '.ucfirst($checkUser['name']).', Your feedback has been submitted successfully, our representative will respond you as soon as possible.',
                    'extra' => array('user_id' => $object_info['user_id'], 'id' => $feedbackId)
                );
                send_fcm_notification($fcmData, $checkUser['device_token']);
            }

            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('success' => 'FEEDBACK_SUBMITTED_SUCCESSFULLY', 'success_label' => 'Feedback submitted successfully.', 'data' => $returnData));
        } else {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'UNABLE_TO_SUBMIT_MESSAGE', 'error_label' => 'Some error occurred, please try again.'));
        }

        /* Return Response */
        $this->response($resp);
    }

     /**
     * Get All Traders Clinic Threads Message
     * @param $_POST
     * @return $resp
     */
    public function getallfeddbacks_post(){
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id', 'limit', 'offset');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }

        /* Process Request */
        $messages = $this->common_model->getPaginateRecordsByOrderByCondition(FEEDBACK, 'created_at', 'DESC', $object_info['limit'], $object_info['offset'] , array('init_user_id' => $object_info['user_id'], 'status' => 1));
        if($messages) {
            /* Check for pagination data info */
            $result = $object_info['limit'];
            $offset = $object_info['offset'];
            $checkNext = $result + $offset;
            $totalMessages = $this->common_model->getRecordCount(FEEDBACK, array('init_user_id' => $object_info['user_id'], 'status' => 1));

            /* Check for hasNext */
            if($checkNext >= $totalMessages) {
                $hasNext = FALSE;
            } else {
                $hasNext = TRUE;
            }

            foreach($messages as $val) {
                if($val['sender_id'] == $object_info['user_id']) {
                    $val['user_data'] = array(
                        'name' => $checkUser['name'],
                        'email' => $checkUser['email'],
                        'profile_pic' => (!empty($checkUser['profile_pic'])) ? base_url().USER_PROFILE_PIC_PATH.$checkUser['profile_pic'] : ''
                    );
                } else {
                    $fromUser = get_user($val['sender_id']);
                    $val['user_data'] = array(
                        'name' => $fromUser['name'],
                        'email' => $fromUser['email'],
                        'profile_pic' => $fromUser['profile_pic']
                    );
                }

                $val['display_date'] = date('D d M, Y h:i A', strtotime($val['created_at']));
                $msgArr[] = $val;
            }

            $dataInfo = array(
                'hasNext' => $hasNext,
                'limit' => $object_info['limit'],
                'offset' => $object_info['offset'],
                'total_records' => $totalMessages
            );

            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('data' => $msgArr, 'dataInfo' => $dataInfo));
        } else {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'NO_MESSAGES_FOUND', 'error_label' => 'No messages are found.'));
        }


        /* Return Response */
        $this->response($resp);
    }

    /**
     * Get feedback form elements
     * @param $_POST
     * @return $resp
     */
    public function getfeedbackformelement_post(){
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }

        /* Process Request */
        $formData = $this->common_model->getSingleRecordById(FEEDBACK_FORM_ELEMENT, array('status' => 1));
        $elements = unserialize($formData['elements']);
        if(!empty($formData) && !empty($elements)) {
            foreach($elements as $val){
                $questions[] = $val;
            }

            $return = array(
                'form_id' => $formData['id'],
                'questions' => $questions,
                'dropdown_max_value' => get_option('feedback_form_rating_numbers'),
                'show_comment_box' => ($formData['show_comment_box'] == 1) ? '1' : '0',
                'comment_field_label' => get_option('feedback_comment_field_label')
            );

            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('data' => $return));
        } else {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'NOTHING_FOUND', 'error_label' => 'No feedback form is available right now, we will update you once new form is posted.'));
        }

        /* Return Response */
        $this->response($resp);
    }

    /**
     * Submit feedback form data
     * @param $_POST
     * @return $resp
     */
    public function submitfeedbackformdata_post(){
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id', 'form_id');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }

        /* Check valid form */
        $checkForm = $this->common_model->getSingleRecordById(FEEDBACK_FORM_ELEMENT, array('id' => $object_info['form_id']));
        if(empty($checkForm)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'FORM_IS_NOT_EXISTS', 'error_label' => 'Some error occurred, please try again.'));
            $this->response($resp);
        }

        /* Process Request */
        $object_info['created_at'] = date('Y-m-d H:i:s');
        $submissionId = $this->common_model->addRecords(FEEDBACK_FORM_SUBMISSION, $object_info);
        if($submissionId) {
            /* Send Push Notification */
            if(!empty($checkUser['device_token'])) {
                if($checkUser['device_type'] == 1) {
                    $fcmData = array(
                        'title' => 'Feedback Submitted',
                        'body' => 'Dear '.ucfirst($checkUser['name']).', Thank You for your feedback, we will connect you as soon as possible.',
                        'type' => 'new_feedback_form_submission',
                        'extra' => array('form_id' => $object_info['form_id'], 'user_id' => $object_info['user_id'])
                    );
                    send_fcm_notification($fcmData, $checkUser['device_token']);
                }
            }

            /* Send Web Notification */
            $heading = 'New Feedback Form Submitted';
            $message = 'New feedback form has been submitted by user '.ucfirst($checkUser['name']).' ('.$checkUser['phone_number'].')';
            send_web_notification($message, $heading, get_cms_url('admin/feedbacks/view/'.$submissionId));

            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('success' => 'FORM_SUBMITTED_SUCCESSFULLY', 'success_label' => 'Thank You for your feedback, we will connect you as soon as possible.'));
        } else {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'UNABLE_TO_SUBMIT_DATA', 'error_label' => 'Some error occurred, please try again.'));
        }

        /* Return Response */
        $this->response($resp);
    }
}