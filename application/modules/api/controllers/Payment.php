<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends REST_Controller {

	public function __construct() {
		header('Access-Control-Allow-Origin: *');
        parent::__construct();
    }

    /**
     * Init Payment
     */
    public function init_post(){
    	/* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('user_id');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid user */
        $checkUser = $this->common_model->getSingleRecordById(USER, array('id' => $object_info['user_id'], 'is_user_deleted' => 0));
        if(empty($checkUser)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_USER', 'error_label' => 'User is not exists in our system.'));
            $this->response($resp);
        }

        if($checkUser['status'] == 0) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'ACCOUNT_BLOCKED', 'error_label' => 'Your account is blocked, please contact to CICM support.'));
            $this->response($resp);
        }

        /* Init Transaction Log */
        write_transaction_logs('Transaction Initiated: '.json_encode($object_info));

        /* Process Request */
        $txnId = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        $addRecord = array(
            'user_id' => $object_info['user_id'],
            'txn_id' => $txnId,
            'status_code' => '0',
            'payment_status' => 'init',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        );
        $paymentId = $this->common_model->addRecords(PAYMENT, $addRecord);
        if($paymentId) {
            $returnData = array(
                'user_id' => $object_info['user_id'], 
                'paymentId' =>  $paymentId, 
                'txn_id' => $txnId, 
                'success_url' => base_url().'api/payment/success/'.$paymentId,
                'cancel_url' => base_url().'api/payment/cancel/'.$paymentId
            );
            $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('data' => $returnData));
        } else {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'UNABLE_TO_PROCESS', 'error_label' => 'Some error occurred, please try again.'));
        }

        /* Return Response */
        $this->response($resp);
    }

    /**
     * Do Payment
     */
    public function doPayment_post() {
        /* Fetching request param */
        $object_info = $_POST;

        /* Check for required value */
        $required_parameter = array('payment_id');
        $chk_error = check_required_value($required_parameter, $object_info);
        if ($chk_error) {
            $resp = array('code' => MISSING_PARAM, 'message' => 'YOU_HAVE_MISSED_A_PARAMETER_' . strtoupper($chk_error['param']));
            $this->response($resp);
        }

        /* Check for valid payment */
        $checkPayment = $this->common_model->getSingleRecordById(PAYMENT, array('id' => $object_info['payment_id']));
        if(empty($checkPayment)) {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_PAYMENT', 'error_label' => 'Invalid payment, please check it again.'));
            $this->response($resp);
        }

        if($object_info['is_reward_points_used'] != 1) {
            /* Processed Transaction Log */
            write_transaction_logs('Transaction Processed: Payment ID: '.$object_info['payment_id'].', PayU Money Response: ' .$checkPayment['api_response']); 
        }

        /* Process Payment Request */
        if($object_info['is_reward_points_used'] == 1) {
            $planId = $object_info['plan_id'];
            $paymentId = $object_info['payment_id'];
            $status = 'success';

            /* updating payment info */
            $totalAmount = $object_info['total_amount'];
            $finalAmount = $object_info['final_amount'];
            $rewardPointsUsed = $object_info['used_reward_points'];
            $updateArr = array(
                'status_code' => 1,
                'is_reward_points_used' => $object_info['is_reward_points_used'],
                'total_amount' => convertDBInt($totalAmount),
                'final_amount' => convertDBInt($finalAmount),
                'used_reward_points' => convertDBInt($rewardPointsUsed),
                'payment_status' => $status,
                'updated_at' => date('Y-m-d H:i:s')
            );
            $this->common_model->updateRecords(PAYMENT, $updateArr, array('id' => $paymentId));

            /* Handle Payment */
            $resp = $this->handle_pyment($planId, $paymentId, $status, $checkPayment);
        } else {
            $apiData = json_decode($checkPayment['api_response']);
            if(empty($apiData)) {
                $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'INVALID_PAYMENT', 'error_label' => 'Invalid payment, please check it again.'));
                $this->response($resp);
            }

            $planId = $apiData->udf2;
            $paymentId = $object_info['payment_id'];
            $status = $apiData->status;

            /* updating payment info */
            $isRewardPointUsed = $apiData->udf3;
            $rewardPointsUsed = $apiData->udf4;
            $amountData =$apiData->udf5;
            $amountData = explode(',', $amountData);
            $totalAmount = $amountData[0];
            $finalAmount = $amountData[1];
            $updateArr = array(
                'is_reward_points_used' => $isRewardPointUsed,
                'total_amount' => convertDBInt($totalAmount),
                'final_amount' => convertDBInt($finalAmount),
                'used_reward_points' => convertDBInt($rewardPointsUsed),
                'updated_at' => date('Y-m-d H:i:s')
            );
            $this->common_model->updateRecords(PAYMENT, $updateArr, array('id' => $paymentId));

            /* Handle Payment */
            $resp = $this->handle_pyment($planId, $paymentId, $status, $checkPayment);
        }

        /* Return Response */
        $this->response($resp);
    }

    /**
     * Handle Payment 
     */
    public function handle_pyment($planId, $paymentId, $status, $checkPayment){
        if($status == 'success') {
            $userData = $this->common_model->getSingleRecordById(USER, array('id' => $checkPayment['user_id']));
            
            /* Getting Plan Data */
            $planData = $this->common_model->getSingleRecordById(MEMBERSHIP_PLAN, array('plan_id' => $planId));
            $isTipsAllowed = ($planData['is_tips_allowed'] == 1) ? 1 : 0;
            $isIntradayScannerAllowed = ($planData['is_intraday_scanner_allowed'] == 1) ? 1 : 0;
            $isTradersClinicAllowed = ($planData['is_traders_clinic_allowed'] == 1) ? 1 : 0;

            $userActiveFeatures = get_user_active_features($checkPayment['user_id']);
            $features = application_features();
            foreach($features as $val) {
                if($val['cond'] == 'can_access_tips') {
                    $isAllowed = $isTipsAllowed;
                } elseif($val['cond'] == 'can_access_intraday_scanner') {
                    $isAllowed = $isIntradayScannerAllowed;
                } elseif($val['cond'] == 'can_access_traders_clinic') {
                    $isAllowed = $isTradersClinicAllowed;
                } else {
                    $isAllowed = $val['value'];
                }

                $appliedFeatures[$val['cond']] = $isAllowed; 
            }

            $startDate = date('Y-m-d');
            $endDate = date('Y-m-d', strtotime('+ '.$planData['plan_benifits_in_days'].' Days'));

            /* Adding user subscription history */
            $subscriptionData = array(
                'user_id' => $checkPayment['user_id'],
                'payment_id' => $paymentId,
                'subscribed_plan_id' => $planId,
                'subscription_start_date' => $startDate,
                'subscription_end_date' => $endDate,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
            $subscriptionId = $this->common_model->addRecords(USER_SUBSCRIPTION_HISTORY, $subscriptionData);
            if(!empty($subscriptionId)) {
                /* Adding Membership Extension Data */
                $membershipExtensionData = array(
                    'user_id' => $checkPayment['user_id'],
                    'membership_extended_by' => ADMIN_USER_ID,
                    'membership_type' => PREMIUM_MEMBER,
                    'membership_start_date' => $startDate,
                    'membership_end_date' => $endDate,
                    'membership_extended_text' => 'Membership Days Extended By Subscription Plan '.ucfirst($planData['plan_title']).' Purchase.',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );
                $membershipExtensionId = $this->common_model->addRecords(USER_MEMBERSHIP_EXTENSION, $membershipExtensionData);
                
                /* Updating user active features and features history */
                $featureData = '';
                foreach($appliedFeatures as $key => $val) {
                    $checkFeature = $this->common_model->getRecordCount(USER_ACTIVE_FEATURES, array('user_id' => $checkPayment['user_id'], 'feature_prefix' => $key));
                    if(!empty($checkFeature)) {
                        $this->common_model->updateRecords(USER_ACTIVE_FEATURES, array('is_active' => $val), array('user_id' => $checkPayment['user_id'], 'feature_prefix' => $key));
                    } else {
                        $featureData = array(
                            'user_id' => $checkPayment['user_id'],
                            'feature_title' => str_replace('_', ' ', $key),
                            'feature_prefix' => $key,
                            'is_active' => $val,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        );
                        $this->common_model->addRecords(USER_ACTIVE_FEATURES, $featureData);
                        unset($featureData);
                    }
                }

                /* Adding user history change records */
                $historyData = array(
                    'user_id' => $checkPayment['user_id'],
                    'modified_by' => ADMIN_USER_ID,
                    'old_active_features' => serialize($userActiveFeatures),
                    'new_active_features' => serialize($appliedFeatures),
                    'created_at' => date('Y-m-d H:i:s')
                );
                $this->common_model->addRecords(USER_ACTIVE_FEATURES_CHANGE_HISTORY, $historyData);

                /* Updating User Membership Data */
                $this->common_model->updateRecords(USER, array('membership_type' => PREMIUM_MEMBER, 'membership_status' => 1, 'updated_at' => date('Y-m-d H:i:s')), array('id' => $checkPayment['user_id']));
                
                /* Updating user reward points */
                $paymentData = $this->common_model->getSingleRecordById(PAYMENT, array('id' => $paymentId));
                if($paymentData['is_reward_points_used'] == 1) {
                    $this->common_model->decreaseUserRewardPoints(USER, revertDBInt($paymentData['used_reward_points']), $checkPayment['user_id']);
                }

                /* Updating user traders clinic limits */
                if($planData['is_traders_clinic_allowed'] == 1) {
                    $allowedLimits = $planData['tc_allowed_query'];
                    $this->common_model->increaseUserTcFormLimit(USER, $allowedLimits, $checkPayment['user_id']);
                }

                /* Send Push Notification */
                if(!empty($userData['device_token'])) {
                    if($userData['device_type'] == 1){
                        $fcmData = array(
                            'title' => 'Subscription Plan',
                            'body' => 'Dear '.ucfirst($userData['name']).', You have successfully susbscribed for CICM plan.',
                            'type' => 'plan_subscribed',
                            'extra' => array('user_id' => $checkPayment['user_id'], 'plan_id' => $planId, 'active_features' => $appliedFeatures)
                        );
                        send_fcm_notification($fcmData, $userData['device_token']);
                    }
                }

                /* Send SMS */
                send_sms($userData['phone_number'], sprintf(get_option('paid_membership_join_message'), ucwords($userData['name']), ucwords($planData['plan_title']), $endDate));
                
                /* Return Data */
                $returnData = array(
                    'user_id' => $checkPayment['user_id'],
                    'plan_id' => $planId,
                    'plan_title' => $planData['plan_title'],
                    'is_reward_points_used' => $paymentData['is_reward_points_used'],
                    'used_reward_points' => revertDBInt($paymentData['used_reward_points']),
                    'total_amount' => revertDBInt($paymentData['total_amount']),
                    'final_amount' => revertDBInt($paymentData['final_amount']),
                    'active_features' => $appliedFeatures,
                    'payment_status' => $status,
                    'txn_id' => $checkPayment['txn_id']
                );
                $resp = array('code' => SUCCESS, 'message' => SUCCESS_MSG, 'response' => array('success' => 'PAYMENT_SUCCESSFULL', 'success_label' => 'Payment Successfull.', 'data' => $returnData));
            } else {
                $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'SOME_ERROR_OCCURRED', 'error_label' => 'Unable to process payment, please try again.'));
            }
        } else {
            $resp = array('code' => ERROR, 'message' => ERROR_MSG, 'response' => array('error' => 'PAYMENT_FAILED', 'error_label' => 'Payment failed, please try again.'));
        }
        return $resp;
    }

    /**
     * payu money success callback
     */
    public function success_post() {
        $paymentId = $this->uri->segment(4);
        if(isset($paymentId)) {
            $resp = $_POST;
            $updateData = array(
                'hash_key' => $resp['hash'],
                'status_code' => 1,
                'payment_status' => $resp['status'],
                'api_response' => json_encode($resp),
                'updated_at' => date('Y-m-d H:i:s')
            );
            $this->common_model->updateRecords(PAYMENT, $updateData, array('id' => $paymentId));
        }
    }

    /**
     * payu money cancel callback
     */
    public function cancel_post() {
        $paymentId = $this->uri->segment(4);
        if(isset($paymentId)) {
            $resp = $_POST;
            $updateData = array(
                'hash_key' => $resp['hash'],
                'status_code' => 1,
                'payment_status' => $resp['status'],
                'api_response' => json_encode($resp),
                'updated_at' => date('Y-m-d H:i:s')
            );
            $this->common_model->updateRecords(PAYMENT, $updateData, array('id' => $paymentId));
        }
    } 
}