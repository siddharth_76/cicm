<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends REST_Controller {

	public function __construct() {
		header('Access-Control-Allow-Origin: *');
        parent::__construct();
        $this->load->library('EXcel');
    }

    /**
     * Checking free user for membership expiration
     * Runs Every Day
     */
    public function checkusermembershipstatus_get() {
    	$sql = 'SELECT `id`, `name`, `email`, `phone_number`, `membership_type`, `device_token`, `device_type` FROM `'.USER.'` WHERE DATE(membership_expire_on) < DATE("'.date('Y-m-d').'") AND user_type_id = '.APP_USER_TYPE.' AND membership_status = 1 AND membership_type = '.FREE_MEMBER.' AND is_user_deleted = 0';
    	$users = $this->common_model->getCustomSqlResult($sql);

    	if(!empty($users)) {
    		$updateData = '';
    		foreach($users as $val) {
    			/* Updating user data */
    			$updateData = array(
    				'membership_status' => 0,
    				'updated_at' => date('Y-m-d H:i:s')
				);
				$this->common_model->updateRecords(USER, $updateData, array('id' => $val['id']));

                /* Getting current active features for features */
                $currentFeatures = $this->common_model->getAllRecordsById(USER_ACTIVE_FEATURES, array('user_id' => $val['id']));
                foreach($currentFeatures as $cv) {
                    $oldFeatures[$cv['feature_prefix']] = $cv['is_active'];
                }

                /* Managing users active features */
                $activeFeatures = unserialize(get_option('active_features_for_membership_expired_users')); 
                foreach($activeFeatures as $ak => $av) {
                    $this->common_model->updateRecords(USER_ACTIVE_FEATURES, array('is_active' => $av, 'updated_at' => date('Y-m-d H:i:s')), array('user_id' => $val['id'], 'feature_prefix' => $ak));
                }

                /* Adding user history change records */
                $historyData = array(
                    'user_id' => $val['id'],
                    'modified_by' => 0,
                    'old_active_features' => serialize($oldFeatures),
                    'new_active_features' => get_option('active_features_for_membership_expired_users'),
                    'created_at' => date('Y-m-d H:i:s')
                );
                $this->common_model->addRecords(USER_ACTIVE_FEATURES_CHANGE_HISTORY, $historyData);

    			/* Add Notification */
    			$membershipType = ($val['membership_type'] == 1) ? 'Free' : 'Paid';

                $notificationMsg = 'Application active features is changed for user '.$val['name'].' because of membership expiration';
                $notiExtra = array(
                    'name' => $val['name'],
                    'old_active_features' => $oldFeatures,
                    'new_active_features' => unserialize(get_option('active_features_for_membership_expired_users'))
                );

                $noData = array(
                    'type' => 'features_changed',
                    'text' => $notificationMsg,
                    'extra' => serialize($notiExtra),
                    'created_at' => date('Y-m-d H:i:s')
                );
                $notificationId = $this->common_model->addRecords(NOTIFICATION, $noData);

	            $notificationMsg1 = $membershipType.' user '.ucfirst($val['name']).' membership is expired on '.date('d/m/Y').'.';
	            $noData1 = array(
	                'type' => 'membership_expired',
	                'text' => $notificationMsg1,
	                'extra' => '',
	                'created_at' => date('Y-m-d H:i:s')
	            );
	            $notificationId1 = $this->common_model->addRecords(NOTIFICATION, $noData1);

                if(!empty($val['device_token'])) {
                    if($val['device_type'] == 1){
                        $fcmData = array(
                            'title' => 'CICM Application Features Updated',
                            'body' => 'Dear '.ucfirst($val['name']).', your CICM application active features has been updated, to get a benifit of all features please subscribe CICM subscription plan.',
                            'type' => 'active_features_changed',
                            'extra' => array('user_id' => $val['id'], 'active_features' => $activeFeatures)
                        );
                        send_fcm_notification($fcmData, $val['device_token']);
                    }
                }

	            unset($updateData);
    		}
    	}
    }


    /**
     * Checking paid user for membership expiration
     * Runs Every Day
     */
    public function checkpaidusermembershipstatus_get() {
        $users = $this->common_model->getAllRecordsById(USER, array('membership_status' => 1, 'membership_type' => PREMIUM_MEMBER, 'user_type_id' => APP_USER_TYPE, 'is_user_deleted' => 0));
        if(!empty($users)){
            foreach($users as $val){
                $userId = $val['id'];
                $userData = $val;
                $isUpdated = '';

                /* Getting User Subscriptions */
                $subscriptions = $this->common_model->getAllRecordsById(USER_SUBSCRIPTION_HISTORY, array('user_id' => $userId, 'status' => 1));
                $totalSubscriptions = count($subscriptions);

                $isTipsActive = $isIntradayScannerActive = $isTradersClinicActive = 0;
                if(!empty($subscriptions)){
                    $isUpdated = 0;
                    foreach($subscriptions as $sval){
                        $planInfo = $this->common_model->getSingleRecordById(MEMBERSHIP_PLAN, array('plan_id' => $sval['subscribed_plan_id']));
                
                        if($planInfo['is_tips_allowed'] == 1 && strtotime($sval['subscription_end_date']) > strtotime(date('Y-m-d'))){
                            $isTipsActive = 1;
                        }

                        if($planInfo['is_intraday_scanner_allowed'] == 1 && strtotime($sval['subscription_end_date']) > strtotime(date('Y-m-d'))){
                            $isIntradayScannerActive = 1;
                        }

                        if($planInfo['is_traders_clinic_allowed'] == 1 && strtotime($sval['subscription_end_date']) > strtotime(date('Y-m-d'))){
                            $isTradersClinicActive = 1;
                        }

                        /* Updating subscription history */
                        if(strtotime(date('Y-m-d')) > strtotime($sval['subscription_end_date'])) {
                            $this->common_model->updateRecords(USER_SUBSCRIPTION_HISTORY, array('status' => 0, 'updated_at' => date('Y-m-d H:i:s')), array('id' => $sval['id']));
                            send_sms($val['phone_number'], sprintf(get_option('paid_membership_expire_message'), ucwords($val['name']), $planInfo['plan_title']));
                            $isUpdated++;
                        }
                    }
                }

                /* Updating user active features */
                if($isUpdated > 0) {
                    $userActiveFeatures = get_user_active_features($userId);
                    $features = application_features();
                    $activeFeatures = unserialize(get_option('active_features_for_membership_expired_users')); 
                    foreach($activeFeatures as $ack => $acv) {
                        if($ack == 'can_access_tips') {
                            $isAllowed = $isTipsActive;
                        } elseif($ack == 'can_access_intraday_scanner') {
                            $isAllowed = $isIntradayScannerActive;
                        } elseif($ack == 'can_access_traders_clinic') {
                            $isAllowed = $isTradersClinicActive;
                        } else {
                            $isAllowed = $acv;
                        }

                        $appliedFeatures[$ack] = $isAllowed;
                    }

                    /* Updating features */
                    foreach($appliedFeatures as $ak => $av) {
                        $this->common_model->updateRecords(USER_ACTIVE_FEATURES, array('is_active' => $av, 'updated_at' => date('Y-m-d H:i:s')), array('user_id' => $userId, 'feature_prefix' => $ak));
                    }

                    /* Adding user history change records */
                    $historyData = array(
                        'user_id' => $userId,
                        'modified_by' => 0,
                        'old_active_features' => serialize($userActiveFeatures),
                        'new_active_features' => serialize($appliedFeatures),
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    $this->common_model->addRecords(USER_ACTIVE_FEATURES_CHANGE_HISTORY, $historyData);

                    /* Add Notification */
                    $membershipType = ($userData['membership_type'] == 1) ? 'Free' : 'Paid';

                    $notificationMsg = 'Application active features is changed for user '.ucfirst($userData['name']).' because of membership plan expiration';
                    $notiExtra = array(
                        'old_active_features' => $userActiveFeatures,
                        'new_active_features' => $appliedFeatures
                    );

                    $noData = array(
                        'type' => 'features_changed',
                        'text' => $notificationMsg,
                        'extra' => serialize($notiExtra),
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    $notificationId = $this->common_model->addRecords(NOTIFICATION, $noData);

                    $notificationMsg1 = $membershipType.' user '.ucfirst($userData['name']).' membership is expired on '.date('d/m/Y').'.';
                    $noData1 = array(
                        'type' => 'membership_expired',
                        'text' => $notificationMsg1,
                        'extra' => '',
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    $notificationId1 = $this->common_model->addRecords(NOTIFICATION, $noData1);

                    /* updating user info */
                    if($totalSubscriptions == $isUpdated) {
                        $this->common_model->updateRecords(USER, array('membership_status' => 0, 'membership_type' => PREMIUM_MEMBER, 'updated_at' => date('Y-m-d H:i:s')), array('id' => $userId));
                    }

                    /* Send Push Notification */
                    if(!empty($userData['device_token'])) {
                        if($userData['device_type'] == 1){
                            $fcmData = array(
                                'title' => 'CICM Application Features Updated',
                                'body' => 'Dear '.ucfirst($userData['name']).', your CICM application active features has been updated, to get a benifit of all features please subscribe CICM subscription plan.',
                                'type' => 'active_features_changed',
                                'extra' => array('user_id' => $userId, 'active_features' => $appliedFeatures)
                            );
                            send_fcm_notification($fcmData, $userData['device_token']);
                        }
                    }
                    unset($appliedFeatures); 
                }
            }
        }
    }

    /**
     * Publish pensing tips
     * Runs Every Day
     */ 
    public function publishpendingtips_get(){
        $sql = 'SELECT * FROM `'.TIP.'` WHERE status = 2 AND is_published = 0 AND DATE(publish_date) = "'.date('Y-m-d').'"';
        $tips = $this->common_model->getCustomSqlResult($sql);

        if(!empty($tips)) {
            foreach($tips as $val){
                /* Adding Notification */
                $noData = array(
                    'type' => 'tip_published',
                    'text' => 'Pending Tip#'.$val['tip_id'].' has been published, please have a look.',
                    'extra' => '',
                    'created_at' => date('Y-m-d H:i:s')
                );
                $notificationId = $this->common_model->addRecords(NOTIFICATION, $noData);

                /* Updating Tip */
                if($notificationId){
                    $updateArr = array(
                        'status' => 1,
                        'is_published' => 1,
                        'updated_at' => date('Y-m-d H:i:s')
                    );
                    $this->common_model->updateRecords(TIP, $updateArr, array('tip_id' => $val['tip_id']));
                }

                /* Send Push Notification */
                $tipMsg = strtoupper($val['exchange']).' '.get_tip_exchange_type($val['exchange_tip_type']).' @'.(!empty($val['price_one']) && !empty($val['price_two'])) ? $val['price_one'].'-'.$val['price_two'] : $val['price_one'].' @stoploss '.$val['stoploss'];
                $fcmData = array(
                    'title' => 'New Tip Posted',
                    'body' => $tipMsg,
                    'type' => 'new_tip_posted',
                    'extra' => array('tip_id' => $val['tip_id'], 'total_added_targets' => count($val['total_added_targets']), 'acheived_target' => $val['acheived_target'])
                );
                send_fcm_notification($fcmData, get_all_device_tokens(1));

                unset($updateArr, $noData);
            }
        }
    }

    public function runtipautomizationcronold_get() {
        set_time_limit(60);
        for ($i = 0; $i < 59; ++$i) {
            $this->tipautomization_get();
            sleep(5);
        }
    }

   public function runtipautomizationcron_get() {
        @ini_set("output_buffering", "Off");
        @ini_set('implicit_flush', 1);
        @ini_set('zlib.output_compression', 0);
        @ini_set('max_execution_time',5000);

        header( 'Content-type: text/html; charset=utf-8' );

        for ($i = 0; $i < 59; $i++) {
            $this->tipautomization_get();

            if(sleep(5)!=0){ 
                break;
            }

            flush();
            ob_flush();
        }
    }

    /**
     * Tips Automization
     * Run every 5 seconds
     */
    public function tipautomization_get(){
        /* Getting Tips */
        if(get_option('is_zerodha_api_active') == 1) {
            $segmentCode = $this->uri->segment(4);            
            if(!empty($segmentCode)) {
                $targetsData = '';
                $tips = $this->common_model->getAllRecordsById(TIP, array('stock_market_code' => $segmentCode, 'status' => 1, 'is_published' => 1, 'is_automization_on' => 1));
                if(!empty($tips)) {
                    foreach($tips as $val) {
                        $tipTargets = $this->common_model->getAllRecordsById(TIP_TARGET, array('tt_tip_id' => $val['tip_id'], 'is_achieved' => 0));
                        foreach($tipTargets as $tval) {
                            $targetsData[$tval['target_number']] = array(
                                'target_number' => $tval['target_number'],
                                'target_amount' => revertDBInt($tval['target_amount']),
                                'is_achieved' => $tval['is_achieved'],
                                'stoploss_traling' => revertDBInt($tval['stoploss_traling'])
                            );
                        }

                        if($val['tip_type'] == BUY_TIP) {
                            $this->buytipsautomization($val, $targetsData);
                        } elseif($val['tip_type'] == SELL_TIP) {
                            $this->selltipsautomization($val, $targetsData);
                        }
                    }
                }
            }
        }
    }

    /**
     * Buy Tip Automization
     */
    public function buytipsautomization($tipData, $tipTargets) {
        /* Getting stored kite connect access token */
        $accessToken = get_option('kite_access_token');
        if(!empty($accessToken)) {
            $marketExchangeCode = strtoupper($tipData['stock_market_code']);
            $insturmentExchangeCode = strtoupper($tipData['exchange']);

            /* Getting instrument current market data from kite connect */
            $response = file_get_contents(KITE_API_URL."instruments/".$marketExchangeCode."/".$insturmentExchangeCode."?api_key=".ZERODHA_API_KEY."&access_token=".$accessToken);
            $response = json_decode($response);

            if(!empty($response)) {
                $tipMsg = $tipData['exchange'].' '.get_tip_exchange_type($tipData['exchange_tip_type']);
                $msg = '';
                $newAcheivedTargets = 0;

                /* Handeling kite connect automization response */
                if($response->status == 'success' && !empty($response->data)) {
                    $kiteData = $response->data;
                    $ltp = $kiteData->last_price;
                    $tipPrice = revertDBInt($tipData['price_one']);
                    $tipStoploss = revertDBInt($tipData['stoploss']);
                    $totalTargets = $tipData['total_added_targets'];
                    $acheivedTargets = $tipData['acheived_target'];

                    $curTime = strtotime(date('H:i'));
                    if($tipData['stock_market_code'] == 'NSE' || $tipData['stock_market_code'] == 'NFO') {
                        $closingTime = strtotime(NSE_CLOSING_TIME);
                    } elseif($tipData['stock_market_code'] == 'CDS') {
                        $closingTime = strtotime(CDS_CLOSING_TIME);
                    } elseif($tipData['stock_market_code'] == 'MCX') {
                        $closingTime = strtotime(MCX_CLOSING_TIME);
                    }

                    /* Check if stoploss triggered */
                    if($tipData['is_call_executed'] != 1  && $curTime <= $closingTime && $ltp <= $tipStoploss){
                        $updateData = array('status' => 3, 'updated_at' => date('Y-m-d H:i:s'));
                        $this->common_model->updateRecords(TIP, $updateData, array('tip_id' => $tipData['tip_id']));
                        $tipMsggages = $this->common_model->getSingleRecordById(TIP_MESSAGE, array('tip_id' => $tipData['tip_id']));
                        if($tipMsggages['is_not_exec'] == 0) {
                            $this->common_model->updateRecords(TIP_MESSAGE, array('is_not_exec' => 1, 'updated_at' => date('Y-m-d H:i:s')), array('tip_id' => $tipData['tip_id']));
                            $msg = strtoupper($tipData['exchange']).'- Our call is not executed, cancel your order.';
                            $tipMsggages['is_not_exec'] == 1;
                        } else {
                            $msg = '';
                        }
                    }

                    /* Checking for if tip is not executed at the market closing time */
                    if($tipData['is_call_executed'] != 1 && $curTime >= $closingTime && $ltp <= $tipPrice) {
                        $updateData = array('status' => 3, 'updated_at' => date('Y-m-d H:i:s'));
                        $this->common_model->updateRecords(TIP, $updateData, array('tip_id' => $tipData['tip_id']));
                        $tipMsggages = $this->common_model->getSingleRecordById(TIP_MESSAGE, array('tip_id' => $tipData['tip_id']));
                        if($tipMsggages['is_not_exec'] == 0) {
                            $this->common_model->updateRecords(TIP_MESSAGE, array('is_not_exec' => 1, 'updated_at' => date('Y-m-d H:i:s')), array('tip_id' => $tipData['tip_id']));
                            $msg = strtoupper($tipData['exchange']).'- Our call is not executed, cancel your order.';
                            $tipMsggages['is_not_exec'] == 1;
                        } else {
                            $msg = '';
                        }
                    } 

                    /* Checking for if call trigered */
                    if($tipData['is_call_executed'] != 1 && $curTime <= $closingTime && $ltp >= $tipPrice) {
                        $updateData = array('is_call_executed' => 1, 'updated_at' => date('Y-m-d H:i:s'));
                        $this->common_model->updateRecords(TIP, $updateData, array('tip_id' => $tipData['tip_id']));
                        $tipMsggages = $this->common_model->getSingleRecordById(TIP_MESSAGE, array('tip_id' => $tipData['tip_id']));
                        if($tipMsggages['is_exec'] == 0) {
                            $this->common_model->updateRecords(TIP_MESSAGE, array('is_exec' => 1, 'updated_at' => date('Y-m-d H:i:s')), array('tip_id' => $tipData['tip_id']));
                            $msg = strtoupper($tipData['exchange']).'- Our call is executed.';
                            $tipMsggages['is_exec'] == 1;
                        } else {
                            $msg = '';
                        }
                    }  

                    /* Targets and executed call calculations */
                    if($tipData['is_call_executed'] == 1 && !empty($tipTargets)) {
                        $latestStoploss = revertDBInt($tipData['trailed_stoploss']);

                        /* If stoploss trigered to ltp */
                        if($ltp <= $latestStoploss && $acheivedTargets > 0) {
                            $updateData = array('status' => 3, 'updated_at' => date('Y-m-d H:i:s'));
                            $this->common_model->updateRecords(TIP, $updateData, array('tip_id' => $tipData['tip_id']));
                            $tipMsggages = $this->common_model->getSingleRecordById(TIP_MESSAGE, array('tip_id' => $tipData['tip_id']));
                            if($tipMsggages['trailing_stoploss_triggered'] == 0) {
                                $this->common_model->updateRecords(TIP_MESSAGE, array('trailing_stoploss_triggered' => 1, 'updated_at' => date('Y-m-d H:i:s')), array('tip_id' => $tipData['tip_id']));
                                $msg = strtoupper($tipData['exchange']).'- Book full profit @ '.$latestStoploss.' and exit long position.';
                                $tipMsggages['trailing_stoploss_triggered'] == 1;
                            } else {
                                $msg = '';
                            }
                        }

                        if($ltp <= $latestStoploss && $acheivedTargets == 0) {
                            $updateData = array('status' => 3, 'updated_at' => date('Y-m-d H:i:s'));
                            $this->common_model->updateRecords(TIP, $updateData, array('tip_id' => $tipData['tip_id']));
                            $tipMsggages = $this->common_model->getSingleRecordById(TIP_MESSAGE, array('tip_id' => $tipData['tip_id']));
                            if($tipMsggages['stoploss_triggered'] == 0) {
                                $this->common_model->updateRecords(TIP_MESSAGE, array('stoploss_triggered' => 1, 'updated_at' => date('Y-m-d H:i:s')), array('tip_id' => $tipData['tip_id']));
                                $msg = strtoupper($tipData['exchange']).'- Stoploss triggered exit long position.';
                                $tipMsggages['stoploss_triggered'] == 1;
                            } else {
                                $msg = '';
                            }
                        }

                        foreach($tipTargets as $key => $val){
                            $targetAmount = $val['target_amount'];
                            if($ltp >= $targetAmount && $ltp > $latestStoploss){
                                for($index = 1; $index <= $key; $index++) {
                                    $checkTarget = $this->common_model->getSingleRecordById(TIP_TARGET, array('tt_tip_id' => $tipData['tip_id'], 'target_number' => $index));
                                    if($checkTarget['is_achieved'] != 1) {
                                        /* Updating Tips Targets */
                                        $updateData = array('is_achieved' => 1, 'updated_at' => date('Y-m-d H:i:s'));
                                        $this->common_model->updateRecords(TIP_TARGET, $updateData, array('tt_tip_id' => $tipData['tip_id'], 'target_number' => $index));
                                        
                                        /* Calculating New Stoploss */
                                        $latestStoplossMul = ($targetAmount * $val['stoploss_traling']) / 100;
                                        $latestStoploss = $targetAmount - $latestStoplossMul;
                                        $latestStoploss = get_automization_stoploss($latestStoploss, $tipData['stock_market_type'], $tipData['stock_market_code'], $tipData['exchange']);

                                        /* Updating Tip Info */
                                        $this->common_model->updateRecords(TIP, array('trailed_stoploss' => convertDBInt($latestStoploss), 'acheived_target' => $index, 'updated_at' => date('Y-m-d H:i:s')), array('tip_id' => $tipData['tip_id']));

                                        /* Calculating up profit */
                                        $diffPrice = $ltp - $tipPrice;
                                        $upCal = ($diffPrice * 100) / $ltp;
                                        $upPrice = number_format($upCal, 2);

                                        /* Adding Follow Up */
                                        $message = strtoupper($tipData['exchange']). ' hit our target '.$index.' up '.$upPrice.'% from our buy call now trail your stop loss @ '.$latestStoploss.' and wait for next  targets.';
                                        $addData = array(
                                            'fu_tip_id' => $tipData['tip_id'],
                                            'fu_created_by' => '0',
                                            'fu_message' => $message,
                                            'is_trailed_stoploss' => 1,
                                            'is_trailed_targets' => 0,
                                            'fu_status' => 1,
                                            'created_at' => date('Y-m-d H:i:s'),
                                            'updated_at' => date('Y-m-d H:i:s')
                                        );
                                        $followUpId = $this->common_model->addRecords(TIP_FOLLOW_UP, $addData);

                                        /* Sending Notification */
                                        $fcmData = array(
                                            'title' => 'New Follow Up',
                                            'body' => $tipMsg,
                                            'type' => 'new_followup',
                                            'extra' => array('tip_id' => $tipData['tip_id'], 'follow_up_id' => $followUpId, 'total_added_targets' => $totalTargets, 'acheived_target' => $index)
                                        );
                                        send_fcm_notification($fcmData, filter_tips_user($tipData['stock_market_type']));

                                        /* Send SMS */
                                        send_sms(implode(',', get_paid_user_number_by_segment($tipData['stock_market_type'])), $message);

                                        /* Send Web Notification */
                                        send_web_notification($message, 'Tip Automization Message', get_cms_url('admin/tips/view-all?s='.$tipData['tip_id']), '');
                                    }
                                }

                                /* If all targets are acheived */
                                $updatedTipInfo = $this->common_model->getSingleRecordById(TIP, array('tip_id' => $tipData['tip_id']));
                                if($totalTargets == $updatedTipInfo['acheived_target']){
                                    /* Send Web Notification */
                                    send_web_notification(strtoupper($tipData['exchange']).'- All targets are acheived, Do you want to add more targets and follow up for this call??', 'All Targets Acheived', get_cms_url('admin/tips/add-new-follow-up/'.$tipData['tip_id']), '');
                                }

                                $newAcheivedTargets = $updatedTipInfo['acheived_target'];
                            }
                        }
                    }

                    /* Adding follow up */
                    if(!empty($msg)) {
                        $addData = array(
                            'fu_tip_id' => $tipData['tip_id'],
                            'fu_created_by' => '0',
                            'fu_message' => $msg,
                            'is_trailed_stoploss' => 0,
                            'is_trailed_targets' => 0,
                            'fu_status' => 1,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        );
                        $followUpId = $this->common_model->addRecords(TIP_FOLLOW_UP, $addData);

                        /* Sending Notification */
                        $fcmData = array(
                            'title' => 'New Follow Up',
                            'body' => $tipMsg,
                            'type' => 'new_followup',
                            'extra' => array('tip_id' => $tipData['tip_id'], 'follow_up_id' => $followUpId, 'total_added_targets' => $totalTargets, 'acheived_target' => $newAcheivedTargets)
                        );
                        send_fcm_notification($fcmData, filter_tips_user($tipData['stock_market_type']));

                        /* Send SMS */
                        send_sms(implode(',', get_paid_user_number_by_segment($tipData['stock_market_type'])), $msg);

                        /* Send Web Notification */
                        send_web_notification($msg, 'Tip Automization Message', get_cms_url('admin/tips/view-all?s='.$tipData['tip_id']), '');
                    }
                }
            }
        }
    }

    /**
     * Sell Tip Automization
     */
    public function selltipsautomization($tipData, $tipTargets) {
        /* Getting stored kite connect access token */
        $accessToken = get_option('kite_access_token');
        if(!empty($accessToken)) {
            $marketExchangeCode = strtoupper($tipData['stock_market_code']);
            $insturmentExchangeCode = strtoupper($tipData['exchange']);

            /* Getting instrument current market data from kite connect */
            $response = file_get_contents(KITE_API_URL."instruments/".$marketExchangeCode."/".$insturmentExchangeCode."?api_key=".ZERODHA_API_KEY."&access_token=".$accessToken);
            $response = json_decode($response);

            if(!empty($response)) {
                $tipMsg = $tipData['exchange'].' '.get_tip_exchange_type($tipData['exchange_tip_type']);
                $msg = '';
                $newAcheivedTargets = 0;

                /* Handeling kite connect automization response */
                if($response->status == 'success' && !empty($response->data)) {
                    $kiteData = $response->data;
                    $ltp = $kiteData->last_price;
                    $tipPrice = revertDBInt($tipData['price_one']);
                    $tipStoploss = revertDBInt($tipData['stoploss']);
                    $totalTargets = $tipData['total_added_targets'];
                    $acheivedTargets = $tipData['acheived_target'];

                    $curTime = strtotime(date('H:i'));
                    if($tipData['stock_market_code'] == 'NSE' || $tipData['stock_market_code'] == 'NFO') {
                        $closingTime = strtotime(NSE_CLOSING_TIME);
                    } elseif($tipData['stock_market_code'] == 'CDS') {
                        $closingTime = strtotime(CDS_CLOSING_TIME);
                    } elseif($tipData['stock_market_code'] == 'MCX') {
                        $closingTime = strtotime(MCX_CLOSING_TIME);
                    }

                    /* Check if stoploss triggered */
                    if($tipData['is_call_executed'] != 1  && $curTime <= $closingTime && $ltp >= $tipStoploss){
                        $updateData = array('status' => 3, 'updated_at' => date('Y-m-d H:i:s'));
                        $this->common_model->updateRecords(TIP, $updateData, array('tip_id' => $tipData['tip_id']));
                        $tipMsggages = $this->common_model->getSingleRecordById(TIP_MESSAGE, array('tip_id' => $tipData['tip_id']));
                        if($tipMsggages['is_not_exec'] == 0) {
                            $this->common_model->updateRecords(TIP_MESSAGE, array('is_not_exec' => 1, 'updated_at' => date('Y-m-d H:i:s')), array('tip_id' => $tipData['tip_id']));
                            $msg = strtoupper($tipData['exchange']).'- Our call is not executed, cancel your order.';
                            tipMsggages['is_not_exec'] == 1;
                        } else {
                            $msg = '';
                        }
                    }

                    /* Checking for if tip is not executed at the market closing time */
                    if($tipData['is_call_executed'] != 1 && $curTime >= $closingTime && $ltp >= $tipPrice) {
                        $updateData = array('status' => 3, 'updated_at' => date('Y-m-d H:i:s'));
                        $this->common_model->updateRecords(TIP, $updateData, array('tip_id' => $tipData['tip_id']));
                        $tipMsggages = $this->common_model->getSingleRecordById(TIP_MESSAGE, array('tip_id' => $tipData['tip_id']));
                        if($tipMsggages['is_not_exec'] == 0) {
                            $this->common_model->updateRecords(TIP_MESSAGE, array('is_not_exec' => 1, 'updated_at' => date('Y-m-d H:i:s')), array('tip_id' => $tipData['tip_id']));
                            $msg = strtoupper($tipData['exchange']).'- Our call is not executed, cancel your order.';
                            tipMsggages['is_not_exec'] == 1;
                        } else {
                            $msg = '';
                        }
                    } 

                    /* Checking for if call trigered */
                    if($tipData['is_call_executed'] != 1 && $curTime <= $closingTime && $ltp <= $tipPrice) {
                        $updateData = array('is_call_executed' => 1, 'updated_at' => date('Y-m-d H:i:s'));
                        $this->common_model->updateRecords(TIP, $updateData, array('tip_id' => $tipData['tip_id']));
                        $tipMsggages = $this->common_model->getSingleRecordById(TIP_MESSAGE, array('tip_id' => $tipData['tip_id']));
                        if($tipMsggages['is_exec'] == 0) {
                            $this->common_model->updateRecords(TIP_MESSAGE, array('is_exec' => 1, 'updated_at' => date('Y-m-d H:i:s')), array('tip_id' => $tipData['tip_id']));
                            $msg = strtoupper($tipData['exchange']).'- Our call is executed.';
                            $tipMsggages['is_exec'] == 1;
                        } else {
                            $msg = '';
                        }
                    }  

                    /* Targets and executed call calculations */
                    if($tipData['is_call_executed'] == 1 && !empty($tipTargets)) {
                        $latestStoploss = revertDBInt($tipData['trailed_stoploss']);

                        /* If stoploss trigered to ltp */
                        if($ltp >= $latestStoploss && $acheivedTargets > 0) {
                            $updateData = array('status' => 3, 'updated_at' => date('Y-m-d H:i:s'));
                            $this->common_model->updateRecords(TIP, $updateData, array('tip_id' => $tipData['tip_id']));
                            $tipMsggages = $this->common_model->getSingleRecordById(TIP_MESSAGE, array('tip_id' => $tipData['tip_id']));
                            if($tipMsggages['trailing_stoploss_triggered'] == 0) {
                                $this->common_model->updateRecords(TIP_MESSAGE, array('trailing_stoploss_triggered' => 1, 'updated_at' => date('Y-m-d H:i:s')), array('tip_id' => $tipData['tip_id']));
                                $msg = strtoupper($tipData['exchange']).'- Book full profit @ '.$latestStoploss.' and exit short position.';
                                $tipMsggages['trailing_stoploss_triggered'] == 1;
                            } else {
                                $msg = '';
                            }
                        }

                        if($ltp >= $latestStoploss && $acheivedTargets == 0) {
                            $updateData = array('status' => 3, 'updated_at' => date('Y-m-d H:i:s'));
                            $this->common_model->updateRecords(TIP, $updateData, array('tip_id' => $tipData['tip_id']));
                            $tipMsggages = $this->common_model->getSingleRecordById(TIP_MESSAGE, array('tip_id' => $tipData['tip_id']));
                            if($tipMsggages['stoploss_triggered'] == 0) {
                                $this->common_model->updateRecords(TIP_MESSAGE, array('stoploss_triggered' => 1, 'updated_at' => date('Y-m-d H:i:s')), array('tip_id' => $tipData['tip_id']));
                                $msg = strtoupper($tipData['exchange']).'- Stoploss triggered exit short position.';
                                $tipMsggages['stoploss_triggered'] == 1;
                            } else {
                                $msg = '';
                            }
                        }

                        foreach($tipTargets as $key => $val){
                            $targetAmount = $val['target_amount'];
                            if($ltp <= $targetAmount && $ltp < $latestStoploss){
                                for($index = 1; $index <= $key; $index++) {
                                    $checkTarget = $this->common_model->getSingleRecordById(TIP_TARGET, array('tt_tip_id' => $tipData['tip_id'], 'target_number' => $index));
                                    if($checkTarget['is_achieved'] != 1) {
                                        /* Updating Tips Targets */
                                        $updateData = array('is_achieved' => 1, 'updated_at' => date('Y-m-d H:i:s'));
                                        $this->common_model->updateRecords(TIP_TARGET, $updateData, array('tt_tip_id' => $tipData['tip_id'], 'target_number' => $index));
                                        
                                        /* Calculating New Stoploss */
                                        $latestStoplossMul = ($targetAmount * $val['stoploss_traling']) / 100;
                                        $latestStoploss = $targetAmount + $latestStoplossMul;
                                        $latestStoploss = get_automization_stoploss($latestStoploss, $tipData['stock_market_type'], $tipData['stock_market_code'], $tipData['exchange']);

                                        /* Updating Tip Info */
                                        $this->common_model->updateRecords(TIP, array('trailed_stoploss' => convertDBInt($latestStoploss), 'acheived_target' => $index, 'updated_at' => date('Y-m-d H:i:s')), array('tip_id' => $tipData['tip_id']));

                                        /* Calculating down profit */
                                        $diffPrice = $tipPrice - $ltp;
                                        $upCal = ($diffPrice * 100) / $ltp;
                                        $upPrice = number_format($upCal, 2);
                                        
                                        /* Adding Follow Up */
                                        $message = strtoupper($tipData['exchange']). ' hit our target '.$index.' down '.$upPrice.'% from our sell call now trail your stop loss @ '.$latestStoploss.' and wait for next  targets.';
                                        $addData = array(
                                            'fu_tip_id' => $tipData['tip_id'],
                                            'fu_created_by' => '0',
                                            'fu_message' => $message,
                                            'is_trailed_stoploss' => 1,
                                            'is_trailed_targets' => 0,
                                            'fu_status' => 1,
                                            'created_at' => date('Y-m-d H:i:s'),
                                            'updated_at' => date('Y-m-d H:i:s')
                                        );
                                        $followUpId = $this->common_model->addRecords(TIP_FOLLOW_UP, $addData);

                                        /* Sending Notification */
                                        $fcmData = array(
                                            'title' => 'New Follow Up',
                                            'body' => $tipMsg,
                                            'type' => 'new_followup',
                                            'extra' => array('tip_id' => $tipData['tip_id'], 'follow_up_id' => $followUpId, 'total_added_targets' => $totalTargets, 'acheived_target' => $index)
                                        );
                                        send_fcm_notification($fcmData, filter_tips_user($tipData['stock_market_type']));

                                        /* Send SMS */
                                        send_sms(implode(',', get_paid_user_number_by_segment($tipData['stock_market_type'])), $message);

                                        /* Send Web Notification */
                                        send_web_notification($message, 'Tip Automization Message', get_cms_url('admin/tips/view-all?s='.$tipData['tip_id']), '');
                                    }
                                }

                                /* If all targets are acheived */
                                $updatedTipInfo = $this->common_model->getSingleRecordById(TIP, array('tip_id' => $tipData['tip_id']));
                                if($totalTargets == $updatedTipInfo['acheived_target']){
                                    /* Send Web Notification */
                                    send_web_notification(strtoupper($tipData['exchange']).'- All targets are acheived, Do you want to add more targets and follow up for this call??', 'All Targets Acheived', get_cms_url('admin/tips/add-new-follow-up/'.$tipData['tip_id']), '');
                                }

                                $newAcheivedTargets = $updatedTipInfo['acheived_target'];
                            }
                        }
                    }

                    /* Adding follow up */
                    if(!empty($msg)) {
                        $addData = array(
                            'fu_tip_id' => $tipData['tip_id'],
                            'fu_created_by' => '0',
                            'fu_message' => $msg,
                            'is_trailed_stoploss' => 0,
                            'is_trailed_targets' => 0,
                            'fu_status' => 1,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        );
                        $followUpId = $this->common_model->addRecords(TIP_FOLLOW_UP, $addData);

                        /* Sending Notification */
                        $fcmData = array(
                            'title' => 'New Follow Up',
                            'body' => $tipMsg,
                            'type' => 'new_followup',
                            'extra' => array('tip_id' => $tipData['tip_id'], 'follow_up_id' => $followUpId, 'total_added_targets' => $totalTargets, 'acheived_target' => $newAcheivedTargets)
                        );
                        send_fcm_notification($fcmData, filter_tips_user($tipData['stock_market_type']));

                        /* Send SMS */
                        send_sms(implode(',', get_paid_user_number_by_segment($tipData['stock_market_type'])), $msg);

                        /* Send Web Notification */
                        send_web_notification($msg, 'Tip Automization Message', get_cms_url('admin/tips/view-all?s='.$tipData['tip_id']), '');
                    }
                }
            }
        }   
    }

    /**
     * Check For NSE & NFO Intraday Tips
     * Run daily at 15:15 PM
     */
    public function checkfornseintradaytips_get(){
        if(get_option('is_zerodha_api_active') == 1) {
            $sql = 'SELECT * FROM '.TIP.' WHERE stock_market_type IN(4, 5, 6) AND exchange_tip_type = '.INTRADAY_TIP.' AND status = 1 AND is_published = 1 AND is_automization_on = 1 AND is_call_executed = 0';
            $tips = $this->common_model->getCustomSqlResult($sql);
            if(!empty($tips)){
                $msg = 'Our call is not executed, cancel your order.';
                foreach($tips as $val){
                    $tipMsg = $val['exchange'].' '.get_tip_exchange_type($val['exchange_tip_type']);
                    $this->common_model->updateRecords(TIP, array('status' => 3, 'updated_at' => date('Y-m-d H:i:s')), array('tip_id' => $val['tip_id']));
                    /* Adding Follow Up */
                    $addData = array(
                        'fu_tip_id' => $val['tip_id'],
                        'fu_created_by' => '0',
                        'fu_message' => $msg,
                        'is_trailed_stoploss' => 0,
                        'is_trailed_targets' => 0,
                        'fu_status' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    );
                    $followUpId = $this->common_model->addRecords(TIP_FOLLOW_UP, $addData);

                    /* Sending Notification */
                    $fcmData = array(
                        'title' => 'New Follow Up',
                        'body' => $tipMsg,
                        'type' => 'new_followup',
                        'extra' => array('tip_id' => $val['tip_id'], 'follow_up_id' => $followUpId, 'total_added_targets' => $val['total_added_targets'], 'acheived_target' => $val['acheived_target'])
                    );
                    send_fcm_notification($fcmData, filter_tips_user($val['stock_market_type']));

                    /* Send SMS */
                    send_sms(implode(',', get_paid_user_number_by_segment($val['stock_market_type'])), $msg);

                    /* Send Web Notification */
                    send_web_notification($msg, 'Tip Automization Message', get_cms_url('admin/tips/view-all?s='.$val['tip_id']), '');
                }
            }
        }
    }

    /**
     * Check For CDS Intraday Tips
     * Run daily at 16:45 PM
     */
    public function checkforcdsintradaytips_get(){
        if(get_option('is_zerodha_api_active') == 1) {
            $sql = 'SELECT * FROM '.TIP.' WHERE stock_market_type IN(3) AND exchange_tip_type = '.INTRADAY_TIP.' AND status = 1 AND is_published = 1 AND is_automization_on = 1 AND is_call_executed = 0';
            $tips = $this->common_model->getCustomSqlResult($sql);
            if(!empty($tips)){
                $tipMsg = $val['exchange'].' '.get_tip_exchange_type($val['exchange_tip_type']);
                $msg = 'Our call is not executed, cancel your order.';
                foreach($tips as $val){
                    $this->common_model->updateRecords(TIP, array('status' => 3, 'updated_at' => date('Y-m-d H:i:s')), array('tip_id' => $val['tip_id']));
                    /* Adding Follow Up */
                    $addData = array(
                        'fu_tip_id' => $val['tip_id'],
                        'fu_created_by' => '0',
                        'fu_message' => $msg,
                        'is_trailed_stoploss' => 0,
                        'is_trailed_targets' => 0,
                        'fu_status' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    );
                    $followUpId = $this->common_model->addRecords(TIP_FOLLOW_UP, $addData);

                    /* Sending Notification */
                    $fcmData = array(
                        'title' => 'New Follow Up',
                        'body' => $tipMsg,
                        'type' => 'new_followup',
                        'extra' => array('tip_id' => $val['tip_id'], 'follow_up_id' => $followUpId, 'total_added_targets' => $val['total_added_targets'], 'acheived_target' => $val['acheived_target'])
                    );
                    send_fcm_notification($fcmData, filter_tips_user($val['stock_market_type']));

                    /* Send SMS */
                    send_sms(implode(',', get_paid_user_number_by_segment($val['stock_market_type'])), $msg);

                    /* Send Web Notification */
                    send_web_notification($msg, 'Tip Automization Message', get_cms_url('admin/tips/view-all?s='.$val['tip_id']), '');
                }
            }
        }
    }

    /**
     * Check For MCX Intraday Tips
     * Run daily at 23:45 PM
     */
    public function checkformcxintradaytips_get(){
        if(get_option('is_zerodha_api_active') == 1) {
            $sql = 'SELECT * FROM '.TIP.' WHERE stock_market_type IN(2) AND exchange_tip_type = '.INTRADAY_TIP.' AND status = 1 AND is_published = 1 AND is_automization_on = 1 AND is_call_executed = 0';
            $tips = $this->common_model->getCustomSqlResult($sql);
            if(!empty($tips)){
                $tipMsg = $val['exchange'].' '.get_tip_exchange_type($val['exchange_tip_type']);
                $msg = 'Our call is not executed, cancel your order.';
                foreach($tips as $val){
                    $this->common_model->updateRecords(TIP, array('status' => 3, 'updated_at' => date('Y-m-d H:i:s')), array('tip_id' => $val['tip_id']));
                    /* Adding Follow Up */
                    $addData = array(
                        'fu_tip_id' => $val['tip_id'],
                        'fu_created_by' => '0',
                        'fu_message' => $msg,
                        'is_trailed_stoploss' => 0,
                        'is_trailed_targets' => 0,
                        'fu_status' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    );
                    $followUpId = $this->common_model->addRecords(TIP_FOLLOW_UP, $addData);

                    /* Sending Notification */
                    $fcmData = array(
                        'title' => 'New Follow Up',
                        'body' => $tipMsg,
                        'type' => 'new_followup',
                        'extra' => array('tip_id' => $val['tip_id'], 'follow_up_id' => $followUpId, 'total_added_targets' => $val['total_added_targets'], 'acheived_target' => $val['acheived_target'])
                    );
                    send_fcm_notification($fcmData, filter_tips_user($val['stock_market_type']));

                    /* Send SMS */
                    send_sms(implode(',', get_paid_user_number_by_segment($val['stock_market_type'])), $msg);

                    /* Send Web Notification */
                    send_web_notification($msg, 'Tip Automization Message', get_cms_url('admin/tips/view-all?s='.$val['tip_id']), '');
                }
            }
        }
    }

    /**
     * Update instruments list
     * Runs daily at 08:30 AM
     */
    public function updateinstrumentlist_get() {
        set_time_limit(0);
        ini_set('max_execution_time', -1);
        ini_set("memory_limit", -1);
        $type = $this->uri->segment(4);
        if(!empty($type)) {
            /* Getting updated instruments list from kite connect */
            $attachment = file_get_contents(KITE_API_URL."instruments/".$type."?api_key=".ZERODHA_API_KEY);
            $file = fopen("./uploads/instruments/".$type."-instrument-list-".date('Y-m-d').".csv","w");
            $isWrite = fwrite($file, $attachment);
            fclose($file);
         
            if($isWrite > 1) {
                $attachment = "./uploads/instruments/".$type."-instrument-list-".date('Y-m-d').".csv";
                $objPHPExcel = PHPExcel_IOFactory::load($attachment);
                $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
                $columnArr = array('A' => 'instrument_token', 'B' => 'exchange_token', 'C' => 'tradingsymbol', 'D' => 'name', 'E' => 'last_price', 'F' => 'expiry', 'G' => 'strike', 'H' => 'tick_size', 'I' => 'lot_size', 'J' => 'instrument_type', 'K' => 'segment', 'L' => 'exchange');

                foreach ($cell_collection as $cell) {
                    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

                    if($row == 1) {
                        $header[$row][$column] = $data_value;
                    } else {
                        if(in_array($column, array('E', 'G', 'H', 'I', ))) {
                            $data_value = convertDBInt($data_value);
                        } elseif($column == 'F') {
                            $data_value = date('Y-m-d', strtotime($data_value));
                        }

                        $arr_data[$row][$columnArr[$column]] = $data_value;
                        $arr_data[$row]['created_at'] = date('Y-m-d H:i:s');
                        $arr_data[$row]['updated_at'] = date('Y-m-d H:i:s');
                    }
                }

                /* Adding instruments records in DB */
                if(!empty($arr_data)) {
                    /* Truncating Old Data */
                    $this->common_model->deleteRecord(INSTRUMENT, array('exchange' => strtoupper($type)));
                    $this->common_model->addBatchRecords(INSTRUMENT, $arr_data);
                }
            }
        }
    }

    public function publishrssnews_get() {
        $rsslinksArr = array();
        $rsslinks = get_option('rss_news_links');

        if(!empty($rsslinks)) {
            $rsslinksArr = explode(',', $rsslinks);
        }

        if(!empty($rsslinksArr)) {
            foreach($rsslinksArr as $val){
                $link = trim($val);
                libxml_disable_entity_loader(false);
                $rss = new DOMDocument();
                $rss->load($link);
                $feed = array();
                foreach ($rss->getElementsByTagName('item') as $node) {
                    $item = array ( 
                        'title' => str_replace('#39;', "'", $node->getElementsByTagName('title')->item(0)->nodeValue),
                        'desc' => str_replace('#39;', "'", preg_replace("/<img[^>]+\>/i", "", $node->getElementsByTagName('description')->item(0)->nodeValue)),
                        'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
                        'date' => date('Y-m-d H:i:s', strtotime($node->getElementsByTagName('pubDate')->item(0)->nodeValue)),
                    );

                    /* Adding record */
                    $checkExistsNews = $this->common_model->getRecordCount(NEWS, array('title' => $item['title']));
                    if($checkExistsNews == 0) {
                        $addData = array(
                            'created_by' => ADMIN_USER_ID,
                            'title' => $item['title'],
                            'description' => $item['desc'],
                            'status' => 1,
                            'created_at' => date('Y-m-d H:i:s', strtotime($item['date'])),
                            'updated_at' => date('Y-m-d H:i:s', strtotime($item['date']))
                        );
                        $newsId = $this->common_model->addRecords(NEWS, $addData);
                        /*if($newsId) {
                            $fcmData = array(
                                'title' => 'New News Posted',
                                'body' => $item['title'],
                                'type' => 'new_news_posted',
                                'extra' => array('news_id' => $newsId)
                            );
                            send_fcm_notification($fcmData, get_all_device_tokens(1));
                        }*/
                    }
                }
            }
        }
        
    }
}
