/**
* Custom jQuery function
* Author: Siddharth Pandey
* Author Email: sid@mobiwetech.com
* version: 1.0
*/
var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();